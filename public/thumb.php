<?php
ini_set('display_errors', true);
error_reporting(E_ALL);

// Define path to application directory
defined('ROOT_PATH')
|| define('ROOT_PATH', realpath(dirname(__FILE__) . '/..'));

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
$app = Zend\Mvc\Application::init(require 'config/application.config.php');
$sm  = $app->getServiceManager();
$job = $sm
    ->get('SlmQueue\Job\JobPluginManager')
    ->get('Application\Job\CreateThumb');
$url = $_SERVER['REQUEST_URI'];
if (!preg_match('/^(.*)\-([0-9]+x[0-9]+)(\.jpg|\.gif|\.jpeg|\.png|\.bmp)$/i', 
    $url, $matches)
) {
    die('File type is not match');
}

$file = $matches[1] . $matches[3];
if (   !file_exists($file)
    || !is_readable($file)) {
    die('file not found');
}
list ($width, $height) = explode('x', $matches[2]);

if (preg_match('/^(.*)\/public\/(.*)$/', $url, $match)) {
    $redirectUrl = $match[2];
}


$job->setContent(
    array(
        'image'  => $file,
        'width'  => $width,
        'height' => $height
    )
);
$job->execute();

header('Location:' . 'http://'.$_SERVER['SERVER_NAME']. '/'.$redirectUrl);
