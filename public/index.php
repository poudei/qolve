<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

define('APPLICATION_PATH', realpath(dirname(__DIR__) . '/module/Application/src/Application/'));
define('QOLVE_PATH', realpath(dirname(__DIR__) . '/module/Qolve/src/Qolve/'));
define('PUBLIC_PATH', realpath(dirname(__DIR__) . '/public'));
define('VENDOR_PATH', realpath(dirname(__DIR__) . '/vendor/'));

define('LIFE_TIME', 3600);
// Setup autoloading

require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
