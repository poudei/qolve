define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var UserModel = Backbone.Model.extend({
        defaults: {
          isFollowed            : 0,
          followersCount        : 0,
          invite                : false,
          userFollowingsCount   : 0,
          occupation            : '',
          school                : '',
          location              : '',
          timezone              : '' ,
          setImageSize          : true
        },
        
        url: function() {
            if (this.get("id") == undefined) {
                return '/api/users/';
            } else {              
                return '/api/users/' + this.get('id');
            }
        },
                
        parse : function(resp){
            if(resp.users && (resp.users.userFollowingsCount == undefined || resp.users.userFollowingsCount == null)){
                resp.users.userFollowingsCount = 0;
            }

            if(resp.users && (resp.users.followersCount == undefined || resp.users.followersCount == null)){
                resp.users.followersCount = 0;
            }
            
            if(resp.users)
                return resp.users;            
            if(resp.user)
                return resp.user;            
            return resp;
        },
                
        toggleFollow: function() {           
           $.ajax({
               url: (this.get('isFollowed') == 1) ? '/api/followings' : '/api/followings/' + this.get('id'),
               type: (this.get('isFollowed') == 1) ? 'POST' : 'DELETE',
               dataType: 'json',
               data: {
                   'userId': this.get('id')
               },
               success: function() {}});          
        }
    });
    return UserModel;
});
