define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var CommentModel = Backbone.Model.extend({
        defaults: {
            content     : "",
            writtenOn   : {},
            user        : {}     
        },     
                
        initialize : function(opt){
            opt = opt || {};
            this.opt = opt;
        },        
                
        url: function() {        
            if(this.opt.url) return this.opt.url;           
            
            if(this.type == 'questions'){
                if(this.get('id'))
                    return '/api/questions/' + this.questionId + '/comments/' + this.get('id');        
                else
                    return '/api/questions/' + this.questionId + '/comments';
            }
            else{
                if(this.get('id'))
                    return '/api/questions/' + this.questionId + '/answers/' + this.answerId + '/comments/' + this.get('id');        
                else
                    return '/api/questions/' + this.questionId + '/answers/' + this.answerId + '/comments';        
            }
        },
                
        parse : function(resp){
            var comment = resp.comment ? resp.comment : resp;
            return comment;
        }
        
    });
    return CommentModel;
});
