define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var NotificationModel = Backbone.Model.extend({
        defaults: {
            fromId      : "",
            questionId  : "",
            answerId    : null,
            content     : null,
            verb        : "",            
        },
                
        url: function() {
            if (this.get("id") == undefined) {
                return '/api/notifications/' ;
            } else {
                return '/api/notifications/' + this.get('id');
            }
        }
    });
    
    return NotificationModel;
});
