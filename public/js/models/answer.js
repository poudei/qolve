define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var AnswerModel = Backbone.Model.extend({
        defaults: {
            privacy         : 1,            
            status          : '',
            votes           : '',
            comments        : '',            
            description     : null,            
            isVoted         : 0,
            share           : null,
            documentsList   : null,
            visibility      : 1
        },
        
        initialize : function(opt){            
            if(opt && opt.questionId){
                this.set('questionId', opt.questionId);
                this.questionId = opt.questionId;
            }                   
        },
                
        url: function() {
            if (this.get("id") == undefined) {
                return '/api/questions/' + this.questionId + '/answers?comment=true'
            } else {
                return '/api/questions/' + this.questionId + '/answers/' + this.get('id');
            }
        },     
        
        reportAnswer: function(reason) {
            var self = this;
            
            $.ajax({
                url         : '/api/questions/' + this.questionId + '/answers/' + this.get('id') + '/reports',
                type        : 'POST',
                dataType    : 'json',                
                data        : {"reason" : reason},
                success     : function() {}
            }, this);
        }, 
        
        toggleVote : function(obj) {            
            $.ajax({
                url      : obj.url,
                type     : obj.type,
                dataType : 'json',
                data : {
                    'score' : obj.score
                }
            }, this);
        }   
    });
    
    return AnswerModel;
});
