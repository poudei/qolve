define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var ListModel = Backbone.Model.extend({
        defaults: {
           name             : "",           
           privacy          : '',
           questions        : 0,
           questionsList    : null,
           follows          : 0,
           listKeywords     : [],
           isFollowed       : 0
        },       
                
        url: function() {
            if (this.get("id") == undefined) {
                return '/api/lists/';
            } else {
                return '/api/lists/' + this.get('id');
            }
        },
                
        parse : function(resp){    
            if(resp.bookmarkList)
                return resp.bookmarkList;
            
            return resp;
        },
                
        toggleFollow: function(credit, visibility) {
            var self = this;
            self.set({
                isFollowed: this.get('isFollowed') == 0 ? 1 : 0
            });
                    
            $.ajax({
                url: this.get('isFollowed') == 1 ? '/api/lists/' + this.get('id') + '/follows' : '/api/lists/' + this.get('id') + '/follows/'+window.current_user.id+'?list_id=' + this.get('id') ,
                type: this.get('isFollowed') == 1 ? 'POST' : 'DELETE',
                dataType: 'json'                
            }, this);
        }
        
    });
    return ListModel;
});
