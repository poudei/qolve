define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var ExploreModel = Backbone.Model.extend({               
      default :{
             "deadline"      : null,
            "levels"        : '',
            "privacy"       : '1',
            "votes"         : '',
            "credits"       : '0',
            "language"      : 'eng',
            "askers"        : '',
            "paid"          : '',
            "voters"        : null,
            "comments"      : null,
            "answers"       : null,
            "description"   : "",
            "answerId"      : null,
            "status"        : null,            
            "createdOn"     : null,
            "modifiedBy"    : null,            
            "modifiedOn"    : null,           
            "user"          : null,
            "location"      : "",
            "documentsList" : null,
            userAnswerId    : null
      }
    });
    return ExploreModel;
});
