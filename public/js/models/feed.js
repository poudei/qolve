define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var QuestionModel = Backbone.Model.extend({
        defaults: {
            "deadline"      : null,
            "levels"        : '',
            "privacy"       : '',
            "votes"         : '',
            "credits"       : '0',
            "language"      : 'eng',
            "askers"        : '',
            "paid"          : '',
            "voters"        : null,
            "comments"      : null,
            "answers"       : null,
            "description"   : "",
            "answerId"      : null,
            "status"        : null,            
            "createdOn"     : null,
            "modifiedBy"    : null,            
            "modifiedOn"    : null,           
            "user"          : null,
            "location"      : ""    ,
            "visibility"    : 1
        },       
                
        url: function() {
            if (this.get("id") == undefined) {
                return '/api/questions/' + '?comment=true';
            } else {
                return '/api/questions/' + this.get('id') + '?comment=true';
            }
        },
                
        parse : function(resp){           
            return resp;
        },
                
        reaskQuestion: function(credit, visibility) {
            var $this = this,
                 type = 'POST',
                 url  = '/api/questions/' + this.get('question').id + '/askers';
         
            if(this.get('question').isAsked == '11' || this.get('question').isAsked == '12' || this.get('question').isAsked == '21' || this.get('question').isAsked == '22'){
                type = 'PUT';
                url = '/api/questions/' + this.get('question').id + '/askers/' + window.current_user.id;
            }
            
            $.ajax({
                url         : url,
                type        : type,
                dataType    : 'json',
                data        : {
                    credit      : credit,
                    visibility  : visibility
                },
                success     : function(resp) {
                    var question = _.clone($this.get('question'));  
                    question.isAsked = resp.question.isAsked;
                    question.credits  = resp.question.credits;
                    $this.set({
                        question : question
                    });
                    $this.trigger('change');
                    window.current_user.credit -= credit;
                }
            }, this);
        },
        
        reportQuestion: function(reason) {
            var $this = this;

            $.ajax({
                url         : '/api/questions/' + this.get('question').id + '/reports',
                type        : 'POST',
                dataType    : 'json',
                data        : {"reason" : reason},
                success: function() {}
            }, this);
        },        
        
        shareQuestionUrl: function(socials) {
            $.ajax({
                url: '/api/social/share',
                data: JSON.stringify({
                    'provider': 'facebook',
                    'object': 'question',
                    'objectId': this.get('question').id
                }),
                type: "POST",
                dataType: 'json'                
            }, this);
        },
                       
        addList : function(opt){
            $.ajax({
                url  : '/api/lists/' + opt.list_id + '/questions',
                type : 'POST',
                data : {questionId : this.get('question').id}, 
                dataType: 'json',
                success: function() {
                  $('.bookmark-question').popover('destroy');
                }
            }, this);
        },
        
        toggleVote : function(obj) {            
            $.ajax({
                url      : obj.url,
                type     : obj.type,
                dataType : 'json',
                data : {
                    'score' : 1
                }
            }, this);
        }
        
    });
    return QuestionModel;
});
