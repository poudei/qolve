define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var ActivityModel = Backbone.Model.extend({
        defaults: {
            userId      : "",
            actorId     : "",
            questionId  : null,
            answerId    : null,
            content     : "",
            privacy     : 2,
            verb        : "",   
            question    : {},
            user        : {},
            actor       : {}
        },
        url: function() {
            return '/api/users/activity';

        }
    });

    return ActivityModel;
});
