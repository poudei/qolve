define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var QuestionModel = Backbone.Model.extend({
        defaults: {
            "deadline"              : null,
            "levels"                : '',
            "privacy"               : '',
            "answer_privilege"      : 1,       
            "votes"                 : '',
            "credits"               : '0',
            "language"              : 'eng',
            "askers"                : '',
            "paid"                  : '',
            "voters"                : null,
            "comments"              : null,
            "answers"               : null,
            "description"           : "",
            "answerId"              : null,
            "status"                : null,            
            "createdOn"             : null,
            "modifiedBy"            : null,            
            "modifiedOn"            : null,           
            "user"                  : null,
            "location"              : "",
            "documentsList"         : null,
            userAnswerId            : null,
            lastVoters              : null,
            keywords                : [],
            share                   : null,
            visibility              : 1
        },       
                
        url: function() {
            if (this.get("id") == undefined) {
                return '/api/questions/' + '?comment=true';
            } else {
                return '/api/questions/' + this.get('id') + '?comment=true';
            }
        },
                
        parse : function(resp){                      
            if(resp.question)
                return resp.question
            return resp;
        },
                
        shareQuestionUrl: function(socials) {
            $.ajax({
                url: '/api/social/share',
                data: JSON.stringify({
                    'provider': 'facebook',
                    'object': 'question',
                    'objectId': this.get('id')
                }),
                type: "POST",
                dataType: 'json'                
            }, this);
        },
                
        reportQuestion: function(reason) {
            var self = this;
            
            $.ajax({
                url         : '/api/questions/' + this.get('id') + '/reports',
                type        : 'POST',
                dataType    : 'json',
                data        : {"reason" : reason},
                success     : function() {}
            }, this);
        },
        
        addList : function(opt){
            $.ajax({
                url  : '/api/lists/' + opt.list_id + '/questions',
                type : 'POST',
                data : {questionId : this.get('id')}, 
                dataType: 'json',
                success: function() {
                  $('.bookmark-question').popover('hide');
                }
            }, this);
        },
                
        reaskQuestion: function(credit, visibility) {
            var $this = this,
                 type = 'POST',
                 url  = '/api/questions/' + this.get('id') + '/askers';
         
            if(this.get('isAsked') == '11' || this.get('isAsked') == '12' || this.get('isAsked') == '21' || this.get('isAsked') == '22'){
                type = 'PUT';
                url = '/api/questions/' + this.get('id') + '/askers/' + window.current_user.id;
            }
            
            $.ajax({
                url         : url,
                type        : type,
                dataType    : 'json',
                data        : {
                    credit      : credit,
                    visibility  : visibility
                },
                success     : function(resp) {
                    $this.set({
                        isAsked : resp.isAsked,
                        credits : resp.credits
                    });                    
                    window.current_user.credit -= credit;
                }
            }, this);
        },
        
        toggleVote : function(obj) {
            $.ajax({
                url         : obj.url,
                type        : obj.type,
                dataType    : 'json',
                data        : {
                    'score' : 1
                }
            }, this);
        }
        
    });
    return QuestionModel;
});
