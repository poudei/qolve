define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',        
    'base/view/itemView',        
    'text!views/templates/notificationList_item.html',   
    'i18n!nls/labels',
    ], function ($, jqueryUi , _, Backbone, ItemView, timelineTpl, Labels) {
        var NotificationListItemView = ItemView.extend({
            tagName   : 'li',
            className : 'notification-item',
            events : {},

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },

            render    : function() {     
                this.template = _.template(timelineTpl);                                
              
                this.$el.html(this.template(_.extend(
                    this.model.toJSON(),{
                        labels : Labels
                    }
                )));                                
                return this;
            }          
        });

        return NotificationListItemView;
    });
