define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'models/user',
    'views/users_list',    
    'text!views/templates/findfriends.html',
    'text!views/templates/user_item.html',
    'i18n!nls/labels',
], function($, Backbone, _, App, UserModel, UsersList, FindfriendsTpl, UserTpl, Labels) {

    var FindfriendsView = Backbone.View.extend({
        className: 'find-friends-wrapper',

        events: {
            'click      .select-provider'           : 'selectProvider',
            'click      .follow-all'                : 'followAll',
            'click      .invite-all'                : 'inviteAll',           
            'click      .invite-item-change-status' : 'toggleInvite',
            'click      .follow-item-change-status' : 'toggleFollow',
            'keyup      #search-friend-whosolves'   : 'searchFriends'
        },
                
        initialize: function(opt) {
            var $this = this;
            _.extend(this, opt);
            
            this.template = _.template(FindfriendsTpl);
            
            this.model = new UserModel({
                id: window.current_user.id
            });

            this.model.fetch({
                success: function() {
                    $this.render();
                }
            });               
        },       
                
        render: function() {             
            this.$el.html(this.template(_.extend(this.model.toJSON(), {
                'social'    : this.social,
                'labels'    : Labels
            })));
            
            this.social && this.selectProvider();                           
            return this;
        }, 
                
        inviteAll : function(){
            $('.users-invite-friends li').each(function(index, el){
                if(!$('.invite-item-change-status', el).hasClass('uninvite-img')){
                    $('.invite-item-change-status', el).addClass('uninvite-img');
                    $('.invite-item-change-status', el).removeClass('invite-img');
                }
            });

            this.followAndInvite(false, 'invite');
        },        
        
        followAll : function(){
            this.followAndInvite(false, 'follow');

            $('.users-already-in-qolve li').each(function(index, el){
                if(!$('.follow-item-change-status', el).hasClass('unfollow-img')){
                    $('.follow-item-change-status', el).addClass('unfollow-img');
                    $('.follow-item-change-status', el).removeClass('follow-img');
                }
            });            
        },
        
        followAndInvite : function(redirect, type){
            var followIds = [],
                inviteIds = [];               
            
            _.each($('.follow-img').closest('li'), function(userBox){
                followIds.push($(userBox).attr('id') + "");
            });
            
            _.each($('.uninvite-img').closest('li'), function(userBox){
                inviteIds.push($(userBox).data('email'));
            });
        
            if(type == 'invite'){
                if(inviteIds.length > 0){
                    $.ajax({
                        url: 'api/social/invite',
                        data: {
                            email : [inviteIds.join(',')]
                        },
                        type        : "POST",               
                        success: function(resp){},
                        error: function(resp) {}
                    }, this);
                }    
            }
            
            if(type == 'follow'){
                if(followIds.length > 0){
                    $.ajax({
                        url: '/api/followings',
                        data: {
                            userId : [followIds.join(',')]
                        },
                        type        : "POST",                
                        success: function(resp){},
                        error: function(resp) {}
                    }, this);
                }    
            }            
            
           redirect && Backbone.history.navigate('/home', true);
        },
        
        toggleInvite: function(e) {
            var el = $(e.target).hasClass('invite-item-change-status') ? $(e.target) : $(e.target).closest('.invite-item-change-status');                        

            if(el.hasClass('uninvite-img')) return;

            el.addClass('uninvite-img');   
            var userEmail     = $(el).closest('li').data('email');                             
           $.ajax({
                    url: 'api/social/invite',
                    data: {
                        email : userEmail
                    },
                    type        : "POST",               
                    success: function(resp){},
                    error: function(resp) {}
            }, this);
        },
                
        toggleFollow: function(e) {            
            var el = $(e.target).hasClass('follow-item-change-status') ? $(e.target) : $(e.target).closest('.follow-item-change-status');            
            if (el.hasClass('follow-img')) {                
                el.removeClass('follow-img');
                el.addClass('unfollow-img');
            } else {                
                el.removeClass('unfollow-img');
                el.addClass('follow-img');
            }
            
            var isFollowed = $(el).closest('li').attr('isfollowed'),
                userId     = $(el).closest('li').attr('id');     
            
            $(el).closest('li').attr('isfollowed', isFollowed == 1 ?  0 : 1);
            $.ajax({
                url         : isFollowed == 0 ? '/api/followings' : '/api/followings/' + userId,
                type        : isFollowed == 0 ? 'POST' : 'DELETE',
                dataType    : 'json',
                data: {
                    'userId': userId
                },
                success: function() {
                }}
            );   
        },
                
        searchFriends : function(e){
            var $this = this;
            if (e.which == 13) {
                if(this.whosolvesUsersView){
                    this.whosolvesUsersView.remove();
                }
                
                this.whosolvesUsersView = new UsersList({                    
                   url: '/api/users/search?name=' + $('#search-friend-whosolves').val()                   
                });
                $('div.who-solves', $this.$el).html(this.whosolvesUsersView.render().$el);              
            }
        },

        completeSocialConnect : function(){            
            if (this._friendWindow && this._friendWindow.closed){
                window.clearInterval(this._fbInterval);                
                $.ajax({
                       url: '/api/social/getprovidercontacts?provider=' + this.provider,                                          
                       type: "GET",
                       dataType: 'json',
                       success: function(resp) {
                           $('.find-friends-step-1').hide();
                           $('.find-friends-step-2').show();
                           if (resp.users['registereds'] && resp.users['registereds'].count > 0) {
                                $('.already-in-qolve').css('display', 'block');
                                _.each(resp.users['registereds'].list, function(user) {
                                    var template = _.template(UserTpl);
                                    user.invite = false;
                                    user.setImageSize = false;
                                    $('.users-already-in-qolve').append('<li id="' + user.id + '" class="user-item">' + template(user) + '</li>');
                                });
                            }

                            if (resp.users['unregistereds'] && resp.users['unregistereds'].count > 0) {
                                $('.invite-friends').css('display', 'block');
                                _.each(resp.users['unregistereds'].list, function(user) {                            
                                    var template = _.template(UserTpl);
                                    user.setImageSize = false;
                                    user.invite = true;
                                    $('.users-invite-friends').append('<li data-email="' + user.email + '" class="user-item">' + template(user) + '</li>');
                                });
                            }
                       },
                       error: function(resp) {
                       }
                }, this);    
            }
        },
        
        selectProvider: function(e) {
            if(!e){
                this.provider = this.social;
            }else{
                var el = $(e.target).hasClass('select-provider') ? $(e.target) : $(e.target).closest('.select-provider');
                this.provider = el.data('provider');
            }

            if (this.provider == 'whosolves') {
                $('.find-friends-whosolves', this.$el).show();   
                $('.find-friends-step-2', this.$el).hide();   
                $('.find-friends-step-1', this.$el).hide();   
                this.searchFriends({which : 13});
            } 
           else {
                this._friendWindow = window.open("/api/social/saveprovidercontacts?provider=" + this.provider, this.provider, "location=0, status=0, width=800, height=400");
                this._fbInterval   = window.setInterval(function ($this) {
                    $this.completeSocialConnect()}, 1E3, this);                    
           }
            
        }
    });

    return FindfriendsView;
});
