define([
    'jquery',
    'backbone',
    'underscore',  
    'text!views/templates/increaseCredits.html',  
    ], function ($, Backbone, _, IncreaseCreditsTpl) {

        var increaseCreditsView = Backbone.View.extend({
            className           : 'increase-credits',
                    
            initialize : function() {
                var $this = this;
                this.template = _.template(IncreaseCreditsTpl);              
            },

            render : function() {
                this.$el.html(this.template({                      
                }));                   
                return this;
            }             
        });

        return increaseCreditsView;
    });
