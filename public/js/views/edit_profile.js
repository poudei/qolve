define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'models/user',
    'jquery-helper',
    'bootstrap-select',
    'text!views/templates/edit_profile.html',
    'text!views/templates/confirmModal.html',
    'i18n!nls/labels',
    'complexify',
    'libs/jquery.complexify.banlist',
    'semantic',
    'jquery.knob',
], function($, Backbone, _, App, UserModel, jqHelper, BootstarpSelect, EditProfileTpl, ConfirmModalTpl, Labels) {

    var EditProfileView = Backbone.View.extend({
        className: 'edit-profile container',
        events: {
            'click      .side-bar-content'          : 'changeContent',
            'click      .submit-edit-profile'       : 'saveEditProfile',
            'click      .submit-change-password'    : 'saveChangePassword',            
            'click      .confirm-question'          : 'delAccount',
            'click      .confirm-decline'           : 'hideConfirmModal',
            'blur       #username'                  : 'checkUsername',
            'click      .delete-account'            : 'showConfirmModal',
            'click      #remove-img'                : 'removeProfilePhoto',
            'click      .dis-connect-from-social'   : 'disconnectAccount',
            'change     #birth_month'               : 'changeBirhtDay',
            'change     #birth_day'                 : 'changeBirhtDay',
            'change     #birth_year'                : 'changeBirhtDay',
        },
                
        initialize: function(opt) {
            var $this = this;
            opt = opt || {};

            this.template = _.template(EditProfileTpl);
            this.model = new UserModel({
                id: window.current_user.id
            });

            this.model.fetch({
                success: function() {
                    $this.render();
                    $this.addUploader();
                }
            });

            $(window).on('resize', function() {
                $('.edit-page-content').css('min-height', ($(window).height() - 82));
            });
        },
                
        render: function() {
            this.$el.html(this.template(_.extend(this.model.toJSON(), {
                labels : Labels
            })));
            this.afterRender();            
            return this;
        },
        
        afterRender : function(){
            $('.ui.checkbox', this.$el).checkbox();
            var d = new Date();
            for (var i = d.getFullYear(); i >= 1905; i--)
                $('#birth_year', this.$el).append('<option vlaue=' + i + '>' + i + '</option>');
            
            $(".knob", this.$el).knob();
            $('.loading-img-wrapper div', this.$el).css('display', 'none');
            $('.edit-page-content', this.$el).css('min-height', ($(window).height() - 82));
            $('#timezone', this.$el).selectpicker({width: '363'});
            $('#timezone', this.$el).selectpicker('val', this.model.get('timezone'));
            $('select#birth_day', this.$el).selectpicker({width: '90'});
            $('select#birth_month', this.$el).selectpicker({width: '120'});
            $('select#birth_year', this.$el).selectpicker({width: '145'});          
            
            var d = new Date(this.model.get('birthday')); 
            $('select#birth_day', this.$el).selectpicker('val', d.getDate());
            $('select#birth_month', this.$el).selectpicker('val', d.getMonth());
            $('select#birth_year', this.$el).selectpicker('val', d.getFullYear());
            
            this.passwordComplexify();
        },        
                
                
        saveEditProfile: function() {   
            if($('.submit-edit-profile').attr('disabled') == 'disabled')
                return;              
            
            var birthday  = ($('#birth_year').val() != 0 && $('#birth_month').val() != 0 && $('#birth_day').val() != 0) ? $('#birth_year').val() + '-' + (parseInt($('#birth_month').val()) + 1) + '-' + $('#birth_day').val() : '',
                $this     = this;            
            
            if(!/^[a-zA-Z0-9._]+$/.test($('#username', this.$el).val())){
                $this.showErrorMsg('username_contain_letters_numbers_underscores');    
                return false;
            }
            
            this.model.set('birthday', new Date(birthday));            
            this.model.save($('#edit-profile').serializeJSON(), {
                success: function() {                   
                    $this.showSuccessMsg('success_edit_profile');  
                    $this.model.unset('profile');
                    $('.login-user-name').html($('#first_name').val());
                    $('.main-menu .my-page').attr('href', '/users/' + $this.model.get('username'));
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('.navbar-fixed-top .login-default-user-small img').attr('src', $this.model.get('imagePath') + '?width=30&height=30');
                },
                error: function(error) {                   
                     $this.showErrorMsg('there_error_try_later'); 
                     $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            });
        },
        
        disconnectAccount : function(e){
            var el      = $(e.target).hasClass('dis-connect-from-social') ? $(e.target) : $(e.target).closest('.dis-connect-from-social'),
              social    = el.data('social'),
              published = 0,
              $this     = this;
      
            if(this.model.get('providers')['social'] == undefined)
                return;
            
            if(this.model.get('providers')['social'] == 0){
                published = 1;
                $('.connection-type', el).html(Labels['connect']);
            }
            else{
                published = 0;
                $('.connection-type', el).html(Labels['disconnect']);
            }
            
             $.ajax({
                url: 'api/social/settings',
                dataType: 'json',
                type: 'POST',
                data: JSON.stringify({
                    'social'    : social,
                    'published' : published               
                }),
                success: function(resp) {                  
                     $this.showSuccessMsg('success_edit_profile');      
                },
                error: function() {                   
                     $this.showErrorMsg('there_error_try_later');    
                }
            });
        },
        
        validateChangePass: function(passwordForm) {
            var strength = $('.password-strength-wrapper .val-strength').data('data-strength');
            
            if (passwordForm.newPassword != passwordForm.confirmPassword) {             
                this.showErrorMsg('newPassword_confirmedPassword_arenot_equal');    
                return false;
            } else {
                if (passwordForm.newPassword.length < 6){             
                    this.showErrorMsg('passwor_must_beleast_6_character');    
                    return false;
                }else{
                    if(strength < 10){
                        this.showErrorMsg('your_password_is_weak');    
                        return false;
                    }else{
                        $('.alert-qolve').css('visibility', 'hidden');
                            return true;
                    }                  
                }
            }
        },
        
        changeBirhtDay : function(){               
             var month = $('#birth_month', this.$el).val(),
                 day   = $('#birth_day', this.$el).val(),
                 year  = $('#birth_year', this.$el).val();         
            
            var d        = new Date(year, month, day);                
          
            if (d.getMonth() != month)
                $('#birth_month', this.$el).selectpicker('val', d.getMonth());
            if (d.getDate() != day)
                $('#birth_day', this.$el).selectpicker('val', d.getDate());
            if (d.getFullYear() != year)
                $('#birth_year', this.$el).selectpicker('val', d.getFullYear());          
            
            return false;
        },
                
        addUploader: function() {
            this.levels = [];
            var $this = this;
            var uploader = new plupload.Uploader({
                runtimes: 'html5,html4',
                file_data_name: 'post',
                multi_selection: false,
                browse_button: 'pickfiles',
                container: 'user-profile-img',
                max_file_size: '10mb',
                progress: true,
                url: '/api/uploads/file?order=1',
                filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }]
            });

            uploader.init();
            uploader.bind('FilesAdded', function(up, files) {
                up.refresh();
                uploader.start();
                $('.loading-img-wrapper div').css('display', 'inline');
            });
            
            uploader.bind('UploadProgress', function(up, file) {                
               $('.knob').val(file.percent).trigger("change");               
            });

            uploader.bind('FileUploaded', function(up, file, resp) {
               $('#user-profile-img img').attr('src', $.parseJSON(resp.response).file.location + '?width=30&height=30');
               $('.loading-img-wrapper div', this.$el).css('display', 'none');
               $this.model.set('profile', $.parseJSON(resp.response).file);                         
            });
        },
                
        saveChangePassword: function() {
            var passwordForm = $('#change-password').serializeJSON(),
                $this        = this;    

            if (!this.validateChangePass(passwordForm))
                return;

            $.ajax({
                url: 'api/user/changepassword',
                dataType: 'json',
                type: 'POST',
                data: JSON.stringify({
                    'credential': passwordForm.oldPassword,
                    'newCredential': passwordForm.newPassword,
                    'newCredentialVerify': passwordForm.confirmPassword
                }),
                success: function(resp) {
                    $this.showSuccessMsg('successfully_yourPasswordChanged');                    
                },
                error: function() {                   
                    $this.showErrorMsg('there_error_try_later');                    
                }
            });
        },
        
        showSuccessMsg : function(verb){
            $('.alert-qolve').html(Labels[verb]);
            $('.alert-qolve').removeClass('alert-error-qolve');
            $('.alert-qolve').addClass('alert-success-qolve');
            $('.alert-qolve').css('visibility', 'visible');
        },
        
        showErrorMsg : function(verb){
            $('.alert-qolve').html(Labels[verb]);
            $('.alert-qolve').removeClass('alert-success-qolve');
            $('.alert-qolve').addClass('alert-error-qolve');
            $('.alert-qolve').css('visibility', 'visible');
        },

        removeProfilePhoto : function(){
            $('#user-profile-img img').attr('src', '');
            this.model.set('imagePath', 'deleted');
        },
        
        checkUsername: function() {            
            var $this = this,
                 el   = $('#username', this.$el);   
            
            if(el.val() == el.data('username'))
                return;
            
            el.data('username', el.val());            
            $.ajax({
                url: '/api/social/checkusername',
                data: JSON.stringify({
                    username: el.val()
                }),
                type: "POST",
                dataType: 'json',
                success: function(resp) {
                    if (resp.exists) {
                        $('.username-wrap .error-signup').css('display', 'block');                        
                        $('.correct-username').hide();
                        
                    } else {
                        $('.correct-username').show();
                        $('#username').attr('checked', 'checkedUsername');
                        $('.username-wrap .error-signup').hide();                        
                    }                                  
                    $('.submit-edit-profile').removeAttr('disabled');
                },
                error: function(resp) {
                    $('.correct-username').hide();                    
                    $('.username-wrap .error-signup').css('display', 'block');
                    $('.submit-edit-profile').attr('disabled', 'disabled');
                }
            }, this);
        },
        
        changeContent: function(e) {
            var el = $(e.target).closest('li');
            $('form', this.$el).css('display', 'none');
            $('#' + el.data('type')).css('display', 'block');
            $('li.active', this.$el).removeClass('active');
            el.addClass('active');
        },
                
        showConfirmModal: function() {
            if ($('.modal-confirm', this.$el).length == 0) {
                var confTemplate = _.template(ConfirmModalTpl);
                this.$el.append(confTemplate({
                    message : Labels['are_you_sure_want_delete_your_account'],
                    labels  : Labels
                }));
            }
            $('.modal-confirm', this.$el).modal('show');
            $('.modal-confirm', this.$el).on('hide.bs.modal', function() {
                $('.modal').addClass('vclose')
            });
            $('.modal-confirm', this.$el).on('hidden.bs.modal', function() {
                $('.modal').removeClass('vclose')
            });
        },
                
        delAccount: function() {
            var $this = this;

            $('.modal-confirm', this.$el).modal('hide');
            $.ajax({
                url: '/api/user/deleteaccount',
                type: "POST",
                data: {
                    'userId': window.current_user.id
                },
                success: function(response, statusText, xhr) {
                    window.location.pathname = "";
                },
                error: function(resp) {
                }
            }, this);
        },
        
        hideConfirmModal: function() {
            $('.modal-confirm', this.$el).modal('hide');
        },
        
        passwordComplexify: function() {
            var $this = this;
            $("#new-password", $this.$el).complexify({}, function(valid, complexity) {
                var val = parseFloat(complexity).toFixed(2);
                $('.password-strength-wrapper .val-strength').data('data-strength', val);
                
                if(val < 10)
                    $('.password-strength-wrapper .val-strength').html(Labels['weak']);
                if(val > 10 && val < 70)
                    $('.password-strength-wrapper .val-strength').html(Labels['normal']);
                if(val > 70)
                    $('.password-strength-wrapper .val-strength').html(Labels['strong']);                
              
                if (!valid) {
                    $('.password-strength-wrapper .progress-success .bar', $this.$el).css({
                        'width': complexity + '%'
                    }).removeClass('progress-success').addClass('progress-danger');
                } else {
                    $('.password-strength-wrapper .progress-success .bar', $this.$el).css({
                        'width': complexity + '%'
                    }).removeClass('progress-danger').addClass('progress-success');
                }
            });
        }
    });

    return EditProfileView;
});
