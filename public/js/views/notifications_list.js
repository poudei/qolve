define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',
    'collections/notifications',
    'views/notifications_list_item',   
    'text!views/templates/notification.html',
    'i18n!nls/labels',
], function($, _, Backbone, App, BaseListView, NotificationsCollection, NotificationsItemView, NotificationTpl, Labels) {
    var NotificationsListView = BaseListView.extend({
        events: {},                
        className : 'notifications-list',        
                
        initialize: function() {
            this.collection = new NotificationsCollection([], {
                mode : 'all'
            });            
            NotificationsListView.__super__.initialize.apply(this, []);
        },
                
        render: function() {
            this.template = _.template(NotificationTpl);
            this.$el.html(this.template({
                labels   : Labels
            }));           
            return this;
        },            
                
        renderItem: function(model) {                        
            var item = new NotificationsItemView({
                model       : model                    
            });
            
            if(!this.olderThanWeek(new Date(model.get('createdOn')), new Date())){
                $('.this-week-notifications-title', this.$el).css('display', 'block');
                $('ul.this-week-notifications', this.$el).append(item.render().$el);
            }
            else{
                $('.older-notifications', this.$el).css('display', 'block');
                $('ul.older-notifications', this.$el).append(item.render().$el);
                $('.older-notifications-title', this.$el).css('display', 'block');
            }
        },
                
        olderThanWeek : function(createdOn, now){              
            var diff = Math.abs(createdOn - now) / 86400000;
            if (diff > 7) {
                return true;
            }
            return false ;           
        }
    });

    return NotificationsListView;
});
