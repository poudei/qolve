define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'views/feeds_list',
    'views/questions_list',
    'text!views/templates/home.html',
    'i18n!nls/labels',
    'jquery-helper',
], function($, Backbone, _, App, FeedsListView, QuestionsListView, HomeTpl, Labels) {

    var homeView = Backbone.View.extend({
        className: 'page-home',
                
        initialize: function(opt) {
            opt = opt || {};
            this.questionType = opt.questionType ? opt.questionType : 'recent';
            this.template = _.template(HomeTpl);
        },
                
        events: {
            'click .questions-type li': 'changeQuestionType',
            'click   #new-feeds-alert' : 'showNewFeeds' 
        },
                
        render: function() {
            this.$el.html(this.template({
                labels : Labels
            }));
            this.questionsList();
            
            if(window.current_user.id){
                this.setInterval();
            }
            return this;
        },
                
        questionsList: function() {
            this.feedsListView && this.feedsListView.remove();
            this.questionsListView && this.questionsListView.remove();

            if (this.questionType == 'top') {
                var filters = {
                    'orderBy': 'votes',
                    'orderDir': 'DESC'
                }

                this.questionsListView = new QuestionsListView({
                    _showUser: true,
                    _filters: filters
                });
                $('.questions-content', this.$el).html(this.questionsListView.render().$el);

            }else{
                this.feedsListView = new FeedsListView({
                    _showUser: true

                });
                $('.questions-content', this.$el).html(this.feedsListView.render().$el);
            }
        },
                
        showNewFeeds: function() {
            this.feedsListView.collection.add(this.newFeeds.list, {'at' : 0});
             $('#new-feeds-alert').hide();
        },
                
        setInterval: function() {
            setInterval(function($this) {
                if ($this.feedsListView && $this.feedsListView.collection.length > 0) {
                    $.ajax({
                        url: '/api/feeds?fromId=' + $this.feedsListView.collection.at(0).get('id'),
                        type: 'GET',
                        dataType: 'json',
                        success: function(resp) {
                            $this.newFeeds = resp.feeds;
                            if ($this.questionType == 'recent' && $this.newFeeds.count > 0) {
                                if ($this.newFeeds.count == 1)
                                    $('#new-feeds-alert span').html(Labels['there_is_one_new_activity']);
                                else
                                    $('#new-feeds-alert span').html(Labels['there_are'] + ' ' + $this.newFeeds.count + ' ' + Labels['new_activities']);
                                $('#new-feeds-alert').show();
                            }
                        },
                        error: function(resp) {
                        }
                    }, this);
                }
            }, 120000, this);
        },
        
        changeQuestionType: function(e) {
            var el = $(e.target),
                    type = el.data('type');

            if (el.hasClass('selected'))
                return;
            $('#new-feeds-alert').hide();
            $('.questions-type li', this.$el).removeClass('selected');
            el.addClass('selected');
            $('.hold-on-loading').css('display', 'none'); 
            this.questionType = type;
            this.questionsList();
        }
    });

    return homeView;
});
