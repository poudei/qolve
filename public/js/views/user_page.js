define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'views/feeds_list',
    'views/lists',
    'views/users_list',    
    'views/following_tags_user',
    'models/user',        
    'text!views/templates/user_page.html',    
    'i18n!nls/labels',        
], function($, Backbone, _, App, FeedsList, ListView, UsersList, FollowtagsUser, UserModel, UserPageTmp, Labels) {

    var UserPageView = Backbone.View.extend({
        className : 'user-page container',
        events: { 
            'click  .follow-change-status'  : 'toggleFollow',
            'click  .lists-user'            : 'listUser',
            'click  .follower-user'         : 'followerUser',        
            'click  .following-user'        : 'followingUser',
            'click  .feeds-user'            : 'feedsUser',
            'click  .followtags-user'       : 'followtagsUser'       
        },        
                
        initialize: function(opt) {
            var $this = this;
            this.template = _.template(UserPageTmp);

            this.model = new UserModel({
                id: opt.id
            });
            this.model.fetch({
                success: function() {
                    $this.render(false);
                }
            });           
        },
        
        render: function(renderHeader) {
            var obj = _.extend(this.model.toJSON(), {
                renderHeader    : renderHeader,
                labels          : Labels
            });
            this.$el.html(this.template(obj));                     
            this.feedsUser();
            return this;
        },        
           
        feedsUser: function(e) {
            var loaded = this.preRender(this.feedsListView, 'feeds');
            
            this.feedsListView = new FeedsList({
                _showUser   : false,
                _userId     : this.model.get('id'),
                actor       : this.model.toJSON(),
                collection  : this.feedsCollection,
                loaded      : loaded
            });

            $('.info-user-content', this.$el).html(this.feedsListView.render().$el);                       
        },
                
        followerUser : function(e){
            var loaded = this.preRender(this.followersListView, 'follower');
            
            this.followersListView = new UsersList({                    
                collection : this.followerCollection,
                loaded     : loaded,
                type       : 'follower',
                userItem   : this.model, 
                url        : '/api/users/' + this.model.get('id') + '/followers'
            });
            
            $('.info-user-content', this.$el).html(this.followersListView.render().$el);           
        },
        
        followtagsUser : function(e){
            var loaded = this.preRender(this.followtagsView, 'followtags');
            
            this.followtagsView = new FollowtagsUser({                    
                collection : this.followtagsCollection,                
                type       : 'followingtags',
                userItem   : this.model, 
                actor      : this.model.toJSON(),
                loaded     : loaded,
                url        : '/api/keywords/follows?userId=' + this.model.get('id')                
            });
            
            $('.info-user-content', this.$el).html(this.followtagsView.render().$el);           
        },
        
        followingUser : function(e){
            var loaded = this.preRender(this.followingListView, 'following');
            
            this.followingListView = new UsersList({                    
                collection : this.followingCollection,
                loaded     : loaded,
                type       : 'following',
                userItem   : this.model, 
                url        : '/api/users/' + this.model.get('id') + '/followings'
            });
            
            $('.info-user-content', this.$el).html(this.followingListView.render().$el);           
        },
        
        listUser : function(e){
            var loaded = this.preRender(this.listView, 'lists');
            
            this.listView = new ListView({                    
                collection : this.listsCollection,
                loaded     : loaded,     
                _userId    : this.model.get('id') 
            });
            
            $('.info-user-content', this.$el).html(this.listView.render().$el);           
        },
                
        preRender : function(view, type){
            var loaded = false,            
                    el = $('.' + type + '-user', this.$el);            
            
            $('.hold-on-loading').data('loading', false);
            $('.hold-on-loading').hide();
            $('.hold-on-loading').waypoint('destroy');
            $('.info-user-items').removeClass('selected');
            el.addClass('selected');    
            
            if (view) {
                this[type + 'Collection'] = view.collection;                  
                view.disposeAllItems();
                view.remove();       
                loaded = true;                                      
            }
            $('.hold-on-loading').hide();
            $('.hold-on-loading').waypoint('destroy');      
            
            return loaded;
        },
                
        toggleFollow : function(){
            var $this = this;
            this.model.set('isFollowed', this.model.get('isFollowed') == 1 ? 0 : 1);
            if ($this.model.get('isFollowed')) {
                var el = $('.follow-change-status', this.$el);
                el.removeClass('follow-img');
                el.addClass('unfollow-img');
                el.html('− ' + Labels['unfollow']);
            } else {
                var el = $('.follow-change-status', $this.$el);
                el.removeClass('unfollow-img');
                el.addClass('follow-img');
                el.html('+ ' + Labels['follow']);
            }               
            this.model.toggleFollow();
        }
    });

    return UserPageView;
});
