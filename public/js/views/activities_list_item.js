define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',        
    'base/view/itemView',        
    'text!views/templates/activitiesList_item.html',   
    'i18n!nls/labels',
    ], function ($, jqueryUi , _, Backbone, ItemView, activityTpl, Labels) {
        var ActivitiesListItemView = ItemView.extend({
            tagName   : 'li',
            className : 'activites-item',
            events : {},

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },

            render    : function() {     
                this.template = _.template(activityTpl);                                
              
                this.$el.html(this.template(_.extend(
                    this.model.toJSON(),{
                        labels : Labels
                    }
                )));                                
                return this;
            }          
        });

        return ActivitiesListItemView;
    });
