define([
    'jquery',
    'underscore',
    'backbone',          
    'base/view/listView',
    'collections/explore',
    'base/model/FilterModel',
    'views/explore_list_item',
    'text!views/templates/explore.html', 
    'text!views/templates/exploreTrendingKey.html', 
    'text!views/templates/explore_follow_keys.html', 
    'i18n!nls/labels',
    'jquery-helper',       
    'jquery-ui',
    'semantic'
], function($, _, Backbone, BaseListView, ExploreCollection, FilterModel, ExploreItemView, ExploreTpl, ExploreTrendingKeyTpl, KeyFollowModalTpl, Labels) {
    var ExploreView = BaseListView.extend({
        className: 'explore-page',
        events: {  
            'click          .explore-modal'              : 'showExploreModal',
            'click          .select-levels'              : 'showLevels',          
            'click          .filter-item'                : 'filterItems',
            'click          .apply-btn'                  : 'applyFilter',   
            'click          .clear-form'                 : 'resetForm',
            'click          .trending-items'             : 'chooseTrendingItem',
            'keyup          #explore-input'              : 'applyFilter',
            'click          .follow-item-change-status'  : 'followTags',
            'click          .follow-search-keys'         : 'followSearckeys'
        },
                
        initialize: function(opt) {
            this.collection = new ExploreCollection([]);
            this.collection._notFetch = true;                    
            
            if(opt && opt.key)
                this.collection.filters.set('keywords', opt.key);            
            
            this.collection.on('reset', function(){
                $('.explore-items', this.$el).removeClass('explore-trendingtags');
                $('.explore-items').empty();
                this.filters = new FilterModel();
            });                     
            
            this.collection.fetch({
                add         : true,
                silent      : true,
                update      : true,
                dataType    : "json",
                type        : 'POST',
                data        : JSON.stringify( this.collection.filters.toJSON())
            });         
            
            ExploreView.__super__.initialize.apply(this, []);
        },              
                
        render: function() {
            this.exploreTpl = _.template(ExploreTpl);
            $(this.$el).html(this.exploreTpl({
                labels : Labels
            }));                
            this.afterRender();
            return this;
        }, 
        
        renderItem: function(model) {     
            if(model.get('trendingKeywords')){
                this.renderTrendingKey(model);
                return;
            }        
            
            var item = new ExploreItemView({
                model: model              
            });
           
            $('ul.explore-items', this.$el).append(item.render().$el);
        }, 
                
        renderTrendingKey : function(model){
            $('.explore-items', this.$el).addClass('explore-trendingtags');
            this.exploreTrendingKeyTpl = _.template(ExploreTrendingKeyTpl);
            $('.explore-items', this.$el).html(this.exploreTrendingKeyTpl({
                 trendingKey : model.get('trendingKeywords'),
                 labels      : Labels 
            }));    
        },
                
        afterRender : function(){
            $('.ui.checkbox', this.$el).checkbox();            
            
            $("#explore-input", this.$el).autocomplete({
                selectFirst: true,               
                source: function(request, response) {
                    var s = [];                  
                    $.ajax({
                        url: "/api/keywords/" +  $('#explore-input', this.$el).val(),
                        dataType: 'json',
                        success: function(data) {
                            var data = data.keyword;
                            for (var i in data) {
                                var cat = {
                                    label: data[i].name,
                                    value: data[i].name
                                }
                                s.push(cat);
                            }
                            response(s, request.term);
                        }
                    });

                },                
                minLength: 3
            });
                
            this.options['key'] != '' && this.options['key'] && $('#explore-input', this.$el).val(this.options['key']);
        },
        
        chooseTrendingItem : function(e){
            var el = $(e.target).hasClass('trending-items') ? $(e.target) : $(e.target).closest('.trending-items');
            var key = el.data('key');
            $('#explore-input').val(key);
            this.collection.reset();
            this.collection.filters.set('keywords', key);                         
            this.collection.fetch({
                add         : true,
                silent      : true,
                update      : true,
                type        : 'POST',
                dataType    : 'JSON',
                data        : JSON.stringify( this.collection.filters.toJSON())
            }); 
        },
                
        resetForm : function(){
            $('.filter-explore-list .selected').removeClass('selected');
            $("input[name='levels[]']:checked").each(function() {
                 $(this).trigger('click');
            });
            $('.keywords').empty();
        },
                
        applyFilter : function(e){
            var filter  = {},
                $this   = this,
                levels  = [];
            if($("input[name='levels[]']:checked").length > 0) levels = [] ; 
            filter['keywords'] = '';
            
             if(e.type == 'keyup' && e.which != 13) return;
            
            $("input[name='levels[]']:checked").each(function() {
                 levels.push($(this).val());
            });         
            
            if(levels.length > 0)
                filter['levels'] = levels.join(',');
            
            _.each($('#explore-input').val().split(/(?:,| |#)+/), function(key){
                if(key != "")
                    filter['keywords'] = filter['keywords'] + key + ' ' ;
            });            
            
            _.each($('.filter-explore-list .selected'), function(el){
                var type = $(el).data('type'),
                    val  = $(el).data('val');    
                    filter[type] = val;                                
            });                   
            
            this.collection.reset();
            this.collection.filters.set(filter);
            this.collection.fetch({
                add         : true,
                silent      : true,
                update      : true,
                dataType    : "json",
                type        : 'POST',
                data        :JSON.stringify( this.collection.filters.toJSON())
            });  
            $('.modal-explore', this.$el).modal('hide');           
        },
                
        filterItems : function(e){           
            var el = $(e.target).hasClass('filter-item') ? $(e.target) : $(e.target).closest('.filter-item'),
                type = el.data('type'),
                val  = el.data('val');
        
             if(el.hasClass('selected')){
                el.removeClass('selected');     
                return;
             }
           
            _.each($('.filter-item[data-type=' + type + ']'), function(el){
                if($(el).hasClass('selected'))
                    $(el).removeClass('selected');
            });
            
            if(el.hasClass('selected'))
                el.removeClass('selected');     
            else
                el.addClass('selected');     
            
        },       
                
        showExploreModal : function(){
            $('.modal-explore').modal('show');
            $('.modal-explore', this.$el).on('hide.bs.modal', function(){$('.modal').addClass('vclose')});
            $('.modal-explore', this.$el).on('hidden.bs.modal', function(){$('.modal').removeClass('vclose')});
        },        
                
        showLevels: function(e) {
            if ($('.show-dialog-select-level').css('display') == 'none') {
                $('.select-levels').removeClass('border-design');
                $('.select-levels .drop-down-icon').rotate(180,5,0);
                $('.select-levels').addClass('show-levels');
            }else{
                $('.select-levels .drop-down-icon').rotate(0,-5,180);                
                
            }
            $('.show-dialog-select-level').slideToggle(function() {
                if($('.show-dialog-select-level').css('display') == 'none'){
                    $('.select-levels').addClass('border-design');
                    $('.select-levels').removeClass('show-levels');
                }
            });
        },
        
        showFollowModal : function(keys){
            var modalTpl = _.template(KeyFollowModalTpl)
            $('#modal-wrapper' ,this.$el).html(modalTpl({
                keys    : keys,
                labels  : Labels
            }));

            $('.modal-follow-keys', this.$el).on('hide.bs.modal', function() {
                $('.modal').addClass('vclose');
            });
            $('.modal-follow-keys', this.$el).on('hidden.bs.modal', function() {
                $('.modal').removeClass('vclose');
            });
            $('.modal-follow-keys .modal-body', this.$el).enscroll({
                showOnHover: false,
                verticalTrackClass: 'track3',
                verticalHandleClass: 'handle3'
            });

            $('.modal-follow-keys', this.$el).modal('show');
        },
                
        followSearckeys : function(e){                                           
           //We show modals just if we have more than one keys to search
           
           if(this.collection.keywordFollow.length == 1){
               if($('.top-explore-page', this.$el).data('isfollow') == undefined){
                    $('.top-explore-page', this.$el).data('isfollow', this.collection.keywordFollow[0]['isFollowed']);
                     $('.top-explore-page', this.$el).data('key', this.collection.keywordFollow[0]['name']);
               }
               
               this.followTags(e, true);
           }else{
               this.showFollowModal(this.collection.keywordFollow);
           }            
            return false;        
        },
        
        followTags : function(e, searchKey){
            var el     = searchKey ? $(e.target).closest('.top-explore-page') : $(e.target).closest('.follow-keys-explore'),
            tagId      = el.data('key'),
           isFollow    = el.data('isfollow');                    
           
           if(isFollow == 1){       
               $('.unfollow-tag', el).html('+ ' + Labels['follow']);
               $('.unfollow-tag', el).removeClass('unfollow-tag').addClass('follow-tag');
           }else{
               $('.follow-tag', el).html('- ' + Labels['unfollow']);
               $('.follow-tag', el).removeClass('follow-tag').addClass('unfollow-tag');
           }
           el.data('isfollow', isFollow == 0  ? 1 : 0);
           $.ajax({
                url: isFollow == 0 ? '/api/keywords/follows' : '/api/keywords/follows/' + tagId,
                data: {"keywords" : [{ "name" : tagId}]},
                type        : isFollow == 0 ? "POST" : 'DELETE',
                dataType    : 'json',
                success: function(resp){},
                error: function(resp) {}
            }, this);
            
            return false;
        },
                
        selectLevel: function(e){
            var count = 0;
            _.each($('.select-level-question'), function(e){                
                if($(e).is(":checked"))
                    count ++;
            });            
            count > 0 ? $('.selected-levels-count').html('( ' + count + Labels['levels_selected'] + ' )') :  $('.selected-levels-count').html('');
        },
        
        afterRenderItems : function(){
            if(!this.collection.keywordFollow || (this.collection.keywordFollow && this.collection.keywordFollow.length == 0)){
                $('.follow-search-keys', this.$el).hide();
            }else{
                var el = $('.top-explore-page', this.$el);
                if (this.collection.keywordFollow.length == 1) {                   
                        $('.top-explore-page', this.$el).data('isfollow', this.collection.keywordFollow[0]['isFollowed']);
                        $('.top-explore-page', this.$el).data('key', this.collection.keywordFollow[0]['name']);                                         
                        
                        if (this.collection.keywordFollow[0].isFollowed == 0) {
                            $('.unfollow-tag', el).html('+ ' + Labels['follow']);
                            $('.unfollow-tag', el).removeClass('unfollow-tag').addClass('follow-tag');
                        } else {
                            $('.follow-tag', el).html('- ' + Labels['unfollow']);
                            $('.follow-tag', el).removeClass('follow-tag').addClass('unfollow-tag');
                        }                    
                }else{
                     $('.unfollow-tag', el).html('+ ' + Labels['follow']);
                     $('.unfollow-tag', el).removeClass('unfollow-tag').addClass('follow-tag');
                }
                
                $('.follow-search-keys', this.$el).show();
            }    
            
            if(this.collection.length == 0){
                $('ul.explore-items', this.$el).html('<div class="no-result">' + Labels['no_result_found'] + '</div>');
            }
        },

        loadMore: function() {
            this.collection.increase().fetch({
                add         : true,
                silent      : true,
                update      : true,
                dataType    : "json",
                type        : 'POST',
                data        : JSON.stringify( this.collection.filters.toJSON())
            });
        },
        
    });

    return ExploreView;
});
