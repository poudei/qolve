define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',
    'base/view/itemView',
    'text!views/templates/user_item.html',
], function($, jqueryUi, _, Backbone, ItemView, userItemTpl) {
    var UserItemView = ItemView.extend({
        tagName: 'li',
        className: 'user-item',
        events: {
            'click  .follow-item-change-status' : 'toggleFollow',
        },
        
        initialize: function(opt) {
            _.extend(this, opt);
            _.bindAll(this, 'render');
            this.model.on('change', this.render);
        },
                
        render: function() {
            this.template   = _.template(userItemTpl);                        
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
                
        toggleFollow: function() {
            var $this = this;            
            
            if(this.callee && this.callee.type == 'following' && window.current_user && window.current_user.id && this.callee.userItem && this.callee.userItem.get('id') == window.current_user.id){
                var id = this.model.get('id');
                this.remove();
                if(this.callee.collection.get(id)) this.callee.collection.remove(id);
                $('.user-page .following-user p').html('(' + this.callee.collection.length + ')');
            }
            
            this.model.set('isFollowed', this.model.get('isFollowed') == 1 ? 0 : 1);
            if ($this.model.get('isFollowed')) {
                var el = $('.follow-change-status', this.$el);
                el.removeClass('follow-img');
                el.addClass('unfollow-img');
            } else {
                var el = $('.follow-change-status', $this.$el);
                el.removeClass('unfollow-img');
                el.addClass('follow-img');
            }
            this.model.toggleFollow();
        }

    });

    return UserItemView;
});
