define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',
    'collections/answers',
    'models/answer',
    'views/answers_list_item',
    'text!views/templates/new_answer.html',
    'text!views/templates/img_uploaded.html',
    'i18n!nls/labels',
    'libs/browserplus-min',
    'libs/plupload.full',
    'jquery-ui',
    'semantic'
], function($, _, Backbone, App, BaseListView, AnswerCollection, AnswerModel, AnswerItemView, NewAnswerTpl, ImgUploadedTpl, Labels) {
    var AnswersListView = BaseListView.extend({
        tagName: 'ul',
        className: 'answers-list',
        events: {
            'click      .send-answer'       : 'sendAnswer',
            'click      .loading-answers'   : 'loadMoreAnswers',
            'click      .del-img-upload'    : 'removeUploadedDoc',
            'click      .visit-img-upload'  : 'previewUploadedDoc',
            'click      .edit-answer'       : 'editAnswer',
            'keyup      #description-answer': 'removeErrors'            
        },
                
        initialize: function(opt) {
            _.bindAll(this, 'render', 'renderItem');
            this.questionModel = opt.questionModel;
            
            this.callee             = opt.callee;    
            this._questionId        = opt._questionId;
            this._userAnswerId      = this.questionModel.get('userAnswerId');
            this._isAcceptAnswer    = this.questionModel.get('isAsked') != 0 && this.questionModel.get('status') != 3;   
       
            this.collection = new AnswerCollection([], {
                question_id: this.questionModel.get('id')
            });

            app = require('app');
            this.collection.filters.set('limit', 5);
            this.collection.on('add', this.renderItem);
            this.collection.on('remove', this.afterAnswerRemove, this);
            this.collection.filters.count = this.questionModel.get('answers');
            this.render();
            this.fillCollection(this.questionModel.get('answersList'));
        },
        
        onAppend : function(){
            if(this.questionModel.get('isAsked') == 0){
                this.addUploader();
            }else{
                if(this.collection.models.length <= 5){
                    $('.new-answer-wrapper', this.$el).hide();                     
                }
                if(this.collection.models.length == 0){
                    $('.answer-count-sen', this.callee.$el).hide();                     
                }               
            }
        },
                
        addUploader: function() {
            this.docs = [];
            this.levels = [];
            var $this = this;
            var uploader = new plupload.Uploader({
                runtimes: 'html5,html4',
                file_data_name: 'post',
                browse_button: 'pickfiles',
                container: 'new-image-upload',
                max_file_size: '10mb',
                url: '/api/uploads/file?order=1',
                filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }]                
            });

            uploader.init();
            uploader.bind('FilesAdded', function(up, files) {
                var imgUploadedTpl = _.template(ImgUploadedTpl);
                $('.new-uploaded-img', this.$el).append(imgUploadedTpl({
                    location: '',
                    id: files[0].id,
                    mode: 'create'
                }));
                up.refresh();
                uploader.start();
            });

            uploader.bind('FileUploaded', function(up, file, resp) {
                var resp = $.parseJSON(resp.response).file;
                $('#' + file.id + ' img').show();
                var size = $.fn.ResizeImg(resp.height, resp.width, '', 120);
                $('#' + file.id + ' img').attr('src', resp.location + '?width=' +  size.width + '&height=' + size.height);
                $('#' + file.id + ' .progress').hide();
                $('#' + file.id).data('upload', resp);
            });

            $(".new-uploaded-img").sortable();
        },
                
        render: function() {
            this.newAnswerTemplate = _.template(NewAnswerTpl);
            $(this.$el).append(this.newAnswerTemplate({
                _userAnswerId   : this._userAnswerId,
                labels          : Labels,
                isAsked         : this.questionModel.get('isAsked'),
                mode            : 'create'
            }));
            
            if(this.questionModel.get('credits') > 0){
                $('.ask-private-wrapper_js', this.$el).hide();
            }
            
            $('.ui.checkbox', this.$el).checkbox();
            app.autoCompleteUsersAndKeywords($('#description-answer', this.$el));
            return this;
        },
                
        fillCollection: function(answersList) {
            var $this = this;
            _.each(answersList, function(answer) {
                var answerModel = new AnswerModel(answer);
                $this.collection.add(answerModel);
            });

            if (this.questionModel.get('answers') <= 5) {
                $('.loading-answers', this.$el).css('display', 'none');
            }
        },
                
        renderItem: function(model, collection, opt) {
            var item = new AnswerItemView({
                model               : model,
                _showUser           : true,
                _questionId         : this.questionModel.get('id'),
                _isAcceptAnswer     : this._isAcceptAnswer,
                questionModel       : this.questionModel.toJSON()
            });
            
            model.questionId = this.questionModel.get('id');
            
            if(opt && opt.newAnswer){
                $('ul.answers-list').prepend($(item.render().$el));
            }else{
                $(item.render().$el).insertBefore($('.new-answer-wrapper', this.$el));
            }
            this.setPagination();
        },
                
        loadMoreAnswers: function() {
            $('.loading-answers img').show();
            $('.loading-answers').css('color', '#ccc');
            $('.loading-answers span').html(Labels.loading_more_solutions);
            this.collection.increase().fetch({
                add: true,
                update: true,
                success: function() {
                    $('.loading-answers img').hide();
                    $('.loading-answers').css('color', '#0099cc');
                    $('.loading-answers').html(Labels.load_more_solutions);
                }
            });
        },
        
        setPagination: function() {
            if (this.collection.length < this.collection.filters.count) {
                $('.loading-answers', this.$el).show();
            } else {
                $('.loading-answers', this.$el).hide();
                $('.loading-answers').css('color', '#0099cc');
                if (this._userAnswerId && this._userAnswerId != ""){                    
                    $('.new-answer-wrapper', this.$el).hide();
                }
            }
        },
                
        removeErrors : function(){
            $('.show-errors-des').hide();
            $('.show-errors-focus').removeClass('show-errors-focus');
            $('.alert-error-qolve ', this.$el).css('display', 'none');
        },
                
        validate: function() {
            if ($('#description-answer').val() == "" && $('.new-uploaded-img li').length == 0) {
                $('.alert-error-qolve ', this.$el).css({
                    'display': 'block',
                    'visibility': 'visible'
                });

                $('textarea').addClass('show-errors-focus');
                $('.mentions-input-box').append('<span class="show-errors-des"></span>');
                $('.mentions-input-box .show-errors-des').show();
                return false;
            } else {
                this.removeErrors();
                return true;
            }
        },
                
        sendAnswer: function() {
            var description = $('#description-answer').val(),
                    $this = this;
            if (!this.validate())
                return;           
            
            var formAnswer = $('.new-answer-form').serializeJSON();
            
            _.each($('.new-uploaded-img li'), function(el) {
                var doc = $(el).data('upload');
                doc.order = $this.docs.length + 1;
                $this.docs.push(doc);
            });
            
            if( this.mode != 'edit'){
                var answerModel = new AnswerModel({
                    questionId  : this.questionModel.get('id'),
                    files       : this.docs
                });
            }else{
                answerModel           =  $('.send-answer', this.$el).data('answer'); 
                formAnswer.visibility = formAnswer.visibility ? formAnswer.visibility : 1;
                formAnswer.privacy    = formAnswer.privacy ? formAnswer.privacy : 1;
            }          
            
            $('#description-answer').val('');
            $('.new-uploaded-img').empty();
            $('#answer-private', this.$el).is(':checked') && $('#answer-private', this.$el).removeAttr('checked');
            $('#answer-anonymous', this.$el).is(':checked') && $('#answer-anonymous', this.$el).removeAttr('checked');
            
            answerModel.set(formAnswer);
            formAnswer.files         = this.docs;
            formAnswer.documentsList = this.docs;
           
            answerModel.save(formAnswer , {
                success: function(answer, resp) {
                    // we have just one answer for each user and each question                                        
                    $('.new-answer-form').hide();
                    answer.set(resp.answer, {silent : true});                    
                    if($this.mode != 'edit'){
                        $this.collection.add(answer,{
                            'newAnswer' : true
                        });
                    }               
                    
                    // After adding new answer we need some changes
                    $this.callee.changesAfterAddNewAnswer();                    
                    $('.new-answer-wrapper form', $this.$el).remove();   
                    $('.question-page .new-answer-wrapper').css('padding', '0px');
                }
            });           
            
        },
        
        removeUploadedDoc: function(e) {
            var el = $(e.target).hasClass('image-uploaded') ? $(e.target) : $(e.target).closest('.image-uploaded');
            el.remove();
        },
                
        previewUploadedDoc: function(e) {
            var el = $(e.target).hasClass('image-uploaded') ? $(e.target) : $(e.target).closest('.image-uploaded'),
                    upload = $(el).data('upload');
            var h = upload.height;
            var w = upload.width;
            upload.location = (upload.location[0] == '/') ? upload.location : '/' + upload.location;
            var size = $.fn.ResizeImg(h, w);
              $('.modal-preview').css({
                    'width'          : size.width,
                    'left'           : '50%',
                    'margin-left'  : -(size.width / 2)
              });
            $('.modal-preview #uploaded-preview-img').attr('src', upload.location + '?width=' + size.width + '&height=' + size.height);                         
            $('.modal-preview').modal('show');
        },
                
        editAnswer: function(e) {
            var el = $(e.target).hasClass('edit-answer') ? $(e.target) : $(e.target).closest('.edit-answer');
            
            var answerId    = el.data('id'),
                answerModel = this.collection.get(answerId),
                answerBox   = $(e.target).closest('.box-item');
        
            this.mode = 'edit';    
            this.newAnswerTemplate = _.template(NewAnswerTpl);
            $(answerBox).html(this.newAnswerTemplate({
                _userAnswerId   : '',
                labels          : Labels,
                mode            : 'edit' ,
                isAsked         : 0,
            }));         
            
            /* set visibility & privacy*/
            
            if(answerModel.get('visibility') == 2)
                $('#answer-anonymous', this.$el).trigger('click');
            
            if(answerModel.get('privacy') == 2)
                $('#answer-private', this.$el).trigger('click');
            
            if(this.questionModel.get('credits') > 0){
                $('.ask-private-wrapper_js', this.$el).hide();
            }
            
            $('.ui.checkbox', this.$el).checkbox();
            
            $('.send-answer', this.$el).data('answer',answerModel);
            
            
            
            app.autoCompleteUsersAndKeywords($('#description-answer', this.$el));
            this.addUploader();
            
            /* set description & uploade img*/
            
            $('#description-answer', this.$el).val(answerModel.get('description'));
            _.each(answerModel.get('documentsList'), function(doc) {
                var imgUploadedTpl = _.template(ImgUploadedTpl);
                $('.new-uploaded-img', this.$el).append(imgUploadedTpl({
                    location    : doc.location,
                    id          : doc.id,                   
                    mode        : 'edit'
                }));
                
                $('#' + doc.id + ' img').show();
                var size = $.fn.ResizeImg(doc.height, doc.width, '', 120);
                $('#' + doc.id + ' img').attr('src', doc.location + '?width=' +  size.width + '&height=' + size.height);              
                $('.new-uploaded-img .image-uploaded:last').data('upload', doc);                
            });
        },
        
        hideModal: function() {
           $(".modal", this.$el).modal('hide');                
        },
                
        afterAnswerRemove: function() {
            var answerSen = '',
                    answers = this.questionModel.get('answers') - 1;
            
            this.questionModel.set('answers', answers);
            this._userAnswerId = '';    
            this.collection.filters.set('offset', this.collection.filters.get('offset') - 1);
            this.collection.filters.count = Math.max(this.collection.filters.count - 1, 0);
            // if the question isReasked there is no need to render new form another time
            if (this.questionModel.get('isAsked') != 0)
                return;

            if (answers > 0) {
                answerSen = answers;
                if (answers == 1)
                    answerSen = answerSen + ' ' + Labels.Solution_answ;
                else
                    answerSen = answerSen + ' ' + Labels.Solutions_answ;
            } else {
                if (this.questionModel.get('askers') == 1) {
                    answerSen = '1 ' + Labels.person_is_waiting_for_solution;
                } else {
                    answerSen = this.questionModel.get('askers') + ' ' + Labels.people_are_waiting_for_solution;
                }
            }

            $('.question-page .answer-count-sen').html(answerSen);
            var tpl = _.template(NewAnswerTpl);
            $('.question-page .new-answer-wrapper').append(tpl({
                mode: 'addNewForm',
                labels: Labels,
                isAsked: 0,
                _userAnswerId: ''
            }));

            $('.question-page .answer-count-question .fuzzy-num').html(_.fuzzyNum(answers));
            $('.question-page .new-answer-wrapper').css({
                display: 'block',
                padding: '30px 0px 15px 0px'
            });

            if (this.questionModel.get('credits') > 0) {
                $('.ask-private-wrapper_js', this.$el).hide();
            }
                    
        }
    });

    return AnswersListView;
});
