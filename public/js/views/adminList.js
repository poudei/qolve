define([
    'jquery',
    'underscore',
    'backbone',   
    'base/view/listView',    
    'collections/admin',       
    'text!views/templates/adminList.html',    
    ], function ($, _, Backbone, BaseListView , AdminCollection, AdminTpl) {
        var AdminListView = BaseListView.extend({
            tagName    : 'ul',
            className  : 'admin-list',        
            events      : {},     
            initialize : function(){
                this.collection = new AdminCollection();
                this.loaded = true;
                this.collection.filters.set('recent', 1);
                
                this.collection.fetch({
                    add         : true,
                    silent      : true,
                    update      : true,
                    dataType    : "json",
                    type        : 'POST',
                    data        : JSON.stringify( this.collection.filters.toJSON())
                });
                
                 AdminListView.__super__.initialize.apply(this, []);
            },
            
            loadMore: function() {
                this.collection.increase().fetch({
                    add         : true,
                    silent      : true,
                    update      : true,
                    dataType    : "json",
                    type        : 'POST',
                    data        : JSON.stringify( this.collection.filters.toJSON())
                });
            },
            
            renderItem: function(model, index) {               
                this.template = _.template(AdminTpl);   
                var item = $(this.template(_.extend(model.toJSON(), {
                    index : index
                })));       
                
                this.$el.append(item);       
            }          
        });

        return AdminListView;
    });
