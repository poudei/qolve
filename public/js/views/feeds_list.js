define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',
    'collections/feeds',
    'views/feeds_list_item',
    'text!views/templates/reask.html',
    'text!views/templates/report.html',
    'text!views/templates/share.html',
    'text!views/templates/docs.html',
    'i18n!nls/labels',
    'semantic',
    'bootstrap-select',  
    'enscroll',
    'bxslider'
], function($, _, Backbone, App, BaseListView, FeedCollection, FeedItemView, ReaskTpl, ReportTpl, ShareTpl, DocsTpl, Labels, segment) {
    var FeedsListView = BaseListView.extend({
        events: {            
            'click   .reask-btn'                    : 'reaskQuestion',
            'click   .item-page-doc'                : 'showAllDocs',
            'click   .popover'                      : 'cancelList',
            'click   .report-btn'                   : 'reportQuestion',
            'click   .share-btn'                    : 'shareQuestion',
            'click   .cancel-btn'                   : 'hideModal',  
            'click   .nothanks-btn'                 : 'hideModal',
            'click   .social-share-wrapper span'    : 'selectShareSocial'
        },
                
        className : 'feeds-list',        
                
        initialize: function(opt) {                  
            _.bindAll(this, 'render', 'renderItem', 'updatePaging');
            var self = this;            
            
            require('app').updateCredits();     
            this.items  = [];
            this.loaded = opt.loaded;
            
            if (!this.loaded) {
                this.collection = new FeedCollection([], {
                    _userId: this.options._userId
                });
                
                this.collection.fetch({
                    add: true,
                    silent: true,
                    update: true
                });
            }else{
                if(self.collection.filters.count > 0){
                    $('.hold-on-loading').data('loading', false);
                    this.resetPagination();
                }
            }

            this.collection.bind('add', function(model, collection, opt) {
                self.renderItem(model, undefined, true);
                if (opt && opt.callback) {
                    opt.callback.call(self);
                }
            });
            
            this.collection.on('fetchSuccess', function(resp, collection) {                
                $('.hold-on-loading').css('display', 'block');
                $('.hold-on-loading').waypoint('destroy');
                $('.hold-on-loading').data('loading', false);     
                
                if (self.collection.filters.count > 0) {
                    $('.hold-on-loading').waypoint(function(event, direction) {                        
                        if (direction == 'down' && !$('.hold-on-loading').data('loading')) {
                            self.loadMore();
                            $('.hold-on-loading').data('loading', true);
                            $('.hold-on-loading').css('display', 'block');
                        } 
                    }, {
                        offset: function() {
                            return $.waypoints('viewportHeight') - $(this).outerHeight();
                        },
                        triggerOnce: false,
                        onlyOnScroll: true
                    });
                }                
                self.renderItems();               
            });
        },
                
        render: function() {
            this.template = _.template(ReaskTpl);
            this.$el.html(this.template({
                labels : Labels
            }));
            
            var reportTemplate = _.template(ReportTpl);
            this.$el.append(reportTemplate({
                labels : Labels
            }));
            
            var shareTemplate = _.template(ShareTpl);
            this.$el.append(shareTemplate({
                labels : Labels
            }));
            
            this.afterRender();           
            this.loaded && this.renderItems(0);  
            return this;
        },
                
        cancelList : function(e){            
            return false;
        },
        
        loadMore: function() {            
            if(this.collection.filters.count > 0){
                this.collection.filters.set('toId', this.collection.last().get('id'));               
            }
            this.collection.increase().fetch({              
                add: true,
                silent: true,
                update: true
            });
        },
        
         showAllDocs : function(e){    
            var docTemplate = _.template(DocsTpl),
                    el      = $(e.target).closest('.item-page-doc'),
                    docs    ,
                    model   ,
                    id = el.data('feedid'); 
                
                model = this.collection.get(id);
                docs  = model.get('question').documentsList;            
            
            $('.modal-documents .show-docs').html(docTemplate({
                docs        : docs,
                id          : model.get('question').id,
                description : model.get('question').description,
                share       : model.get('question').share, 
                keywords    : model.get('question').keywords,
                labels      : Labels
            }));
            
            if(this.bxSlider){
                this.bxSlider.destroySlider();
            }
            
            $('.modal-documents').modal('show');
            this.slider = $('.modal-documents .bxslider').bxSlider({
                mode : 'fade'
            });       
            
            $('.scrollbox3', this.$el).enscroll({
                showOnHover: false,
                verticalTrackClass: 'track3',
                verticalHandleClass: 'handle3'
            });
       },
                
        updatePaging: function() {    
            if (this.collection.filters.count <= 0) {
                $('.hold-on-loading span').html(Labels['no_more_items']);
                $('.hold-on-loading').css('display', 'none');
            } else {
                $('.hold-on-loading span').html(Labels['loading_more']);
                $('.hold-on-loading').css('display', 'block');
            }
            if (this.collection.models.length < 1) {
                $('.hold-on-loading').css('display', 'none');
                $('.hold-on-loading span').html(Labels['no_items']);
            }
        },
                
        afterRender: function() {
            $(".slider-reask", this.$el).slider({
                orientation: "vertical",
                min  : 0,
                step : 1,
                max  : Math.min(window.current_user.credit, 50),
                slide: function(event, ui) {
                    $('.slider-mount-shown').setToFixedWith(70);
                    if(ui.value == 0)
                        $('.slider-mount-shown').html(Labels['free']);
                    else
                        $('.slider-mount-shown').html(ui.value);
                    
                    $('.modal-reask').data('credit', ui.value);
                }
            });
            
             $('.ui.checkbox', this.$el).checkbox();
             $('.modal', this.$el).on('hide.bs.modal', function(){$('.modal').addClass('vclose')});
             $('.modal', this.$el).on('hidden.bs.modal', function(){$('.modal').removeClass('vclose')});                          
        },
        
        renderItem: function(model, index, prepend) {            
             this.options.actor && model.set({
                 actor :  this.options.actor
             }, {silent : true});
             
            var item = new FeedItemView({
                model       : model,
                _showUser   : this.options._showUser,
                lists       : require('app').getCurrentUserLists().toJSON()
            });
            
            this.items[model.cid] = item;
            if(prepend){
                $('ul.questions-list', this.$el).prepend(item.render().$el);
            }else{
                $('ul.questions-list', this.$el).append(item.render().$el);
            }
        },  
        
        afterRenderItems : function(){
            if(this.collection.length == 0 && !this.options._userId ){
                $(this.$el).html('<div class="no-questions"> <span style="font-weight:bold">' + Labels['you_just_arrived'] + '</span><br> <br>' +
                '<span style="font-weight:bold;font-size:16px;"></span>' + 
                Labels['where_the_magic_happens'] +
                '<a href="/newQuestion">' + 
                Labels['asking_ur_problem'] + ' </a>' +
                Labels['kick_out'] + 
                '<br>' +     
                Labels['Start'] + ' ' +
                Labels['asking'] + ' ' +
                Labels['and_get_rid_of_this_message_ASAP'] + '</div>');
            }else{
                if(this.collection.length == 0){                  
                      $(this.$el).html('<div class="no-questions">' + Labels['user_has_no_activities_yet'] + '</div>');
                }
            }
        },        
                
        reaskQuestion: function() {    
            var questionId      = $('.modal-reask').data('questionid'),
                    question    = this.collection.where({questionId : questionId})[0],
                    credit      = $('.modal-reask').data('credit'),
                    visibility  = $('.modal-reask .question-reask-visibility').is(':checked') ? 2 : 1;
            this.hideModal();
            question.reaskQuestion(credit, visibility);
        },
        
        reportQuestion: function() { 
            var reason          = $('.modal-report input:checked').val();
            var questionId      = $('.modal-report').data('itemid'),
                    question    = this.collection.where({'questionId' : questionId})[0];                    
            this.hideModal();
            question.reportQuestion(reason);
        },
                
       shareQuestion: function() {    
            var questionId      = $('.modal-share').data('itemid'),
                    question    = this.collection.where({'questionId' : questionId})[0];                    
            this.hideModal();
            question.shareQuestionUrl();
       },
       
       selectShareSocial : function(e){
            var socialType = $(e.target).data('share');
            $('.share-' + socialType).toggleClass('active');
       },        
               
       hideModal: function() {
           $(".modal", this.$el).modal('hide');                
       }
    });

    return FeedsListView;
});
