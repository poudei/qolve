define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',
    'base/view/itemView',
    'text!views/templates/question_item.html',
    'text!views/templates/vote.html',    
    'i18n!nls/labels',
], function($, jqueryUi, _, Backbone, ItemView, questionTpl, VoteTpl, Labels) {
    var QuestionListItemView = ItemView.extend({
        tagName: 'li',
        className: 'box-item',
        events: {
            'click   .reask-question'             : 'reaskQuestion',
            'click   .btn-vote'                   : 'toggleVote',
            'click   .show-comments-list'         : 'showComments',
            'click   .report-question'            : 'report',
            'click   .more-items'                 : 'showMoreMenu',
            'click   .more-items-menu'            : 'clickOnMenu',            
            'click   .bookmark-question'          : 'showBookmarkPopover',
            'click   .add-to-list'                : 'addToList',  
            'click   .bookmark-list-select'       : 'toggleBookmarkList' ,
            'keyup   .add-list-bookmark'          : 'addNewList',
            'click   .add-list-btn'               : 'addNewList',   
            'click   .cancel-btn-list'            : 'cancelList',
            'click   .share-question-url'         : 'shareQuestionUrl',  
            'click   .edit-question'              : 'editQuestion',
            'click   .unfollow-follow-user-sh'    : 'followShareUser',
            'click   .share-people'               : 'showShareMenu',  
            'click   .delete-question'            : 'deleteQuestion'  
        },
        
        initialize: function() {
            _.bindAll(this, 'render', 'renderVoteTpl');                  
            this.type = 'questions';
        },                
      
        render: function() {
            this.template = _.template(questionTpl);
            var model = this.model.toJSON();

            if (model.asker == undefined)
                model.asker = this.options.asker;

            var obj = _.extend(this.model.toJSON(), {
                _showUser : this.options._showUser,
                page      : this.options.page ? this.options.page : 'questions',
                lists     : this.options['list'],
                labels    : Labels
            });
            this.$el.html(this.template(obj));
            $('.item-page-doc', this.$el).data('questionid', this.model.get('id'));
            this.afterRender();
            return this;
        },
                
        setLists : function(){
            $('.bookmark-list-select').selectpicker();
            $('.ui.checkbox').checkbox();
            $('.bookmark-question-wrapper').data('id', this.model.get('id'));
        },
                
        afterRender : function(){
            $('.bookmark-question-wrapper', this.$el).popover({
                placement   : 'left',
                html        : true,
                container   : '.bookmark-question-wrapper'
            });               
            $('[data-toggle=tooltip]').tooltip();
            
            $('.copy-link-clipboard', this.$el).zclip({
                path : '/js/libs/ZeroClipboard.swf',
                copy : function(){ return $('.copy-link-clipboard', this.$el).data('link');}
            });
        },
        
        editQuestion : function(){
            Backbone.history.navigate('/editQuestion/' + this.model.get('id') , true);
        },
        
        deleteQuestion : function(){            
            var $this = this;
            this.model.destroy({
                success: function(model, resp) {
                    Backbone.history.navigate('home', true);
                }
            });            
        },
                
        renderVoteTpl: function() {
            var self = this,
                    url,
                    type,                    
                    votes,
                    isVoted,
                    score;  

            switch (this.model.get('isVoted')) {
                case 0:
                    url = '/api/questions/' + this.model.get('id') + '/votes';
                    type = 'POST';
                    isVoted = 1;                    
                    votes = this.model.get('votes') + 1;
                    break;
                case 1:
                    url = '/api/questions/' + this.model.get('id') + '/votes/delete';
                    type = 'DELETE';
                    isVoted = 0;
                    votes = this.model.get('votes') - 1;
                    break;                
            }

            this.model.set({
                'isVoted' : isVoted,
                'votes'   : votes
            });
            
            this.model.toggleVote({
                score       : score ,
                url         : url,
                type        : type
            });

            var voteTemplate = _.template(VoteTpl);
            (isVoted == 1) ? $('.votes-question-wrapper', this.$el).addClass('voted-active') : $('.votes-question-wrapper', this.$el).removeClass('voted-active');
            $('.votes-question-wrapper', this.$el).html(voteTemplate({
                'isVoted'   : isVoted,
                'votes'     : votes,
                'type'      : 'question',
                'labels'    : Labels
            }));
        }

    });

    return QuestionListItemView;
});
