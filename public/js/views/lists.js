define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',
    'models/list',
    'collections/lists',
    'views/list_item',    
    'text!views/templates/list.html',  
    'i18n!nls/labels',
    'semantic'
], function($, _, Backbone, App, BaseListView, ListModel, ListCollection, ListItemView, ListTpl, Labels) {
    var ListView = BaseListView.extend({
        events: {       
            'keyup #add-new-list'               : 'addNewList',
            'click .add-btn'                    : 'addNewList',    
            'click .back-btn-list .all-list'    : 'backToList'        
        },
                
        className : 'lists',              
                
        initialize: function(opt) {
            var $this = this;
            var opt = opt || {};
            
            this.loaded = opt.loaded;
            
            if(!this.loaded){
                this.collection = new ListCollection([], {
                    _userId : opt._userId
                });  
            }                
            
            this._userId = (opt._userId) ? opt._userId : window.current_user.id;
            
            if(this._userId){
               $(this.$el).css('min-height', ($(window).height() - 90));               
            }            
            
            this.collection.on('fetchSuccess', function(resp, collection) {
                $this.renderItems();               
            });      
            
            this.collection.on('add', function(model, collection) {
                $this.renderItem(model, true);               
            }); 
            
            if (!this.loaded) {
                this.collection.fetch({
                    add: true,
                    silent: true,
                    update: true
                });
            }
        },
                
        render: function() {
            this.template = _.template(ListTpl);               
            this.$el.html(this.template({
                _userId : this._userId,
                labels  : Labels
            })); 
            
            if(this.loaded){               
               this.renderItems();                                         
            }   
            return this;
        },          
        
        renderItem: function(model, prepend) {   
            $('.list-wrapper', this.$el).show();
            this.listItem = new ListItemView({
                model       : model               
            });
            
            if(model.get('userId') == this._userId || model.get('id') == 'Uncategorized'){
                if(prepend){
                    $('.user-lists', this.$el).prepend(this.listItem.render().$el);
                }else{
                    $('.user-lists', this.$el).append(this.listItem.render().$el);
                }                
            }else{                
                  $('.followed-lists', this.$el).append(this.listItem.render().$el);                
            }
                
        },
                
        afterRenderItems : function(){
            if( $('.followed-lists li', this.$el).length == 0){
                 $('.followed-lists-title', this.$el).hide();
            }else{
                 $('.followed-lists-title', this.$el).show();
            }

            if( $('.user-lists li', this.$el).length == 0){
                 $('.my-list-title', this.$el).hide();   
            }else{
                $('.my-list-title', this.$el).show();   
            }                
             
            $('.ui.checkbox', this.$el)
              .checkbox()
            ;           
           
            if(this.collection.length == 0){                
              $(this.$el).html('<div class="no-user">' + Labels['no_Created_and_following_list'] + '</div>');                
            }        
        },
                
        addNewList : function(e){
            var $this = this;
            if ((e.type == 'keyup' && e.which == 13) || e.type == 'click') {    
                new ListModel().save({
                    name    : $('#add-new-list').val(),
                    privacy : $('.list-privacy').is(':checked') ? 1 : 2
                }, {
                    success : function(model){       
                        $this.collection.add(model);
                        $('.my-list-title', $this.$el).show();   
                    }
                });                
                
                $('#add-new-list').val('');
                $('.list-privacy').is('checked') && ('.list-privacy').trigger('click');
            }            
        },
                
       backToList : function(){      
            $('.lists').toggleClass('no-background');            
            $('.list-item-questions #questions-list-items').empty();
            $('.list-page').show();
            $('.list-item-questions').hide();
       }
    });

    return ListView;
});
