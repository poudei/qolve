define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',    
    'collections/comments',   
    'models/comment',  
    'text!views/templates/comment_item.html',
    'text!views/templates/commentList.html',
    'text!views/templates/new_comment.html',
    'i18n!nls/labels',
    ], function ($, _, Backbone, App, BaseListView , CommentCollection, CommentModel, CommentTpl, CommentListTpl, NewCommentTpl, Labels) {
        var CommentsListView = BaseListView.extend({
            tagName    : 'ul',
            className  : 'comments-list',        
            events : {
                'click .remain-comments'        : 'loadMoreComments',
                'click .add-new-comment span'   : 'showAddNewComment',
                'keyup .add-comment-text'       : 'sendNewComment',
                'click .del-icon-comment'       : 'delComment'
            },        
                    
            initialize : function(commentsList, opt){                
                _.bindAll(this, 'render', 'renderItem', 'updateRemainCount');
                this.opt = opt;
                
                commentsList = commentsList || {};
                commentsList.count =  commentsList.length ? commentsList.length : 0;
                
                this.collection = new CommentCollection([], {
                    type        : opt.type,
                    answerId    : opt.id,
                    questionId  : opt.questionId || opt.id
                });                  
                this.collection.on('fetchSuccess', this.updateRemainCount);                
                this.collection.on('add', this.renderItem);    
                window.cc = this.collection.filters;
                this.collection.filters.set({                    
                    'offset'    : commentsList && commentsList ? commentsList.length : 0,
                    'limit'     : 5
                });
                
                this.collection.count =  this.opt.callee.model.get('comments');            
                this.render(this.collection.count - commentsList.length, this.collection.count > commentsList.length);                
                
                commentsList && commentsList.count > 0 && this.fillCollection(commentsList);
            }, 
                    
            fillCollection : function(commentsList){
                var $this = this;  
                for(var i = commentsList.length - 1; i >= 0; i--){                                        
                    var commentModel = new CommentModel(commentsList[i]);
                    $this.collection.add(commentModel);
                }                
            },        
             
            updateRemainCount : function(){
                $('.loading-img', this.$el).hide();
                var count = this.collection.count - this.collection.models.length;
                if(count > 0){
                    $('.remain-comments span', this.$el).html(this.collection.count - this.collection.models.length);
                }else{
                    $('.remain-comments', this.$el).css('display', 'none');
                }                
                this.collection.filters.set('offset', this.collection.filters.get('offset') + this.collection.filters.get('limit'));
            },
            
            showAddNewComment : function(e){            
                if(window.current_user && window.current_user.id){
                    this.newCommentTpl = _.template(NewCommentTpl);
                    $('.add-new-comment', this.$el).html(this.newCommentTpl({
                        labels : Labels,
                        user   : window.current_user
                    }));
                }
            },
                    
            sendNewComment : function(e){
                if (e.type == 'keyup' && e.keyCode == 13) {
                    var $this = this,
                            content = $('.add-comment-text', this.$el).val();
                    $('.add-comment-text', this.$el).val('');
                    var newComment = new CommentModel();

                    if (this.opt['type'] == 'questions') {
                        newComment.questionId = this.opt['id'];
                    } else {
                        newComment.answerId = this.opt['id'];
                        newComment.questionId = this.opt['questionId'];
                    }

                    newComment.type = this.opt['type'];

                    newComment.save({
                        content: content
                    }, {
                        success: function(resp) {
                            newComment.newItem = true;
                            $this.collection.add(newComment);
                            $this.collection.count++;                          
                        }
                    });
                }
            },        
                    
            loadMoreComments : function(){
                $('.loading-img', this.$el).show();
                
                this.collection.fetch({
                    add         : true,
                    update      : true,
                    remove      : false
                });
            },
                    
            render : function(count, display){                       
                // invisible answers comments is shown but can't add New comment                
                if(this.opt['type'] == 'answers' && this.opt['callee'].model.get('invisible') == 1){
                    var notNewComment = true;
                    
                    if(this.collection.count == 0){
                        this.$el.append('<li class="no-comment-yet"><span>No comments yet</span></li>')
                        
                    }
                }
                    
                this.template = _.template(CommentListTpl);             
                this.$el.append(this.template({
                    remainCount   : count,
                    notNewComment : notNewComment,
                    labels        : Labels
                }));
                
                !display && $('.remain-comments', this.$el).css('display', 'none');
            },
            
            renderItem: function(model) {               
                this.template = _.template(CommentTpl);   
                var el = $(this.template(_.extend(model.toJSON(), {
                    labels : Labels
                })));
                el.data('id', model.get('id'));
                
                if(model.newItem){
                   el.insertBefore($('.add-new-comment', this.$el));
                }else{
                   el.insertAfter($('.remain-comments', this.$el));
                }
                
                if (this.opt['type'] == 'questions') {
                    model.questionId = this.opt['id'];
                } else {
                    model.answerId = this.opt['id'];
                    model.questionId = this.opt['questionId'];
                }
                
                model.type = this.opt['type'];                
            },
                    
            delComment : function(e){
                var el    = $(e.target).closest('.box-item'),
                    id    = el.data(),
                    $this = this;
            
                this.collection.get(id).destroy({
                    success : function(){
                        $this.collection.count --;
                        $this.collection.filters.set('offset', $this.collection.filters.get('offset') - 1);                    
                        $this.collection.count == 0 && $('.remain-comments', $this.$el.closest('.box-item')).hide();
                        el.remove();
                    }
                });
            }
        });

        return CommentsListView;
    });
