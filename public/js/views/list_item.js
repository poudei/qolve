define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',        
    'base/view/itemView',        
    'text!views/templates/list_item.html', 
    'views/questions_list',
    'i18n!nls/labels',
    ], function ($, jqueryUi , _, Backbone, ItemView, listItemTpl, QuestionListView, Labels) {
        var ListItemView = ItemView.extend({
            tagName   : 'li',
            className : 'list-item',
            events : {
               'click'                      : 'showAllQuestions',
               'click .del-icon-list'       : 'delListItem',
               'click .follow-list-item'    : 'toggleFollow'    
            },

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },

            render    : function() {     
                this.template = _.template(listItemTpl);
                var model = this.model.toJSON();            
                var obj = _.extend(this.model.toJSON(), {
                    labels : Labels
                });
                this.$el.html(this.template(obj));                                
                return this;
            },
                    
            delListItem : function(e){
                this.model.destroy();
                this.$el.remove();
                return false;
            },
                    
            toggleFollow : function(){
                this.model.toggleFollow();
                return false;
            },
                    
            showAllQuestions : function(){                                
                if(parseInt(this.model.get('questionsList').length) <= 0)
                    return ;
                $('.show-list-name').html(this.model.get('name'));
                $('#main-container').removeClass('main-container-custom-design');                
                $('.lists').toggleClass('no-background');                
               
                this.questionList = new QuestionListView({                    
                    _showUser       : true,
                    _filters        :{
                        limit   : 1000
                    },    
                    question_url    : '/api/lists/' + this.model.get('id') +'/questions'
                });
                $('.list-page').hide();
                $('.list-item-questions').show();                
                $('.list-item-questions #questions-list-items').append(this.questionList.$el);
            }
                    
            
        });

        return ListItemView;
    });
