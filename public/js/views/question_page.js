define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'models/question',    
    'views/answers_list',
    'views/questions_list_item',    
    'bxslider',
    'enscroll',
    'text!views/templates/question_page.html',
    'text!views/templates/reask.html',
    'text!views/templates/report.html',
    'text!views/templates/share.html',
    'text!views/templates/docs.html',
    'i18n!nls/labels',
    'jquery-helper',
    'semantic',
], function($, Backbone, _, App, QuestionModel, AnswersList, QuestionItem, Bxslider, Enscroll, QuestionPageTmp, ReaskTpl, ReportTpl, ShareTpl ,DocsTpl, Labels) {

    var QuestionPageView = Backbone.View.extend({
        events: {
            'click   .item-page-doc'                : 'showAllDocs',
            'click   .reask-btn'                    : 'reaskQuestion',            
            'click   .report-btn'                   : 'reportQuestion',
            'click   .cancel-btn'                   : 'hideModal',
            'click   .nothanks-btn'                 : 'hideModal',
            'click   .share-btn'                    : 'shareQuestion',
            'click   .share-question-url'           : 'shareQuestionUrl',  
            'click   .social-share-wrapper span'    : 'selectShareSocial'
        },                
                
        className: 'question-page questions-list',
        
        initialize: function(opt) {
            var $this = this;
            this.template = _.template(QuestionPageTmp);
            this.model = new QuestionModel({
                id      : opt.id                
            });
            
            app = require('app'); 
            app.updateCredits();            
            app.getUserFollowers();
            app.getKeywords();
            
            this.model.fetch({
                success: function() {
                    $this.render();
                    $this.processScroll();
                    $this.renderQuestion();
                    $this.renderAnswers();                   
                }
            });                  
        },
        
        render: function() {                 
            this.$el.html(this.template(_.extend(this.model.toJSON(),{
                labels : Labels
            })));              
            
            var modalTpl = _.template(ReaskTpl);
            this.$el.append(modalTpl({
                labels : Labels
            }));
            
            var modalTpl = _.template(ReportTpl);
            this.$el.append(modalTpl({
                labels : Labels
            }));
            
            var reportTemplate = _.template(ShareTpl);
            this.$el.append(reportTemplate({
                labels : Labels
            }));
            
            this.afterRender();
            return this;
        },
        
        afterRender : function(){        
            $(".slider-reask", this.$el).slider({
                orientation: "vertical",
                min  : 0,
                step : 1,
                max  : Math.min(window.current_user.credit, 50),
                slide: function(event, ui) {
                    $('.slider-mount-shown').setToFixedWith(70);
                    if(ui.value == 0)
                        $('.slider-mount-shown').html(Labels['free']);
                    else
                        $('.slider-mount-shown').html(ui.value);
                    $('.modal-reask').data('credit', ui.value);
                }
            });
            
            $('.ui.checkbox', this.$el).checkbox();             
            $('.modal', this.$el).on('hide.bs.modal', function(){$('.modal').addClass('vclose')});
            $('.modal', this.$el).on('hidden.bs.modal', function(){$('.modal').removeClass('vclose')});           
        },
                
        renderQuestion : function(){
            this.questionItem && this.questionItem.remove();
            this.questionItem = new QuestionItem({
                    page        : 'questionPage',
                   _showUser    : true,
                    model       : this.model,
                    list        : require('app').getCurrentUserLists().toJSON(),
                    labels      : Labels
            });                   
           
            $('.question-page-item', this.$el).html(this.questionItem.render().$el);            
            $('[data-toggle=tooltip]', this.$el).tooltip();
        },
                
        renderAnswers : function(){    
            this.answersList  && this.answersList.remove();
            this.answersList = new AnswersList({
               questionModel : this.model,
               callee        : this   
            });
            
            $('.answers-list', this.$el).html(this.answersList.$el); 
            
            if(!this.model.get('userAnswerId') || (this.model.get('userAnswerId') && this.model.get('userAnswerId') == ""))
                if(window.current_user && window.current_user.id){
                    this.answersList.onAppend();
                }
        },       
        
        processScroll: function() {
            var $win = $(window)
                    , $nav = $('#primary-nav')
                    , navTop = $('.social-btn-wrapper').offset().top
                    , isFixed = 1
                    , isAddH = false;
            
            if($win.width() < 860)
                return;
            
            socialPosition();
            $win.unbind('scroll', socialPosition);
            $win.bind('scroll', socialPosition);

            function socialPosition() {
                var i, scrollTop = $win.scrollTop();

                if (scrollTop >= navTop && !isFixed) {
                    isFixed = 1;
                    var rightPosition = (($win.width() - 800) / 2) - 54;
                    $('.social-btn-wrapper').addClass('fixedPosition');
                    $('.social-btn-wrapper').css('right', rightPosition);
                    $('.social-btn-wrapper').css('top', '50px');
                }
                else if (scrollTop <= navTop && isFixed) {
                    isFixed = 0;
                    $('.social-btn-wrapper').removeClass('fixedPosition');
                    $('.social-btn-wrapper').css('right', '-54px');
                    $('.social-btn-wrapper').css('top', '0px');
                }
            }
        },
                
        reaskQuestion: function() {
            var questionId     = $('.modal-reask').data('questionid'),                    
                    credit     = $('.modal-reask').data('credit'),
                    visibility  = $('.modal-reask .question-reask-visibility').is(':checked') ? 2 : 1;
            this.hideModal();
            this.model.reaskQuestion(credit, visibility);
        },      
        
        shareQuestionUrl : function(e){
            var item = this.model.get('question') ? this.model.get('question') : this.model.toJSON();                        
            
            var shareSocial = $(e.target).data('share');
            $('.modal-share .social-share-wrapper span').removeClass('active');
            $('.modal-share .share-' + shareSocial).addClass('active');
            
            $('.modal-share').modal('show');
            $('.modal-share').data('itemid', item.id);            
        },
                
        shareQuestion: function() {    
            var questionId      = $('.modal-share').data('itemid'),
                    question    = this.model;
            this.hideModal();
            question.shareQuestionUrl();
        },
        
        selectShareSocial : function(e){
            var socialType = $(e.target).data('share');
            $('.share-' + socialType).toggleClass('active');
        },        
          
        showAllDocs : function(e){    
            var docTemplate = _.template(DocsTpl),
                    el      = $(e.target).closest('.item-page-doc'),
                    docs    ,
                    model   ;
            
            if(el.data('type') == 'answer'){
                var id = el.data('answerid');
                id = id ? id.split('_')[1] : '';                
                
                model = this.answersList.collection.where({'id' : id})[0];
                docs  = model.get('documentsList');
            }else{
                model = this.model;
                docs  = this.model.get('documentsList');
            }
            $('.modal-documents .show-docs').html(docTemplate({
                docs        : model.get('documentsList'),
                description : model.get('description'),
                share       : model.get('share'), 
                keywords    : model.get('keywords'),
                id          : ''
            }));
            
            $('.modal-documents').modal('show');
            
            if(this.bxSlider){
               this.bxSlider.destroySlider();
            }
            
            this.bxSlider = $('.modal-documents .bxslider').bxSlider({
                mode : 'fade'
            });       
            
            $('.scrollbox3', this.$el).enscroll({
                showOnHover: false,
                verticalTrackClass: 'track3',
                verticalHandleClass: 'handle3'
            });
       },
       
       reportQuestion: function(){    
            var itemId   = $('.modal-report').data('itemid');
             var reason  = $('.modal-report input:checked').val();
         
            if(itemId != this.model.get('id')){     
                var item = this.answersList.collection.where({'id' : itemId})[0];                    
                item.reportAnswer(reason);
            }else{                
                this.model.reportQuestion(reason);
            }            
            this.hideModal();            
        },
        
        changesAfterAddNewAnswer : function(){        
            this.model.set('answers', this.answersList.collection.length);
            var answers = _.fuzzyNum(this.model.get('answers'));            
                    
            if (this.answersList.collection.length == 1)
                $('.answer-count', this.$el).html(answers + ' ' + Labels['answer']);
            else
                $('.answer-count', this.$el).html(answers + ' ' + Labels['answers']);
            $('.answer-count-question .fuzzy-num', this.$el).html(answers);
            $('.edit-question', this.$el).remove();
            $('.delete-question', this.$el).remove();            
        },
        
        hideModal: function() {
           $(".modal", this.$el).modal('hide');                
        }
    });

    return QuestionPageView;
});
