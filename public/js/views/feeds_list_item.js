define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',        
    'base/view/itemView',        
    'text!views/templates/timeline.html',       
    'text!views/templates/vote.html',
    'models/list',
    'i18n!nls/labels',
    ], function ($, jqueryUi , _, Backbone, ItemView, TimelineTpl, VoteTpl, ListModel, Labels) {
        var FeedListItemView = ItemView.extend({
            tagName   : 'li',
            className : 'box-item',
            events : {
               'click   .reask-question'             : 'reaskQuestion',
               'click   .btn-vote'                   : 'toggleVote',               
               'click   .report-question'            : 'report',             
               'click   .more-items'                 : 'showMoreMenu',
               'click   .more-items-menu'            : 'clickOnMenu',
               'click   .bookmark-question'          : 'showBookmarkPopover',
               'click   .add-to-list'                : 'addToList',  
               'click   .bookmark-list-select'       : 'toggleBookmarkList' ,
               'keyup   .add-list-bookmark'          : 'addNewList',
               'click   .share-question-url'         : 'shareQuestionUrl',  
               'click   .add-list-btn'               : 'addNewList',   
               'click   .cancel-btn-list'            : 'cancelList',
               'click   .unfollow-follow-user-sh'    : 'followShareUser',
               'click   .share-people'               : 'showShareMenu' 
            },

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },                  

            render    : function() {     
                this.template = _.template(TimelineTpl);
                var model = this.model.toJSON();
                
                if(model.asker == undefined) model.asker = this.options.asker;               
                var obj = _.extend(this.model.toJSON(), {
                    _showUser : this.options._showUser,
                    page      : this.options.page ? this.options.page : 'questions' ,
                    lists     : this.options['list'],
                    labels    : Labels  
                });
                this.$el.html(this.template(obj));   
                $('.item-page-doc', this.$el).data('feedid', this.model.get('id'))
                this.afterRender();
                return this;
            },       
                    
            renderVoteTpl: function() {
                var self = this,
                        url,
                        type,                     
                        votes,
                        isVoted;

                switch (this.model.get('question').isVoted) {
                    case 0:
                        url = '/api/questions/' + this.model.get('question').id + '/votes';
                        type = 'POST';
                        isVoted = 1;
                        votes = this.model.get('question').votes + 1;
                        break;
                    case 1:
                        url = '/api/questions/' + this.model.get('question').id + '/votes/delete';
                        type = 'DELETE';
                        isVoted = 0;
                        votes = this.model.get('question').votes - 1;
                        break;                   
                }

                var question     = this.model.get('question');
                question.isVoted = isVoted;
                question.votes   = votes;
                this.model.set('question', question);            

                this.model.toggleVote({                 
                    url     : url,
                    type    : type
                });

                var voteTemplate = _.template(VoteTpl);
                (isVoted == 1) ? $('.votes-question-wrapper', this.$el).addClass('voted-active') : $('.votes-question-wrapper', this.$el).removeClass('voted-active');
                $('.votes-question-wrapper', this.$el).html(voteTemplate({
                    'isVoted'   : isVoted,
                    'votes'     : votes,
                    'type'      : 'question', 
                    'page'      : 'timeline',
                    'labels'    : Labels
                }));
            }
        });

        return FeedListItemView;
    });
