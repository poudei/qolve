define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'jquery-helper',
    'text!views/templates/layout.html',    
    'text!views/templates/footer.html', 
    'text!views/templates/toolbar.html', 
    'i18n!nls/labels',
    'semantic',
    'jquery-ui'
    ], function ($, Backbone, _, App, jqHelper, LayoutTpl, FooterTpl, ToolbarTpl, Labels) {

        var layoutView = Backbone.View.extend({
            className           : 'layout',
                    
            events : {                
                'click  .search-toolbar' : 'showTrendingMenu',
                'keyup  .search-toolbar' : 'gotToExplorePage',
                'click  .search-toolbar' : 'getTrendingTags',                
                'click  .feedback'       : 'showFeedbackModal',
                'click  .send-feedback'  : 'sendFeedBack'
            },            

            render : function() {
                var layoutTpl  = _.template(LayoutTpl),
                    toolbarTpl = _.template(ToolbarTpl),
                    footerTpl  = _.template(FooterTpl);
                
                this.$el.html(layoutTpl({
                    labels : Labels
                }));                   
                $('.navbar-fixed-top', this.$el).html(toolbarTpl({
                    user    : window.current_user,
                    labels  : Labels
                }));
                
                $('.footer-wrapper', this.$el).html(footerTpl({
                    labels  : Labels
                }));
                
                this.setAutoCompleteSearch();
                this.afterRender();
                return this;
            },             
                    
            showFeedbackModal : function(){                
                $('.modal-feedback', this.$el).modal('show');
            },   
            
            afterRender : function(){   
                var $this = this;
                
                $('.modal-taturial', this.$el).on('hide.bs.modal', function(){$('.modal').addClass('vclose')});
                $('.modal-taturial', this.$el).on('hidden.bs.modal', function(){$('.modal').removeClass('vclose')});  
                $('.modal-feedback .ui.checkbox', this.$el).checkbox();
                
                $('.modal-taturial', this.$el).on('shown.bs.modal', function(){                 
                    if($this.bxSlider){
                        $this.bxSlider.destroySlider();
                    }
                    
                    $this.bxSlider = $('.modal-taturial .bxslider').bxSlider({
                        speed : 5,
                        mode : 'fade'
                    });                  
                });      
                
                $('.search-toolbar', this.$el).bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){ 
                    var w = $('.search-toolbar').outerWidth() - 6;
                    $($this.autoComSearchToolbar).css('width', w + 'px');
                    
                    if(w == '230')
                        $($this.autoComSearchToolbar).show();
                });
                $('.modal-feedback', this.$el).on('hide.bs.modal', function(){$('.modal').addClass('vclose')});
                $('.modal-feedback', this.$el).on('hidden.bs.modal', function(){$('.modal').removeClass('vclose')}); 
            }, 
            
            sendFeedBack: function() {
                var formFeed = $('form.feedback-form', this.$el).serializeJSON();              
                $.ajax({
                    url: '/api/feedbacks',
                    dataType: 'json',
                    type: 'POST',
                    data: formFeed                    
                });
                $('.modal-feedback .close').trigger('click');
                $('.modal-feedback textarea', this.$el).val('');
                
            },
            
            getTrendingTags : function(e){
                var $this = this;
                
                if(this.trendingTags){
                    $('.search-toolbar', $this.$el).autocomplete("search", "");
                    e.preventDefault();                  
                    return false;
                }
                
                $.ajax({
                    url         : "/api/explores",
                    dataType    : 'json',                    
                    success: function(data){
                        $this.trendingTags = data.trendingKeywords;                                                
                        $('.search-toolbar', $this.$el).autocomplete("search", "");
                    }
                });              
                
                $('.search-toolbar', this.$el).addClass('search-toolbar-focus');
            },
            
            setAutoCompleteSearch: function() {
                var $this = this;
                
                $(".search-toolbar", this.$el).autocomplete({
                    selectFirst: true,        
                    open   : function(event, ui){
                          var widget = $(this).data('ui-autocomplete'),
                            menu   = widget.menu,
                            $ul    = menu.element,
                            id     = $ul.attr('id'),
                            w      = $('.search-toolbar').outerWidth() - 6;
                            
                          $('#' + id).css('width', w + 'px'); 
                          
                          $this.autoComSearchToolbar = '#' + id;
                          if($('.search-toolbar').val() == ''){
                            $('#' + id).append('<li class="ui-menu-item" role="presentation"><a href="/explore">Advanced explore</a></li>');  
                          }else{
                            $('#' + id).append('<li class="ui-menu-item" role="presentation"><a href="/explore/' + $('.search-toolbar').val() + '">Advanced explore</a></li>');  
                          }                   
                          
                          if(w != '230')
                            $($this.autoComSearchToolbar).hide();
                    },
                    source : function(request, response) {
                        var s = [];                         
                        if($('.search-toolbar', this.$el).val() == ''){                            
                            for (var i in $this.trendingTags) {
                                    var cat = {
                                        label: $this.trendingTags[i].name,
                                        value: $this.trendingTags[i].name
                                    }
                                    s.push(cat);
                            }
                            response(s, request.term);
                            return;
                        }
                        
                        if($('.search-toolbar', this.$el).val().length < 3)
                            return;                       
                       
                        $.ajax({
                            url         : "/api/keywords/" + $('.search-toolbar', this.$el).val(),
                            dataType    : 'json',                    
                            success: function(data) {
                                var data = data.keyword;
                                for (var i in data) {
                                    var cat = {
                                        label: data[i].name,
                                        value: data[i].name
                                    }
                                    s.push(cat);
                                }
                                response(s, request.term);
                            }
                        });

                    },
                    minLength: 0                    
                });                
            },            
            
            gotToExplorePage : function(e){
                if (e.which == 13) {
                    var val = /^#/.test($(e.target).val()) ? $(e.target).val().split('#')[1] : $(e.target).val();
                    Backbone.history.navigate('/explore/' + val, true);
                }
            }
        });

        return layoutView;
    });
