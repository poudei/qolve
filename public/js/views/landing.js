define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'jquery-helper',
    'text!views/templates/login.html',
    'text!views/templates/landing.html',
    'i18n!nls/labels',
    'floatlabels',
    'semantic',
    'jquery-ui',
    'bootstrap-select',    
    'timezone',
    'bxslider',
    ], function ($, Backbone, _, App, jqHelper, LoginTmp, LandingTmp, Labels) {

        var landingView = Backbone.View.extend({
            className           : 'page-landing',
                    
            initialize : function() {
                var $this = this;
                this.template = _.template(LandingTmp);
                $('#login-form').on('click', function(){
                    $this.showModal();
                });
                App = require('app');
            },

            events : {
                'click   #login-form'         : 'showModal',
                'click   .login-qolve'        : 'login',
                'keyup   #username'           : 'login',
                'keyup   #password'           : 'login',  
                'keydown .age'                : 'checkAge',  
                'click   .send-invitation'    : 'sendInvitation',
                'focus   input'               : 'removeError'
            },

            render : function() {
                this.$el.html(this.template({
                      labels : Labels
                }));             
                
                $('body').addClass('body-background');
                return this;
            },
            
            onAppend : function(){                
                $('.content input:not(#keys-required)').floatlabel({
                    labelStartTop   : '2px' ,
                    labelClass      : 'floating-label'
                });
                
                $('.ui.checkbox', this.$el).checkbox();                
                $('.device-type-wrapper select', this.$el).selectpicker({width : '150'});                                             

                $('.landing-layout .bxslider').bxSlider({
                    mode            : 'fade',
                    auto            : true,
                    pause           : 4000,
                    hideControlOnEnd: true,       
                    onSlideAfter   : function(img_index){
                        if($(img_index[0]).data('dark') == false){                         
                            $('.page-landing .bx-wrapper .bx-pager.bx-default-pager a').css('background', 'rgba(255,255,255,0.7)');
                            $('.page-landing .bx-wrapper .bx-pager.bx-default-pager a.active').css('background', 'rgba(255,255,255,1)');
                        }else{                            
                            $('.page-landing .bx-wrapper .bx-pager.bx-default-pager a').css('background', 'rgba(87,87,87,0.7)');
                            $('.page-landing .bx-wrapper .bx-pager.bx-default-pager a.active').css('background', 'rgba(57,57,57,1)');
                        }
                    },             
                    onSlideBefore   : function(img_index){
                        if($(img_index[0]).data('dark') == false){
                            $('.page-landing .logo-whosolves img').attr('src', '/img/layout/WhoSolvesLandingLogo_W.png');
                            $('.page-landing .share-fb img').attr('src', '/img/layout/LandingFBIcon_W.png');
                            $('.page-landing .share-twitter img').attr('src', '/img/layout/LandingTwIcon_W.png');
                            $('.page-landing .landing-icone-text').css('color', '#FFF');
                            $('.page-landing .bx-prev, .page-landing .bx-next').addClass('white-arrow');                            
                        }else{
                            $('.page-landing .logo-whosolves img').attr('src', '/img/layout/WhoSolvesLandingLogo_B.png');
                            $('.page-landing .share-fb img').attr('src', '/img/layout/LandingFBIcon_B.png');
                            $('.page-landing .share-twitter img').attr('src', '/img/layout/LandingTwIcon_B.png');
                            $('.page-landing .landing-icone-text').css('color', 'rgba(87,87,87,1)');
                            $('.page-landing .bx-prev, .page-landing .bx-next').removeClass('white-arrow');                            
                        }
                    }
                });       
                
                $('.page-landing .bx-prev, .page-landing .bx-next').addClass('white-arrow');              
                $('.page-landing .bx-wrapper .bx-pager.bx-default-pager a').css('background', 'rgba(255,255,255,0.7)');                
                $('.page-landing .bx-wrapper .bx-pager.bx-default-pager a.active').css('background', 'rgba(255,255,255,1)');

                
                $('input#keys-required', this.$el).tagit({
                    placeholderText: 'Favorite tags e.g. Mathematics, Biology, History ...',
                    animate: false, 
                    allowSpaces : false,
                    tabIndex   : 5,
                    afterTagAdded: function() {
                       if ($('.tagit.ui-widget li', this.$el).length > 1) {
                           $('.tagit.ui-widget li.tagit-new input', this.$el).attr('placeholder', '');
                           $('.tagit.ui-widget li.tagit-new input', this.$el).css('min-width', '70px ');
                       } else {
                           $('.tagit.ui-widget li.tagit-new input', this.$el).css('min-width', '285px ');
                           $('.tagit.ui-widget li.tagit-new input', this.$el).attr('placeholder', 'Favorite tags e.g. Mathematics, Biology, History ...');
                       }
                   },
                   beforeTagRemoved: function() {
                       $('.send-basic-info .submit-message').html('');                 
                       if ($('.tagit.ui-widget li', this.$el).length > 2) {
                           $('.tagit.ui-widget li.tagit-new input', this.$el).attr('placeholder', '');
                           $('.tagit.ui-widget li.tagit-new input', this.$el).css('min-width', '70px ');
                       } else {
                           $('.tagit.ui-widget li.tagit-new input', this.$el).css('min-width', '285px ');
                           $('.tagit.ui-widget li.tagit-new input', this.$el).attr('placeholder', 'Favorite tags e.g. Mathematics, Biology, History ...');
                       }
                   }, 
                    autocomplete: {
                        source: function(request, response) {
                            var s = [];                           
                            $.ajax({
                                url: "/api/keywords/" + $('.tagit-new .ui-widget-content').val(),
                                dataType: 'json',
                                success: function(data) {                             
                                    var data = data.keyword;
                                    for (var i in data) {
                                        var cat = {
                                            label: data[i].name,
                                            value: data[i].name
                                        }
                                        s.push(cat);
                                    }
                                    response(s, request.term);
                                }
                            });
                        }
                    },
                    allowSpaces: true
                });
            },              
                    
            checkAge: function(e) {
                var charCode = (e.which) ? e.which : e.keyCode;
                if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
            },

            showModal: function(){       
                 if ($('.modal-login').length == 0) {
                    this.templateLogin = _.template(LoginTmp);
                    $('.login-box', this.$el).html(this.templateLogin({
                        labels: Labels
                    }));
                    $('.modal', this.$el).on('hide.bs.modal', function() {
                        $('.modal').addClass('vclose')
                    });
                    $('.modal', this.$el).on('hidden.bs.modal', function() {
                        $('.modal').removeClass('vclose')
                    });

                    $('#username , #password').floatlabel({
                        labelStartTop: '2px',
                        labelClass: 'floating-label'
                    });
                }else{
                    $('#username , #password').val('');
                    $('.error-login').hide();
                }                
                $('[data-toggle=tooltip]', this.$el).tooltip();
                $('.modal-login', this.$el).modal('show');
                $('#username', this.$el).focus();
            },
            
            sendInvitation : function(){
               if(this.checkValidation()){
                   var forms = $('.send-basic-info', this.$el).serializeJSON();
                   forms.timezone = jstz.determine().name();                                
                   if($('.send-basic-info .send-invitation').attr('sent') ){
                       return;
                   }
                   $('.send-basic-info .send-invitation').attr('sent', true);                    
                   
                   $.ajax({
                        url: 'api/registers',
                        data: JSON.stringify(forms),
                        type: "POST",
                        dataType: 'json',
                        success: function(resp) {                                       
                            $('.send-basic-info .send-invitation').css('width', '396');
                            $('.send-basic-info .submit-message').hide();
                            $('.send-basic-info .send-invitation').addClass('after-invitation-send');                                               
                            $('.send-basic-info .send-invitation').html("Your info has been submitted successfully. We\' ll notfiy you in no time.");                            
                            $('.send-basic-info .send-invitation').removeClass('send-invitation');
                            
                            // cleaning the form
                            
                            $('.send-basic-info input').val('').blur();
                            $('.send-basic-info .tagit-choice').remove();
                            $('.send-basic-info .device-type-wrapper select').selectpicker('val','0');
                            $('.send-basic-info .male').trigger('click');
                        },
                        error: function(resp) {                            
                            $('.send-basic-info .submit-message').html("You\'ve already registered, or something else went wrong. Sorry. We\'ll look into it right away.");                            
                        }
                    }, this);
               }
            },
            
            checkValidation : function(){           
                var info = $('.send-basic-info', this.$el).serializeJSON(),
                        reduireds = ['name', 'email', 'education'],
                        $this = this,
                        valid = true;

                _.each(reduireds, function(req) {
                    if (info[req] == '') {
                        $('[name=' + req + ']', $this.$el).attr('placeholder', 'This field is required');
                        $('[name=' + req + ']', $this.$el).addClass('error-validation');
                        valid = false;
                    }
                });
                
                if (info['age'] == ''  || info['age'] == '00') {
                   $('[name=age]', $this.$el).attr('placeholder', '?');
                   $('[name=age]', $this.$el).addClass('error-validation');
                }

                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        emailEl = $('[name=email]', $this.$el);

                if (!re.test(emailEl.val())) {
                    emailEl.closest('.floatlabel-wrapper').find('label').html('Your email is not valid');
                    emailEl.closest('.floatlabel-wrapper').find('label').addClass('error-validation');
                    valid = false;
                }
                
                if ($('#keys-required').val() != '') {
                    if ($('#keys-required').val().split(',').length < 3) {
                        valid = false;
                        $('.send-basic-info .submit-message').html('Please choose three tags, at least');
                    }
                } else {
                    valid = false;
                    $('.send-basic-info .submit-message').html('Please choose three tags, at least');
                }
                
                return valid;
            },
                    
            removeError : function(e){
                var label = $(e.target).data('label');
                $(e.target).closest('.floatlabel-wrapper').find('.error-validation').removeClass('error-validation');
                $(e.target).attr('placeholder', label);
                $(e.target).closest('.floatlabel-wrapper').find('label').html(label);   
            },

            login : function(e){
                var $this = this;
                 $('.error-login').css('visibility', 'hidden');
                if ((e.type == 'keyup' && e.keyCode == 13) || (e.type == 'click')) {
                    $('.login-loading-icon').show();
                    $.ajax({
                        url: '/api/user/login',
                        data: {
                            'identity': $('#login-box').find('#username').val(),
                            'credential': $('#login-box').find('#password').val(),
                        },
                        type: "POST",
                        success: function(resp) {
                            $('.error-login').css('visbility', 'hidden');        
                            $('.login-loading-icon').hide();                
                            App.setNavbarNotifications();
                            window.current_user = resp.user;
                            $('.login-user-name').html(resp.user.name);
                            $('.my-page').attr('href', '/users/' + resp.user.id);
                            
                            window.location.pathname = "/home";
                            
                            $('.modal-backdrop').hide();
                            $('.modal-login', this.$el).modal('hide');
                        },
                        error: function(resp) {
                            $('.login-loading-icon').hide();
                            $('.error-login').css('visibility', 'visible');
                        }
                    }, this);
                }
            }
        });

        return landingView;
    });
