define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'text!views/templates/forgotPassword.html',
    'i18n!nls/labels',    
], function($, Backbone, _, App, ForgotPasswordTpl, Labels) {

    var forgotPasswordView = Backbone.View.extend({
        className: 'forgot-password',
                
        initialize: function(opt) {
            opt = opt || {};
           this.render();
        },
                
        events: {  
            'click .reset-password-btn'      : 'sendRequest',
            'keyup .email-reset-pass input'  : 'sendRequest'     
        },
                
        render: function() {
            var template = _.template(ForgotPasswordTpl);
            this.$el.html(template({
                labels : Labels
            }));            
            
            $('input', this.$el).focus();            
            return this;
        },
        
        sendRequest : function(e){
            if ((e.type == 'keyup' && e.keyCode == 13) || (e.type == 'click')) {
                $('.forgot-password .step-1').toggle();
                $('.forgot-password .step-2').toggle();
                $.ajax({
                    url: '/api/user/forgotpassword',
                    data: JSON.stringify({
                        'email': $('input', this.$el).val()
                    }),
                    type: "POST",
                    dataType: 'json',
                    success: function(resp) {
                    },
                    error: function(resp) {
                    }
                }, this);
            }
        }  
    });

    return forgotPasswordView;
});
