define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'text!views/templates/signupIndicator.html',
    'text!views/templates/signup.html',
    'text!views/templates/signup_findfriends.html',
    'text!views/templates/user_item.html',
    'text!views/templates/followTags.html',
    'text!views/templates/followSearchTagsItem.html',
    'i18n!nls/labels',
    'datetimepicker',
    'libs/browserplus-min',
    'libs/plupload.full',
    'semantic',
    'bootstrap-select',    
    'complexify',
    'libs/jquery.complexify.banlist',
    'floatlabels'
], function($, Backbone, _, App, SignupIndicatorTpl, SignupTpl, FindFriendsTpl, UserTpl, followTagsTpl, followTagsItemTpl, Labels) {

    var signupView = Backbone.View.extend({
        className: 'page-signup',
        events: {
            'blur       .username'                  : 'checkUsername',
            'keyup      .username'                  : 'keyupUsername',
            'blur       .password'                  : 'checkPassword',
            'blur       .name'                      : 'checkName',
            'blur       .email'                     : 'checkEmail',
            'click      .next-register'             : 'saveGoToNext',
            'click      .next-home'                 : 'nextHome',            
            'click      .follow-item-change-status' : 'toggleFollow',
            'click      .invite-item-change-status' : 'toggleInvite',
            'click      .next-to-tags'              : 'followAndInvite',
            'click      li.trending-items'          : 'followTags',            
            'keyup      .search-tags'               : 'searchTags',
            'click      .follow-all'                : 'followAll',
            'click      .invite-all'                : 'inviteAll',
            'change     #birth_month'               : 'changeBirhtDay',
            'change     #birth_day'                 : 'changeBirhtDay',
            'change     #birth_year'                : 'changeBirhtDay',
        },
                
        initialize: function(opt) {                
            var template = _.template(SignupIndicatorTpl);
            this.$el.html(template({
                step    : opt.step,
                labels  : Labels
            }));               
            
            switch (opt.step) {
                case 'register':                    
                    this.renderRegister();
                    this.afterRender();                   
                    break;
                case 'findfriends':
                    this.renderFindFriends();
                    this.getSocialFriends();
                    break;
                case 'followTags':
                    this.renderFollowTags();     
                    this.getFollowTags();
                    break;
            }               
        },              
                
        renderFollowTags : function(){
            var template = _.template(followTagsTpl);
            $('.signup-container', this.$el).append(template({
                labels : Labels
            }));            
            
            $(".search-tags", this.$el).autocomplete({
                selectFirst: true,             
                source: function(request, response) {
                    var s = [];
                    $.ajax({
                        url: "/api/keywords/" + $('.search-tags', this.$el).val(),
                        dataType: 'json',
                        success: function(data) {
                            var data = data.keyword;
                            for (var i in data) {
                                var cat = {
                                    label: data[i].name,
                                    value: data[i].name
                                }
                                s.push(cat);
                            }
                            response(s, request.term);
                        }
                    });

                },
                minLength: 3
            });
            return this;
        },
                
        renderRegister: function() {
            var template = _.template(SignupTpl);
            $('.signup-container', this.$el).append(template(_.extend({
                step    : 'register',
                labels  : Labels
            }, window.current_user))); 
            
            var d = new Date();                 
            for(var i = d.getFullYear(); i >= 1905; i--)
                $('#birth_year', this.$el).append('<option vlaue=' + i + '>' + i + '</option>');          
            
            return this;
        },
                
        renderFindFriends: function() {
            var template = _.template(FindFriendsTpl);
            $('.signup-container', this.$el).append(template({
                labels : Labels
            }));            
            return this;
        },
        
        onAppend : function(){
             $('.username , .password , .email, .name , .about').floatlabel({
                labelStartTop   : '2px' ,
                labelClass      : 'floating-label'
            });
            this.checkUsername();
            this.addUploader();
        },
                
        checkName: function() {   
            if ($('.name').val() == '') {
                $('.name-wrap .error-signup').css('display', 'block');
                $('.name-wrap input', this.$el).addClass('error-validation');
                $('.correct-name').hide();                
            } else {
                $('.correct-name').show();
                $('.name-wrap .error-signup').hide();                
                $('.name-wrap input', this.$el).removeClass('error-validation');
            }
            this.checkActiveNextRegister();
        },
                
        checkEmail: function() {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if (!re.test($('.email').val())) {
                $('.email-wrap .error-signup').css('display', 'block');
                $('.email-wrap input', this.$el).addClass('error-validation');
                $('.correct-email').hide();                
            } else {
                $('.correct-email').show();
                $('.email-wrap input', this.$el).removeClass('error-validation');
                $('.email-wrap .error-signup').hide();                
            }
            this.checkActiveNextRegister();
        },
                
        checkPassword: function() {
            if ($('.password', this.$el).val().length < 6) {
                $('.password-wrap .error-less-char').css('display', 'block');     
                $('.password-wrap input', this.$el).addClass('error-validation');
                $('.correct-password').hide();
            } else {
                if($('.password-strength-wrapper .val-strength').data('data-strength') < 10){
                    $('.password-wrap input', this.$el).addClass('error-validation');
                    $('.password-wrap .error-less-char').hide();
                    $('.error-weak-pass', this.$el).css('display', 'block');
                    $('.correct-password').hide();
                }else{
                    $('.correct-password').show();
                    $('.password-wrap input', this.$el).removeClass('error-validation');
                    $('.password-wrap .error-signup').hide();                
                }
            }       
            this.checkActiveNextRegister();
        },
        
        searchTags : function(e){
             if(e.type == 'keyup' && e.which == 13){
                 this.getFollowTags($(e.target).val());
             }
        },
        
        changeBirhtDay : function(){               
             var month = $('#birth_month', this.$el).val(),
                 day   = $('#birth_day', this.$el).val(),
                 year  = $('#birth_year', this.$el).val();         
            
            var d        = new Date(year, month, day);                
          
            if (d.getMonth() != month)
                $('#birth_month', this.$el).selectpicker('val', d.getMonth());
            if (d.getDate() != day)
                $('#birth_day', this.$el).selectpicker('val', d.getDate());
            if (d.getFullYear() != year)
                $('#birth_year', this.$el).selectpicker('val', d.getFullYear());          
            
            return false;
        },
        
        followTags : function(e){
            var el     = $(e.target).hasClass('.trending-tags') ? $(e.target) : $(e.target).closest('.trending-tags'),
            tagId      = el.data('id'),
           isFollow    = el.data('isfollow');           
           
           if(isFollow == 1){       
               $('.unfollow-tag', el).html('+ ' + Labels['follow']);
               $('.unfollow-tag', el).removeClass('unfollow-tag').addClass('follow-tag');
           }else{
               $('.follow-tag', el).html('- ' + Labels['unfollow']);
               $('.follow-tag', el).removeClass('follow-tag').addClass('unfollow-tag');
           }
           
           $('.tag-name', el).toggleClass('active');
           $($('.follow-btn-title:not(.active)')[0]).addClass('active');
           $('.follow-btn-title.active').length == 3 && $('.next-home').removeClass('deactive');
           el.data('isfollow', isFollow == 0  ? 1 : 0);
           $.ajax({
                url: isFollow == 0 ? '/api/keywords/follows' : '/api/keywords/follows/' + tagId,
                data: {
                    "keywords" : [{ "name" : tagId}]
                },                
                type        : isFollow == 0 ? "POST" : 'DELETE',
                dataType    : 'json',
                success: function(resp){},
                error: function(resp) {}
            }, this);
        },
                
        validate: function() {
            $('input').trigger('blur');                      
            if(this.checkActiveNextRegister())
                return true;
           return false;
        },
                
        saveGoToNext: function() {              
            if (!this.validate())
                return false;
            this.saveUsernamePass();
            if(this.saveUserInfo())
                Backbone.history.navigate('/signup/findfriends', true);
        },
        
        checkActiveNextRegister : function(){    
            if ($('.email-wrap .error-signup').css('display') == 'block'){
                $('.next-register').addClass('deactive');
                return false;
            }
            if ($('.password-wrap .error-signup').css('display') == 'block'){
                $('.next-register').addClass('deactive');
                return false;
            }
            if($('.name-wrap .error-signup').css('display') == 'block'){
                $('.next-register').addClass('deactive');
                return false;
            }
            if ($('.username-wrap .error-signup').css('display') == 'block'){
                $('.next-register').addClass('deactive');
                return false; 
            }
            if($('.password', this.$el).val().length < 6) {
                $('.next-register').addClass('deactive');
                return false; 
            }
            if(!$('.username-wrap').data('checked')){
                $('.next-register').addClass('deactive');
                return false; 
            }
            
            $('.next-register').removeClass('deactive');
            return true;
            
        },
                
        saveUserInfo: function() {         
    
            // check validation of Birthday
            var birthday = ($('#birth_year').val() != 0 && $('#birth_month').val() != 0 && $('#birth_day').val() != 0) ? $('#birth_year').val() + '-' + $('#birth_month').val() + '-' + $('#birth_day').val() : '';
            if(!$.fn.validateDate(birthday)){               
                $('.birthday-signup .error-signup').css('display', 'block');
                return false;
            }else{
                $('.birthday-signup .error-signup').hide();
            }
            
            if(!/^[a-zA-Z0-9._]+$/.test($('.username', this.$el).val())){
                $('.username-wrap input', this.$el).addClass('error-validation');
                $('.not-vali-username', this.$el).css('display', 'block');
                return false;
            }else{
                $('.username-wrap input', this.$el).removeClass('error-validation');
                $('.not-vali-username', this.$el).hide();
            }
            
            $.ajax({
                url: '/api/users/' + window.current_user.id,
                data: {
                    about    : $('.about', this.$el).val(),                    
                    gender   : $('[name=gender]:checked').val(),
                    birhday  : birthday
                },
                type: "PUT",
                dataType: 'json',
                success: function(resp){},
                error: function(resp) {}
            }, this);
            
            return true;
        },
        
        saveUsernamePass: function() {
            $.ajax({
                url: '/api/social/setuserpass',
                data: JSON.stringify({
                    username: $('.username', this.$el).val(),
                    password: $('.password', this.$el).val(),
                    email    : $('.email', this.$el).val()
                }),
                type: "POST",
                dataType: 'json',
                success: function(resp){},
                error: function(resp){}
            }, this);
        },    
        
        keyupUsername :function(){
            $('.username-wrap').data('checked', false);
        },
        
        checkUsername: function() {                 
             var $this = this,
                 el   = $('.username', this.$el);   
            if(el.val() == "")     
                return;
            
            if(el.val() == el.data('username'))
                return;
            
            el.data('username', el.val());               
            if($('.username-wrap').data('checked')) return;
            $('.username-wrap').data('checked', false);

            if(!/^[a-zA-Z0-9._-]+$/.test($('.username', this.$el).val())){
                $('.username-wrap .error-signup').hide();
                $('.not-vali-username', this.$el).css('display', 'block');                
                $('.username-wrap input', this.$el).addClass('error-validation');
                $('.correct-username').hide();
                return false;
            }else{
                $('.username-wrap input', this.$el).removeClass('error-validation');
                $('.not-vali-username', this.$el).hide();
            }
            
            $.ajax({
                url: '/api/social/checkusername',
                data: JSON.stringify({
                    username: $('.username', this.$el).val()
                }),
                type: "POST",
                dataType: 'json',
                success: function(resp) {
                    if (resp.exists) {
                        $('.username-wrap .error-signup').css('display', 'block');
                        $('.not-vali-username', this.$el).hide();
                        $('.username-wrap').data('checked', false);
                        $('.correct-username').hide();
                        $('.username-wrap input', this.$el).addClass('error-validation');

                    } else {
                        $('.correct-username').show();
                        $('.username-wrap .error-signup').hide();
                        $('.not-vali-username', this.$el).hide();                        
                        $('.username-wrap').data('checked', true);
                        $('.username-wrap input', this.$el).removeClass('error-validation');
                    }              
                    $this.checkActiveNextRegister();
                },
                error: function(resp) {
                    $('.correct-username').hide();                    
                    $('.username-wrap').data('checked', false);
                    $('.username-wrap .error-signup').css('display', 'block');
                    $('.not-vali-username').hide();
                    $this.checkActiveNextRegister();
                }
            }, this);
        },
        
        addUploader: function() {
            this.levels = [];
            var $this = this;
            var uploader = new plupload.Uploader({
                runtimes: 'html5,html4',
                file_data_name: 'profile',
                multi_selection: false,
                browse_button: 'pickfiles',
                container: 'new-image-upload',
                max_file_size: '10mb',
                progress: true,
                url: '/api/uploads/file',
                filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }]
            });

            uploader.init();
            uploader.bind('FilesAdded', function(up, files) {
                up.refresh();
                uploader.start();
            });

            uploader.bind('FileUploaded', function(up, file, resp) {
                $('.user-img-upload img').attr('src', $.parseJSON(resp.response).user.imagePath)
            });
        },
                
        afterRender: function() {
            $(".form_datetime", this.$el).datetimepicker({
                format: "yyyy-mm-dd  hh:ii",
                autoclose: true,
                minView : 2
            });                       
            
            $('#timezone', this.$el).selectpicker({width: '551'});
            $('.birthday-wrapper select#birth_day', this.$el).selectpicker({width : '70'});
            $('.birthday-wrapper select#birth_month', this.$el).selectpicker({width : '100'});
            $('.birthday-wrapper select#birth_year', this.$el).selectpicker({width : '90'});            
            $('.ui.checkbox', this.$el).checkbox();
            $(".form_datetime", this.$el).datetimepicker('setStartDate', new Date());
          
            this.passwordComplexify();
            
             // Set now to birthday
            var d = new Date(); 
            $('#birth_month', this.$el).selectpicker('val', d.getMonth());
            $('#birth_day', this.$el).selectpicker('val', d.getDate());           
        },
        
        followAndInvite : function(){
            var followIds = [],
                inviteIds = [];               
            
            _.each($('.unfollow-img').closest('li'), function(userBox){
                followIds.push($(userBox).attr('id') + "");
            });
            
            _.each($('.uninvite-img').closest('li'), function(userBox){
                inviteIds.push($(userBox).attr('email'));
            });
        
            if(inviteIds.length > 0){
                $.ajax({
                    url: 'api/social/invite',
                    data: {
                        email : [inviteIds.join(',')]
                    },
                    type        : "POST",               
                    success: function(resp){},
                    error: function(resp) {}
                }, this);
            }
            
            if(followIds.length > 0){
                $.ajax({
                    url: '/api/followings',
                    data: {
                        userId : [followIds.join(',')]
                    },
                    type        : "POST",                
                    success: function(resp){},
                    error: function(resp) {}
                }, this);
            }
            
            Backbone.history.navigate('/signup/followTags', true);
        },
        
        inviteAll : function(){
            $('.users-invite-friends li').each(function(index, el){
                if(!$('.invite-item-change-status', el).hasClass('uninvite-img')){
                    $('.invite-item-change-status', el).addClass('uninvite-img');
                    $('.invite-item-change-status', el).removeClass('invite-img');
                }
            });
        },        
        
        followAll : function(){
            $('.users-already-in-qolve li').each(function(index, el){
                if(!$('.follow-item-change-status', el).hasClass('unfollow-img')){
                    $('.follow-item-change-status', el).addClass('unfollow-img');
                    $('.follow-item-change-status', el).removeClass('follow-img');
                }
            });
        },
                
        nextHome : function(e){
            if($(e.target).hasClass('deactive'))
                return;
            Backbone.history.navigate('/home', true);
        },
                
        getFollowTags : function(name) {
            $.ajax({
                url     : (name == undefined) ? '/api/keywords?limit=1000' : '/api/keywords/' +  name + '?limit=1000',
                type    : "GET",                
                success: function(resp) {                                        
                   var template = _.template(followTagsItemTpl); 
                   $('.show-tags').empty();
                   var keys = resp['keyword'].list ? resp['keyword'].list : resp['keyword'];
                   $('.show-tags').append(template({
                       'trendingKey' : keys,
                       'labels'      : Labels   
                   }));                    
                },
                error: function(resp) { }
            }, this);
        },
                
        getSocialFriends: function() {
            $.ajax({
                url: '/api/social/saveandgetprovidercontacts',                 
                type: "GET",
                dataType: 'json',
                success: function(resp) {
                    if (resp.users['registereds'] && resp.users['registereds'].count > 0) {
                        $('.already-in-qolve').css('display', 'block');
                        _.each(resp.users['registereds'].list, function(user) {
                            var template = _.template(UserTpl);
                            user.invite = false;
                            user.setImageSize = false;
                            $('.users-already-in-qolve').append('<li id="' + user.id + '" class="user-item">' + template(user) + '</li>');
                        });
                    }

                    if (resp.users['unregistereds'] && resp.users['unregistereds'].count > 0) {
                        $('.invite-friends').css('display', 'block');
                        _.each(resp.users['unregistereds'].list, function(user) {                            
                            var template = _.template(UserTpl);
                            user.invite = true;
                            user.setImageSize = false;
                            $('.users-invite-friends').append('<li id="' + user.email + '" class="user-item">' + template(user) + '</li>');
                        });
                    }
                },
                error: function(resp) {
                }
            }, this);
        },              
      
        toggleInvite: function(e) {
            var el = $(e.target).hasClass('invite-item-change-status') ? $(e.target) : $(e.target).closest('.invite-item-change-status');            
            if (el.hasClass('invite-img')) {                
                el.removeClass('invite-img');
                el.addClass('uninvite-img');
            } 
        },
                
        toggleFollow: function(e) {
            var el = $(e.target).hasClass('follow-item-change-status') ? $(e.target) : $(e.target).closest('.follow-item-change-status');            
            if (el.hasClass('follow-img')) {                
                el.removeClass('follow-img');
                el.addClass('unfollow-img');
            } else {                
                el.removeClass('unfollow-img');
                el.addClass('follow-img');
            }            
        },
        
         passwordComplexify: function() {
            var $this = this;
            $(".password", this.$el).complexify({}, function(valid, complexity) {
                var val = parseFloat(complexity).toFixed(2);                
                $('.password-strength-wrapper .val-strength').data('data-strength', val);
                
                if(val < 10)
                    $('.password-strength-wrapper .val-strength').html(Labels['weak']);
                if(val > 10 && val < 70)
                    $('.password-strength-wrapper .val-strength').html(Labels['normal']);
                if(val > 70)
                    $('.password-strength-wrapper .val-strength').html(Labels['stong']);
                
                if (!valid) {
                    $('.password-strength-wrapper .progress-success .bar', $this.$el).css({
                        'width': complexity + '%'
                    }).removeClass('progress-success').addClass('progress-danger');
                } else {
                    $('.password-strength-wrapper .progress-success .bar', $this.$el).css({
                        'width': complexity + '%'
                    }).removeClass('progress-danger').addClass('progress-success');
                }
            });
        }
    });

    return signupView;
});
