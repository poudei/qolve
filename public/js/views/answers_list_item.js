define([
    'jquery',
    'underscore',
    'backbone',        
    'base/view/itemView',    
    'text!views/templates/answer_item.html',    
    'text!views/templates/confirmModal.html',
    'text!views/templates/vote.html',
    'i18n!nls/labels',
    ], function ($, _, Backbone, ItemView, ItemTpl, ConfirmModalTpl, VoteTpl, Labels) {
        var AnswerListItemView = ItemView.extend({
            tagName   : 'li',
            className : 'box-item answer-list-item',
          
            events : {               
               'click   .voting-answer'         : 'toggleVote'  ,
               'click   .show-comments-list'    : 'showComments',
               'click   .report-answer'         : 'report',
               'click   .confirm-question'      : 'acceptAnswer',
               'click   .accept-answer'         : 'showConfirmModal',
               'click   .confirm-decline'       : 'hideConfirmModal',
               'click   .more-items'            : 'showMoreMenu',
               'click   .delete-answer'         : 'deleteAnswer'
            },

            initialize : function() {
                _.bindAll(this, 'render');
                var self = this;              
                this.model.on('change', this.render);   
                this.type = 'answers';
                this._questionId        = this.options['_questionId'];
                this._isAcceptAnswer    = this.options['_isAcceptAnswer'];
            },

            render    : function() {             
                this.template = _.template(ItemTpl);                
                var obj = _.extend(this.model.toJSON(), {
                    _showUser        : this.options._showUser,
                    _isAcceptAnswer  : this._isAcceptAnswer,
                    labels           : Labels
                });
                this.$el.html(this.template(obj));                                
                $('.edit-answer', this.$el).data('id', this.model.get('id'));
                return this;
            },
                    
            hideConfirmModal : function(){
                $('.modal-confirm', this.$el).modal('hide');
            },
                    
            showConfirmModal : function(){        
                if($('.modal-confirm', this.$el).length == 0){
                    var confTemplate = _.template(ConfirmModalTpl);
                    this.$el.append(confTemplate({
                        message : Labels['are_sure_wantto_transfer_credit'],
                        labels  : Labels
                    }));                    
                }       
                $('.modal-confirm', this.$el).modal('show');
                $('.modal-confirm', this.$el).on('hide.bs.modal', function(){$('.modal').addClass('vclose')});
                $('.modal-confirm', this.$el).on('hidden.bs.modal', function(){$('.modal').removeClass('vclose')});
            },
                    
            acceptAnswer : function(){                                        
                var $this = this;
                
                $('.modal-confirm', this.$el).modal('hide');
                $.ajax({
                    url  : '/api/questions/' + this._questionId + '/askers/' + window.current_user.id,                    
                    type : "PUT",                    
                    data : {
                      answerId : this.model.get('id')  
                    },
                    success: function(resp) {
                        $this.model.set('status', 2);
                    },
                    error: function(resp) {
                
                    }
                }, this);
            },
            
            deleteAnswer : function(){
                var $this = this;
                
                this.remove();    
                this.model.destroy({
                    success: function(model, resp) {
                           
                    }
                });
            },
                    
            renderVoteTpl: function(score) {
                var self = this,
                        url,
                        type,
                        fscore,
                        votes,
                        isVoted;

                switch (this.model.get('isVoted')) {
                    case 0:
                        url     = '/api/questions/' + this._questionId + '/answers/' +  this.model.get('id') + '/votes';
                        type    = 'POST';
                        isVoted = score;
                        votes   = this.model.get('votes') + score;
                        break;
                    case score:
                        url     = '/api/questions/' + this._questionId + '/answers/' + this.model.get('id') + '/votes/delete';
                        type    = 'DELETE';
                        isVoted = 0;
                        votes   = this.model.get('votes') - score;
                        break;
                    default:
                        url     = '/api/questions/' + this._questionId + '/answers/' +  this.model.get('id') + '/votes';
                        type    = 'POST';
                        isVoted = score;
                        if (this.model.get('isVoted') != score && score == -1) {
                            votes = this.model.get('votes') - 2;
                        } else {
                            votes = this.model.get('votes') + 2;
                        }
                        break;
                }

                this.model.set({
                    'isVoted' : isVoted,
                    'votes'   : votes
                },{silent : true});

                this.model.toggleVote({
                    score  : score,
                    url    : url,
                    type   : type
                });

                var voteTemplate = _.template(VoteTpl);
                $('.votes-question-wrapper', this.$el).html(voteTemplate({
                    'isVoted'   : isVoted,
                    'votes'     : votes,
                    'labels'    : Labels,
                    'type'      : 'answer' 
                }));
            }
                    
          
        });

        return AnswerListItemView;
    });
