define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',
    'collections/question',
    'views/questions_list_item',
    'text!views/templates/reask.html',
    'text!views/templates/report.html',
    'text!views/templates/docs.html',
    'i18n!nls/labels',
    'enscroll'
], function($, _, Backbone, App, BaseListView, QuestionCollection, QuestionItemView, ReaskTpl, ReportTpl, DocsTpl, Labels) {
    var QuestionListView = BaseListView.extend({
        events: {
            'click    .nothanks-btn'                : 'hideModal',
            'click    .reask-btn'                   : 'reaskQuestion',
            'click   .report-btn'                   : 'reportQuestion',
            'click   .share-btn'                    : 'shareQuestion',
            'click   .item-page-doc'                : 'showAllDocs',
            'click   .cancel-btn'                   : 'hideModal',  
            'click   .social-share-wrapper span'    : 'selectShareSocial'
        },
                
        className : 'questions-list',      
        tagName   : 'ul',
                
        initialize: function() {
            _.bindAll(this, 'render', 'renderItem');
            this.collection = new QuestionCollection([], {
                _userId         : this.options._userId,
                question_url    : this.options['question_url']
            });
            
            app = require('app'); 
            app.updateCredits();
            
            this.collection.on('add', this.renderItem);
          
            if(this.options._filters)
                this.collection.filters.set(this.options._filters);
            QuestionListView.__super__.initialize.apply(this, []);
        },
                
        render: function() {
            this.template = _.template(ReaskTpl);
            this.$el.html(this.template({
                labels : Labels
            }));
            
            var reportTemplate = _.template(ReportTpl);
            this.$el.append(reportTemplate({
                labels : Labels
            }));
            
            this.afterRender();
            return this;
        },
                
        showAllDocs : function(e){    
            var docTemplate = _.template(DocsTpl),
                    el      = $(e.target).closest('.item-page-doc'),
                    docs    ,
                    model   ,
                    id = el.data('questionid'); 
                
                model = this.collection.get(id);
                docs  = model.get('documentsList');            
            
            $('.modal-documents .show-docs').html(docTemplate({
                docs        : docs,
                labels      : Labels,
                description : model.get('description'),
                share       : model.get('share'), 
                keywords    : model.get('keywords'),
                id          : model.get('id')
            }));
            
            $('.modal-documents').modal('show');            
            if(this.bxSlider){
                this.bxSlider.destroySlider();
            }
            
            this.bxSlider = $('.modal-documents .bxslider').bxSlider({
                mode : 'fade'
            });       
            
            $('.scrollbox3', this.$el).enscroll({
                showOnHover: false,
                verticalTrackClass: 'track3',
                verticalHandleClass: 'handle3'
            });
       },
                
        afterRender: function() {
            $(".slider-reask", this.$el).slider({
                orientation: "vertical",
                min  : 0,
                step : 1,
                max  : Math.min(window.current_user.credit, 50),
                slide: function(event, ui) {
                    $('.slider-mount-shown').setToFixedWith(70);
                    if(ui.value == 0)
                        $('.slider-mount-shown').html(Labels['free']);
                    else
                        $('.slider-mount-shown').html(ui.value);
                    
                    $('.modal-reask').data('credit', ui.value);
                }
            });
              $('.ui.checkbox', this.$el).checkbox();
              
             $('.modal', this.$el).on('hide.bs.modal', function(){$('.modal').addClass('vclose')});
             $('.modal', this.$el).on('hidden.bs.modal', function(){$('.modal').removeClass('vclose')});           
        },
        
        renderItem: function(model) {            
             this.options.actor && model.set({
                 actor :  this.options.actor
             }, {silent : true});
             
            var item = new QuestionItemView({
                model       : model,
                _showUser   : this.options._showUser,
                list        : require('app').getCurrentUserLists().toJSON()
            });

            $(this.$el).append(item.render().$el);
        },       
        
        afterRenderItems : function(){
            if(this.collection.length == 0){
                $(this.$el).html('<div class="no-questions" style="text-align:center">' + Labels['there_is_no_hot_problems_with_your_following_tags'] + '<br><a href="/explore">' + Labels['explore_and_follow_more_tags_to_see_hot_problems']+ '</a></div>');
            }
        },        
                
        reaskQuestion: function() {    
            var questionId      = $('.modal-reask').data('questionid'),
                    question    = this.collection.get(questionId),
                    credit      = $('.modal-reask').data('credit'),
                    visibility  = $('.modal-reask .question-reask-visibility').is(':checked') ? 2 : 1;
            this.hideModal();
            question.reaskQuestion(credit, visibility);            
        },
        
        reportQuestion: function() {    
            var questionId      = $('.modal-report').data('itemid'),
                    question    = this.collection.where({id : questionId})[0];                    
            this.hideModal();
            question.reportQuestion();
        },
        
        shareQuestion: function() {    
            var questionId      = $('.modal-share').data('itemid'),
                    question    = this.collection.where({id : questionId})[0];                    
            this.hideModal();
            question.shareQuestionUrl();
        },
        
        hideModal: function() {
           $(".modal", this.$el).modal('hide');                
        },
        
        selectShareSocial : function(e){
            var socialType = $(e.target).data('share');
            $('.share-' + socialType).toggleClass('active');
        },        
    });

    return QuestionListView;
});
