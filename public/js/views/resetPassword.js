define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'text!views/templates/resetPassword.html',
    'i18n!nls/labels',    
    'complexify',
    'libs/jquery.complexify.banlist',
    'floatlabels'
], function($, Backbone, _, App, ResetPasswordTpl, Labels) {

    var resetPasswordView = Backbone.View.extend({
        className: 'reset-password',
                
        initialize: function(opt) {
            opt = opt || {};
           this.opt = opt; 
           this.render();
        },
                
        events: {  
            'click .save-password-btn' : 'sendRequest'
        },
                
        render: function() {
            var template = _.template(ResetPasswordTpl);
            this.$el.html(template({
                labels : Labels
            }));           
            
            this.passwordComplexify();           
            return this;
        },
        
        onAppend : function(){
            $('input', this.$el).floatlabel({
               labelStartTop   : '2px' ,
               labelClass      : 'floating-label'
           });
        },
        
        sendRequest : function(){           
            if($('input.new-pass', this.$el).val() != $('input.new-confirm-pass', this.$el).val()){
                $('.error-valdiation', this.$el).html(Labels['newPassword_confirmedPassword_arenot_equal']);
                $('.error-valdiation', this.$el).css('display', 'block');
                return false;
            }else{
                $('.error-valdiation', this.$el).html(Labels['there_error_try_later']);
                 $('.error-valdiation', this.$el).css('display', 'none');
            }
            
            $.ajax({
                url: '/api/user/resetpassword?req_key=' + this.opt.token,
                data: JSON.stringify({
                    'newpass'     : $('input.new-pass', this.$el).val(),
                    'confirmpass' : $('input.new-confirm-pass', this.$el).val()
                }),
                type: "POST",
                dataType: 'json',
                success: function(resp){
                    if(resp.error){
                        $('.error-valdiation', this.$el).css('display', 'block');
                    }else{
                        Backbone.history.navigate('/signIn', true);
                        $('.error-valdiation', this.$el).css('display', 'none');
                    }                    
                },
                error: function(resp){
                    $('.error-valdiation', this.$el).css('display', 'block');
                }
            }, this);
        },
       
        passwordComplexify: function() {
            var $this = this;
            $(".new-pass", this.$el).complexify({}, function(valid, complexity) {
                var val = parseFloat(complexity).toFixed(2);
                if(val == 0) 
                    val = 0;
                $('.password-strength-wrapper .val-strength').html(val);
                if (!valid) {
                    $('.password-strength-wrapper .progress-success .bar', $this.$el).css({
                        'width': complexity + '%'
                    }).removeClass('progress-success').addClass('progress-danger');
                } else {
                    $('.password-strength-wrapper .progress-success .bar', $this.$el).css({
                        'width': complexity + '%'
                    }).removeClass('progress-danger').addClass('progress-success');
                }
            });
        }
    });

    return resetPasswordView;
});
