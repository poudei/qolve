define([
    'jquery',
    'underscore',
    'backbone',
    'app',  
    'collections/keywords',
    'text!views/templates/followTagsUser.html',    
    'i18n!nls/labels',      
], function($, _, Backbone, App, KeywordsCollection, FollowTagUserTpl, Labels) {
    var FollowingTagsView = Backbone.View.extend({        
        tagName : 'ul',        
        className : 'followingtags-user',         
        events: {
            'click .follow-tag-users' : 'unfollowTags'
        },
            
        initialize: function(opt) {
            _.extend(this, opt);
            var self = this;
            
            if(!this.loaded){
                this.collection = new KeywordsCollection([], {
                    url : opt.url
                });               
            
                this.collection.fetch({
                    add     : true,
                    silent  : true,
                    update  : true
                });
            }           

            this.collection.on('fetchSuccess', function(resp, collection) {        
               self.renderItems();               
            });
            
            $(this.$el).css('min-height', ($(window).height() - 80));                                 
        },
                
        render: function() {
            if(this.loaded){
               this.collection.filters.set('offset', 0);
               this.renderItems();                                         
            }    
            return this;
        }, 
        
        renderItems: function() {
            for (var i = this.collection.filters.get('offset'); i < this.collection.models.length; i++) {
                var template = _.template(FollowTagUserTpl);
                this.$el.append(template(_.extend(
                        this.collection.models[i].toJSON(), {
                            labels : Labels
                        })
                ));
            }           
            this.afterRenderItems();
        },
        
        afterRenderItems : function(){    
            if(this.collection.length == 0){                
               $(this.$el).html('<div class="no-user">' + this.userItem.get('name') + ' follows no tags.'  + '</div>');                
            }
        }, 
                
        disposeAllItems : function(){
            this.remove();
        },
                
        unfollowTags : function(e){
            var el     = $(e.target).hasClass('.follow-tag-users-wrap') ? $(e.target) : $(e.target).closest('.follow-tag-users-wrap'),
            tagId      = el.data('key'),
            id         = el.data('id'), 
           isFollow    = el.data('isfollow');                    
           
           if(isFollow == 1){       
               $('.unfollow-tag', el).html('+ ' + Labels['follow']);
               $('.unfollow-tag', el).removeClass('unfollow-tag').addClass('follow-tag');
           }else{
               $('.follow-tag', el).html('- ' + Labels['unfollow']);
               $('.follow-tag', el).removeClass('follow-tag').addClass('unfollow-tag');
           } 
           
           el.data('isfollow', isFollow == 0  ? 1 : 0);
           
           if(window.current_user && window.current_user.id && this.options['actor'].id == window.current_user.id){
               if(this.collection.get(id))this.collection.remove(id);
               el.remove();
           }           
           
           $.ajax({
                url         : isFollow == 0 ? '/api/keywords/follows' : '/api/keywords/follows/' + tagId,
                data        : {"keywords" : [{ "name" : tagId}]},
                type        : isFollow == 0 ? "POST" : 'DELETE',
                dataType    : 'json',
                success: function(resp){},
                error: function(resp) {}
            }, this); 
            
            return false;
        }
        
    });

    return FollowingTagsView;
});
