define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',
    'collections/activities',
    'views/activities_list_item',   
    'text!views/templates/activities.html',
    'i18n!nls/labels',
], function($, _, Backbone, App, BaseListView, ActivitiesCollection, ActivitiesItemView, ActivitiesTpl, Labels) {
    var ActivitiesListView = BaseListView.extend({
        events: {},                
        className : 'activities-list',        
                
        initialize: function(opt) {        
            this.collection = new ActivitiesCollection([], {});                              
            ActivitiesListView.__super__.initialize.apply(this, []);
        },
                
        render: function() {
            this.template = _.template(ActivitiesTpl);
            this.$el.html(this.template({
                labels   : Labels
            }));         
            this.renderItems();                                                    
            return this;
        },       
                
        renderItem: function(model) {                        
            var item = new ActivitiesItemView({
                model       : model                    
            });
            if(!this.olderThanWeek(new Date(model.get('createdOn')), new Date())){
                $('.this-week-activities-title', this.$el).css('display', 'block');
                $('ul.this-week-activities', this.$el).append(item.render().$el);
            }
            else{
                $('ul.older-activities', this.$el).css('display', 'block');
                $('ul.older-activities', this.$el).append(item.render().$el);
                $('.older-activities-title', this.$el).css('display', 'block');
            }
        },
                
        olderThanWeek : function(createdOn, now){              
            var diff = Math.abs(createdOn - now) / 86400000;
            if (diff > 7) {
                return true;
            }
            return false ;           
        }
    });

    return ActivitiesListView;
});
