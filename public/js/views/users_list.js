define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',
    'collections/users',
    'views/user_item',  
    'i18n!nls/labels',        
], function($, _, Backbone, App, BaseListView, UserCollection, ListItemView, Labels) {
    var UserListView = BaseListView.extend({
        events: {},
        tagName : 'ul',        
        className : 'users',              
                
        initialize: function(opt) {
            _.extend(this, opt);
            
            if(!this.loaded)
                this.collection = new UserCollection([], {
                    url : opt.url
                });              
            
            $(this.$el).css('min-height', ($(window).height() - 80));          
            UserListView.__super__.initialize.apply(this, []);
        },
                
        render: function() {
            this.loaded && this.renderItems(0);  
            return this;
        }, 
        
        afterRenderItems : function(){    
            if(this.collection.length == 0){
                if(this.type == 'follower')
                    $(this.$el).html('<div class="no-user">' + this.userItem.get('name') + ' ' + Labels['follows_no_one'] + '</div>');
                else
                    if(this.type == 'following')
                        $(this.$el).html('<div class="no-user"> ' + Labels['no_one_follows'] + ' '+ this.userItem.get('name') + '. </div>');
            }
        }, 
        
        renderItem: function(model) {             
            var item = new ListItemView({
                model       : model ,                
                callee      : this
            });         
            this.items[model.cid] = item;
            this.$el.append(item.render().$el);            
        }          
        
    });

    return UserListView;
});
