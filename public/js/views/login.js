define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'jquery-helper',
    'text!views/templates/login_page.html',  
    'i18n!nls/labels',
    'floatlabels'
    ], function ($, Backbone, _, App, jqHelper, LoginPageTmp, Labels) {

        var loginView = Backbone.View.extend({
            className           : 'page-login',
                    
            initialize : function() {
                var $this = this;
                this.template = _.template(LoginPageTmp);                
                App = require('app');
            },

            events : {               
                'click .login-qolve'    : 'login',
                'keyup #username'       : 'login',
                'keyup #password'       : 'login'
            },

            render : function() {
                this.$el.html(this.template({
                    labels  : Labels
                }));                   
                $('[data-toggle=tooltip]', this.$el).tooltip();
                
                if(window.location.search.substring(1).search('error=loginbyprovider') != -1){
                    $('.error-login-registered', this.$el).show();                    
                }
                return this;
            }, 
            
            onAppend : function(){
                $('#username , #password' ).floatlabel({
                    labelStartTop   : '2px' ,
                    labelClass      : 'floating-label'
                });
            },

            login : function(e){
                var $this = this;
                 $('.error-login').css('visibility', 'hidden');
                if ((e.type == 'keyup' && e.keyCode == 13) || (e.type == 'click')) {
                    $('.login-loading-icon').show();
                    $.ajax({
                        url: '/api/user/login',
                        data: {
                            'identity': $('#login-box').find('#username').val(),
                            'credential': $('#login-box').find('#password').val(),
                        },
                        type: "POST",
                        success: function(resp) {
                            $('.error-login').css('visbility', 'hidden');        
                            $('.login-loading-icon').hide();                         
                            App.setNavbarNotifications();
                            window.current_user = resp.user;
                            $('.login-user-name').html(resp.user.name);
                            $('.my-page').attr('href', '/users/' + resp.user.id);                           
                            
                            window.location.pathname = "/home";
                            $('.error-login-registered', this.$el).hide();                    
                            $('.modal-backdrop').hide();
                            $('.modal-login', this.$el).modal('hide');
                        },
                        error: function(resp) {
                            $('.login-loading-icon').hide();
                            $('.error-login-registered', this.$el).hide();                    
                            $('.error-login').css('visibility', 'visible');
                        }
                    }, this);
                }
            }
        });

        return loginView;
    });
