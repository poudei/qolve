define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/listView',
    'collections/notifications',    
    'text!views/templates/notificationNavbar.html',
    'i18n!nls/labels',
], function($, _, Backbone, App, BaseListView, NotificationCollection, NotificationNavbarTpl, Labels) {
    var NotificationNavbarView = Backbone.View.extend({
        events: {  },                
        className : 'notification-navbar-list dropdown-menu nav-menu',      
        tagName   : 'ul',
                
        initialize: function() {
            _.bindAll(this, 'render', 'showNotificationMenu');
            this.collection = new NotificationCollection([],{mode : 'new'});                  
            this.collection.filters.set('limit', 5);
            this.collection.on('fetchSuccess', this.render);
            this.collection.fetch();            
            this.setInterval();
            this.registerEvent();           
        },   
                
        registerEvent : function(){
            $('.alarm-navbar .notifications-count').on('click', this.showNotificationMenu);
            $('.alarm-navbar').on('click', this.showNotificationMenu);
        },
                
        setInterval : function(){
            setInterval(function($this){                  
                $this.collection.reset();  
                $this.collection.fetch({
                    update    : false,
                    remove    : true
                });
            },120000, this);
        },        
                
        render: function() {
            $(this.$el).css('background-image', 'none');        
            
            this.template = _.template(NotificationNavbarTpl);      
            this.$el.html(this.template({
                notifications : this.collection.toJSON(),
                labels        : Labels  
            }));
            
            this.setNotificationsCount();
            return this;
        },
                
        showNotificationMenu : function(e){                        
            if($(e.target).closest('.notification-navbar-list').length > 0)
                return true;
            
            $('.notification-navbar-list').toggle();
            $('.alarm-navbar').toggleClass('main-menu-hover');
            if (!this.collection._isSeen) {
                this.collection._isSeen = true;
                if (this.collection.length > 0) {
                     $('.alarm-navbar .notifications-count').css('display', 'none');
                    $.ajax({
                        url: '/api/notifications/' + this.collection.models[0].get('id'),
                        type: 'PUT',
                        data:{
                            seen : 1
                        }
                    });
                }
            }                      
            return false;
        },
                
        setNotificationsCount : function(){
            if(this.collection.filters.count > 0){
                $('.alarm-navbar .notifications-count').css('display', 'block');
                $('.alarm-navbar .notifications-count').html(this.collection.filters.count);
            }           
        }
    });

    return NotificationNavbarView;
});
