define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',            
    'text!views/templates/explore_item.html',        
    ], function ($, jqueryUi , _, Backbone, ExploreItemTpl) {
        var ExploreItemView = Backbone.View.extend({
            tagName   : 'li',
            className : 'box-item explore-item',
            events : {
              
            },

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },

            render    : function() {     
                this.template = _.template(ExploreItemTpl);
                var model = this.model.toJSON();  
                this.$el.html(this.template(this.model.toJSON()));                                
                return this;
            }                   
           
        });

        return ExploreItemView;
    });
