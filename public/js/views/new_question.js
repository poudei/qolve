define([
    'jquery',
    'underscore',
    'backbone',
    'models/question',
    'text!views/templates/new_question.html',
    'text!views/templates/img_uploaded.html',        
    'i18n!nls/labels',
    'libs/browserplus-min',
    'libs/plupload.full',
    'datetimepicker',    
    'jquery-helper',
    'jquery-ui',
    'bootstrap-select',    
    'mentionInput',
    'elastic',    
    'tagit',
    'semantic',
], function($, _, Backbone, QuestionModel, NewQuestionTpl, ImgUploadedTpl, Labels, BrowserPlus, Plupload, Datepicker, JqueryHelper, JqueryUi, BootstarpSelect, Elastic, Tagit, Semantic) {
    var NewQuestionView = Backbone.View.extend({
        className: 'new-question',
        events: {
            'click      .send-question'                     : 'sendQuestion',
            'click      .select-levels'                     : 'showLevels',
            'click      .show-dialog-select-level .box'     : 'selectLevel',
            'click      .show-dialog-select-level label'    : 'selectLevel',
            'click      .show-credit-slider-wrapper'        : 'showSlider',
            'click      .del-img-upload'                    : 'removeUploadedDoc',
            'click      .visit-img-upload'                  : 'previewUploadedDoc',
            'click      .ask-private-wrapper'               : 'showSharePeople',  
            'click      .private-answer'                    : 'privateAnswer',
            'click      .deadline-choose .radio label'      : 'setDeadline',
            'keyup      .description'                       : 'removeErrors',
            'click      .deadline-choose label'             : 'setDeadline',
            'click      .ask-anonymous .box'                : 'setToggleActiveStyle',
            'click      .ask-anonymous label'               : 'setToggleActiveStyle'            
        },
                
        initialize: function(opt) {             
            var $this = this;
            _.bindAll(this, 'render');
            
            if(opt && opt.id){
              this.model = new QuestionModel({id : opt.id});
              this.model.fetch({
                  success : function(model, resp){
                      $this.fillData();
                  }
              });
            }            
            else
              this.model = new QuestionModel();
           
            app = require('app'); 
            app.getUserFollowers();
            app.getKeywords();
            app.updateCredits(function(credit){             
                if(opt && opt.id){
                     $(".slider-credit", this.$el).slider( "option", "max", Math.min(50, $this.model.get('credits') + window.current_user.credit ));
                }
            });   
            this.shareUsers = [];          
        },
         
        fillData : function(){ 
            var $this = this;
            
           /* fill description*/
           
           $('#new-question-form [name=description]').val($this.model.get('description'));
           
           /* fill docs*/
            
            _.each(this.model.get('documentsList'), function(doc) {
                var imgUploadedTpl = _.template(ImgUploadedTpl);
                $('.new-uploaded-img', this.$el).append(imgUploadedTpl({
                    location    : doc.location,
                    id          : doc.id,
                    mode        : 'edit'
                }));
                
                $('#' + doc.id + ' img').show();
                var size = $.fn.ResizeImg(doc.height, doc.width, '', 120);
                $('#' + doc.id + ' img').attr('src', doc.location + '?width=' +  size.width + '&height=' + size.height);              
                $('.new-uploaded-img .image-uploaded:last').data('upload', doc);
            });
            
            /* fill levels*/
            
             _.each(this.model.get('levels'), function(level) {
                  var el = $(".select-level-question[value='" + level.toLowerCase() + "']");
                  $("label", el.closest('li')).trigger('click');
             });
             
             /* set credits*/
             
            
            $(".slider-credit", this.$el).slider( "value", this.model.get('credits'));
            $(".slider-credit", this.$el).slider( "option", "min", this.model.get('userCredit'));
            $(".slider-credit", this.$el).slider( "option", "max", Math.min(50, this.model.get('userCredit') + window.current_user.credit ));
            
            if(this.model.get('credits') > 0){
                $('.show-credit-slider-wrapper .show-credits').html(this.model.get('credits') + ' ' + Labels['Credits']);
                $('#credits-val', this.$el).val(this.model.get('credits'));
                $('.private-answer').show();
            }else{
                $('.private-answer').hide();
            }
            
            /* set language*/
            
            $('.select-lan', this.$el).selectpicker('val', this.model.get('language'));
            
            /* set privacy & visibility*/
            if(this.model.get('privacy') == 2){
                $('.ask-private-wrapper label', this.$el).trigger('click');
                
                  /* set share people*/                
                _.each(this.model.get('share'), function(item, index){
                    $('.tagit-new input').val(item.username);
                    $('.tagit-new input').trigger('blur');
                });   
                this.model.set('privacy', 1);
            }
            
            if(this.model.get('visibility') == 2){
                $('.ask-anonymous label', this.$el).trigger('click');
                this.model.set('visibility', 1);
            }
            
            if(this.model.get('answerPrivilege') == 2){
                $('.private-answer label', this.$el).trigger('click');
                this.model.set('answerPrivilege', 1);
            }
            
            /*set dieadline*/
            if(this.model.get('deadline') != undefined && this.model.get('deadline') != ''){
                 $('.deadline-item label[data-type=deadline-selected]').trigger('click');                 
                 $(".form_datetime ", this.$el).datetimepicker('setDate',new Date(this.model.get('deadline')));
            }else{
                $('.deadline-item label[data-type=no-deadline]').trigger('click');
            }       
            
            $('.send-question', this.$el).html(Labels['update_problem']);            
        },
                
        sendQuestion: function() {    
            var $this = this;
            this.docs = [];            
            
            $("input[name='levels[]']:checked").each(function() {
                 $this.levels.push($(this).val());
            });
            
            _.each($('.new-uploaded-img li'), function(el) {
                var doc = $(el).data('upload');
                doc.order = $this.docs.length + 1;
                $this.docs.push(doc);
            });            
            
            if(!this.validate()) return;          
            
            var question = $('#new-question-form').serializeJSON(),
                    obj = _.extend(question, {
                files   : this.docs,
                levels  : this.levels.join(','),
                share   : this.findShares(),
                keyword : this.findKeys()                
            });
            
            if(question.deadline  == '')           
                obj.deadline = null;
            
            this.model.save(obj, {
                success: function(resp) {
                    Backbone.history.navigate('home', true);
                }
            });
        },
        
        render: function() {
            this.newQuestionTemplate = _.template(NewQuestionTpl);
            $(this.$el).html(this.newQuestionTemplate({
                labels : Labels
            }));          
            this.afterRender();
            return this;
        },
                
        closeWinModal : function(e){ 
          return 'You may lose your data';          
        },       
                
        removeErrors : function(){
            $('.show-errors-des').hide();
            $('.show-errors-focus').removeClass('show-errors-focus');
            $('.alert-error-qolve ', this.$el).css('display', 'none');
        },
                
        setToggleActiveStyle : function(){
            $('.ask-anonymous', this.$el).toggleClass('active-item');
        },
                
        validate : function(){
            var form = $('#new-question-form').serializeJSON();
            this.removeErrors();
            
            if ((form.description == "" && $('.new-uploaded-img li').length == 0)) {
                $('.alert-error-qolve ', this.$el).css({
                    'display': 'block',
                    'visibility': 'visible'
                });
                $('.mentions-input-box').prepend('<span class="show-errors-des"></span>');
                $('.mentions-input-box .show-errors-des').show();
                $('.description').addClass('show-errors-focus');
                $('.alert-error-qolve .message-err').html(Labels['fill_description_or_upload_your_problem_image']);                
                
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }
            
            if (form.privacy == '2' && this.findShares().length == 0) {                
                $('#share-people').prepend('<span class="show-errors-des"></span>');
                $('#share-people .show-errors-des').show();
                $('#share-people').addClass('show-errors-focus');
                $('#share-people ul').addClass('show-errors-focus');
                $('.alert-error-qolve ', this.$el).css({
                    'display': 'block',
                    'visibility': 'visible'
                });
                $('.alert-error-qolve .message-err').html(Labels['you_have_mention_one']);
                
                 $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }
            
           this.removeErrors();                       
           return true;            
        },  
                
        findKeys : function(){
            var keys = [];                     
            var regExp = /#([a-z][A-Z])*/g, 
                   ids = $('.description').val().split(' ');
           
            for (var i = 0; i < ids.length; i++) {
                if (regExp.test(ids[i]))
                    keys.push(ids[i])                        
            }
            
            return keys;
        },
                
        findShares : function(){
            var shares = [];
            _.each($('.tagit-choice .tagit-label'), function(el){
                shares.push({
                   username : $(el).html(),
                   id      : '' 
                });
            });
            
            return shares;
        },
                
        afterRender: function() {                        
            var startDate =  new Date();
            $(".form_datetime", this.$el).datetimepicker({
                format      : "yyyy-mm-dd  hh:ii",
                autoclose   : true,
                minuteStep  : 5,
                todayBtn    : true,
                startDate   : this.findStartDate()
            }).on('changeDate', function(ev){
                if(!$('input[type=radio]', $('.set-deadline[data-type=deadline-selected]').closest('.deadline-item')).is(':checked')){
                    $('.set-deadline[data-type=deadline-selected] ').trigger('click');                
                }
            });             
            
            app.autoCompleteUsersAndKeywords($('.description', this.$el), this.shareUsers);             
            
            $('.private-answer', this.$el).hide();            

            $('#share-people input', this.$el).tagit({     
                placeholderText : Labels['add_people'],
                animate         : false,
                afterTagAdded   : function(){
                    if($('.tagit.ui-widget li', this.$el).length > 1){
                        $('.tagit.ui-widget li.tagit-new input', this.$el).attr('placeholder', '');
                    }else{
                        $('.tagit.ui-widget li.tagit-new input', this.$el).attr('placeholder', Labels['add_people']);
                    }
                }, 
                beforeTagRemoved : function(){
                    if($('.tagit.ui-widget li', this.$el).length > 2){
                        $('.tagit.ui-widget li.tagit-new input', this.$el).attr('placeholder', '');
                    }else{
                        $('.tagit.ui-widget li.tagit-new input', this.$el).attr('placeholder', Labels['add_people']);
                    }
                }, 
                autocomplete: {                    
                    source: function(search, showChoices) {                        
                        var filter = search.term.toLowerCase();
                        var choices = $.grep(app.followers.toJSON(), function(element) {
                            // Only match autocomplete options that begin with the search term.
                            // (Case insensitive.)
                            return (element.username.toLowerCase().indexOf(filter) === 0);
                        });
                        var drpDown= [];
                        for(var i = 0; i < choices.length ; i++)
                            drpDown.push(choices[i].username);
                        return  showChoices(drpDown);
                    }
                },
                allowSpaces: true                
            });
                       
            $('.select-lan', this.$el).selectpicker();          
            $('.ui.checkbox', this.$el).checkbox();
            
            $(".slider-credit", this.$el).slider({
                min: 0,
                step: 1,
                max: Math.min(parseInt(window.current_user.credit), 50),
                slide: function(event, ui) {
                    $('.slider-val').setToFixedWith(17);
                    
                    if(ui.value == 0){
                        $('.private-answer').hide();
                    }else{
                        $('.private-answer').show();
                    }
                    
                    if(ui.value > 1){
                        var credits = ui.value + ' Credits';
                    }else{
                        var credits = ui.value + ' Credit';
                    }
                    
                    $('.slider-val').html(credits);
                    $('#credits-val').val(ui.value);
                    $('.show-credits').html(credits);
                }
            });
            
            $( ".new-uploaded-img", this.$el).sortable({
                dropOnEmpty : false
            });          
            
            this.setInterval();
        },
        
        addUploader: function(){           
            this.levels =[];
            var $this = this;
            var uploader = new plupload.Uploader({
                runtimes        : 'html5,html4',
                file_data_name  : 'post',
                multi_selection : false,
                browse_button   : 'pickfiles',
                container       : 'new-image-upload',
                max_file_size   : '10mb',       
                progress        : true,
                url             : '/api/uploads/file?order=1',
                filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }]
            });          
            
            uploader.init();
            uploader.bind('FilesAdded', function(up, files) {
                var imgUploadedTpl = _.template(ImgUploadedTpl);
                $('.new-uploaded-img', this.$el).append(imgUploadedTpl({
                    location    : '',
                    id          :  files[0].id,
                    mode        :  'create'
                }));
                
                up.refresh();
                uploader.start();
            }); 

            uploader.bind('UploadProgress', function(up, file) {                
                $('.progress-bar').width(file.percent + "%");
                $('.progress-bar span' , this.$el).html(file.percent + "%");
            });

            uploader.bind('FileUploaded', function(up, file, resp) {
                var resp = $.parseJSON(resp.response).file;
                $('#' + file.id + ' img').show();
                var size = $.fn.ResizeImg(resp.height, resp.width, '', 120);
                $('#' + file.id + ' img').attr('src', resp.location + '?width=' +  size.width + '&height=' + size.height); 
                $('#' + file.id + ' .progress').hide();
                $('#' + file.id).data('upload', resp);
            });                         
        },
                
        showSlider: function(e) {
            if ($('.show-credit-slider').css('display') == 'none') {
                $('.show-credit-slider-wrapper').removeClass('border-design');
                $('.show-credit-slider-wrapper .drop-down-icon').rotate(180,5,0);   
            }else{
                $('.show-credit-slider-wrapper .drop-down-icon').rotate(0,-5,180);
            }
            $('.show-credit-slider').slideToggle(function() {                                
                $('.show-credit-slider').css('display') == 'none' && $('.show-credit-slider-wrapper').addClass('border-design');
            });
        },
                
        showLevels: function(e) {
            if ($('.show-dialog-select-level').css('display') == 'none') {
                $('.select-levels').removeClass('border-design');
                $('.select-levels .drop-down-icon').rotate(180,5,0);
            }else{
                $('.select-levels .drop-down-icon').rotate(0,-5,180);                
            }
            $('.show-dialog-select-level').slideToggle(function() {
                $('.show-dialog-select-level').css('display') == 'none' && $('.select-levels').addClass('border-design');
            });
        },
                
        selectLevel: function(e){
            var count = 0;
            _.each($('.select-level-question'), function(e){                
                if($(e).is(":checked"))
                    count ++;
            });            
            
            if(count > 0){
                count == 1 ? $('.selected-levels-count').html(count + ' ' + Labels['level_selected']) : $('.selected-levels-count').html(count + ' ' + Labels['levels_selected']) ;
            }else{ 
                $('.selected-levels-count').html('');                
            }
            
        },
                
        removeUploadedDoc : function(e){
            var el = $(e.target).hasClass('image-uploaded') ? $(e.target) : $(e.target).closest('.image-uploaded');
            el.remove();
        },
                
        setDeadline : function(e){
            if($(e.target).data('type') == 'no-deadline'){
                $('.dead-line-value input', this.$el).val('');
            }else{
                if($('.deadline-choose input[name=deadline]').val() == ""){
                    $('.today').trigger('click');
                }
            }
        },
        
        privateAnswer : function(){
            if(!$('.private-answer', this.$el).hasClass('active-item')){
                $('.private-answer input', this.$el).attr('checked', 'checked');
                $('.private-answer', this.$el).addClass('active-item');
            }else{
                $('.private-answer', this.$el).removeClass('active-item');
                $('.private-answer input', this.$el).removeAttr('checked');
            }            
        },
                
        showSharePeople: function(e) {
            if ($('#share-people').css('display') == 'none') {
                $('.ask-private-wrapper').removeClass('border-design');               
                $('#share-people .ui-widget-content').css('overflow', 'hidden');
            }
            this.removeErrors();
            $('.ask-private-wrapper', this.$el).toggleClass('active-item');            
            if($('.ask-private-wrapper', this.$el).hasClass('active-item'))            
                $('#ask-private', this.$el).attr('checked', 'checked');
            else
                $('#ask-private', this.$el).removeAttr('checked');
            
            $('#share-people', this.$el).slideToggle({
                done : function(){
                    if( $('#share-people').css('display') == 'none'){
                     $('.ask-private-wrapper').addClass('border-design');
                     $('#share-people').removeAttr('placeholder');                     
                     $('#share-people').blur(); 
                }else{                    
                    $('#share-people .ui-widget-content').css('overflow', 'auto');
                    $('#share-people').attr('placeholder', 'Add People...');
                    $('#share-people').focus();
                }   
                }
            });
        },
        
        previewUploadedDoc : function(e){
              var el = $(e.target).hasClass('image-uploaded') ? $(e.target) : $(e.target).closest('.image-uploaded'),
              upload = $(el).data('upload');
              var h = upload.height;
              var w = upload.width;
              var size = $.fn.ResizeImg(h, w);
              upload.location = (upload.location[0] == '/') ? upload.location : '/' + upload.location;
              $('.modal-preview').css({
                    'width'          : size.width,
                    'left'           : '50%',
                    'margin-left'  : -(size.width / 2)
              });
              $('.modal-preview #uploaded-preview-img').attr('src', upload.location + '?width=' + size.width + '&height=' + size.height);              
              $('.modal-preview').modal('show');
        },
        
        findStartDate : function(){
            var startDate = new Date();
            return startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate() + ' ' + startDate.getHours() + ':' +  startDate.getMinutes();
        },
                
        setInterval : function(){
            setInterval(function($this){                  
               $(".form_datetime", $this.$el).datetimepicker('setStartDate', $this.findStartDate());
            },300000, this);
        },       
    });

    return NewQuestionView;
});
