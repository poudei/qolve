define([
    'jquery',
    'backbone',
    'underscore',
    'app',    
    'text!views/templates/help.html',  
    'text!views/templates/faq.html',  
    'i18n!nls/labels',    
    ], function ($, Backbone, _, App, HelpTpL, FAQTpl, Labels) {

        var HelpView = Backbone.View.extend({
            className           : 'page-help',
                    
            initialize : function(opt) {
                var $this = this;
                opt = opt || {};

                if(opt.type == 'faq'){
                    this.template = _.template(FAQTpl);                                
                }else{
                    this.template = _.template(HelpTpL);                                
                }               

            },            

            render : function() {
                this.$el.html(this.template({
                    labels  : Labels
                }));                                  
                
                return this;
            }

        });

        return HelpView;
    });
