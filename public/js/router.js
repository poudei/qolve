define([
    'jquery',
    'underscore',
    'backbone',
    'views/landing',
    'views/home',
    'views/user_page',
    'views/question_page',
    'views/edit_profile',
    'views/new_question',
    'views/explore_list',    
    'views/notifications_list',
    'views/signup',
    'views/login',
    'views/find_friends',
    'views/forgotPassword',
    'views/resetPassword',
    'views/activities_list',
    'views/increase_credits',
    'views/adminList',
    'views/help'
], function($, _, Backbone, LandingView, HomeView, UserPageView, QuestionPageView, EditProfileView, NewQuestionView, ExploreView, NotificationsListView, SignupView, LoginView, FindfriendsView, ForgotPasswordView, ResetPasswordView, ActivitiesListView, IncreaseCreditsView, AdminListView, HelpView) {
    var AppRouter = Backbone.Router.extend({
        routes: {                    
            ''                      : 'landingPage',
            'home'                  : 'home',
            'signIn'                : 'login',
            'users/:id'             : 'userPage',
            'users/:id/me'          : 'currentUserPage',
            'questions/:id'         : 'questionPage',
            'settings'              : 'editProfile',
            'activity'              : 'activity',
            'newQuestion'           : 'newQuestion',
            'editQuestion/:id'      : 'newQuestion',
            'explore'               : 'explore',
            'admin'                 : 'adminView',
            'explore/:key'          : 'explore',            
            'notifications'         : 'notifications',
            'signup/:step'          : 'signup',    
            'findFriends'           : 'findfriends',
            'findFriends/:social'   : 'findfriends',    
            'forgotPassword'        : 'forgotPassword',
            'forgotPassword/:id'    : 'forgotPassword',
            'resetPassword'         : 'resetPassword',
            'resetPassword/:id'     : 'resetPassword',            
            'increaseCredits'       : 'increaseCredits',
            'help'                  : 'help',
            'FAQ'                   : 'faq'
        },
                
        before: function(id, route) {
            var page = route.split('/')[0];                       
            $('#main-container').removeClass('main-container-custom-design');            
            this.setStyle(page);            
            
            if(page != 'users' && page !='FAQ' && page != 'help' && page != 'questions' && page != 'explore' && page != '' && page != 'forgotPassword' && page != 'signIn' && page != 'resetPassword'){
                if (window.current_user == undefined || window.current_user.id == undefined) {
                    Backbone.history.navigate('', true);
                    return false;
                }
            }
            
            $('body').removeClass('body-background');
        },
        
        landingPage: function(){    
            if(window.current_user && window.current_user.id){
                Backbone.history.navigate('home', true);
                return;
            }
            
            this.landingView && this.landingView.remove();            
            this.landingView = new LandingView();       
            $('#main-container').append(this.landingView.render().$el);
            this.landingView.onAppend();
        },    
                
        home: function() {
            this.homeView && this.homeView.remove();                            
            this.homeView = new HomeView();
            $('#main-container').html(this.homeView.render().$el);
        },
        
        login: function() {
            this.loginView && this.loginView.remove();                            
            this.loginView = new LoginView();
            $('#main-container').html(this.loginView.render().$el);
            this.loginView.onAppend();
        },
        
        userPage: function(id){
            this.userPageView && this.userPageView.remove();            
            this.userPageView = new UserPageView({
                id : id
            });
            $('#main-container').html(this.userPageView.$el);
        },
                
        currentUserPage: function(id){
            this.userPageView && this.userPageView.remove();            
            this.userPageView = new UserPageView({
                id      : id,
                page    : 'me'
            });
            $('#main-container').html(this.userPageView.$el);
        },
                
        questionPage: function(id) {
             this.questionPageView && this.questionPageView.remove();
             this.questionPageView = new QuestionPageView({
                id : id
            });
            $('#main-container').html(this.questionPageView.$el);
        }, 
                        
        editProfile : function(){
            this.editProfileView && this.editProfileView.remove();
            this.editProfileView = new EditProfileView();
            $('#main-container').html(this.editProfileView.$el);
        },
                
        newQuestion : function(id){
            this.newQuestionView && this.newQuestionView.remove();
            this.newQuestionView = new NewQuestionView({
                id   : id
            });
            $('#main-container').html(this.newQuestionView.render().$el);
            this.newQuestionView.addUploader();
        },
                
        explore : function(key){
            this.exploreView && this.exploreView.remove();
            this.exploreView = new ExploreView({
                key : key
            });
            $('#main-container').html(this.exploreView.render().$el);            
        },
                
        notifications : function(){
            this.notificationsListView && this.notificationsListView.remove();
            this.notificationsListView = new NotificationsListView();
            $('#main-container').html(this.notificationsListView.render().$el);            
        },
                
       signup : function(step){
            this.signupView && this.signupView.remove();            
            this.signupView = new SignupView({'step' : step});
            $('#main-container').html(this.signupView.render().$el);            
            step == 'register' && this.signupView.onAppend();
       },
       
       findfriends : function(social){
            this.findfriendsView && this.findfriendsView.remove();            
            this.findfriendsView = new FindfriendsView({'social' : social});
            $('#main-container').html(this.findfriendsView.$el);                        
       },
       
       forgotPassword : function(){
            this.forgotPasswordView && this.forgotPasswordView.remove();            
            this.forgotPasswordView = new ForgotPasswordView();
            $('#main-container').html(this.forgotPasswordView.$el);                        
       },
               
       resetPassword : function(token){
            this.resetPasswordView && this.resetPasswordView.remove();            
            this.resetPasswordView = new ResetPasswordView({
                token : token
            });
            $('#main-container').html(this.resetPasswordView.$el);
            this.resetPasswordView.onAppend();
       },
       
       activity : function(){
            this.activitiesListView && this.activitiesListView.remove();            
            this.activitiesListView = new ActivitiesListView();
            $('#main-container').html(this.activitiesListView.render().$el);                        
       },     
       
       increaseCredits : function(){
            this.increaseCreditsView && this.increaseCreditsView.remove();            
            this.increaseCreditsView = new IncreaseCreditsView();
            $('#main-container').html(this.increaseCreditsView.render().$el);                        
       },    
       
       adminView : function(){
            this.adminListView && this.adminListView.remove();            
            this.adminListView = new AdminListView();
            $('#main-container').html(this.adminListView.render().$el);                        
       },

       help : function(){
            this.helpView && this.helpView.remove();            
            this.helpView = new HelpView();
            $('#main-container').html(this.helpView.render().$el);                        
       },

        faq : function(){
            this.helpView && this.helpView.remove();            
            this.helpView = new HelpView({
                type : 'faq'
            });
            $('#main-container').html(this.helpView.render().$el);                        
        },
               
       setStyle: function(page) {
            switch (page) {
                case '':
                    $('#main-container').addClass('main-container-nomiddel');
                    $('.navbar-fixed-top').css('display', 'none');                    
                    $('#main-container').css('margin-top', '0px');
                    break;
                case 'explore':
                    $('.footer-wrapper').css('display', 'block');
                    $('#main-container').css('margin-top', '40px');
                    $('.navbar-fixed-top').css('display', 'block');
                    break;
                case 'signup':
                    $('.footer-wrapper').css('display', 'block');
                    $('#main-container').css('margin-top', '170px');
                    $('.navbar-fixed-top').css('display', 'none');
                    $('#main-container').css('border-top', '1px solid #ccc');
                    $('#main-container').addClass('main-container-custom-design');
                    break;                
                case 'newQuestion':
                case 'editQuestion':
                case 'FAQ': 
                case 'findFriends':
                case 'lists':
                case 'notifications':
                case 'activity':
                    $('.footer-wrapper').css('display', 'block');
                    $('#main-container').css('margin-top', '40px');
                    $('.navbar-fixed-top').css('display', 'block');
                    $('#main-container').addClass('main-container-custom-design');
                    break;
                case 'help':                 
                    $('.footer-wrapper').css('display', 'block');
                    $('#main-container').css('margin-top', '60px');
                    $('.navbar-fixed-top').css('display', 'block');
                    $('#main-container').addClass('main-container-help-design');   
                    break;
                case 'settings':
                    $('.footer-wrapper').css('display', 'block');
                    $('.navbar-fixed-top').css('display', 'block');
                    $('#main-container').addClass('main-container-custom-design');
                    $('#main-container').css('margin-top', '0px');
                    break;
                case 'questions' :
                    $('.footer-wrapper').css('display', 'block');
                    $('#main-container').removeClass('main-container-nomiddel');
                    $('.navbar-fixed-top').css('display', 'block');
                    $('#main-container').css('margin-top', '80px');
                    break;
                default :
                    $('.footer-wrapper').css('display', 'block');
                    $('#main-container').removeClass('main-container-nomiddel');
                    $('.navbar-fixed-top').css('display', 'block');
                    $('#main-container').css('margin-top', '40px');
                    break;
            }
        }
    });
    
    return AppRouter;
});
