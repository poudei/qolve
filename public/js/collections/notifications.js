define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/notification'
    ], function ($, _, Backbone, BaseCollections, NotificationModel) {

        var NotificationsCollection = BaseCollections.extend({
            model : NotificationModel,                 
                    
            initialize : function(models, opt){
                this.mode = opt.mode;
                NotificationsCollection.__super__.initialize.apply(this, [this.model, opt]);
            },         
        
            url : function(){                                        
                return  '/api/notifications?mode=' + this.mode;                
            },

            parse: function( resp ){                
                var resp = resp.notifications;
               
                if(resp && this.filters){
                    this.filters.count = parseInt(resp.count) ;
                }
                return resp.list;
            }
        });

        return NotificationsCollection;
    });
