define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/question'
    ], function ($, _, Backbone, BaseCollections, QuestionModel) {

        var QuestionCollection = BaseCollections.extend({
            model : QuestionModel,       
        
            initialize : function(models, opt){
                this.opt = opt;                      
                QuestionCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
        
            url : function(){              
                if(this.opt.question_url)
                    return this.opt.question_url;
                return  '/api/questions';                
            },

            parse: function( resp ){                
                var resp = resp.questions;
                if(resp && this.filters){
                    this.filters.count = parseInt(resp.count) ;
                }
                return resp.list;
            }
        });

        return QuestionCollection;
    });
