define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/activity'
    ], function ($, _, Backbone, BaseCollections, ActivityModel) {

        var ActivitesCollection = BaseCollections.extend({
            model : ActivityModel,                 
                    
            initialize : function(models, opt){
                this.mode = opt.mode;
                ActivitesCollection.__super__.initialize.apply(this, [this.model, opt]);
            },         
        
            url : function(){                                        
                return  '/api/activities'     ;
            },

            parse: function( resp ){         
                var resp = resp.activities;
                if(resp && this.filters){
                    this.filters.count = parseInt(resp.count) ;
                }
                return resp.list;
            }
        });

        return ActivitesCollection;
    });
