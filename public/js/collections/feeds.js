define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/feed'
    ], function ($, _, Backbone, BaseCollections, QuestionModel) {

        var FeedCollection = BaseCollections.extend({
            model : QuestionModel,       
        
            initialize : function(models, opt){
                this.opt = opt;               
              
                FeedCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
        
            url : function(){               
                if(this.opt._userId){
                    return '/api/users/' + this.opt._userId + '/timeline';  
                }                
                return  '/api/feeds';                
            },

            parse: function( resp ){
                var feeds = resp.feeds;
                if(this.opt._userId){
                     var feeds = resp.timeline;
                 }         
                 
                if(resp && this.filters){
                    this.filters.count = parseInt(feeds.count) ;
                }                
                if(feeds.list.length < this.filters.get('limit'))
                    this.filters.count = 0;
                    
                return feeds.list;
            }
        });

        return FeedCollection;
    });
