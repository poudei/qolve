define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/list'
    ], function ($, _, Backbone, BaseCollections, ListModel) {

        var ListCollection = BaseCollections.extend({
            model : ListModel,                                     
            initialize : function(models, opt){
                this.opt = opt || {};               
                ListCollection.__super__.initialize.apply(this, [this.model, opt]);
                this.filters.set('limit', 1000);
            },        
        
            url : function(){                                    
                if(this.opt && this.opt.mode && this.opt._userId){                    
                    return '/api/lists?userId=' + this.opt._userId + '&mode=' + this.opt.mode;
                }else{
                    if(this.opt && this.opt.mode && !this.opt._userId){                    
                        return '/api/lists?&mode=' + this.opt.mode;
                    }else{
                        if(this.opt && this.opt._userId){                    
                            return '/api/lists?&userId=' + this.opt._userId;
                        }
                    }
                }
                return  '/api/lists?userId=' + window.current_user.id ;                
            },

            parse: function( resp ){                                
                var resp = resp.bookmarkLists,
                  lists  = [];      
                if(resp && this.filters){
                    this.filters.count = parseInt(resp.count) ;
                }                
                _.each(resp.create, function(list){
                    lists.push(list);
                });                
                
                _.each(resp.follow, function(list){
                    lists.push(list);
                });                
                
                return lists;
            }
        });

        return ListCollection;
    });
