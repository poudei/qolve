define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/answer'
    ], function ($, _, Backbone, BaseCollections, AnswerModel) {

        var AnswerCollection = BaseCollections.extend({
            model : AnswerModel,       
        
            initialize : function(models, opt){
                this.opt = opt;                                     
                AnswerCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
        
            url : function(){                          
                var url = '/api/questions/' + this.opt.question_id + '/answers?comment=true';
                return url;
            },

            parse: function( resp ){        
               if(resp && this.filters){
                    this.filters.count = resp.answers.count ;
               }               
               return resp.answers.list;
            }
        });

        return AnswerCollection;
    });
