define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/explore'
], function($, _, Backbone, BaseCollections, ExploreModel) {

    var ExploreCollection = BaseCollections.extend({
        model: ExploreModel,
                
        url: function() {             
            return '/api/explores';
        },
                
        parse: function(resp) {
             resp = resp || {};   
             this.filters.count = resp.count || -1;
             this.keywordFollow = resp.keywordFollow;
             if(resp['trendingKeywords'])
                 return resp;             
             
             return resp.questions;
        }
    });

    return ExploreCollection;
});
