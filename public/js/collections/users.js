define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/user'
], function($, _, Backbone, BaseCollections, UserModel) {

    var UsersCollection = BaseCollections.extend({
        model: UserModel,
        
        initialize : function(models, opt){
            opt = opt || {};
            this.optUrl = opt.url;
            UsersCollection.__super__.initialize.apply(this, []);
            
           
        },
                
        url: function() {         
            if(this.optUrl){               
                return this.optUrl;
            }             
            
            return '/api/followers';                        
        },
                
        parse: function(resp) {           
           resp = (resp.followers) ? resp.followers : (resp.followings ? resp.followings : (resp.users ? resp.users : resp));             
           
           if(resp && this.filters){
                this.filters.count = parseInt(resp.count) ;               
           }
           return ((resp.list) ? resp.list : resp);
        }
    });

    return UsersCollection;
});
