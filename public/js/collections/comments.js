define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/comment'
    ], function ($, _, Backbone, BaseCollections, CommentModel) {

        var CommentCollection = BaseCollections.extend({
            model : CommentModel,       
        
            initialize : function(models, opt){
                this.opt = opt;                      
                CommentCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
        
            url : function(){   
                if(this.opt['type'] == 'questions')
                    return '/api/questions/' + this.opt['questionId'] + '/comments';
                else
                    return '/api/questions/' + this.opt['questionId'] + '/answers/' + this.opt['answerId'] + '/comments';                
            },

            parse: function( resp ){               
                var comments = resp.comments,
                    list     = [];    
                
                if(resp && this.filters){
                    this.filters.count = comments.count ;
                }
                
                for(var i = comments.list.length - 1 ; i >= 0; i--){
                    if(this.opt['type'] == 'questions'){
                         comments.list[i].questionId =  this.opt['id'];
                    }else{
                        comments.list[i].questionId = this.opt['questionId'];
                        comments.list[i].answerId =  this.opt['id'];
                    }
                    list.push(comments.list[i]);
                }                
                return list;
            }
        });

        return CommentCollection;
    });
