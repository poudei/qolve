define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/explore'
], function($, _, Backbone, BaseCollections, ExploreModel) {

    var AdminCollection = BaseCollections.extend({
        model: ExploreModel,
                
        url: function() {             
            return '/admin/audits/question';
        },
                
        parse: function(resp) {
             resp = resp || {};   
             this.filters.count = resp.count || -1;         
             return resp.questions;
        }
    });

    return AdminCollection;
});
