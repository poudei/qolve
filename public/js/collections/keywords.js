define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/collection',
    'models/keyword'
], function($, _, Backbone, BaseCollections, KeywordModel) {

    var KeywordsCollection = BaseCollections.extend({
        model: KeywordModel,
        
        initialize: function(models, opt) {
            var opt = opt || {};
            this.optUrl = opt.url;
            KeywordsCollection.__super__.initialize.apply(this, [this.model, opt]);
            this.filters.set('limit', 1000);
        },   
                
        url: function() {
            if(this.optUrl){
                return this.optUrl;
            }  
            return '/api/keywords';
        },
                
        parse: function(resp) {
           var resp = resp.keyword ? resp.keyword : resp.keywords;
           resp = resp.list ? resp.list : resp;
           return resp;
        }
    });

    return KeywordsCollection;
});
