define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var FilterModel = Backbone.Model.extend({
        defaults: {
            limit: 20,
            offset: 0
        }
    });
    return FilterModel;
});


