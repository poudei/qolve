define([
    'jquery',
    'jquery-ui',
    'underscore',
    'backbone',           
    'views/comments_list',     
    'models/list',
    'text!views/templates/share_users.html',
    'i18n!nls/labels',
    'copy-clipboard',
    'enscroll',
    'elastic',        
    'semantic',
    'animo'
    ], function ($, jqueryUi , _, Backbone, CommentsListView, ListModel, ShareUsersTpl, Labels) {
        var ItemView = Backbone.View.extend({
            tagName   : 'li',
            className : 'box-item',                          
                    
            reaskQuestion : function(){
                var question = this.model.get('question') ? this.model.get('question') : this.model.toJSON();
                
                if((question && (question.isAsked == '21'|| question.isAsked == '22' || 
                    question.isAsked == '12')|| question.isAsked == '11')){
                    var userCredit = question ? question.userCredit : this.model.get('userCredit');                    
                     $(".slider-reask").slider({min : parseInt(userCredit)});
                     $(".slider-reask").slider({max : Math.min(50, parseInt(userCredit) + window.current_user.credit )});
                     $(".slider-reask").slider('value', parseInt(userCredit));
                     
                     if(parseInt(userCredit) == 0)
                        $('.modal-reask .slider-mount-shown').html(Labels['free']);
                     else
                         $('.modal-reask .slider-mount-shown').html(userCredit);
                     
                     if(question.isAsked == '12'|| question.isAsked == '22')
                        $('.modal-reask .set-visibility input').trigger('click');
                    
                    $('.modal-reask .reask-btn').html(Labels['raise']);
                }else{
                    $('.modal-reask .reask-btn').html(Labels['reask']);
                    if(question.credits != 0){
                        $(".slider-reask").slider({min : 1});
                        $(".slider-reask").slider('value', 1);
                        $('.modal-reask .slider-mount-shown').html(1);
                    }else{
                        $(".slider-reask").slider({min : 0});
                        $(".slider-reask").slider('value', 0);
                        $('.modal-reask .slider-mount-shown').html(Labels['free']);
                    }                    
                }
                
                if(question.isAsked == 0 && (window.current_user.credit < question.credits) && question.credits != 0){
                    $('.modal-reask form').hide();
                    $('#not-enough-credits').show();
                }else{
                    $('.modal-reask form').show();
                    $('#not-enough-credits').hide();
                }
                
                $('.modal-reask').modal('show');               
                $('.modal-reask').data('questionid', question.id);
            },
                    
            report : function(){
                var item = this.model.get('question') ? this.model.get('question') : this.model.toJSON();
                
                $('.modal-report').modal('show');
                $('.modal-report').data('itemid', item.id);            
            },       
                    
            shareQuestionUrl : function(){
                var item = this.model.get('question') ? this.model.get('question') : this.model.toJSON();
                
                $('.modal-share').modal('show');
                $('.modal-share').data('itemid', item.id);            
            },   
            
            clickOnMenu : function(){return},
            
            afterRender : function(){                              
                $('[data-toggle=tooltip]', this.$el).tooltip();                   
            },
                    
            followShareUser: function(e) {
                var el = $(e.target),
                        userId = el.attr('id'),
                        isFollow = el.data('isfollowed');

                if(isFollow == 1){
                    $(el).removeClass('unfollow-user-share').addClass('follow-user-share');
                    $(el).html('+ ' + Labels['follow']);
                }else{ 
                    $(el).removeClass('follow-user-share').addClass('unfollow-user-share');
                    $(el).html('− ' + Labels['unfollow']);
                }
                                
                el.data('isfollowed', isFollow == 0 ? 1 : 0);
                
                $.ajax({
                    url  : isFollow == 0 ?  '/api/followings' : '/api/followings/' + userId,
                    data : {
                        "userId": userId
                    },
                    type: isFollow == 0 ? 'POST' : 'DELETE',
                    dataType: 'json',
                    success: function(resp) {
                    },
                    error: function(resp) {
                    }
                }, this);
            },
                    
            showShareMenu : function(){
                if($('.modal-share-user', this.$el).length == 0){
                    this.template = _.template(ShareUsersTpl);
                    
                    var question = this.model.get('question');
                    
                    if(!question)
                        this.$el.append(this.template({
                          share  : this.model.get('share'),
                          labels : Labels      
                        }));
                    else
                        this.$el.append(this.template({
                            'share' : this.model.get('question').share,
                            labels  : Labels      
                        }));
                    $('.modal-share-user', this.$el).on('hide.bs.modal', function(){$('.modal').addClass('vclose')});
                    $('.modal-share-user', this.$el).on('hidden.bs.modal', function(){$('.modal').removeClass('vclose')}); 
                    $('.modal-share-user .modal-body', this.$el).enscroll({
                        showOnHover: false,
                        verticalTrackClass: 'track3',
                        verticalHandleClass: 'handle3'
                    });
                }
                $('.modal-share-user', this.$el).modal('show');
            },
                    
            showMoreMenu : function(){
                $('.more-items-menu', this.$el).toggle();
                $('.copy-link-clipboard', this.$el).zclip('remove');
                $('.copy-link-clipboard', this.$el).zclip({
                    path : '/js/libs/ZeroClipboard.swf',
                    copy : function(){return window.location.origin + $('.copy-link-clipboard', this.$el).data('link')}
                });
            },
                    
            toggleVote : function(e){
                var el = $(e.target).data('score') != undefined || $(e.target).hasClass('vote-item') ? $(e.target) : $(e.target).closest('.vote-item'),
                 score = el.data('score'),
                 $this = this;    
                this.renderVoteTpl(score);                
                $('#vote-animate-wrapper', this.$el).animo({animation: 'tada' });                
            },                    
            
            showBookmarkPopover : function(){
                $('.bookmark-question').popover('destroy');
                $('.bookmark-question', this.$el).popover({
                    trigger     : 'manual',
                    placement   : 'left',
                    animation   : true,    
                    html        : true,
                    content: function() {                        
                        var lists = require('app').getCurrentUserLists();                                                
                        var listItems = "<input class='add-list-bookmark' container='.bookmark-question-wrapper' placeholder='Create a new list'/><span class='add-list-btn'></span> <div>" +
                                        "<div><div class='ui checkbox select-list-item-privacy'>" +
                                        "<input checked='checked' class='list-privacy' type='checkbox' name='privacy' value='2'/>" +
                                        "<div class='box'></div>" +
                                        "</div>" +
                                        "<label class='select-list-label-privacy'>Every one can see your list</label>" +
                                        "</div>" +
                                        "<select class='bookmark-list-select'>";                               
                        _.each(lists.toJSON(), function(list){
                            listItems += '<option value="' + list.id + '">' + list.name + '</option>'
                        });
                        listItems += '</select><span class="cancel-btn-list popover-btn">Cancel</span><span class="add-to-list popover-btn">Save to list</span>';                        
                        return listItems;
                    }  
                });                
                $('.bookmark-question', this.$el).popover("show");
                $('.bookmark-list-select', this.$el).selectpicker({
                    noneSelectedText : 'Loading lists ...'
                });
                $('.ui.checkbox', this.$el).checkbox();  
            },
                    
            toggleBookmarkList : function(e){                
                var el = $(e.target).hasClass('bookmark-list-select') ? $(e.target) : $(e.target).closest('.bookmark-list-select');
                if($('select.bookmark-list-select option').length == 0) return;
                $('div.dropdown-menu', el).toggle();
            },      
            
            cancelList : function(){
                $('.bookmark-question').popover('destroy');
            },
                    
            addNewList: function(e) {
                var $this = this;
                if ((e.type == 'keyup' && e.which == 13) || e.type == 'click') {
                    new ListModel().save({
                        name: $('.add-list-bookmark', this.$el).val(),
                        privacy: $('.list-privacy', this.$el).is(':checked') ? 1 : 2
                    }, {
                        success: function(model) {
                           $("<option></option>", {value: model.get('id'), text: model.get('name')}).appendTo('select');
                           $('.bookmark-list-select').selectpicker('refresh');
                           $('.bookmark-list-select').selectpicker('val', model.get('id'));
                           require('app').getCurrentUserLists().add(model);                           
                        }
                    });

                    $('.add-list-bookmark', this.$el).val('');
                    $('.list-privacy', this.$el).is('checked') && ('.list-privacy').trigger('click');
                }
            },
                    
            addToList : function(){
                this.model.addList({
                    list_id  : $('.bookmark-list-select', this.$el).selectpicker('val')
                });
            },
            
            showComments : function(e){                
                if($('.show-comments-list', this.$el).hasClass('comments-shown')){
                    return;
                }
                
                this.$el.css('padding', '0px');
                
                $('.show-comments-list', this.$el).addClass('comments-shown');
                $('ul.data-icon', this.$el).css('border-bottom', '1px solid #e7e7e7');
                $('.show-comments-list .fuzzy-num', this.$el).addClass('fuzzy-num-active');
                $('.show-comments-list .commentInactive', this.$el).removeClass('commentInactive').addClass('commentActive');
                
                if(!this.commentsList){
                    this.commentsList = new CommentsListView(this.model.get('commentsList'), {
                        type        : this.type,
                        id          : this.model.get('id'),
                        questionId  : this._questionId,
                        callee      : this   
                    });
                    $('.comments-list-wrapper', this.$el).html(this.commentsList.$el);
                }                                
            }           
        });

        return ItemView;
    });
