define([
    'jquery',
    'backbone', 
    'router',
    'underscore',
    'collections/users',
    'collections/keywords',
    'collections/explore',
    'jquery-ui',
    'views/notification_navbar',
    'collections/lists',
    'i18n!nls/labels', 
    'elastic',
    'mentionInput',        
], function($, Backbone, Router, _, UserCollection, KeywordsCollection, ExploreCollection, jqueryUi, NotificationNavbar, ListCollection, Labels) {

    var initialize = function() {
        var router = new Router();       
    };    
    
    getUserFollowers = function() {
        if (!this.followers) {
            this.followers = new UserCollection([], {
                url : '/api/users/me/follows'
            });
            this.followers.fetch();
        }
        return this.followers;
    };
    
    getKeywords = function() {
        if (!this.keywords) {
            this.keywords = new KeywordsCollection();
            this.keywords.fetch();
        }
        return this.keywords;
    };
    
    var setNavbarNotifications = function(){
        this.notificationNavbar = new NotificationNavbar();
         $('.alarm-navbar').append(this.notificationNavbar.$el);
    };

    var eventsRegister = function() {       
        $('.loading-menu').bind({
            ajaxStart: function() {
                $('.loading-menu').css('visibility', 'visible');
            },
            ajaxStop : function() {
                $('.loading-menu').css('visibility', 'hidden');
            },
            ajaxError : function(e, error) {
                $('.loading-menu').css('visibility', 'visible');
                switch (error.status) {
                    case 401:
                       window.location.pathname = "/signIn";                       
                       break;
                }
            }
        });              
        
        $('.navbar-fixed-top .show-menu-by-hover').on('click', function(e) {            
            showHideMenu();
            hideNotificationBar(e);
            if (!$(e.target).hasClass('menu-tag'))
                return false;
        });

        $(document).on('click', function(e) {
            var el = $('.navbar-fixed-top .main-menu');
            showHideMenu(true);
            hideMoreMenu(e);
            hideNotificationBar(e);
            hidePopovers(e);
            searchToolbar(e);
        });

        $('#main-container').css('min-height', ($(window).height() - 42));

        $(window).on('resize', function() {
            $('#main-container').css('min-height', ($(window).height() - 42));
        });
    };
    
    var searchToolbar = function(e){
        var el = $(e.target);
        if(!(el.hasClass('search-toolbar') || el.closest('.search-toolbar').length > 0 || el.hasClass('search-toolbar') || el.closest('.search-toolbar').length > 0)){
            $('.search-toolbar').removeClass('search-toolbar-focus');
        }      
    };
    
    var hideMoreMenu = function(e){        
        var el = $(e.target);
        if(!(el.hasClass('more-items-menu') || el.closest('.more-items-menu').length > 0 || el.hasClass('more-items') || el.closest('.more-items').length > 0)){
            $('.more-items-menu').hide();
        }      
    };
    
    var hidePopovers = function(e){
        var el = $(e.target);
        if(!(el.hasClass('popover') || el.closest('.popover').length > 0 || el.hasClass('bookmark-question'))){
            $('.bookmark-question').popover('destroy');
        }      
    };

    var MixinFn = function() {
        _.mixin({
            setImagePath : function(imagePath){
                if((/^\//).test(imagePath))
                    return imagePath;
                if((/^http/).test(imagePath))
                    return imagePath;
                return '/' + imagePath;
            },
            
            fuzzyDate: function(date, user) {
                if (!date && !user)
                    return '';
                var fuzzyDate = dateDiff(new Date(date), new Date(), user);                
                return fuzzyDate;
            },
            
            minimizeDescription : function(des, length){
                if(!des || des == '')
                    return des;
                
                if(des.length < length)
                    return ' : \"' + des + '\"';
                else
                    return  ' : \"' + des.substr(0,40) + '...' + '\"'
            },
            
            changeDescription : function(des, share){
                var share = share || [],
                     keys = des.match(/#[a-zA-Z0-9]+/g);
                _.each(keys, function(key){
                   var key_url = key.split('#'); 
                   des = des.replace(key, '<a class="link-explore-page" href="/explore/' + key_url[1] + '">' + key + '</a>');
                });                
                
                var users = des.match(/@[a-zA-Z0-9._]+/g);
                _.each(users, function(user){                    
                   var user = user.split('@')[1],
                     userId = user;
                   
                   for(var i = 0; i < share.length; i++) {
                       if(share[i].username == user){
                           userId = share[i].username;    
                       } 
                   }
                   
                   if(userId != ''){
                        user = '@' + user;
                        des = des.replace(user, '<a class="link-explore-page" href="/users/' + userId + '"> ' + user + '</a>');
                   }
                });
                
                return des;
            },                    
                    
            fuzzyNum: function(number){            
                var KILO    = 1000,
                    MEGA    = 1000000,
                    GIGA    = 1000000000,
                    number  = parseInt(number);
            
                 if(Math.abs(number) < KILO)
                        return number;
                 if (Math.abs(number) < MEGA)
                        return number/KILO + 'K';
                 if (Math.abs(number) < GIGA)
                     return number/MEGA + 'M';
                 return number/GIGA + 'G';
            },      
                    
            creditCheck: function(date, credit) {
                if (!date && credit == 0)
                    return '';
                if (!date && credit != 0){
                    if(credit == 1)
                        return credit + ' Credit';
                    return credit + ' Credits';
                }                 

                if (credit == 0)
                    return '';

                if (new Date(date) < new Date())
                    return credit + ' Credits';
                
                if(credit == 1)
                    return credit + ' Credit';
                return credit + ' Credits';
            }
        });
    };
    
    var hideNotificationBar = function(e){
      if($(e.target).hasClass('navbar-alarm') || $(e.target).hasClass('notifications-count') || $(e.target).hasClass('alarm-navbar'))  
          return false;
      if($('.notification-navbar-list').css('display') == 'block'){
          $('.notification-navbar-list').toggle();
           $('.alarm-navbar').removeClass('main-menu-hover');
      } 
    };

    var showHideMenu = function(hide) {
        var el = $('.navbar-fixed-top .main-menu');
        if(hide && el.css('display') == 'block'){
            el.toggle();
            $('.show-menu-by-hover').removeClass('main-menu-hover');
        }
        else{
           !hide && el.toggle();
           !hide &&  $('.show-menu-by-hover').toggleClass('main-menu-hover');
        }
    };

    var updateDate = function() {       
        window.fuzzyDateIn && window.clearInterval(window.fuzzyDateIn);
        window.fuzzyDateIn = window.setInterval(function() {
            _.each($('.update-time'), function(el) {
                var date = $(el).data('date'),
                    user = $(el).data('user');                
                $(el).html(_.fuzzyDate(new Date(date), user));
            });
        }, 60000);
    };

    var dateDiff = function(d1, d2, user, el) {
        if (d1 < d2 && !user) {
            el && $('.credit-declare', el.closest('.time-question')).html(Labels['not_available']);
            return Labels['the_deadline_has_passed'];
        }

        var diff = Math.abs(d2 - d1);        
        if (!user) {
            if (Math.floor(diff / 86400000)) {
                if(Math.floor(diff / 86400000) == 1){
                    return Math.floor(diff / 86400000) + ' ' + Labels['day_countdown'];
                }else{
                    return Math.floor(diff / 86400000) + ' ' + Labels['days_countdown'];
                }
            } else if (Math.floor(diff / 3600000)) {
                if(Math.floor(diff / 3600000) == 1){
                    return Math.floor(diff / 3600000) + ' ' + Labels['hour_countdown'];
                }else{
                    return Math.floor(diff / 3600000) + ' ' + Labels['hours_countdown'];
                }
                
            } else if (Math.floor(diff / 60000)) {
                if(Math.floor(diff / 60000) == 1){
                    return Math.floor(diff / 60000) + ' ' + Labels['minute_countdown'];
                }else{
                    return Math.floor(diff / 60000) + ' ' + Labels['minutes_countdown'];
                }                
            } else {
                return  Labels['the_deadline_has_passed'];
            }
        } else {
            if (Math.floor(diff / 86400000)) {
                if(Math.floor(diff / 86400000) == 1){
                    return Labels['yesterday'] + ' ';
                }else{
                    return Math.floor(diff / 86400000) + ' ' + Labels['days'] + ' ' + Labels['ago'];
                }                
            } else if (Math.floor(diff / 3600000)) {
                if(Math.floor(diff / 3600000) == 1){
                    return Math.floor(diff / 3600000) + ' ' + Labels['hour'] + ' ' + Labels['ago'];
                }else{
                    return Math.floor(diff / 3600000) + ' ' + Labels['hours'] + ' ' + Labels['ago'];
                }                
            } else if (Math.floor(diff / 60000)) {
                if(Math.floor(diff / 60000) == 1){
                    return Math.floor(diff / 60000) + ' ' + Labels['minute'] + ' ' + Labels['ago'];
                }else{
                    return Math.floor(diff / 60000) + ' ' + Labels['minutes'] + ' ' + Labels['ago'];
                }                
            } else {
                return Labels['a_few_seconds'] + ' ' + Labels['ago'];
            }
        }
    };
    
    var autoCompleteUsersAndKeywords = function(el, arr) {
        var $this = this;
        el.mentionsInput({
            avatar: false,
            triggerChar : ['#', '@'],
            valSelect   : arr,                       
            onDataRequest: function(mode, query, callback, index) {
                if(index == 1)
                    var data = $this.followers.toJSON();
                else
                    var data = $this.keywords.toJSON();
                data = _.filter(data, function(item) {                    
                    return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1
                });
                
                if(index == 1){
                    var newWrapData = [];                
                    _.each(data, function(d){
                        var obj = {
                            name : d.username,
                            id   : d.id                        
                        };                    
                        newWrapData.push(obj);
                    });                
                    callback.call(this, newWrapData);
                }else{
                    callback.call(this, data);
                }
            }
        });
    };

     var updateCredits = function(callBack) {
        if (window.current_user && window.current_user.id) {
            $.ajax({
                url: '/api/users/me/prefs/credit',
                type: 'GET',
                success: function(resp) {        
                    var credit = (resp.credit) ? parseFloat(resp.credit) : 0;
                    window.current_user.credit = credit;
                    if(credit != 0){
                         $('#primary-nav .credits-user a').html(Labels['you_have'] + ' ' + _.fuzzyNum(credit) + ' ' + Labels['credits']);
                    }else{
                         $('#primary-nav .credits-user a').html(Labels['you_have_no_credit']);
                    }            
                   
                    $(".slider-reask").slider({'max' : Math.min(credit, 50)});
                    $(".slider-credit").slider({'max' : Math.min(credit, 50)});
                    
                    if(typeof callBack == 'function')
                        callBack(credit);
                },
                error: function(err) {}
            }, this);
        }
    };
    
    getCurrentUserLists = function(){        
        if(this.listCollection)
            return this.listCollection;
        
        this.listCollection = new ListCollection([], {
            mode : 'create'
        });
        
        if(window.current_user && window.current_user.id){
            this.listCollection.filters.set('limit', '100');
            this.listCollection.fetch();
            this.listCollection.on('fetchSuccess', function(){
                $('.bookmark-question', $('.popover').closest('.bookmark-question-wrapper')).trigger('click');            
            });
        }
        return this.listCollection;
    };

    var processScroll = function() {
        var $win = $(window)
                , $nav = $('#primary-nav')
                , navTop = 40
                , isFixed = 1
                , isAddH = false;

        processScroll2();
        $win.unbind('scroll', processScroll2);
        $win.bind('scroll', processScroll2);

        function processScroll2() {
            var i, scrollTop = $win.scrollTop();

            if (scrollTop >= navTop && !isFixed) {
                isFixed = 1;
                $('.navbar-fixed-top .navbar-inner').addClass('scroll-navbar');
            }
            else if (scrollTop <= navTop && isFixed) {
                isFixed = 0;                
                $('.navbar-fixed-top .navbar-inner').removeClass('scroll-navbar');
            }
        }
    };

    return {
        processScroll                   : processScroll,
        initialize                      : initialize,
        eventsRegister                  : eventsRegister,
        dateDiff                        : dateDiff,
        updateDate                      : updateDate,
        MixinFn                         : MixinFn,
        updateCredits                   : updateCredits,     
        getUserFollowers                : getUserFollowers,              
        getKeywords                     : getKeywords,
        autoCompleteUsersAndKeywords    : autoCompleteUsersAndKeywords,
        setNavbarNotifications          : setNavbarNotifications,
        getCurrentUserLists             : getCurrentUserLists
    };
});
