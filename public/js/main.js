require.config({  
    waitSeconds: 60,
    paths: {        
        'jquery'                    : 'vendor/jquery/jquery',
        'jquery-ui'                 : 'vendor/jquery-ui/jquery-ui-1.10.3.custom',
        'underscore'                : 'vendor/underscore-amd/underscore',
        'underscore_string'         : 'vendor/underscore.string/lib/underscore.string',
        'backbone'                  : 'vendor/backbone-amd/backbone',
        'bootstrap'                 : 'vendor/bootstrap-js/bootstrap',
        'text'                      : 'vendor/requirejs-text/text',        
        'backbone.routefilter'      : 'vendor/backbone.routefilter/src/backbone.routefilter',
        'tagit'                     : 'libs/tagit',
        'jquery-helper'             : 'libs/jquery-helper' ,
        'complexify'                : 'libs/jquery.complexify' ,
        'semantic'                  : 'libs/semantic.min',
        'mentionInput'              : 'libs/jquery.mentionsInput',                
        'datetimepicker'            : 'libs/bootstrap-datetimepicker',  
        'elastic'                   : 'libs/jquery.elastic',
        'enscroll'                  : 'libs/enscroll-0.4.2.min',
        'waypoints'                 : 'libs/waypoints',
        'bxslider'                  : 'libs/jquery.bxslider',
        'bootstrap-select'          : 'libs/bootstrap-select.min',
        'copy-clipboard'            : 'libs/jquery.zclip',
        'animo'                     : 'libs/animo',
        'i18n'                      : 'vendor/requirejs-i18n/i18n',        
        'floatlabels'               : 'libs/floatlabels.min',
        'timezone'                  : 'libs/jstz-1.0.4.min',
        'jquery.knob'               : 'libs/jquery.knob'
    },
    locale: 'en-us',
    shim: {
        'underscore.string': {
            deps: ['underscore']
        },
        'backbone': {
            deps: ['underscore', 'jquery']
        },
        bootstrap: {
            deps: ['jquery']
        },
        'backbone.routefilter' : {
            deps : ['backbone', 'underscore']
        },        
        'jquery-ui':{
            deps: ['jquery']
        },
        'jquery.knob':{
            deps: ['jquery']
        },
        'copy-clipboard':{
            deps: ['jquery']
        },
        'complexify':{
            deps: ['jquery']
        },
        'bootstrap-select':{
            deps: ['jquery']
        },
        'bxslider':{
            deps: ['jquery']
        },
        'floatlabels':{
            deps: ['jquery']
        },
        'waypoints' : {
            deps: ['jquery']
        },
        elastic : {
            deps : ['jquery']
        },
        enscroll : {
            deps : ['jquery']
        },
        timezone : {
            deps : ['jquery']
        },
        'mentionInput' : {
            deps: ['jquery', 'underscore']            
        },
        'datetimepicker' : {
            deps: ['bootstrap']
        },       
        'tagit':{
            deps: ['jquery-ui']
        },
        'semantic' : {
            deps: ['jquery-ui']
        },
        'jquery-helper' : {
            deps: ['jquery']
        },
        'animo' : {
            deps: ['jquery']
        }       
    }
});

require(['jquery', 'underscore', 'backbone', 'bootstrap', 'app', 'views/layout', 'backbone.routefilter', 'jquery-ui'], function($, _, Backbone, Bootstrap, App, Layout) {
    if(this.layoutView){
        this.layoutView.remove();
    }
    
    App.MixinFn();
    
    this.layoutView = new Layout();
    $('body').append(this.layoutView.render().$el);
    
    App.initialize();
    App.eventsRegister();
    App.updateDate();   
    App.processScroll();   
    
    if(window.current_user && window.current_user.id){      
        App.setNavbarNotifications();        
    }
    
    
    Backbone.history.start({pushState: true, root: '/'});    
    
});

