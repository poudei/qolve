({
    appDir: './',
    baseUrl: "./js",
    mainConfigFile: './js/main.js',
    dir:'./release',
    logLevel:0,
    preserveLicenseComments:!1,
    optimize:"uglify2",
    removeCombined: true,
    findNestedDependencies: true,
    optimizeCss: 'standard',
    waitSeconds: 60,
    // 3rd party script alias names
    paths: {        
        'app'                       : 'app',
        'jquery'                    : 'vendor/jquery/jquery',
        'jquery-ui'                 : 'vendor/jquery-ui/jquery-ui-1.10.3.custom',
        'underscore'                : 'vendor/underscore/underscore',
        'underscore_string'         : 'vendor/underscore.string/lib/underscore.string',
        'backbone'                  : 'vendor/backbone/backbone',
        'bootstrap'                 : 'vendor/bootstrap-js/bootstrap',
        'text'                      : 'vendor/requirejs-text/text',        
        'backbone.routefilter'      : 'vendor/backbone.routefilter/src/backbone.routefilter',
        'tagit'                     : 'libs/tagit',
        'jquery-helper'             : 'libs/jquery-helper' ,
        'complexify'                : 'libs/jquery.complexify' ,
        'semantic'                  : 'libs/semantic.min',
        'mentionInput'              : 'libs/jquery.mentionsInput',                
        'datetimepicker'            : 'libs/bootstrap-datetimepicker',  
        'elastic'                   : 'libs/jquery.elastic',
        'enscroll'                  : 'libs/enscroll-0.4.2.min',
        'waypoints'                 : 'libs/waypoints',
        'bxslider'                  : 'libs/jquery.bxslider',
        'bootstrap-select'          : 'libs/bootstrap-select.min',
        'copy-clipboard'            : 'libs/jquery.zclip',
        'animo'                     : 'libs/animo',
        'i18n'                      : 'vendor/requirejs-i18n/i18n',        
        'floatlabels'               : 'libs/floatlabels.min',
        'timezone'                  : 'libs/jstz-1.0.4.min',
        'jquery.knob'               : 'libs/jquery.knob'
    },

// Sets the use.js configuration for your application
    use: {

         'underscore.string': {
            deps: ['underscore']
        },
        bootstrap: {
            deps: ['jquery']
        },
        'backbone.routefilter' : {
            deps : ['backbone', 'underscore']
        },        
        'jquery-ui':{
            deps: ['jquery']
        },
        'copy-clipboard':{
            deps: ['jquery']
        },
        'complexify':{
            deps: ['jquery']
        },
        'bootstrap-select':{
            deps: ['jquery']
        },
        'bxslider':{
            deps: ['jquery']
        },
        'jquery.knob':{
            deps: ['jquery']
        },
        'floatlabels':{
            deps: ['jquery']
        },
        'waypoints' : {
            deps: ['jquery']
        },
        'elastic' : {
            deps : ['jquery']
        },
        'enscroll' : {
            deps : ['jquery']
        },
        'mentionInput' : {
            deps: ['jquery', 'underscore']            
        },
        'datetimepicker' : {
            deps: ['bootstrap']
        },       
        'tagit':{
            deps: ['jquery-ui']
        },
        'semantic' : {
            deps: ['jquery-ui']
        },
        'jquery-helper' : {
            deps: ['jquery']
        },
        'animo' : {
            deps: ['jquery']
        },
        timezone : {
            deps : ['jquery']
        },
        'underscore': {
            attach: "_" //attaches "_" to the window object
        },
        'backbone': {
            deps : ['underscore', 'jquery'],
            attach: "Backbone"  //attaches "Backbone" to the window object
        }


    }, // end Use.js Configuration
    // Modules to be optimized:
   
   
           name: "main"
   

})
