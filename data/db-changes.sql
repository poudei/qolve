---2013-11-17 - Aireza
CREATE TABLE session
(
      id character varying(255) NOT NULL,
      name character varying(255),
      modified bigint,
      lifetime integer,
      data text,
      CONSTRAINT session_pkey PRIMARY KEY (id )
)
WITH (
      OIDS=FALSE
);
ALTER TABLE session
  OWNER TO postgres;
