<?php
namespace Qolve\Stdlib\Hydrator;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as BaseHydrator;

class DoctrineObject extends BaseHydrator
{
    /**
     * Extract values from an object
     *
     * @param  object $object
     * @return array
     */
    public function extract($object, $info = array(), $currentUser = null)
    {
        $data = parent::extract($object);

        if (isset($data['id']) && !is_numeric($data['id'])) {
            return $data;
        }

        if ($object instanceof \Application\Entity\User) {
            unset($data['password']);
        }

        if ($object instanceof \Application\Entity\UserProfile) {
            unset($data['userId'], $data['deleted'], $data['createdOn'],
                    $data['modifiedBy'], $data['modifiedOn']);
        }

        if ($object instanceof \Qolve\Entity\Answer) {
            unset($data['questionId']);
        }

        if ($object instanceof \Qolve\Entity\Comment) {
            unset($data['questionId'], $data['answerId']);
        }

        if ($object instanceof \Qolve\Entity\Document) {
            unset($data['questionId'], $data['answerId']);
        }

        if ($object instanceof \Qolve\Entity\Vote) {
            unset($data['questionId'], $data['answerId']);
        }

        if ($object instanceof \Application\Entity\Notification) {
            unset($data['toId']);
        }

        if (isset($data['userId']) && !empty($data['userId']) && !isset($data['user'])) {
            $user = $this->objectManager->getRepository('Application\Entity\User')
                    ->find($data['userId']);
            if ($user instanceof \Application\Entity\User) {
                $data['user'] = $this->extract($user);
            }
        }

        if (isset($data['actorId']) && !empty($data['actorId']) && !isset($data['actor'])) {
            $actor = $this->objectManager->getRepository('Application\Entity\User')
                    ->find($data['actorId']);
            if ($actor instanceof \Application\Entity\User) {
                $data['actor'] = $this->extract($actor);
            }
        }

        if (isset($data['fromId']) && !empty($data['fromId']) && !isset($data['from'])) {
            $from = $this->objectManager->getRepository('Application\Entity\User')
                    ->find($data['fromId']);
            if ($from instanceof \Application\Entity\User) {
                $data['from'] = $this->extract($from);
            }
        }

        if (isset($data['followerId']) && !empty($data['followerId']) && !isset($data['follower'])) {
            $follower = $this->objectManager->getRepository('Application\Entity\User')
                    ->find($data['followerId']);
            if ($follower instanceof \Application\Entity\User) {
                $data['follower'] = $this->extract($follower);
            }
        }

        if (isset($data['folloyeeId']) && !empty($data['folloyeeId']) && !isset($data['folloyee'])) {
            $folloyee = $this->objectManager->getRepository('Application\Entity\User')
                    ->find($data['folloyeeId']);
            if ($folloyee instanceof \Application\Entity\User) {
                $data['folloyee'] = $this->extract($folloyee);
            }
        }

        if (isset($data['questionId']) && !empty($data['questionId']) && !isset($data['question'])) {
            $question = $this->objectManager->getRepository('Qolve\Entity\Question')
                    ->find($data['questionId']);
            if ($question instanceof \Qolve\Entity\Question) {
                $data['question'] = $this->extract($question, $info, $currentUser);
            }
        }

        if (isset($data['answerId']) && !empty($data['answerId']) && !isset($data['answer'])) {
            $answer = $this->objectManager->getRepository('Qolve\Entity\Answer')
                    ->find($data['answerId']);
            if ($answer instanceof \Qolve\Entity\Answer) {
                $data['answer'] = $this->extract($answer, $info, $currentUser);
            }
        }

        if (isset($data['keywordId']) && !empty($data['keywordId']) && !isset($data['keyword'])) {
            $keyword = $this->objectManager->getRepository('Qolve\Entity\Keyword')
                    ->find($data['keywordId']);
            if ($keyword instanceof \Qolve\Entity\Keyword) {
                $data['keyword'] = $keyword->getName();
            }
        }

        if ($object instanceof \Application\Entity\User) {

            if (array_search('credit', $info) !== false) {
                if ($currentUser instanceof \Application\Entity\User) {
                    similar_text($currentUser->getId(), $object->getId(), $percent);
                    if ($percent == 100) {
                        $data['credit'] = (float) $this->objectManager
                                ->getRepository('Application\Entity\UserPref')
                                ->getUserKey($object->getId(), 'credit');
                    }
                }
            }

            if (array_search('listsCount', $info) !== false) {
                if ($currentUser instanceof \Application\Entity\User) {
                    similar_text($currentUser->getId(), $object->getId(), $percent);
                    if ($percent == 100) {
                        $data['listsCount'] = $this->objectManager
                                ->getRepository('Qolve\Entity\BookmarkList')
                                ->countTotalListsOfUser($object->getId());
                    } else {
                        $data['listsCount'] = $this->objectManager
                                ->getRepository('Qolve\Entity\BookmarkList')
                                ->countPublicListsOfUser($object->getId());
                    }
                } else {
                    $data['listsCount'] = $this->objectManager
                            ->getRepository('Qolve\Entity\BookmarkList')
                            ->countPublicListsOfUser($object->getId());
                }
            }

            if (isset($info['followersList'])) {
                $offset = isset($info['followersList']['offset']) ?
                    $info['followersList']['offset'] : 0;
                $limit = isset($info['followersList']['limit']) ?
                    $info['followersList']['limit'] : 10;
                
                //TODO: cache, write method for it - ignore
                $follows = $this->objectManager->getRepository('Qolve\Entity\UserFollow')
                        ->findBy(array('userId' => $object->getId()), null, $limit, $offset);

                $data['followersList'] = array(
                    'list'  => array(),
                    'count' => null
                );
                foreach ($follows as $follow) {
                    $follower = $this->objectManager->getRepository('Application\Entity\User')
                        ->find($follow->getFollowerId());

                    if ($follower instanceof \Application\Entity\User) {
                        $data['followersList']['list'][] = $this->extract($follower);
                    }
                }

                $data['followersList']['count'] = (int) $this->objectManager
                        ->getRepository('Application\Entity\UserPref')
                        ->getUserKey($object->getId(), 'followersCount');
            }

            if (isset($info['followingsList'])) {
                $offset = isset($info['followingsList']['offset']) ?
                    $info['followingsList']['offset'] : 0;
                $limit = isset($info['followingsList']['limit']) ?
                    $info['followingsList']['limit'] : 10;
                
                //TODO: cache, write method for it - ignore
                $follows = $this->objectManager->getRepository('Qolve\Entity\UserFollow')
                        ->findBy(array('followerId' => $object->getId()), null, $limit, $offset);

                $data['followingsList'] = array(
                    'list'  => array(),
                    'count' => null
                );
                foreach ($follows as $follow) {
                    $following = $this->objectManager->getRepository('Application\Entity\User')
                        ->find($follow->getUserId());

                    if ($following instanceof \Application\Entity\User) {
                        $data['followingsList']['list'][] = $this->extract($following);
                    }
                }

                $data['followingsList']['count'] = (int) $this->objectManager
                        ->getRepository('Application\Entity\UserPref')
                        ->getUserKey($object->getId(), 'userFollowingsCount');
            }

            if ($currentUser instanceof \Application\Entity\User) {
                
                //TODO: cache, write method for it (iFollowed) -> Done
                $follow = $this->objectManager->getRepository('Qolve\Entity\UserFollow')
                        ->findOneBy(array(
                            'userId'        => $object->getId(),
                            'followerId'    => $currentUser->getId()
                        ));
                $data['isFollowed'] = (int) ($follow instanceof \Qolve\Entity\UserFollow);

            } else {
                $data['isFollowed'] = 0;
            }

            if ($currentUser instanceof \Application\Entity\User) {
                similar_text($currentUser->getId(), $object->getId(), $percent);
                if ($percent == 100) {
                    
                    //TODO: cache, write method for it (getUserProviders) -> Done
                    $providers = $this->objectManager->getRepository('Application\Entity\UserProvider')
                            ->findByUserId($object->getId());
                    $data['providers'] = array();
                    foreach ($providers as $provider) {
                        $data['providers'][$provider->getProvider()] = $provider->getPublished();
                    }
                }
            }

            $prefs = $this->objectManager->getRepository('Application\Entity\UserPref')
                    ->getUserPref($object->getId());
            if (isset($prefs['credit'])) {
                unset($prefs['credit']);
            }
            $data = array_merge($data, $prefs);

            $profile = $this->objectManager->getRepository('Application\Entity\UserProfile')
                    ->findOneByUserId($object->getId());
            if ($profile instanceof \Application\Entity\UserProfile) {
                $_profile = $this->extract($profile, array(), $currentUser);
                $data = array_merge($data, $_profile);
            }
        }

        if ($object instanceof \Qolve\Entity\Question) {
            //TODO: cache, write method for it (getAsker at QuestionRepo) -> Done
            $asker = $this->objectManager->getRepository('Qolve\Entity\Asker')
                    ->findOneBy(array(
                        'questionId' => $object->getId(),
                        'status'     => \Qolve\Entity\Asker::STATUS_ASKER
                    ));

            if (array_search('asker', $info) !== false) {
                if ($asker instanceof \Qolve\Entity\Asker) {
                    $user = $this->objectManager->getRepository('Application\Entity\User')
                        ->find($asker->getUserId());
                    if ($user instanceof \Application\Entity\User) {
                        $_asker = $this->extract($user);

                        if ($currentUser) {
                            similar_text($currentUser->getId(), $user->getId(), $percent);
                        }

                        if ($asker->getVisibility() == \Qolve\Entity\Asker::VISIBILITY_ANONYMOUS &&
                                (!$currentUser || $percent < 100)) {

                            $_asker = array_map(function () {return null;}, $_asker);
                            $_asker['name'] = \Qolve\Entity\Asker::ANONYMOUS_NAME;

                            $_asker['invisible'] = 1;
                        } else {
                            $_asker['invisible'] = 0;
                        }

                        $data['asker'] = $_asker;
                    }
                }
            }

            if (array_search('keywords', $info) !== false) {
                $keywords = $this->objectManager->getRepository('Qolve\Entity\QuestionKeyword')
                        ->getKeywordsOfQuestion($object->getId());
                if (!empty($keywords)) {
                    foreach ($keywords as $keyword) {
                        if ($currentUser instanceof \Application\Entity\User) {
                            
                            //TODO: cache, write method for it (isFollowed) -> Done
                            $keywordFollow = $this->objectManager->getRepository('Qolve\Entity\KeywordFollow')
                                    ->isFollowed($currentUser->getId(), $keyword['id']);
                            $key['isFollowed'] = (int) ($keywordFollow instanceof \Qolve\Entity\KeywordFollow);
                        } else {
                            $key['isFollowed'] = 0;
                        }

                        $key['name']        = $keyword['name'];
                        $data['keywords'][] = $key;
                    }
                }
            }

            if (isset($info['answersList'])) {
                $offset = isset($info['answersList']['offset']) ?
                    $info['answersList']['offset'] : 0;
                $limit = isset($info['answersList']['limit']) ?
                    $info['answersList']['limit'] : 5;
                $answers = $this->objectManager->getRepository('Qolve\Entity\Answer')
                        ->getAnswers($object->getId(), $offset, $limit);

                $data['answersList'] = array(
                    'list'  => array(),
                    'count' => $object->getAnswers()
                );
                foreach ($answers as $answer) {
                    $data['answersList']['list'][] = $this->extract($answer, $info, $currentUser);
                }
            }

            if (isset($info['commentsList'])) {
                $offset = isset($info['commentsList']['offset']) ?
                    $info['commentsList']['offset'] : 0;
                $limit = isset($info['commentsList']['limit']) ?
                    $info['commentsList']['limit'] : 5;
                $comments = $this->objectManager->getRepository('Qolve\Entity\Comment')
                        ->getComments($object->getId(), null, $commentsCount, $offset, $limit);

                $data['commentsList'] = array(
                    'list'  => array(),
                    'count' => $commentsCount
                );
                foreach ($comments as $comment) {
                    $data['commentsList']['list'][] = $this->extract($comment);
                }
            }

            if (isset($info['documentsList'])) {
                $offset = isset($info['documentsList']['offset']) ?
                    $info['documentsList']['offset'] : 0;
                $limit = isset($info['documentsList']['limit']) ?
                    $info['documentsList']['limit'] : 5;
                $documents = $this->objectManager->getRepository('Qolve\Entity\Document')
                        ->findBy(array(
                            'questionId' => $object->getId(),
                            'answerId'   => null
                        ), array(
                            'order'      => 'ASC'
                        ), $limit, $offset);

                $data['documentsList'] = array(
                    'list'  => array(),
                    'count' => null
                );
                foreach ($documents as $document) {
                    $data['documentsList']['list'][] = $this->extract($document);
                }

                $data['documentsList']['count'] = $this->objectManager->getRepository('Qolve\Entity\Document')
                        ->countBy(array(
                            'questionId' => $object->getId(),
                            'answerId'   => null
                        ));
            }

            if ($currentUser instanceof \Application\Entity\User) {
                
                //TODO: cache, write method for it (ifAsked) -> Doned
                $asked = $this->objectManager->getRepository('Qolve\Entity\Asker')
                        ->findOneBy(array(
                            'userId'        => $currentUser->getId(),
                            'questionId'    => $object->getId()
                        ));

                if ($asked instanceof \Qolve\Entity\Asker) {
                    $data['isAsked'] = $asked->getStatus() * 10
                            + $asked->getVisibility();
                    $data['userCredit'] = $asked->getCredit();
                } else {
                    $data['isAsked'] = 0;
                    $data['userCredit'] = 0;
                }
            } else {
                $data['isAsked'] = 0;
                $data['userCredit'] = 0;
            }

            if ($currentUser instanceof \Application\Entity\User) {
                $vote = $this->objectManager->getRepository('Qolve\Entity\Vote')
                        ->findOneBy(array(
                            'userId'        => $currentUser->getId(),
                            'questionId'    => $object->getId(),
                            'answerId'      => null
                        ));

                if ($vote instanceof \Qolve\Entity\Vote) {
                    $data['isVoted'] = (int) $vote->getScore();
                } else {
                    $data['isVoted'] = 0;
                }
            } else {
                $data['isVoted'] = 0;
            }

            if (array_search('userAnswerId', $info) !== false) {
                if ($currentUser instanceof \Application\Entity\User) {
                    //TODO: cache, write method for it (getUserAnswer) -> Done
                    $answer = $this->objectManager->getRepository('Qolve\Entity\Answer')
                            ->findOneBy(array(
                                'userId'        => $currentUser->getId(),
                                'questionId'    => $object->getId()
                            ));

                    if ($answer instanceof \Qolve\Entity\Answer) {
                        $data['userAnswerId'] = $answer->getId();
                    } else {
                        $data['userAnswerId'] = null;
                    }
                } else {
                    $data['userAnswerId'] = null;
                }
            }

            // Add User's shares list
            
            //TODO: cache, write method for it -> Done
            $shares = $this->objectManager->getRepository('Qolve\Entity\Share')
                ->findByQuestionId($object->getId());
            if (!empty($shares)) {
                foreach ($shares as $share) {
                    if (!$share instanceof \Qolve\Entity\Share) {
                        continue;
                    }

                    $shareUser =  $this->objectManager->getRepository('Application\Entity\User')
                        ->find($share->getUserId());
                    if ($shareUser instanceof \Application\Entity\User) {

                        $data['share'][] = $this->extract($shareUser, array(), $currentUser);
                    }
                }
            } else {

                $data['share'] = array();
            }

            if ($currentUser instanceof \Application\Entity\User &&
                    $asker instanceof \Qolve\Entity\Asker) {

                similar_text($currentUser->getId(), $asker->getUserId(), $percent);

                //TODO: cache, write method for it (isDeletable) ->Done
                $answer = $this->objectManager->getRepository('Qolve\Entity\Answer')
                        ->findOneByQuestionId($object->getId());

                if ($percent == 100 &&
                        $object->getStatus() == \Qolve\Entity\Question::STATUS_OPEN &&
                        !$answer instanceof \Qolve\Entity\Answer) {

                    $data['deletable'] = true;
                } else {
                    $data['deletable'] = false;
                }
            } else {
                $data['deletable'] = false;
            }

            if ($asker instanceof \Qolve\Entity\Asker) {
                $data['visibility'] = $asker->getVisibility();
            }


            if (!empty($data['levels'])) {
                $data['levels'] = explode(',', $data['levels']);
            } else {
                $data['levels'] = array();
            }
        }

        if ($object instanceof \Qolve\Entity\Answer) {

            $invisible = 0;
            if ($object->getPrivacy() == \Qolve\Entity\Answer::PRIVACY_PRIVATE) {
                if (!$currentUser instanceof \Application\Entity\User) {
                    $invisible = 1;
                } else {
                    similar_text($object->getUserId(), $currentUser->getId(), $percent);
                    if ($percent < 100) {
                        
                        //TODO: cache.. use isAsked  -> DONE BUT NOT TEST
                        $asker = $this->objectManager->getRepository('Qolve\Entity\Asker')
                                ->findOneBy(array(
                                    'questionId' => $object->getQuestionId(),
                                    'userId'     => $currentUser->getId()
                                ));
                        if (!$asker instanceof \Qolve\Entity\Asker) {
                            //TODO: cache, write method for it (hasPrivilege)
                            $privilege = $this->objectManager->getRepository('Qolve\Entity\AnswerPrivilege')
                                    ->findOneBy(array(
                                                'answerId' => $object->getId(),
                                                'userId'   => $currentUser->getId()
                                                ));
                            if (!$privilege instanceof \Qolve\Entity\AnswerPrivilege) {
                                $invisible = 1;
                            }
                        }
                    }
                }
            }

            if (isset($info['commentsList'])) {
                $offset = isset($info['commentsList']['offset']) ?
                    $info['commentsList']['offset'] : 0;
                $limit = isset($info['commentsList']['limit']) ?
                    $info['commentsList']['limit'] : 5;
                $comments = $this->objectManager->getRepository('Qolve\Entity\Comment')
                    ->getComments($object->getQuestionId(), $object->getId(), $commentsCount, $offset, $limit);

                $data['commentsList'] = array(
                        'list'  => array(),
                        'count' => $commentsCount
                        );
                foreach ($comments as $comment) {
                    $data['commentsList']['list'][] = $this->extract($comment);
                }
            }

            if ($currentUser instanceof \Application\Entity\User) {
                $vote = $this->objectManager->getRepository('Qolve\Entity\Vote')
                    ->findOneBy(array(
                                'userId'        => $currentUser->getId(),
                                'questionId'    => $object->getQuestionId(),
                                'answerId'      => $object->getId()
                                ));
                if ($vote instanceof \Qolve\Entity\Vote) {
                    $data['isVoted'] = (int) $vote->getScore();
                } else {
                    $data['isVoted'] = 0;
                }
            } else {
                $data['isVoted'] = 0;
            }

            if ($invisible) {
                unset($data['description']);
            } else {


                if (isset($info['documentsList'])) {
                    $offset = isset($info['documentsList']['offset']) ?
                        $info['documentsList']['offset'] : 0;
                    $limit = isset($info['documentsList']['limit']) ?
                        $info['documentsList']['limit'] : 5;
                    $documents = $this->objectManager->getRepository('Qolve\Entity\Document')
                        ->findBy(array(
                                    'questionId' => $object->getQuestionId(),
                                    'answerId'   => $object->getId()
                                    ), array(
                                        'order'      => 'ASC'
                                        ), $limit, $offset);

                    $data['documentsList'] = array(
                            'list'  => array(),
                            'count' => null
                            );
                    foreach ($documents as $document) {
                        $data['documentsList']['list'][] = $this->extract($document);
                    }

                    $data['documentsList']['count'] = $this->objectManager->getRepository('Qolve\Entity\Document')
                            ->countBy(array(
                                'questionId' => $object->getQuestionId(),
                                'answerId'   => $object->getId()
                            ));
                }
            }

            if (isset($data['user'])) {

                if ($currentUser) {
                    similar_text($currentUser->getId(), $data['user']['id'], $percent);
                }

                if ($object->getVisibility() == \Qolve\Entity\Answer::VISIBILITY_ANONYMOUS &&
                        (!$currentUser || $percent < 100)) {

                    $data['user'] = array_map(function () {return null;}, $data['user']);
                    $data['user']['name'] = \Qolve\Entity\Answer::ANONYMOUS_NAME;
                }

                $data['user']['invisible'] = (int) ($object->getVisibility() == \Qolve\Entity\Answer::VISIBILITY_ANONYMOUS);
            }

            $data['invisible'] = (int) $invisible;
        }

        if ($object instanceof \Qolve\Entity\Keyword) {

            if (!empty($currentUser) && $currentUser->getId() != null) {
                //TODO: cache, write method for it (isFollowed) -> already done
                $keywordFollow = $this->objectManager->getRepository('Qolve\Entity\KeywordFollow')
                    ->findOneBy(array('userId' => $currentUser->getId(), 'keywordId' => $object->getId()));
                if ($keywordFollow instanceof \Qolve\Entity\KeywordFollow) {
                    $data['isFollowed'] = 1;
                } else {
                    $data['isFollowed'] = 0;

                }
            } else {
                $data['isFollowed'] = 0;
            }
        }

        if ($object instanceof \Qolve\Entity\BookmarkList) {

            $invisible = 0;
            if ($object->getPrivacy() == \Qolve\Entity\BookmarkList::PRIVACY_PRIVATE) {
                if (!$currentUser instanceof \Application\Entity\User) {
                    $invisible = 1;
                } else {
                    similar_text($object->getUserId(), $currentUser->getId(), $percent);
                    if ($percent < 100) {
                        $invisible = 1;
                    }
                }
            }

            if ($invisible) {
                $data = array_map(function () {return null;}, $data);
            } else {

                if (isset($info['questionsList'])) {
                    $offset = isset($info['questionsList']['offset']) ?
                        $info['questionsList']['offset'] : 0;
                    $limit = isset($info['questionsList']['limit']) ?
                        $info['questionsList']['limit'] : 10;
                    
                    //TODO: cache, write method for it (getLastQuestionsOfList) - without limit ->DONE
                    $questions = $this->objectManager->getRepository('Qolve\Entity\Bookmark')
                            ->getQuestionsOfList($object->getId(), $qCount, $offset, $limit);

                    $data['questionsList'] = array(
                        'list'  => array(),
                        'count' => $qCount
                    );
                    foreach ($questions as $question) {
                        $data['questionsList']['list'][] = $this->extract($question, $info, $currentUser);
                    }
                }

                if (array_search('listKeywords', $info) !== false) {
                    $data['listKeywords'] = $this->objectManager->getRepository('Qolve\Entity\BookmarkList')
                            ->getListKeywords($object->getId());
                }
            }

            if ($currentUser instanceof \Application\Entity\User) {
                //TODO: cache, write method for it (isFollowed) -> Done
                $follow = $this->objectManager->getRepository('Qolve\Entity\BookmarkListFollow')
                        ->findOneBy(array(
                            'listId'        => $object->getId(),
                            'userId'        => $currentUser->getId()
                        ));
                $data['isFollowed'] = (int) ($follow instanceof \Qolve\Entity\BookmarkListFollow);

            } else {
                $data['isFollowed'] = 0;
            }

            $data['invisible'] = $invisible;
        }

        if ($object instanceof \Application\Entity\Notification) {

            $fromAnonymous = false;

            switch ($object->getVerb()) {
            case 'CreateQuestion':
            case 'PickBestAnswer':
            case 'RaiseCredit':
            case 'ReaskQuestion':
            case 'UpdateQuestion':
                //TODO: cache, use getQuestionAsker
                $asker = $this->objectManager->getRepository('Qolve\Entity\Asker')
                    ->findOneBy(array(
                        'questionId' => $object->getQuestionId(),
                        'userId'     => $object->getFromId()
                    ));
                if ($asker instanceof \Qolve\Entity\Asker &&
                        $asker->getVisibility() == \Qolve\Entity\Asker::VISIBILITY_ANONYMOUS) {

                    $fromAnonymous = true;
                }
                break;

            case 'CreateAnswer':
            case 'UpdateAnswer':
                $answer = $this->objectManager->getRepository('Qolve\Entity\Answer')
                    ->findOneById($object->getAnswerId());
                if ($answer instanceof \Qolve\Entity\Answer &&
                        $answer->getVisibility() == \Qolve\Entity\Answer::VISIBILITY_ANONYMOUS) {

                    $fromAnonymous = true;
                }
                break;
            }

            if ($fromAnonymous) {
                $data['from'] = array_map(function () {return null;}, $data['from']);
                $data['from']['name'] = 'ANONYMOUS';
            }

            $data['from']['invisible'] = (int) ($fromAnonymous);


            if ($object->getVerb() == 'FollowBookmarkList') {
                $list = $this->objectManager->getRepository('Qolve\Entity\BookmarkList')
                        ->find($object->getContent());
                if ($list instanceof \Qolve\Entity\BookmarkList) {
                    $data['list'] = $this->extract($list, array(), $currentUser);
                }
            }
        }

        return $data;
    }
}
