
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
CREATE EXTENSION pg_trgm;

--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--- Independent table, Shard by created_on
--- Indexed by id
--- SHOULD FULLY BE CACHED !!!


CREATE TABLE rbac_permission(
	perm_id bigint NOT NULL,	--- PK
	perm_name varchar(32)  NOT NULL
);
ALTER TABLE public.rbac_permission OWNER TO whosolves;



CREATE TABLE rbac_role(
	role_id bigint NOT NULL,	--- PK
	parent_role_id bigint NULL,	
	role_name varchar(32) NOT NULL
);
ALTER TABLE public.rbac_role OWNER TO whosolves;
ALTER TABLE ONLY rbac_role
	ADD CONSTRAINT rbac_role_pkey PRIMARY KEY (role_id);
	

CREATE TABLE rbac_role_permission(
	role_id bigint NOT NULL,	--- PK
	perm_id bigint NOT NULL  	--- PK
);
ALTER TABLE public.rbac_role_permission OWNER TO whosolves;
ALTER TABLE ONLY rbac_role_permission
	ADD CONSTRAINT rbac_role_permission_pkey PRIMARY KEY (role_id, perm_id);
	

CREATE TABLE "user"(
	id bigint NOT NULL,	--- PK
    role_id bigint  NULL,
    customer_id varchar(128) NULL,
    contact_id bigint  NULL,
    email varchar(255)  NOT NULL,
    password varchar(128) NOT NULL,
    username varchar(255) NULL,
    name varchar(350) NOT NULL,
    image_path varchar(255) NULL,
    about varchar(80) NULL,
    gender smallint NULL,
    birthday timestamp NULL,
    gender_hide smallint NULL,
    birthday_hide smallint NULL,
    status smallint NOT NULL,
    deleted smallint NOT NULL,
	created_on timestamp NULL,
	modified_by bigint  NULL,
	modified_on timestamp NULL
	---
);
ALTER TABLE public."user" OWNER TO whosolves;

--- Dependent table to user table (user_id)
--- Indexed by user_id
CREATE TABLE user_profile(
    user_id bigint NOT NULL,        --- PK
    occupation varchar(255) NULL,
    school varchar(255) NULL,
    location varchar(255) NULL,
    timezone varchar(255) NULL,
    deleted smallint NOT NULL,
    created_on timestamp NULL,
    modified_by bigint  NULL,
    modified_on timestamp NULL
);
ALTER TABLE public.user_profile OWNER TO whosolves;

--- Dependent table to user table (user_id)
--- Indexed by user_id & key
CREATE TABLE user_pref(
    user_id bigint NOT NULL,        --- PK
    "key" varchar(50) NOT NULL,   --- PK
    value varchar(50) NULL
);
ALTER TABLE public.user_pref OWNER TO whosolves;

--- Dependent table to user table (user_id)
--- Indexed by user_id
CREATE TABLE user_follow(
	user_id bigint NOT NULL,	--- PK
	follower_id bigint NOT NULL	--- PK
);
ALTER TABLE public.user_follow OWNER TO whosolves;

CREATE TABLE user_social(
	user_id bigint NOT NULL,	--- PK
	social varchar(50) NOT NULL,	--- PK
    social_id varchar(30) NOT NULL,
    published smallint NOT NULL
);
ALTER TABLE public.user_social OWNER TO whosolves;

CREATE TABLE user_provider(
    user_id bigint NOT NULL,	--- PK
    provider_id varchar(30) NOT NULL,   --- PK
    provider varchar(50) NOT NULL,	
    published smallint NULL,
    access_token varchar(1024) NULL
);
ALTER TABLE public.user_provider OWNER TO whosolves;

CREATE TABLE user_provider_contact(
    id bigint NOT NULL,	--- PK
    user_id bigint NOT NULL,	
    provider varchar(50) NOT NULL,	
    contact_provider_id varchar(30) NOT NULL,
    contact_id varchar(30) NULL,
    name varchar(350) NOT NULL,
    image_path varchar(255) NULL,
    email varchar(255) NULL
);
ALTER TABLE public.user_provider_contact OWNER TO whosolves;

create TABLE user_gcm(
    user_id bigint NOT NULL, --- PK
    gcm_regid varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
	created_on timestamp NULL
);
ALTER TABLE public.user_gcm OWNER TO whosolves;

--- Independent table, Shard by name
--- Indexed by id
CREATE TABLE keyword(
	id serial NOT NULL,	--- PK
	name varchar(32) NOT NULL
);
ALTER TABLE public.keyword OWNER TO whosolves;


--- Dependent table to question table (question_id)
--- Indexed by question_id
CREATE TABLE keyword_follow(
	keyword_id bigint NOT NULL,	--- PK
	user_id bigint NOT NULL,	--- PK
    levels varchar(500) NOT NULL
);
ALTER TABLE public.keyword_follow OWNER TO whosolves;


--- Independent table, Shard by created_on
--- Indexed by id
CREATE TABLE question(
	id bigint NOT NULL,	--- PK
	deadline timestamp NULL,
	levels varchar(500) NOT NULL,	--- primary school 1 [1st], ..., high school 1 [7th],..., college [13th], undergraduate [14th], master's degree [15th], doctoral [16th], professional school [17th]
	privacy smallint NOT NULL,	--- public : 1, custom : 2
	answer_privilege smallint NOT NULL,	--- public : 1, private : 2
	votes smallint NOT NULL,	--- sum of vote up minus sum of vote down
	credits real NOT NULL,	--- sum of credits
	language varchar(5) NOT NULL,
	askers integer NOT NULL,	--- number of
	voters integer NOT NULL,	--- number of
	comments integer NOT NULL,	--- number of
	answers integer NOT NULL,	--- number of
	description varchar(10000) NULL,
	answer_id bigint NULL,	--- best answer
	status smallint NOT NULL,	--- open : 1, resolved : 2, closed : 3
	paid smallint NOT NULL,	--- not started : 1,  in progress : 2, done : 2
	deleted smallint NOT NULL,
	created_on timestamp NOT NULL,
	modified_by bigint NULL,
	modified_on timestamp NULL
);
ALTER TABLE public.question OWNER TO whosolves;

--- Dependent table to question table (question_id)
--- Indexed by question_id
CREATE TABLE question_keyword(
	question_id bigint NOT NULL,	--- PK
	keyword_id bigint NOT NULL	--- PK
);
ALTER TABLE public.question_keyword OWNER TO whosolves;

--- Dependent table to question table (question_id)
--- Indexed by question_id
CREATE TABLE asker(
	question_id bigint NOT NULL,	--- PK
	user_id bigint NOT NULL,	--- PK
	status smallint NOT NULL,	--- 1 : asker, 2 : re-asker
	credit real NOT NULL,
	visibility smallint NOT NULL,	--- public : 1 , anonymous : 2
	answer_id bigint NULL,	---	accepted answer
	asked_on timestamp NULL
);
ALTER TABLE public.asker OWNER TO whosolves;

--- Dependent table to question table (question_id)
--- Indexed by question_id
CREATE TABLE share(
	question_id bigint NOT NULL,	--- PK
	user_id bigint NOT NULL	--- PK
);
ALTER TABLE public.share OWNER TO whosolves;

--- Dependent table to question table (question_id)
--- Indexed by question_id
CREATE TABLE question_follow(
	question_id bigint NOT NULL,	--- PK
	user_id bigint NOT NULL	--- PK
);
ALTER TABLE public.question_follow OWNER TO whosolves;

--- Dependent table to question table (question_id)
--- Indexed by id
CREATE TABLE answer(
	id bigint NOT NULL,	--- PK
	question_id bigint NOT NULL,
    user_id bigint NOT NULL,
    privacy smallint NOT NULL,	--- public : 1, private : 2
    visibility smallint NOT NULL,	--- public : 1 , anonymous : 2
    status smallint NOT NULL,	--- normal : 1, accepted : 2, best: 3
	votes smallint NOT NULL,	--- sum of vote up minus sum of vote down
	comments integer NOT NULL,	--- number of
    voters integer NOT NULL,	--- number of
    description varchar(10000) NULL,
    deleted smallint NOT NULL,
	created_on timestamp NOT NULL,
	modified_by bigint NULL,
	modified_on timestamp NULL
);
ALTER TABLE public.answer OWNER TO whosolves;


--Dependent table to user table (user_id)
CREATE TABLE answer_privilege(
    user_id bigint NOT NULL, ---PK
    answer_id bigint NOT NULL ---PK
);
ALTER TABLE public.answer_privilege OWNER TO whosolves;


--- Dependent table to question/answer table (question_id & answer_id)
--- location should be handled using distributed file system
CREATE TABLE document(
	id bigint NOT NULL,	--- PK
	question_id bigint NOT NULL,	---- question_id
	answer_id bigint NULL,	---- answer_id or null
	type smallint NOT NULL,	--- image : 1, video : 2
	"order" smallint NOT NULL,
    width smallint NULL,
    height smallint NULL,
	location varchar(255) NULL	--- ???
);
ALTER TABLE public.document OWNER TO whosolves;


--- Dependent table to question/answer table (question_id & answer_id)
--- Indexed by question_id & answer_id
CREATE TABLE comment(
	id bigint NOT NULL,	--- PK
	question_id bigint NOT NULL,	---- question_id
	answer_id bigint NULL,	---- answer_id or null
	user_id bigint NOT NULL,
	content varchar(1000) NULL,
	written_on timestamp NULL
);
ALTER TABLE public.comment OWNER TO whosolves;


--- Dependent table to question/answer table (question_id & answer_id)
--- Indexed by question_id & answer_id
CREATE TABLE vote(
    id bigint NOT NULL, --- PK
	question_id bigint NOT NULL,	---- question_id
	answer_id bigint NULL,	---- answer_id or null
	user_id bigint NOT NULL,
	score smallint NOT NULL,	--- up : +1, down : -1
	voted_on timestamp NULL
);
ALTER TABLE public.vote OWNER TO whosolves;

--- Dependent table to user table (id)
--- Indexed by id & to_id
CREATE TABLE notification(
    id bigint  NOT NULL, --- PK
	to_id bigint NOT NULL, --- PK
    "to" varchar(255) NUll,
	from_id bigint  NULL,
    question_id bigint  NULL,
    answer_id bigint NULL,
    content varchar(1000) NULL,
    verb varchar(255) NOT NULL,
    seen smallint NOT NULL,
	created_on timestamp NOT NULL

	);
ALTER TABLE public.notification OWNER TO whosolves;

--- Dependent table to user table (id)
--- Indexed by id
CREATE TABLE bookmark_list(
    id bigint  NOT NULL, --- PK
    name varchar(255) NULL,
    user_id bigint NOT NULL,
    privacy smallint NULL, --- public : 1, privacy : 2
    questions integer NULL,
    follows integer NULL,
    created_on timestamp NOT NULL
);
ALTER TABLE public.bookmark_list OWNER TO whosolves;

--- Dependent table to question/bookmark_list table (id)
--- Indexed by question_id & list_id
CREATE TABLE bookmark(
    question_id bigint NOT NULL,    ---PK
    list_id bigint NOT NULL         ---PK
);
ALTER TABLE public.bookmark OWNER TO whosolves;

--- Dependent table to user/bookmark_list table (id)
--- Indexed by user_id & list_id
CREATE TABLE bookmark_list_follow(
    user_id bigint NOT NULL,	--- PK
    list_id bigint NOT NULL	--- PK
);
ALTER TABLE public.bookmark_list_follow OWNER TO whosolves;

--- Independent table, Shard by created_on (?)
--- Indexed by keyword_id
--- SHOULD FULLY BE CACHED !!!
CREATE TABLE explore(
	question_id bigint NOT NULL,	--- PK
	votes smallint NOT NULL,	--- sum of vote up minus sum of vote down
	credits smallint NOT NULL,	--- sum of credits
	keyword varchar(32) NOT NULL,	---
	levels varchar(500) NOT NULL,	--- primary school 1 [1st], ..., high school 1 [7th],..., college [13th], undergraduate [14th], master's degree [15th], doctoral [16th], professional school [17th]
	status smallint NOT NULL,	--- open : 1, resolved : 2, cancelled : 3
	created_on timestamp NOT NULL
);

--- Dependent table to user table (id)
--- Indexed by id & user_id
CREATE TABLE "transaction"(
    id bigint  NOT NULL, --- PK
    user_id bigint NOT NULL,
    debit real NULL,
    credit real NULL,
    status smallint NOT NULL, --- blocked: 1, done: 2
    notes varchar(1000) NULL,
    created_on timestamp NOT NULL
);
ALTER TABLE public."transaction" OWNER TO whosolves;



CREATE TABLE queue_default (
  id SERIAL, ---PK
  queue varchar(64) NOT NULL,
  data text NOT NULL,
  status smallint NOT NULL,
  created timestamp NOT NULL,
  scheduled timestamp NOT NULL,
  executed timestamp NULL DEFAULT NULL,
  finished timestamp NULL DEFAULT NULL,
  message varchar(256) DEFAULT NULL,
  trace text,
  PRIMARY KEY (id)
);

ALTER TABLE public.queue_default OWNER TO whosolves;

CREATE TABLE feed(
    id bigint NOT NULL, --PK
    user_id bigint NULL,
    actor_id bigint NULL,
	question_id bigint NULL,
	answer_id bigint NULL,
	content varchar(1000) NULL,
    privacy smallint NOT NULL,	--- public : 1, private : 2
    verb varchar(50) NOT NULL,
	created_on timestamp NOT NULL
	);
ALTER TABLE public.feed OWNER TO whosolves;

CREATE TABLE oauth_client (
    client_id VARCHAR(80) NOT NULL,
    client_secret VARCHAR(80) NOT NULL,
    name varchar(50) NULL,
    redirect_uri VARCHAR(2000) NOT NULL
    );
ALTER TABLE public.oauth_client OWNER TO whosolves;

CREATE TABLE oauth_access_token (
    id serial NOT NULL,
    token VARCHAR(40) NOT NULL,
    token_secret VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );
ALTER TABLE public.oauth_access_token OWNER TO whosolves;

CREATE TABLE oauth_authorization_code (
    authorization_code VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    redirect_uri VARCHAR(2000),
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );
ALTER TABLE public.oauth_authorization_code OWNER TO whosolves;

CREATE TABLE oauth_refresh_token (
    refresh_token VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );
ALTER TABLE public.oauth_refresh_token OWNER TO whosolves;

CREATE table report (
    id serial NOT NULL, ---PK
    reporter_id bigint NOT NULL,
    reason smallint NOT NULL,
    question_id bigint NOT NULL,
    answer_id bigint NULL,
    created_on timestamp NOT NULL
    );
ALTER TABLE public.report OWNER TO whosolves;


CREATE table password (
    user_id bigint NOT NULL,
    request_key varchar(100) NOT NULL,
    request_time timestamp NOT NULL
    );
ALTER TABLE public.password OWNER TO whosolves;


CREATE TABLE session
(
      id character varying(255) NOT NULL,
      name character varying(255),
      modified bigint,
      lifetime integer,
      data text,
      CONSTRAINT session_pkey PRIMARY KEY (id )
)
WITH (
      OIDS=FALSE
);
ALTER TABLE public.session OWNER TO whosolves;


CREATE table reg_request (
    id serial NOT NULL,
    user_id bigint NULL,
    email varchar(100) NOT NULL,
    name varchar(100) NOT NULL,
    education varchar(100) NOT NULL,
    gender smallint NOT NULL,
    age smallint NOT NULL,
    timezone varchar(255) NULL,
    type smallint NULL,
    keywords varchar(1000) NOT NULL,
    request_time timestamp NOT NULL
);
ALTER TABLE public.reg_request OWNER TO whosolves;


CREATE TABLE feedback(
    id              bigint NOT NULL, --PK
    user_id         bigint NOT NULL,
    type            smallint NOT NULL,
    content         varchar(10000) NOT NULL,
    rank            smallint NOT NULL,
    client_ip       varchar(30) NULL,
    user_agent      varchar(255) NULL,
    accept_language varchar(30) NULL,
    created_on      timestamp NOT NULL
);
ALTER TABLE public.feedback OWNER TO whosolves;

DROP TABLE IF EXISTS transaction_log;
CREATE TABLE transaction_log(
    id              serial NOT NULL, --PK
    user_id         bigint NOT NULL,
    solver_id       bigint NULL,
    question_id     bigint NULL,
    user_paid       real NOT NULL,
    partner_name    varchar(50) NULL,
    partner_share   real NOT NULL,
    qolve_share     real NOT NULL,
    question_cost   real NOT NULL,
    status          smallint NOT NULL,
    created_on      timestamp NOT NULL,
    CONSTRAINT transaction_log_pkey PRIMARY KEY (id)
);
ALTER TABLE public.transaction_log OWNER TO whosolves;


--
-- Name: rbac_permission; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY rbac_permission
	ADD CONSTRAINT rbac_permission_pkey PRIMARY KEY (perm_id);

--
-- Name: rbac_role; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY rbac_role
	ADD CONSTRAINT rbac_role_pkey PRIMARY KEY (role_id);

--
-- Name: rbac_role_permission; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY rbac_role_permission
	ADD CONSTRAINT rbac_role_permission_pkey PRIMARY KEY (role_id, perm_id);

--
-- Name: asker_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY asker
    ADD CONSTRAINT asker_pkey PRIMARY KEY (question_id, user_id);


--
-- Name: asker_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY answer
    ADD CONSTRAINT answer_pkey PRIMARY KEY (id);


--
-- Name: comment_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (id);


--
-- Name: document_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY document
    ADD CONSTRAINT document_pkey PRIMARY KEY (id);


--
-- Name: question_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY question
    ADD CONSTRAINT question_pkey PRIMARY KEY (id);


--
-- Name: share_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY share
    ADD CONSTRAINT share_pkey PRIMARY KEY (question_id, user_id);


--
-- Name: keyword_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY keyword
    ADD CONSTRAINT keyword_pkey PRIMARY KEY (id, name);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);

--
-- Name: user_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY user_profile
    ADD CONSTRAINT user_profile_pkey PRIMARY KEY (user_id);

--
-- Name: user_pref_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY user_pref
    ADD CONSTRAINT user_pref_pkey PRIMARY KEY (user_id, "key");


--
-- Name: vote_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY vote
    ADD CONSTRAINT vote_pkey PRIMARY KEY (id);


--
-- Name: notification_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id, to_id);


--
-- Name: bookmark_list_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY bookmark_list
    ADD CONSTRAINT bookmark_list_pkey PRIMARY KEY (id);


--
-- Name: bookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY bookmark
    ADD CONSTRAINT bookmark_pkey PRIMARY KEY (list_id, question_id);


--
-- Name: bookmark_list_follow_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY bookmark_list_follow
    ADD CONSTRAINT bookmark_list_follow_pkey PRIMARY KEY (list_id, user_id);


--
-- Name: user_follow_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY user_follow
    ADD CONSTRAINT user_follow_pkey PRIMARY KEY (user_id, follower_id);


--
-- Name: keyword_follow_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY keyword_follow
    ADD CONSTRAINT keyword_follow_pkey PRIMARY KEY (user_id, keyword_id);

--
-- Name: question_follow_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY question_follow
    ADD CONSTRAINT question_follow_pkey PRIMARY KEY (question_id, user_id);

--
-- Name: user_follow_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY question_keyword
    ADD CONSTRAINT question_keyword_pkey PRIMARY KEY (question_id, keyword_id);

--
-- Name: explore_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY explore
    ADD CONSTRAINT explore_pkey PRIMARY KEY (question_id);

--
-- Name: transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

ALTER TABLE ONLY "transaction"
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);

ALTER TABLE ONLY password
    ADD CONSTRAINT password_pkey PRIMARY KEY (user_id);

--
-- Name: queue_default; Type: CONSTRAINT; Schema: public; Owner: whosolves; Tablespace:
--

---ALTER TABLE ONLY queue_default
   --- ADD CONSTRAINT queue_default_pkey PRIMARY KEY (id);


ALTER TABLE ONLY feed
    ADD CONSTRAINT feed_pkey PRIMARY KEY (id);

ALTER TABLE ONLY oauth_client
    ADD CONSTRAINT oauth_client_pkey PRIMARY KEY (client_id);

ALTER TABLE ONLY oauth_access_token
    ADD CONSTRAINT oauth_access_token_pkey PRIMARY KEY (id);

ALTER TABLE ONLY oauth_authorization_code
    ADD CONSTRAINT oauth_authorization_code_pkey PRIMARY KEY (authorization_code);

ALTER TABLE ONLY oauth_refresh_token
    ADD CONSTRAINT oauth_refresh_token_pkey PRIMARY KEY (refresh_token);

ALTER TABLE ONLY report
    ADD CONSTRAINT report_pkey PRIMARY KEY (id);

ALTER TABLE ONLY answer_privilege
    ADD CONSTRAINT answer_privilege_pkey PRIMARY KEY (user_id, answer_id);

ALTER TABLE ONLY user_social
    ADD CONSTRAINT user_social_pkey PRIMARY KEY (user_id, social);

ALTER TABLE ONLY user_provider
    ADD CONSTRAINT user_provider_pkey PRIMARY KEY (user_id, provider_id);

ALTER TABLE ONLY user_provider_contact
    ADD CONSTRAINT user_provider_contact_pkey PRIMARY KEY (id);

ALTER TABLE ONLY user_gcm
    ADD CONSTRAINT user_gcm_pkey PRIMARY KEY (user_id);

ALTER TABLE ONLY reg_request
    ADD CONSTRAINT reg_request_pkey PRIMARY KEY (id);

ALTER TABLE ONLY feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (id);

--
-- Name: public; Type: ACL; Schema: -; Owner: whosolves
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM whosolves;
GRANT ALL ON SCHEMA public TO whosolves;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--



