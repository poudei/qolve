<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\Feedback;

class RequestAdminRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');
        
        $limit    = $this->params()->fromQuery('limit', 20);
        $offset   = $this->params()->fromQuery('offset', 0);
        
        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);
        
        $requests = $objectManager->getRepository('Qolve\Entity\RegRequest')
            ->getNotRegisteredRequests($offset, $limit, $count);
            
        $_requests = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($requests as $request) {
            $_requests['list'][] = $hydrator->extract($request);
        }
        
        return new JsonModel(array("requests" => $_requests));
    }
}
