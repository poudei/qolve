<?php
namespace Qolve\Controller;

use Application\Controller\AbstractController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\Document,
    Application\Entity\User,
    Zend\Json\Json as Json,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\Crypt\Password\Bcrypt;

class RandomController extends AbstractController
{
    const baseUrl = "http://qolve.dev";

    public function createAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $preUsers = $objectManager->getRepository('Application\Entity\User')
            ->findAll();

        if (count($preUsers) > 0) {
            var_dump('OK');
            exit;
        }

        set_time_limit (0);

        for ($i=1; $i<=20; $i++) {
            $preUsers = $objectManager->getRepository('Application\Entity\User')
                ->findAll();

            $user = $this->createRandomUser($i);

            $n = rand(0, 5);
            for ($j=1; $j<=$n; $j++) {
                $this->createRandomFollower($user, $preUsers);
                $this->createRandomFollowing($user, $preUsers);
            }

            $n = rand(1, 4);
            for ($j=1; $j<=$n; $j++) {
                $this->createRandomQuestion($user, $preUsers);
            }

            echo $i;
        }
        
        var_dump('OK');
        exit;
    }

    public function deleteAction()
    {
        set_time_limit (0);

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $data = $objectManager->getRepository('Qolve\Entity\Answer')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\AnswerPrivilege')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Asker')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Bookmark')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\BookmarkList')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\BookmarkListFollow')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Comment')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Document')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Keyword')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\KeywordFollow')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Question')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\QuestionFollow')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\QuestionKeyword')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Report')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Share')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Transaction')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\UserFollow')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Qolve\Entity\Vote')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Application\Entity\Feed')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Application\Entity\Notification')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Application\Entity\User')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Application\Entity\UserPref')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Application\Entity\UserProfile')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();

        $data = $objectManager->getRepository('Application\Entity\UserProvider')
            ->findAll();
        foreach ($data as $obj) {
            $objectManager->remove($obj);
        }
        $objectManager->flush();
        
        
        //Clear Cache
        exec("php index.php orm:clear-cache:metadata");
        exec("php index.php orm:clear-cache:query");
        exec("php index.php orm:clear-cache:result");
        

        var_dump("Done");
        exit;
    }

    private function createRandomUser($i)
    {
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $now  = new \DateTime('now', new \DateTimeZone('UTC'));

        $passCost = $this->getServiceLocator()
            ->get('ZfcUser\Authentication\Adapter\Db')
            ->getOptions()
            ->getPasswordCost();
        $bcrypt = new Bcrypt();
        $bcrypt->setCost($passCost);

        $user = new User();
        $user->setEmail("qolve".$i."@qolve.com");
        $user->setPassword($bcrypt->create('123456'));
        $user->setName('Qolve'.$i);
        $user->setUsername('qolve'.$i);
        $user->setStatus(1);
        $user->setDeleted(0);
        $user->setCreatedOn($now);
        $user->setModifiedOn($now);

        $objectManager->persist($user);
        $objectManager->flush($user);

        $userId = $user->getId();

        //        $prof = imagecreate(115, 115);
        //
        //        $bg         = imagecolorallocate($prof, 255, 255, 255);
        //        $text_color = imagecolorallocate($prof, 180,180,180);
        //
        //        imagestring($prof, 2, 10, 40, $i ." : ".$userId, $text_color);

        $prof = imagecreatefromjpeg(
            PUBLIC_PATH . '/../data/sample/users/' .
            rand(1, 10) . '.jpg');

        if (!file_exists("./public/upfiles/users/$userId/profile")) {
            mkdir("./public/upfiles/users/$userId/profile", 0777, true);
        }

        imagejpeg($prof, PUBLIC_PATH . "/upfiles/users/$userId/profile/prof".$i.".jpeg");
        imagedestroy($prof);

        $user->setImagePath("upfiles/users/$userId/profile/prof".$i.".jpeg");
        $objectManager->flush($user);

        //Add user to Elastic
        $createdOn = $user->getCreatedOn()->format('Y-m-d H:i:s');
        $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));
            
        $elastica  = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];
        
        $item = array (
            'user_id'    => $user->getId(),
            'name'       => $user->getname(),
            'username'   => $user->getUsername(),
            'email'      => $user->getEmail(),
            'status'     => $user->getStatus(),
            'deleted'    => $user->getDeleted(),
            'created_on' => $createdOn
        );
        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('user');
        $content       = new \Elastica\Document($user->getId(), $item);

        $elasticaType->addDocument($content);
        $elasticaType->getIndex()->refresh();

        return $user;
    }

    private function createRandomFollower($user, $preUsers)
    {
        $r = rand(0, count($preUsers)-1);
        if ($r >= count($preUsers)) {
            return;
        }
        $follower = $preUsers[$r];

        $ch = curl_init(self::baseUrl . "/api/user/login");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "identity=".$follower->getEmail()."&credential=123456");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        preg_match('/(?<=PHPSESSID\=)(.+?)((?=\;+))/', $result, $sessionId);
        $sessionId = $sessionId[1];

        $data = array (
            'userId' => $user->getId()
        );

        $ch = curl_init(self::baseUrl . "/api/followings");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=$sessionId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    private function createRandomFollowing($user, $preUsers)
    {
        $r = rand(0, count($preUsers)-1);
        if ($r >= count($preUsers)) {
            return;
        }
        $following = $preUsers[$r];

        $ch = curl_init(self::baseUrl . "/api/user/login");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "identity=".$user->getEmail()."&credential=123456");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        preg_match('/(?<=PHPSESSID\=)(.+?)((?=\;+))/', $result, $sessionId);
        $sessionId = $sessionId[1];

        $data = array (
            'userId' => $following->getId()
        );

        $ch = curl_init(self::baseUrl . "/api/followings");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=$sessionId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    private function createRandomQuestion($user, $preUsers)
    {
        $descriptions = array(
            "Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis.",
            "Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis. Proin odio risus tristique. Nunc convallis ultricies duis sapien, leo suspendisse nunc ultrices. Pretium elit eu lorem feugiat, lorem nisl sit debitis in vitae.",
            "Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis. Proin odio risus tristique. Nunc convallis ultricies duis sapien, leo suspendisse nunc ultrices. Pretium elit eu lorem feugiat, lorem nisl sit debitis in vitae. Enim donec et sociis porttitor amet eget, molestie mi ac per nibh metus amet, lorem vel suscipit, eleifend aenean id molestie, euismod fames aliquam. Lorem suspendisse elit ultrices eros, mattis magna vehicula volutpat elit ante, enim vestibulum torquent erat mauris maecenas elementum, accumsan condimentum nulla nam proin ut. Elit arcu feugiat convallis in interdum, et risus, mauris molestie in ac, dignissim diam pede felis leo pretium nullam, placerat nec molestie aliquet. Massa ultricies orci ullamcorper urna suspendisse, pede mi consectetuer ante metus, velit donec mi habitant aenean modi, nulla lacinia lorem lacus conubia, lacinia magna dictum. Tristique venenatis lorem. Vel neque at netus mauris felis ante, sed vel et malesuada urna accumsan, pede libero est pretium, pretium ut dolor ornare mi. Ut ut dui pellentesque nunc nulla, vestibulum dolor nibh imperdiet, aliquam elementum placerat vehicula pede, nam scelerisque ultricies odio amet id vestibulum. A at quisque.",
            " Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis. Proin odio risus tristique. Nunc convallis ultricies duis sapien, leo suspendisse nunc ultrices. Pretium elit eu lorem feugiat, lorem nisl sit debitis in vitae. Enim donec et sociis porttitor amet eget, molestie mi ac per nibh metus amet, lorem vel suscipit, eleifend aenean id molestie, euismod fames aliquam. Lorem suspendisse elit ultrices eros, mattis magna vehicula volutpat elit ante, enim vestibulum torquent erat mauris maecenas elementum, accumsan condimentum nulla nam proin ut. Elit arcu feugiat convallis in interdum, et risus, mauris molestie in ac, dignissim diam pede felis leo pretium nullam, placerat nec molestie aliquet. Massa ultricies orci ullamcorper urna suspendisse, pede mi consectetuer ante metus, velit donec mi habitant aenean modi, nulla lacinia lorem lacus conubia, lacinia magna dictum. Tristique venenatis lorem. Vel neque at netus mauris felis ante, sed vel et malesuada urna accumsan, pede libero est pretium, pretium ut dolor ornare mi. Ut ut dui pellentesque nunc nulla, vestibulum dolor nibh imperdiet, aliquam elementum placerat vehicula pede, nam scelerisque ultricies odio amet id vestibulum. A at quisque.Wisi pede, bibendum vitae a eros lacus quis, at phasellus, suspendisse nulla facilisis curabitur erat, metus wisi suspendisse eleifend. Urna lacus tellus cras dictum. Enim elit a morbi, tellus et sagittis ut felis pulvinar. Volutpat blandit leo lacus vulputate. Et nec vitae, rhoncus nec auctor, a justo est diam, pellentesque lectus posuere habitasse, lacinia erat ante et nunc.",
            " Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis. Proin odio risus tristique. Nunc convallis ultricies duis sapien, leo suspendisse nunc ultrices. Pretium elit eu lorem feugiat, lorem nisl sit debitis in vitae. Enim donec et sociis porttitor amet eget, molestie mi ac per nibh metus amet, lorem vel suscipit, eleifend aenean id molestie, euismod fames aliquam. Lorem suspendisse elit ultrices eros, mattis magna vehicula volutpat elit ante, enim vestibulum torquent erat mauris maecenas elementum, accumsan condimentum nulla nam proin ut. Elit arcu feugiat convallis in interdum, et risus, mauris molestie in ac, dignissim diam pede felis leo pretium nullam, placerat nec molestie aliquet. Massa ultricies orci ullamcorper urna suspendisse, pede mi consectetuer ante metus, velit donec mi habitant aenean modi, nulla lacinia lorem lacus conubia, lacinia magna dictum. Tristique venenatis lorem. Vel neque at netus mauris felis ante, sed vel et malesuada urna accumsan, pede libero est pretium, pretium ut dolor ornare mi. Ut ut dui pellentesque nunc nulla, vestibulum dolor nibh imperdiet, aliquam elementum placerat vehicula pede, nam scelerisque ultricies odio amet id vestibulum. A at quisque.Wisi pede, bibendum vitae a eros lacus quis, at phasellus, suspendisse nulla facilisis curabitur erat, metus wisi suspendisse eleifend. Urna lacus tellus cras dictum. Enim elit a morbi, tellus et sagittis ut felis pulvinar. Volutpat blandit leo lacus vulputate. Et nec vitae, rhoncus nec auctor, a justo est diam, pellentesque lectus posuere habitasse, lacinia erat ante et nunc. Et commodo. Eget bibendum risus. Litora tellus, iaculis quisque ligula orci urna venenatis quam, sagittis in eu mauris morbi, bibendum risus. Proin erat, mauris tincidunt vel sit, a suscipit consequat quis scelerisque amet nec. Vitae porta elementum euismod pellentesque. Malesuada ante, lectus lacus sed enim orci commodi. Leo ultrices ligula elit at, a morbi vitae, wisi enim nam, sem imperdiet aenean platea interdum metus."
        );
        
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $ch = curl_init(self::baseUrl . "/api/user/login");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "identity=".$user->getEmail()."&credential=123456");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        preg_match('/(?<=PHPSESSID\=)(.+?)((?=\;+))/', $result, $sessionId);
        $sessionId = $sessionId[1];

        //create description
        if (rand(1,2) == 2) {
            $n = rand(1,3);
            $shareds = array();
            for ($i=1; $i<=$n; $i++) {
                $r = rand(0, count($preUsers)-1);
                if ($r >= count($preUsers)) {
                    continue;
                }
                $shareds[] = array(
                    'id'        => $preUsers[$r]->getId(),
                    'username'  => $preUsers[$r]->getEmail()
                );
            }
            $shareds = array_unique($shareds);
        } else {
            $shareds = array();
        }

        $n = rand(1,5);
        $keys  = array();
        for ($i=1; $i<=$n; $i++) {
            $keys[] = '#key' . rand(1, 100);
        }
        $keys = array_unique($keys);
        if (!empty($keys)) {
            $keys = join(' ', $keys);
        } else {
            $keys = '';
        }
        
        $description = $descriptions[rand(0, count($descriptions) - 1)];

        $data = array (
            "levels"      => "high school 1, primary school 2, primary school 3",
            "language"    => "en",
            "credits"     => 0,
            "privacy"     => empty($shareds) ? Question::PRIVACY_PUBLIC : Question::PRIVACY_SHARED,
            "description" => "$keys $description",
            "visibility"  => \Qolve\Entity\Asker::VISIBILITY_PUBLIC,
            "share"       => $shareds
        );

        $ch = curl_init(self::baseUrl . "/api/questions");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=$sessionId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $response = json_decode(strstr($result, '{'), true);
        $question = $objectManager->getRepository('Qolve\Entity\Question')
            ->find($response['feed']['question']['id']);

        //create comments
        $n = rand(0, 5);
        for ($i=1; $i<=$n; $i++) {
            $this->createRandomComment($question, null, $preUsers);
        }

        //create answers
        $n = rand(1,2);
        if ($n == 1) {
            $n = rand(0, 15);
        } else {
            $n = rand(15, 20);
        }
        
        for ($i=1; $i<=$n; $i++) {
            $this->createRandomAnswer($question, $preUsers);
        }

        $userId     = $user->getId();
        $questionId = $question->getId();

        //create documents
        $n = rand(1,4);
        for ($i=1; $i<=$n; $i++) {
            $document = new Document();
            $document->setOrder($i);
            $document->setType(Document::TYPE_IMAGE);
            $document->setLocation("upfiles/users/$userId/questions/$questionId/pic".$i.".jpeg");
            $document->setQuestionId($questionId);
            $objectManager->persist($document);
            $objectManager->flush($document);

            //            $im = imagecreate(614, 278);
            //
            //            $bg         = imagecolorallocate($im, 237, 237, 237);
            //            $text_color = imagecolorallocate($im, $red = rand(100, 200), $blue = rand(100, 200), $green = rand(100, 200));
            //
            //            imagestring($im, 5, 200, 125,  "$i :question $questionId", $text_color);

            $im = imagecreatefromjpeg(
                PUBLIC_PATH . '/../data/sample/questions_answers/' .
                rand(1, 2) . '.jpg');

            if (!file_exists("./public/upfiles/users/$userId/questions/$questionId")) {
                mkdir("./public/upfiles/users/$userId/questions/$questionId", 0777, true);
            }

            imagejpeg($im, PUBLIC_PATH . "/upfiles/users/$userId/questions/$questionId/pic".$i.".jpeg");
            imagedestroy($im);
        }
    }

    private function createRandomComment($question, $answer, $preUsers)
    {
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        if (count($preUsers) == 0) {
            return;
        }

        if ($question->getPrivacy() == Question::PRIVACY_PUBLIC) {
            $writer = $preUsers[rand(0, count($preUsers)-1)];

        } else {
            $shareds = $objectManager->getRepository('Qolve\Entity\Share')
                ->findByQuestionId($question->getId());

            $r = rand(0, count($shareds)-1);
            foreach ($preUsers as $_user) {
                similar_text($_user->getId(), $shareds[$r]->getUserId(), $percent);
                if ($percent == 100) {
                    $writer = $_user;
                }
            }
        }

        $ch = curl_init(self::baseUrl . "/api/user/login");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "identity=".$writer->getEmail()."&credential=123456");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        preg_match('/(?<=PHPSESSID\=)(.+?)((?=\;+))/', $result, $sessionId);
        $sessionId = $sessionId[1];

        if (!isset($answer)) {
            $content = $writer->getName().' writes comment for question '.$question->getId().'.';
            $url = self::baseUrl . "/api/questions/".$question->getId()."/comments";
        } else {
            $content = $writer->getName().' writes comment for answer '.$answer->getId().'.';
            $url = self::baseUrl . "/api/questions/".$question->getId()."/answers/".$answer->getId()."/comments";
        }

        $data = array(
            'content' => $content
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=$sessionId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    private function createRandomAnswer($question, $preUsers)
    {
        $descriptions = array(
            "Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis.",
            "Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis. Proin odio risus tristique. Nunc convallis ultricies duis sapien, leo suspendisse nunc ultrices. Pretium elit eu lorem feugiat, lorem nisl sit debitis in vitae.",
            "Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis. Proin odio risus tristique. Nunc convallis ultricies duis sapien, leo suspendisse nunc ultrices. Pretium elit eu lorem feugiat, lorem nisl sit debitis in vitae. Enim donec et sociis porttitor amet eget, molestie mi ac per nibh metus amet, lorem vel suscipit, eleifend aenean id molestie, euismod fames aliquam. Lorem suspendisse elit ultrices eros, mattis magna vehicula volutpat elit ante, enim vestibulum torquent erat mauris maecenas elementum, accumsan condimentum nulla nam proin ut. Elit arcu feugiat convallis in interdum, et risus, mauris molestie in ac, dignissim diam pede felis leo pretium nullam, placerat nec molestie aliquet. Massa ultricies orci ullamcorper urna suspendisse, pede mi consectetuer ante metus, velit donec mi habitant aenean modi, nulla lacinia lorem lacus conubia, lacinia magna dictum. Tristique venenatis lorem. Vel neque at netus mauris felis ante, sed vel et malesuada urna accumsan, pede libero est pretium, pretium ut dolor ornare mi. Ut ut dui pellentesque nunc nulla, vestibulum dolor nibh imperdiet, aliquam elementum placerat vehicula pede, nam scelerisque ultricies odio amet id vestibulum. A at quisque.",
            " Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis. Proin odio risus tristique. Nunc convallis ultricies duis sapien, leo suspendisse nunc ultrices. Pretium elit eu lorem feugiat, lorem nisl sit debitis in vitae. Enim donec et sociis porttitor amet eget, molestie mi ac per nibh metus amet, lorem vel suscipit, eleifend aenean id molestie, euismod fames aliquam. Lorem suspendisse elit ultrices eros, mattis magna vehicula volutpat elit ante, enim vestibulum torquent erat mauris maecenas elementum, accumsan condimentum nulla nam proin ut. Elit arcu feugiat convallis in interdum, et risus, mauris molestie in ac, dignissim diam pede felis leo pretium nullam, placerat nec molestie aliquet. Massa ultricies orci ullamcorper urna suspendisse, pede mi consectetuer ante metus, velit donec mi habitant aenean modi, nulla lacinia lorem lacus conubia, lacinia magna dictum. Tristique venenatis lorem. Vel neque at netus mauris felis ante, sed vel et malesuada urna accumsan, pede libero est pretium, pretium ut dolor ornare mi. Ut ut dui pellentesque nunc nulla, vestibulum dolor nibh imperdiet, aliquam elementum placerat vehicula pede, nam scelerisque ultricies odio amet id vestibulum. A at quisque.Wisi pede, bibendum vitae a eros lacus quis, at phasellus, suspendisse nulla facilisis curabitur erat, metus wisi suspendisse eleifend. Urna lacus tellus cras dictum. Enim elit a morbi, tellus et sagittis ut felis pulvinar. Volutpat blandit leo lacus vulputate. Et nec vitae, rhoncus nec auctor, a justo est diam, pellentesque lectus posuere habitasse, lacinia erat ante et nunc.",
            " Lorem ipsum dolor sit amet, aptent aliquam rutrum taciti dis. Proin odio risus tristique. Nunc convallis ultricies duis sapien, leo suspendisse nunc ultrices. Pretium elit eu lorem feugiat, lorem nisl sit debitis in vitae. Enim donec et sociis porttitor amet eget, molestie mi ac per nibh metus amet, lorem vel suscipit, eleifend aenean id molestie, euismod fames aliquam. Lorem suspendisse elit ultrices eros, mattis magna vehicula volutpat elit ante, enim vestibulum torquent erat mauris maecenas elementum, accumsan condimentum nulla nam proin ut. Elit arcu feugiat convallis in interdum, et risus, mauris molestie in ac, dignissim diam pede felis leo pretium nullam, placerat nec molestie aliquet. Massa ultricies orci ullamcorper urna suspendisse, pede mi consectetuer ante metus, velit donec mi habitant aenean modi, nulla lacinia lorem lacus conubia, lacinia magna dictum. Tristique venenatis lorem. Vel neque at netus mauris felis ante, sed vel et malesuada urna accumsan, pede libero est pretium, pretium ut dolor ornare mi. Ut ut dui pellentesque nunc nulla, vestibulum dolor nibh imperdiet, aliquam elementum placerat vehicula pede, nam scelerisque ultricies odio amet id vestibulum. A at quisque.Wisi pede, bibendum vitae a eros lacus quis, at phasellus, suspendisse nulla facilisis curabitur erat, metus wisi suspendisse eleifend. Urna lacus tellus cras dictum. Enim elit a morbi, tellus et sagittis ut felis pulvinar. Volutpat blandit leo lacus vulputate. Et nec vitae, rhoncus nec auctor, a justo est diam, pellentesque lectus posuere habitasse, lacinia erat ante et nunc. Et commodo. Eget bibendum risus. Litora tellus, iaculis quisque ligula orci urna venenatis quam, sagittis in eu mauris morbi, bibendum risus. Proin erat, mauris tincidunt vel sit, a suscipit consequat quis scelerisque amet nec. Vitae porta elementum euismod pellentesque. Malesuada ante, lectus lacus sed enim orci commodi. Leo ultrices ligula elit at, a morbi vitae, wisi enim nam, sem imperdiet aenean platea interdum metus."
        );
        
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        if (count($preUsers) == 0) {
            return;
        }

        if ($question->getPrivacy() == Question::PRIVACY_PUBLIC) {
            $writer = $preUsers[rand(0, count($preUsers)-1)];

        } else {
            $shareds = $objectManager->getRepository('Qolve\Entity\Share')
                ->findByQuestionId($question->getId());

            $r = rand(0, count($shareds)-1);
            foreach ($preUsers as $_user) {
                similar_text($_user->getId(), $shareds[$r]->getUserId(), $percent);
                if ($percent == 100) {
                    $writer = $_user;
                }
            }
        }

        $ch = curl_init(self::baseUrl . "/api/user/login");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "identity=".$writer->getEmail()."&credential=123456");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        preg_match('/(?<=PHPSESSID\=)(.+?)((?=\;+))/', $result, $sessionId);
        $sessionId = $sessionId[1];

        $data = array(
            'description' => $descriptions[rand(0, count($descriptions) - 1)]
        );

        $ch = curl_init(self::baseUrl . "/api/questions/".$question->getId()."/answers");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=$sessionId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode(strstr($result, '{'), true);

        if (!isset($response['answer']['id'])) {
            return;
        }

        $answer = $objectManager->getRepository('Qolve\Entity\Answer')
            ->find($response['answer']['id']);

        $userId     = $writer->getId();
        $questionId = $question->getId();
        $answerId   = $answer->getId();

        //create documents
        $n = rand(1,4);
        for ($i=1; $i<=$n; $i++) {
            $document = new Document();
            $document->setOrder($i);
            $document->setType(Document::TYPE_IMAGE);
            $document->setLocation("upfiles/users/$userId/questions/$questionId/answers/$answerId/answerpic".$i.".jpeg");
            $document->setQuestionId($questionId);
            $document->setAnswerId($answerId);
            $objectManager->persist($document);
            $objectManager->flush($document);

            //            $im = imagecreate(614, 278);
            //
            //            $bg         = imagecolorallocate($im, 237, 237, 237);
            //            $text_color = imagecolorallocate($im, $red = rand(100, 200), $blue = rand(100, 200), $green = rand(100, 200));
            //
            //            imagestring($im, 5, 200, 125,  "$i : Answer $answerId", $text_color);

            $im = imagecreatefromjpeg(
                PUBLIC_PATH . '/../data/sample/questions_answers/' .
                rand(1, 2) . '.jpg');

            if (!file_exists("./public/upfiles/users/$userId/questions/$questionId/answers/$answerId")) {
                mkdir("./public/upfiles/users/$userId/questions/$questionId/answers/$answerId", 0777, true);
            }

            imagejpeg($im, PUBLIC_PATH . "/upfiles/users/$userId/questions/$questionId/answers/$answerId/answerpic".$i.".jpeg");
            imagedestroy($im);
        }

        //create comments
        $n = rand(0,5);
        for ($i=1; $i<=$n; $i++) {
            $this->createRandomComment($question, $answer, $preUsers);
        }
    }
}
