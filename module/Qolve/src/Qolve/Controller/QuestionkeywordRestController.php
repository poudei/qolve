<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Keyword,
    Qolve\Entity\QuestionKeyword,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

class QuestionkeywordRestController extends AbstractRestfulController
{
    public function create($keys)
    {
       if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');

        $builder    = new AnnotationBuilder();
        $questionId = $this->getEvent()
            ->getRouteMatch()
            ->getParam('question_id');
        $keyRepo    = $objectManager->getRepository('Qolve\Entity\Keyword');

        !is_array($keys['keyword']) ?
            $keys['keyword'] = array($keys['keyword']) :
            '';

        foreach ($keys['keyword'] as $key) {

            $key     = strtolower($key);
            $keyword = $keyRepo->getKeyByName($key);
            if (!$keyword instanceof Keyword) {

                $keyword = $objectManager
                    ->getRepository('Qolve\Entity\Keyword')
                    ->insertKey($key);

                $questionKey = new QuestionKeyword();
                $questionKey->setQuestionId($questionId);
                $questionKey->setKeywordId($keyword->getId());
                $objectManager->persist($questionKey);

                $objectManager->flush();
            }
        }

        return new JsonModel(array('success'));
    }

    public function delete($name)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $hydrator   = $this->getServiceLocator()->get('Hydrator');
        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');

        $keyword    = $objectManager
            ->getRepository('Qolve\Entity\Keyword')
            ->getKeyByName($name);

        if (!$keyword instanceof Keyword) {

            $this->getResponse()->setStatusCode(
                Error::KeywordNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::KeywordNotFound_message
                )
            );
        }

        $keywordData = $hydrator->extract($keyword);
        $keywordId   = $keywordData['id'];
        $questKey    = $objectManager
            ->getRepository('Qolve\Entity\QuestionKeyword')
            ->find(array(
                'questionId' => $questionId, 
                'keywordId'  => $keywordId)
        );

        if ($questKey instanceof QuestionKeyword) {
            $objectManager->remove($questKey);
            $objectManager->flush();
            return new JsonModel(array('success'));

        } else {
            $this->getResponse()->setStatusCode(
                Error::QuestionKeywordNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionKeywordNotFound_message
                )
            );
        }
    }
}
