<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\Asker,
    Qolve\Entity\Keyword,
    Qolve\Entity\Document,
    Qolve\Entity\Answer,
    Qolve\Entity\QuestionKeyword,
    Qolve\Entity\Share,
    Qolve\Entity\Report,
    Qolve\Entity\Transaction,
    Qolve\Entity\TransactionLog,
    Qolve\Library\Constants,
    Application\Entity\Feed,
    Application\Entity\User,
    Application\Entity\UserPref,
    Application\Entity\UserProfile,
    Application\Job\SendNotification,
    Application\Job\PublishFeed,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

    require_once(VENDOR_PATH . '/stripe-php/lib/Stripe.php');

class QuestionRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');

        $limit           = $this->params()->fromQuery('limit', 10);
        $offset          = $this->params()->fromQuery('offset', 0);
        $lastId          = $this->params()->fromQuery('lastId');
        $orderBy         = $this->params()->fromQuery('orderBy', 'modifiedOn');
        $orderDir        = $this->params()->fromQuery('orderDir', 'DESC');
        $byComment       = $this->params()->fromQuery('comment', false);
        $byAnswer        = $this->params()->fromQuery('answer', false);
        $byDocument      = $this->params()->fromQuery('document', true);

        $limit           = $this->normalize('limit', $limit);
        $offset          = $this->normalize('offset', $offset);

        $order = array($orderBy => $orderDir);

        $info = array(
            'keywords',
            'userAnswerId',
            'asker'
        );

        if ($byAnswer) {
            $info[] = 'answersList';
        }

        if ($byComment) {
            $info[] = 'commentsList';
        }

        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $user = $this->identity();

        if (!$user instanceof User) {
            $criteria = array();
        } else {
            $userId   = $user->getId();
            $criteria = array();
            if ($lastId) {
                $criteria['id'] = array(
                    'greaterThan' => $lastId
                );
            }

            $elastica = $this->getServiceLocator()
                ->get('Elastica\Client');
            $elasticaClient = $elastica['client'];
            $index          = $elastica['index'];
            $keywordFollows = $objectManager
                ->getRepository('Qolve\Entity\KeywordFollow')
                ->findByUserId($userId);

            if (!empty($keywordFollows)) {
                foreach ($keywordFollows as $keywordFollow) {
                    $keyword = $objectManager
                        ->getRepository('Qolve\Entity\Keyword')
                            ->find($keywordFollow->getkeywordId());
                    if ($keyword instanceof Keyword) {
                        $keywords[] = $keyword->getName();
                    }
                }
            }

            $elasticaQuery = new \Elastica\Query();
            $search        = new \Elastica\Search($elasticaClient);
            $elasticaIndex = $elasticaClient->getIndex($index);
            $elasticaType  = $elasticaIndex->getType('question');

            if (!empty($keywords)) {
                $boolMust   = new \Elastica\Query\Bool();
                $boolShould = new \Elastica\Query\Bool();
                foreach ($keywords as $key) {
                    $term = new \Elastica\Query\Term();
                    $term->setParam('keywords', trim(strtolower($key)));
                    $boolShould->addShould($term);
                }
                $boolMust->addMust($boolShould);

                $fromDate = date('Y-m-d H:i:s', strtotime('-7 days', strtotime('now')));
                $range    = new \Elastica\Query\Range();

                $range->addField('last_update', array('from' => $fromDate));
                $boolMust->addMust($range);

                $elasticaQuery = new \Elastica\Query($boolMust);

                $elasticaQuery->setSort(array('recent_votes' => array('order' => 'desc')));
                $elasticaQuery->setFrom($offset);
                $elasticaQuery->setSize($limit);

                $resultSet = $search->addIndex($index)
                    ->addType('question')
                    ->search($elasticaQuery);
                $count = $search->addIndex($index)
                    ->addType('question')
                    ->count($elasticaQuery);
                foreach ($resultSet as $result) {

                    $returns[] = $result->getData();
                }
            }
        }

        if (empty($returns)) {
            return new JsonModel(array(
                "questions" => array(
                    'list'  => array(),
                    'count' => 0
                    )
                )
            );
        }

        $_questions = array(
            'list'  => array(),
            'count' => $count
        );

        foreach ($returns as $return) {

            $question = $objectManager->getRepository('Qolve\Entity\Question')
                ->find($return['question_id']);
            if ($question instanceof Question) {
                $_question = $hydrator->extract($question, $info, $user);

                $votes = $objectManager->getRepository('Qolve\Entity\Vote')
                    ->findBy(array(
                        'questionId' => $question->getId()
                    ), array(
                        'votedOn' => 'DESC'
                    ), 3, 0
                );

                $_question['recentVoters'] = array();
                if (!empty($votes)) {
                    foreach ($votes as $vote) {
                        $voter = $objectManager->getRepository('Application\Entity\User')
                            ->find($vote->getUserId());
                        if ($voter instanceof User) {
                            $_question['lastVoters'][] = $hydrator->extract($voter);
                        }
                    }
                }
            }
            $_questions['list'][] = $_question;

        }

        return new JsonModel(array("questions" => $_questions));
    }

    public function get($id)
    {
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $userId             = $this->getEvent()->getRouteMatch()->getParam('user_id');

        $byComment          = $this->params()->fromQuery('comment', false);
        $commentsLimit      = $this->normalize('limit', $this->params()->fromQuery('commentsLimit', 5));
        $commentsOffset     = $this->normalize('offset', $this->params()->fromQuery('commentsOffset', 0));

        $byAnswer           = $this->params()->fromQuery('answer', true);
        $answersLimit       = $this->normalize('limit', $this->params()->fromQuery('answersLimit', 5));
        $answersOffset      = $this->normalize('offset', $this->params()->fromQuery('answersOffset', 0));

        $byDocument         = $this->params()->fromQuery('document', true);
        $documentsLimit     = $this->normalize('limit', $this->params()->fromQuery('documentsLimit', 100));
        $documentsOffset    = $this->normalize('offset', $this->params()->fromQuery('documentsOffset', 0));

        $currentUser = $this->identity();

        $info = array(
            'asker',
            'keywords',
            'userAnswerId'
        );

        if ($byAnswer) {
            $info[] = 'answersList';
        }

        if ($byComment) {
            $info[] = 'commentsList';
        }

        if ($byDocument) {
            $info[] = 'documentsList';
        }

        if (!empty($userId)) {
            $user = $objectManager->getRepository('Application\Entity\User')->find($userId);

            if (!$user instanceof User) {
                $this->getResponse()->setStatusCode(
                    Error::UserNotFound_code
                );
                return new JsonModel(
                    array(
                        'errors' => [
				 'code'    => Error::UserNotFound_code,
				 'message' => Error::UserNotFound_message 
				 ]
                    )
                );
            }

            $asker = $objectManager->getRepository('Qolve\Entity\Asker')
                ->findOneBy(array('userId' => $userId, 'questionId' => $id));
            if (!$asker instanceof Asker) {
                $this->getResponse()->setStatusCode(
                    Error::AskerNotFound_code
                );
                return new JsonModel(
                    array(
                        'errors' => [
				 'code'    => Error::AskerNotFound_code,
				 'message' => Error::AskerNotFound_message 
				 ]
                    )
                );
            }

            $question = $objectManager->getRepository('Qolve\Entity\Question')
                ->find($asker->getQuestionId());

        } else {
            $question = $objectManager->getRepository('Qolve\Entity\Question')->find($id);
            if (!$question instanceof Question) {
                $this->getResponse()->setStatusCode(
                    Error::QuestionNotFound_code
                );
                return new JsonModel(
                    array(
                        'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                    )
                );
            }
        }

        $_question = $hydrator->extract($question, $info, $currentUser);

        return new JsonModel(array("question" => $_question));
    }

    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                [
                    'errors' => [
        				'code'    => Error::AuthenticationFailed_code,
        				'message' => Error::AuthenticationFailed_message 
        			]
                ]
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                [
                    'errors' => [
        				'code'    => Error::UserUnavailable_code,
        				'message' => Error::UserUnavailable_message 
        			]
                ]
            );
        }

        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $user = $this->identity();

        $info = [
            'keywords',
            'asker',
            'documentsList',
            'commentsList',
            'answersList'
        ];


        if (  (!isset($data['files'])
              || empty($data['files']))
            &&
              (!isset($data['description'])
              || empty($data['description']))
        ) {

                $this->getResponse()->setStatusCode(400);
                return new JsonModel(
                    array(
                        'errors' => [
				 'code'    => Error::EmptyData_code,
				 'message' => Error::EmptyData_message 
				 ]
                    )
                );
            }

        if (isset($data['files'])) {

            $files = $data['files'];
            foreach ($files as $file) {
                if (!file_exists('public/'.$file['location'])) {
                    $this->getResponse()->setStatusCode(404);
                    return new JsonModel(
                        [
                            'errors' => [
                				'code'    => Error::FileNotFound_code,
                				'message' => Error::FileNotFound_message 
                			]
                        ]
                    );
                }
            }
        }
        if (!isset($data['credits'])) {

            $data['credits'] = (float) 0;
        } else {
            $data['credits'] = (float) ($data['credits']);
        }

        if ($data['credits'] < 0) {

            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                [
                    'errors' => [
        				'code'    => Error::CreditIsNegative_code,
        				'message' => Error::CreditIsNegative_message 
        			]
                ]
            );
        }

        $partnerName  = null;
        $qolveShare   = null;
        $partnerShare = null;
        $userPaid     = null;
        $data['device'] = isset($data['device']) ? 
            $data['device'] : 
            null;

        if ($data['credits'] > 0) {
            if (   ($data['device'] == Constants::IOS_DEVICE)
                || ($data['device'] == Constants::ANDROID_DEVICE)
            ) {
                
                $qolveShare      = $data['credits'] * 0.1;
                $partnerShare    = $data['userPaid'] - $data['credits'];
                $data['credits'] = $data['credits'] * 0.9;
                $userPaid        = $data['userPaid'];

                if ($data['device'] == Constants::IOS_DEVICE) {
                    $partnerName = !isset($data['partnerName']) ? 
                        Constants::IOS_PARTNER :
                        $data['partnerName'];
                } else {
                    $partnerName = !isset($data['partnerName']) ? 
                        Constants::GOOGLE_PARTNER :
                        $data['partnerName'];
                }

            } else {
                empty($data['cardToken']) ? $data['cardToken'] = null : '';
                empty($data['cardId']) ? $data['cardId'] = null : '';
 
                $payment  = $this->getServiceLocator()
                    ->get('payment');

                //charge amount with stripe
                $charge = $payment->charge(
                    $user,
                    $data['cardToken'],
                    $data['cardId'],
                    $data['credits']
                );

                if (!$charge) {
                    $this->getResponse()->setStatusCode(400);
                    return new JsonModel(
                        [   
                            'error' => 'Token Id and Card Id not send'
                        ]
                    );
                }
            }
        }

        if (   !isset($data['privacy'])
            || (array_search($data['privacy'], [1,2]) === false)) {
            $data['privacy'] = Question::PRIVACY_PUBLIC;
        }

        if (   $data['privacy'] == Question::PRIVACY_SHARED
            && empty($data['share'])) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::ShareNotSent_code,
				 'message' => Error::ShareNotSent_message 
				 ]
                )
            );
        }

        $keywords = [];
        $mentions = [];

        if (isset($data['description'])) {

            preg_match_all('/(?<=\#)(.+?)((?=\s+)|$)/',
                $data['description'], $keywords
            );
            preg_match_all('/(?<=\@)(.+?)((?=\s+)|$)/',
                $data['description'], $mentions
            );
            $keywords = array_map('strtolower', $keywords[1]);
            $mentions = array_map('strtolower', $mentions[1]);
        }

        if (isset($data['answerPrivilege'])) {
            if (   $data['answerPrivilege'] < 1
                || $data['answerPrivilege'] > 2
            ) {
                $data['answerPrivilege'] = Question::ANSWER_PRIVILEGE_PROTECTED;
            }
        }

        if ($data['privacy'] == Question::PRIVACY_PUBLIC) {
            if ($data['credits'] > 0) {

                $data['answerPrivilege'] = isset($data['answerPrivilege'])
                    ? $data['answerPrivilege']
                    : Question::ANSWER_PRIVILEGE_PROTECTED;
            }

            if ($data['credits'] == 0) {

                $data['answerPrivilege'] = Question::ANSWER_PRIVILEGE_PUBLIC;
            }
        }

        if ($data['privacy'] == Question::PRIVACY_SHARED) {
            $data['answerPrivilege'] = Question::ANSWER_PRIVILEGE_PUBLIC;
        }

        if (isset($data['levels'][0])) {
            !is_array($data['levels']) ?
                $data['levels'] = array($data['levels']) : '';

            $data['levels'] = implode(',', $data['levels']);

            if (preg_match('/\,$/', $data['levels'])) {
                $data['levels'] = preg_replace('/\,$/', '', $data['levels']);
            }

            if (preg_match('/\,/', $data['levels'])) {

                $data['levels'] = explode(',', $data['levels']);
                $data['levels'] = array_map(
                    function ($level) {return trim($level);},
                    $data['levels']
                );
                $data['levels'] = implode(',', $data['levels']);
            }
        } else {
            $data['levels'] = "General";
        }

        if (isset($data['deadline'])) {
            if (isset($data['timezone'])) {
                    $timezone = $data['timezone'];
                    $userPref = $objectManager
                        ->getRepository('Application\Entity\UserPref')
                        ->find(
                            [
                                'userId' => $user->getId(),
                                'key'    => 'timezone'
                            ]
                    );

                    if (   ($userPref instanceof UserPref)
                        && ($userPref->getValue() != $timezone)
                    ) {
                        $userPref->setValue($timezone);
                        $objectManager->persist($userPref);
                    } else if (!$userPref instanceof UserPref) {

                        $userPref = new UserPref();
                        $userPref->setUserId($user->getId());
                        $userPref->setKey('timezone');
                        $userPref->setValue($timezone);
                        $objectManager->persist($userPref);
                    }
                    date_default_timezone_set($timezone);
            } else {

                $userProfile = array();
                $userProfile = $objectManager
                    ->getRepository('Application\Entity\UserProfile')
                    ->find($user->getId());

                if ($userProfile instanceof UserProfile
                    && !is_null($userProfile->getTimezone())
                ) {
                    date_default_timezone_set($userProfile->getTimezone());

                } else {
                    $timezone = date_default_timezone_get();
                    date_default_timezone_set($timezone);
                }
            }
            $deadline = new \DateTime($data['deadline']);
            $deadline = $deadline->format('r');
            $deadline = gmdate('Y-m-d H:i:s', strtotime($deadline));
            $deadline = new \DateTime($deadline, new \DateTimeZone('UTC'));
            $now      = new \DateTime('now', new \DateTimeZone('UTC'));

            if ($deadline < $now) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(
                    array(
                        'errors' => [
				 'code'    => Error::DeadlineInvalid_code,
				 'message' => Error::DeadlineInvalid_message 
				 ]
                    )
                );
            }

            $data['deadline'] = $deadline;

        }

        $hydrator  = $this->getServiceLocator()->get('Hydrator');
        $builder   = new AnnotationBuilder();
        $question  = new Question();
        $asker     = new Asker();

        $askerForm = $builder->createForm($asker);
        $form      = $builder->createForm($question);
        
        $keyRepo   = $objectManager->getRepository('Qolve\Entity\Keyword');
        $form->setHydrator($hydrator);
        $askerForm->setHydrator($hydrator);
        $askerForm->bind($asker);
        $form->bind($question);
        $form->setData($data);

        if ($form->isValid()) {
            $question = $form->getData();
            $now = new \DateTime('now', new \DateTimeZone('UTC'));
            $question->setStatus(Question::STATUS_OPEN);
            $question->setAskers(1);
            $question->setVoters(0);
            $question->setPaid(1);
            $question->setVotes(0);
            $question->setComments(0);
            $question->setAnswers(0);
            $question->setDeleted(false);
            $question->setCreatedOn($now);
            $question->setAnswerPrivilege($data['answerPrivilege']);
            $objectManager->persist($question);
            $askerData = array(
                'credit'     => $question->getCredits(),
                'visibility' => !empty($data['visibility'])
                    ? (int)$data['visibility'] : Asker::VISIBILITY_PUBLIC
            );
            $askerForm->setData($askerData);

            if (!$askerForm->isValid()) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array(
                    'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $askerForm->getMessages() 
				 ]
                    )
                );
            }

            $asker = $askerForm->getData();
            $asker->setQuestionId($question->getId());
            $asker->setUserId($user->getId());
            $asker->setStatus(Asker::STATUS_ASKER);
            $asker->setCredit($data['credits']);
            $asker->setAskedOn($now);
            $objectManager->persist($asker);

            //Add to user's Activities (and Timeline)
            $feed = new Feed();
            $feed->setUserId($user->getId());
            $feed->setActorId($user->getId());
            $feed->setQuestionId($question->getId());
            $feed->setPrivacy(max(array(
                $question->getPrivacy(),
                $asker->getVisibility()))
            );
            $feed->setVerb('CreateQuestion');
            $feed->setCreatedOn($now);
            $objectManager->persist($feed);

            if (!empty($keywords)) {
                foreach ($keywords as $key => $name) {
                    $keyword = $objectManager
                        ->getRepository('Qolve\Entity\Keyword')
                        ->getKeyByName($name);
                    if (!$keyword instanceof Keyword) {
                        $keyword = $objectManager
                            ->getRepository('Qolve\Entity\Keyword')
                            ->insertKey($name);
                        if (!$keyword instanceof Keyword) {
                            unset($keywords[$key]);
                        }
                    }
                }
                foreach ($keywords as $name) {
                    $objectManager
                            ->getConnection()
                            ->getShardManager()
                            ->selectGlobal();

                    $keyword         = $objectManager
                        ->getRepository('Qolve\Entity\Keyword')
                        ->getKeyByName($name);

                    $keywordData     = $hydrator->extract($keyword);
                    $keywordId       = $keywordData['id'];
                    $questionKey     = new QuestionKeyword();
                    $questionKeyForm = $builder->createForm($questionKey);
                    $questionKeyForm->setHydrator($hydrator);
                    $questionKeyForm->bind($questionKey);

                    if (!$questionKeyForm->isValid()) {

                        $this->getResponse()->setStatusCode(400);
                        return new JsonModel(array(
                            'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $questionKeyForm->getMessages() 
				 ]
                            )
                        );

                    }

                    $questionKey = $questionKeyForm->getData();
                    $questionKey->setQuestionId($question->getId());
                    $questionKey->setKeywordId($keywordId);
                    $objectManager->persist($questionKey);

                }
            }

            if (!empty($data['share'])) {

                $shareUsers = $data['share'];

                foreach ($shareUsers as $shareUser => &$value) {
                    if (empty($value['id'])) {

                        $return = $objectManager
                            ->getRepository('Application\Entity\User')
                            ->getOneByElastic(
                                $serviceLocator, 
                                'username', 
                                $value['username']
                            );

                        $newUser = array();
                        if (!empty($return)) {
                            $newUser = $objectManager
                                ->getRepository('Application\Entity\User')
                                ->find($return['user_id']);
                        }

                        if (!$newUser instanceof User) {
                            unset($shareUsers[$shareUser]);
                            continue;
                        }

                        $value['id'] = $newUser->getId();
                    }
                    $share  = new Share();
                    $share->setQuestionId($question->getId());
                    $share->setUserId($value['id']);
                    $objectManager->persist($share);

                    $sharedIds[] = $value['id'];
                }
            }
            //create Transaction
            if ($asker->getCredit() > 0) {
                $notes = json_encode(array(
                    'questionId' => $question->getId()
                ));

                $transaction = new Transaction();
                $transaction->setUserId($asker->getUserId());
                $transaction->setDebit($asker->getCredit());
                $transaction->setStatus(Transaction::STATUS_BLOCKED);
                $transaction->setNotes($notes);
                $transaction->setCreatedOn($now);
                $objectManager->persist($transaction);
            }

            // *** crate transaction log *** //
            if (   ($asker->getCredit() > 0) 
                && (   ($data['device'] == Constants::IOS_DEVICE)
                    || ($data['device'] == Constants::ANDROID_DEVICE)
                )
            ) {
                $transactionLog = new TransactionLog();
                $transactionLog->setUserId($asker->getUserId());
                $transactionLog->setUserPaid($userPaid);
                $transactionLog->setQolveShare($qolveShare);
                $transactionLog->setPartnerName($partnerName);
                $transactionLog->setPartnerShare($partnerShare);
                $transactionLog->setQuestionCost($asker->getCredit());
                $transactionLog->setQuestionId($question->getId());
                $transactionLog->setCreatedOn($now);

                if ($data['device'] == Constants::IOS_DEVICE) {
                    $transactionLog->setStatus(Constants::PAY_FROM_IOS_TO_QOLVE);
                } else {
                    $transactionLog->setStatus(Constants::PAY_FROM_GOOGLE_TO_QOLVE);
                }

                $objectManager->persist($transactionLog);
            }

            $objectManager->flush();

            // *** Insert into Elastica *** //

            $elastica  = $this->getServiceLocator()
                ->get('Elastica\Client');

            $elasticaClient  = $elastica['client'];
            $index           = $elastica['index'];

            if (!empty($keywords)) {
                $keywords = array_map('strtolower', $keywords);
            }

            $createdOn = $question->getCreatedOn()->format('Y-m-d H:i:s');
            $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));
            $levels    = $question->getLevels();
            $deadline  = $question->getDeadline();

            if (!empty($deadline)) {
                $deadline  = $question->getDeadline()->format('Y-m-d H:i:s');
            }

            if (!empty($levels)) {

                if (!preg_match('/\,/', $levels)) {

                    $levels = preg_replace('/\s/', '', $levels);
                    $levels = array($levels);

                } else {

                    $levels = explode(',', $levels);
                    $levels = array_map(function ($level) {
                        return preg_replace('/\s/', '', $level);},
                        $levels
                    );
                }
            }

            $item = [
                'question_id' => $question->getId(),
                'answers'     => $question->getAnswers(),
                'votes'       => (int)$question->getVotes(),
                'levels'      => $levels,
                'credits'     => (float)$question->getCredits(),
                'keywords'    => $keywords,
                'status'      => (int)$question->getStatus(),
                'paid'        => (int)$question->getPaid(),
                'privacy'     => (int)$question->getPrivacy(),
                'recent_votes'=> 0,
                'last_update' => 0,
                'deadline'    => $deadline,
                'created_on'  => $createdOn
            ];

            $elasticaIndex = $elasticaClient->getIndex($index);
            $elasticaType  = $elasticaIndex->getType('question');
            $content       = new \Elastica\Document($question->getId(), $item);

            $elasticaType->addDocument($content);
            $elasticaType->getIndex()->refresh();

            //Create question activity in elasticsearch
            $params = [
                'question_id'     => $question->getId(),
                'user_id'         => $user->getId(),
                'reaskers'        => null,
                'claimed'         => null,
                'accepted'        => null,
                'best_answer'     => null,
                'question_report' => null,
                'answer_report'   => null,
                'bought'          => null,
                'keywords'        => $keywords,
                'credits'         => (float)$question->getCredits(),
                'paid'            => $question->getPaid(),
                'status'          => (int)$question->getStatus(),
                'deadline'        => $deadline,
                'created_on'      => $createdOn
            ];

            $objectManager
                ->getRepository('Qolve\Entity\Question')
                ->setQuestionActivity(
                    $this->getServiceLocator(),
                    $params
            );

            if (   $question->getPrivacy() == Question::PRIVACY_PUBLIC
                && $asker->getVisibility() == Asker::VISIBILITY_PUBLIC) {

                    //Add to Queue for publish Feed
                    $feedsQueue = $this->serviceLocator
                        ->get('\SlmQueue\Queue\QueuePluginManager')
                        ->get('feeds');
                    $data = array(
                        'verb'          => '\Qolve\Verb\CreateQuestion',
                        'questionId'    => $question->getId(),
                        'askerId'       => $user->getId(),
                        'keywords'      => !empty($keywords) ? $keywords : NULL
                    );

                    $data['to'] = array('userFollowers');
                    $job = new PublishFeed($this->serviceLocator);
                    $job->setContent($data);
                    $feedsQueue->push($job);


                    //Add to Queue for publish Status
                /*
                   $queue = $this->serviceLocator->get('\SlmQueue\Queue\QueuePluginManager')
                   ->get('status');
                   $data = array(
                   'verb'          => '\Qolve\Verb\CreateQuestion',
                   'questionId'    => $question->getId(),
                   'askerId'       => $user->getId()
                   );
                   $job = new \Application\Job\PublishStatus($this->serviceLocator);

                   $job->setContent($data);
                $queue->push($job);*/
            }

            //Add to Queue for send Notification
            $queue = $this->serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('notifications');
            $data = array(
                'verb'       => '\Qolve\Verb\CreateQuestion',
                'questionId' => $question->getId(),
                'askerId'    => $user->getId()
            );

            if (   ($question->getPrivacy() == Question::PRIVACY_SHARED)
                && !empty($data['share'])) {
                $data['to']          = 'shareds'; //high
                $data['sharedUsers'] = $sharedIds;
                $job = new SendNotification($this->serviceLocator);
                $job->setContent($data);
                $queue->push($job);
            } else if (   $question->getPrivacy()
                       == Question::PRIVACY_PUBLIC
            ){
                $mentionsId = array();
                foreach($mentions as $mention) {

                    $return = $objectManager
                        ->getRepository('Application\Entity\User')
                        ->getOneByElastic($serviceLocator, 'username', $mention);

                    $mention = array();
                    if (!empty($return)) {
                        $mention = $objectManager
                            ->getRepository('Application\Entity\User')
                            ->find($return['user_id']);
                    }

                    if (!$mention instanceof User) {
                        continue;
                    }
                    $mentionsId[] = $mention->getId();
                }
                if (!empty($mentionsId)) {
                    $data['to']       = 'mentions';
                    $data['mentions'] = $mentionsId;

                    $job = new SendNotification($this->serviceLocator);
                    $job->setContent($data);
                    $queue->push($job);
                }

                $data['to'] = 'userFollowers';
                $job = new SendNotification($this->serviceLocator);
                $job->setContent($data);
                $queue->push($job);
            }
            $userId     = $user->getId();
            $questionId = $question->getId();

            // upload file for web-browser
            if (!empty($files)) {
                foreach ($files as $file) {
                    $fileName    = $file['name'];
                    $tempPath    = $file['location'];
                    $location    = "/upfiles/users/$userId/questions/$questionId/$fileName";

                    if (!file_exists("./public/upfiles/users/$userId/questions/$questionId/")) {
                        mkdir("./public/upfiles/users/$userId/questions/$questionId/",
                            0777,
                            true
                        );
                    }
                    $destination = copy("./public/$tempPath" ,
                        "./public/upfiles/users/$userId/questions/$questionId/$fileName"
                    );
                    if (!$destination) {

                        unlink("./public/$tempPath");
                        system('rm -rf ' . escapeshellarg("./public/upfiles/tempo/$userId"),
                            $retval
                        );
                        $this->getResponse()->setStatusCode(500);
                        return new JsonModel(
                            array(
                                'errors' => [
				 'code'    => Error::FileTransferingProblem_code,
				 'message' => Error::FileTransferingProblem_message 
				 ]
                            )
                        );
                    }

                    $type = substr($file['type'], 0, 5);
                    switch ($type) {
                    case 'image':
                        $type = Document::TYPE_IMAGE;
                        break;
                    case 'video':
                        $type = Document::TYPE_VIDEO;
                        break;
                    }

                    $data = array(
                        'order'    => $file['order'],
                        'type'     => (int)$type,
                        'location' => $location
                    );
                    list($width, $height) =
                        getimagesize(PUBLIC_PATH.$location);
                    $document = new Document();
                    $form     = $builder->createForm($document);
                    $form->setHydrator($hydrator);
                    $form->bind($document);
                    $form->setData($data);
                    if (!$form->isValid()) {
                        $this->getResponse()->setStatusCode(400);
                        return new JsonModel(
                            array(
                                'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]
                            )
                        );
                    }
                    $document = $form->getData();

                    $document->setQuestionId($question->getId());
                    $document->setWidth((int)$width);
                    $document->setHeight((int)$height);
                    $objectManager->persist($document);
                    $objectManager->flush($document);

                    unlink("./public".$tempPath);
                }
                system('rm -rf ' . escapeshellarg("./public/upfiles/tempo/$userId"),
                    $retval
                );
            }

            return new JsonModel(array(
                'feed' => $hydrator->extract($feed, $info, $user)
            ));

        } else {

            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]));
        }
    }

    public function update($id, $data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AuthenticationFailed_code,
				 'message' => Error::AuthenticationFailed_message 
				 ]
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserUnavailable_code,
				 'message' => Error::UserUnavailable_message 
				 ]
                )
            );
        }

        $info = array(
            'keywords',
            'asker',
            'documentsList',
            'commentsList',
            'answersList'
        );
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $hydrator    = $this->getServiceLocator()->get('Hydrator');
        $user        = $this->identity();
        $userId      = $user->getId();
        $preKeyNames = array();
        $preShare    = array();
        $answer      = $objectManager
            ->getRepository('Qolve\Entity\Answer')
            ->findByQuestionId($id);
        $question    = $objectManager
            ->getRepository('Qolve\Entity\Question')
            ->find($id);
        $asker       = $objectManager
            ->getRepository('Qolve\Entity\Asker')
            ->findOneBy(array('questionId' => $id, 'userId' => $userId));
        $preData     = $hydrator->extract($question);

        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionIsNotOpen_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionIsNotOpen_code,
				 'message' => Error::QuestionIsNotOpen_message 
				 ]
                )
            );
        }

        if (!$asker instanceof Asker
            || $asker->getStatus() != Asker::STATUS_ASKER
        ) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::PermissionDenied_code,
				 'message' => Error::PermissionDenied_message 
				 ]
                )
            );
        }

        if (!empty($answer)) {
            $this->getResponse()->setStatusCode(
                Error::QuestionHasAnswer_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionHasAnswer_code,
				 'message' => Error::QuestionHasAnswer_message 
				 ]
                )
            );
        }

        if ($preData['status'] != Question::STATUS_OPEN) {
            $this->getResponse()->setStatusCode(
                Error::QuestionIsNotOpen_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionIsNotOpen_code,
				 'message' => Error::QuestionIsNotOpen_message 
				 ]
                )
            );
        }
        $editableProperties = [
            'files',
            'deadline',
            'timezone',
            'language',
            'privacy',
            'levels',
            'description',
            'keyword',
            'share',
            'credits',
            'visibility',
            'answerPrivilege',
            'cardToken',
            'cardId',
            'device',
            'userPaid',
            'partnerName'
        ];

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (isset($data['files'])) {
            $files = $data['files'];
            unset($data['files']);
        }

        if (isset($data['share'])) {
            $currentShares = $data['share'];
            unset($data['share']);
        }

        if (isset($data['levels'])) {

            if (preg_match('/\,$/', $data['levels'])) {

                $data['levels'] = preg_replace('/\,$/', '', $data['levels']);

            }

            if (preg_match('/\,/', $data['levels'])) {
                $data['levels'] = array($data['levels']);
                $data['levels'] = implode(',', $data['levels']);
            }
        } else {
            $data['levels'] = "General";
        }

        $keywords = array();
        $mentions = array();
        if (isset($data['description'])) {

            preg_match_all('/(?<=\#)(.+?)((?=\s+)|$)/',
                $data['description'], $keywords
            );

            preg_match_all('/(?<=\@)(.+?)((?=\s+)|$)/',
                $data['description'], $mentions
            );
            $preDesc  = $preData['description'];
            preg_match_all('/(?<=\@)(.+?)((?=\s+)|$)/',
                $preData['description'], $preMentions
            );

            $preMentions = array_map('strtolower', $preMentions[1]);
            $keywords    = array_map('strtolower', $keywords[1]);
            $mentions    = array_map('strtolower', $mentions[1]);
        }

        if (isset($data['credits'])) {

            $data['credit'] = $data['credits'];
            unset($data['credits']);
        }

        if (isset($data['privacy'])
            && $data['privacy'] != $question->getPrivacy()
        ) {
            $privacyIsChanged = true;
        } else {
            $privacyIsChanged = false;
        }

        if (isset($data['visibility'])
            && $data['visibility'] != $asker->getVisibility()
        ) {
            $visibilityIsChanged = true;
        } else {
            $visibilityIsChanged = false;
        }

        if (isset($data['deadline'])) {
            if (isset($data['timezone'])) {
                    $timezone = $data['timezone'];
                    $userPref = $objectManager
                        ->getRepository('Application\Entity\UserPref')
                        ->find(array(
                            'userId' => $user->getId(),
                            'key'    => 'timezone'
                    ));

                    if (   ($userPref instanceof UserPref)
                        && ($userPref->getValue() != $timezone)
                    ) {
                        $userPref->setValue($timezone);
                        $objectManager->persist($userPref);
                    } else if (!$userPref instanceof UserPref) {

                        $userPref = new UserPref();
                        $userPref->setUserId($user->getId());
                        $userPref->setKey('timezone');
                        $userPref->setValue($timezone);
                        $objectManager->persist($userPref);
                    }
                    date_default_timezone_set($timezone);
            } else {

                $userProfile = array();
                $userProfile = $objectManager
                    ->getRepository('Application\Entity\UserProfile')
                    ->find($user->getId());

                if ($userProfile instanceof UserProfile) {
                    date_default_timezone_set($userProfile->getTimezone());

                } else {
                    $timezone = date_default_timezone_get();
                    date_default_timezone_set($timezone);
                }
            }

            $deadline = new \DateTime($data['deadline']);
            $deadline = $deadline->format('r');
            $deadline = gmdate('Y-m-d H:i:s', strtotime($deadline));
            $deadline = new \DateTime($deadline, new \DateTimeZone('UTC'));
            $now      = new \DateTime('now', new \DateTimeZone('UTC'));

            if ($deadline < $now) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(
                    [
                        'errors' => [
                            'code'    => Error::DeadlineInvalid_code,
                            'message' => Error::DeadlineInvalid_message 
                        ]
                    ]
                );
            }
            $data['deadline'] = $deadline;
        }

        $data     = array_merge($preData, $data);
        $builder  = new AnnotationBuilder();
        $form     = $builder->createForm($question);
        $form->setHydrator($hydrator);
        $form->setBindOnValidate(false);
        $form->bind($question);
        $form->setData($data);

        if ($form->isValid()) {

            $form->bindValues();
            $now = new \DateTime('now', new \DateTimeZone('UTC'));

            $privacy = $question->getPrivacy();

            if ($privacy == Question::PRIVACY_PUBLIC) {
                $shares = $objectManager
                    ->getRepository('Qolve\Entity\Share')
                    ->findByQuestionId($question->getId());
                foreach ($shares as $share) {
                    if ($share instanceof Share) {
                        $objectManager->remove($share);
                    }
                }
            }

            if (isset($data['credit'])) {
                if (!isset($data['device'])) {
                    $this->getResponse()->setStatusCode(400);
                    return new JsonModel(
                        [
                            'errors' => [
                                'code'    => Error::Undefined_device_code,
                                'message' => Error::Undefined_device_message 
                            ]
                        ]
                    );
                }

                $qolveShare     = null;
                $partnerShare   = null;
                $partnerName    = null;
                $userPaid       = null;
                $data['device'] = isset($data['device']) ? 
                    $data['device'] : 
                    null;

                if (   ($data['device'] == Constants::IOS_DEVICE)
                    || ($data['device'] == Constants::ANDROID_DEVICE)
                ) {

                    $qolveShare      = $data['credit'] * 0.1;
                    $partnerShare    = $data['userPaid'] - $data['credit'];
                    $data['credit']  = $data['credit'] * 0.9;
                    $userPaid        = $data['userPaid'];
                    if ($data['device'] == Constants::IOS_DEVICE) {
                        $partnerName = !isset($data['partnerName']) ? 
                            Constants::IOS_PARTNER :
                            $data['partnerName'];
                    } else {
                        $partnerName = !isset($data['partnerName']) ? 
                            Constants::GOOGLE_PARTNER :
                            $data['partnerName'];
                    }
                }

                empty($data['cardToken']) ? $data['cardToken'] = null : '';
                empty($data['cardId']) ? $data['cardId'] = null : '';

                $askerResult = $objectManager
                    ->getRepository('Qolve\Entity\Asker')
                    ->raiseCredit(
                        $this->getServiceLocator(),
                        $data['credit'],
                        $question,
                        $user,
                        $data['cardToken'],
                        $data['cardId'],
                        $userPaid,
                        $qolveShare,
                        $partnerShare,
                        $partnerName,
                        $data['device']
                );


                if (is_array($askerResult) && isset($askerResult['error'])) {
                    $this->getResponse()->setStatusCode(400);
                    return new JsonModel(array(
                        'error' => $askerResult['error']['message']
                    ));
                }

                $charge         = $askerResult['charge'];
                $askerResult    = $askerResult['asker'];
                $info['charge'] = $charge;
            }


            if(isset($data['visibility'])) {
                !empty($data['visibility']) ?
                    $data['visibility'] = (int)$data['visibility'] :
                    $data['visibility'] = Asker::VISIBILITY_PUBLIC;

                $asker->setVisibility($data['visibility']);
                $objectManager->persist($asker);
            }

            $currentShareId = array();
            if (   !empty($currentShares)
                && $privacy == Question::PRIVACY_SHARED
            ) {
                foreach ($currentShares as &$currentShare) {

                    if (empty($currentShare['id'])) {

                        $return = $objectManager
                            ->getRepository('Application\Entity\User')
                            ->getOneByElastic(
                                $serviceLocator,
                                'username',
                                $currentShare['username']
                        );

                        $newUser = array();
                        if (!empty($return)) {
                            $newUser = $objectManager
                                ->getRepository('Application\Entity\User')
                                ->find($return['user_id']);
                        }

                        if ($newUser instanceof User) {
                            $currentShareId[] = $newUser->getId();
                        }


                    } else {
                        $currentShareId[] = $currentShare['id'];

                    }
                }

                $shares = $objectManager
                    ->getRepository('Qolve\Entity\Share')
                    ->findByQuestionId($question->getId());

                foreach ($shares as $share) {
                    if (!$share instanceof Share) {
                        $this->getResponse()->setStatusCode(
                            Error::Error_code
                        );
                        return new JsonModel(
                            array(
                                'errors' => [
				 'code'    => Error::Error_code,
				 'message' => Error::Error_message 
				 ]
                            )
                        );
                    }
                    $share  = $hydrator->extract($share);
                    array_push($preShare, $share['userId']);
                }
                sort($preShare);
                sort($currentShareId);

                $sameShares   = array_intersect($currentShareId, $preShare);
                $deleteShares = array_diff($preShare, $sameShares);
                $newShares    = array_diff($currentShareId, $sameShares);

                if ($preShare !== $currentShareId) {
                    foreach ($deleteShares as $deleteshare) {
                        $share = $objectManager
                            ->getRepository('Qolve\Entity\Share')
                            ->findOneBy(array(
                                'userId'     => $deleteshare,
                                'questionId' => $id
                            )
                        );
                        if (!$share instanceof Share) {
                            continue;
                        }
                        $objectManager->remove($share);
                    }

                    if (!empty($newShares)) {
                        foreach ($newShares as $newShare) {
                            $share = new Share();
                            $share->setQuestionId($id);
                            $share->setUserId($newShare);
                            $objectManager->persist($share);
                        }
                    }
                }
            }
            if (!empty($keywords)) {
                $questionKeywords = $objectManager
                    ->getRepository('Qolve\Entity\QuestionKeyword')
                    ->findByQuestionId($id);

                foreach ($questionKeywords as $questionKeyword) {

                    if (!$questionKeyword instanceof QuestionKeyword) {
                        $this->getResponse()->setStatusCode(
                            Error::QuestionKeywordNotFound_code
                        );
                        return new JsonModel(
                            array(
                                'errors' => [
				 'code'    => Error::QuestionKeywordNotFound_code,
				 'message' => Error::QuestionKeywordNotFound_message 
				 ]
                            )
                        );
                    }
                }

                foreach ($questionKeywords as $questionKeyword) {

                    $keyword = $objectManager
                            ->getRepository('Qolve\Entity\Keyword')
                            ->find($questionKeyword->getKeywordId());

                    array_push($preKeyNames, $keyword->getName());
                }

                sort($preKeyNames);
                sort($keywords);

                $sameKeys   = array_intersect($keywords, $preKeyNames);
                $deleteKeys = array_diff($preKeyNames, $sameKeys);
                $newKeys    = array_diff($keywords, $sameKeys);

                if ($preKeyNames !== $keywords) {
                    foreach ($deleteKeys as $name) {
                        $keyword = $objectManager
                            ->getRepository('Qolve\Entity\Keyword')
                            ->getKeyByName($name);
                        if (!$keyword instanceof Keyword) {
                            $this->getResponse()->setStatusCode(
                                Error::KeywordNotFound_code
                            );
                            return new JsonModel(
                                array(
                                    'errors' => [
				 'code'    => Error::KeywordNotFound_code,
				 'message' => Error::KeywordNotFound_message 
				 ]
                                )
                            );
                        }

                        $preKeyword       = $hydrator->extract($keyword);
                        $keywordId        = $preKeyword['id'];
                        $questionKeyword  = $objectManager
                            ->getRepository('Qolve\Entity\QuestionKeyword')
                            ->findOneBy(array(
                                'questionId' => $id,
                                'keywordId'  => $keywordId)
                        );

                        if (!$questionKeyword instanceof QuestionKeyword) {
                            $this->getResponse()->setStatusCode(
                                Error::QuestionKeywordNotFound_code
                            );

                            return new JsonModel(
                                array(
                                    'errors' => [
				 'code'    => Error::QuestionKeywordNotFound_code,
				 'message' => Error::QuestionKeywordNotFound_message 
				 ]
                                )
                            );
                        }

                        $objectManager->remove($questionKeyword);
                    }
                    if (!empty($newKeys)) {
                        foreach ($newKeys as $name) {

                            $name    = strtolower($name);
                            $keyword = $objectManager
                                ->getRepository('Qolve\Entity\Keyword')
                                ->getKeyByName($name);
                            if (!$keyword instanceof Keyword) {
                                $keyword = $objectManager
                                    ->getRepository('Qolve\Entity\Keyword')
                                    ->insertKey($name);
                                if ($keyword instanceof Keyword){
                                    $keywordId = $keyword->getId();
                                }

                            } else {
                                $preKeyword = $hydrator->extract($keyword);
                                $keywordId  = $preKeyword['id'];
                            }

                            $questionKey     = new QuestionKeyword();
                            $questionKeyForm = $builder->createForm($questionKey);
                            $questionKeyForm->setHydrator($hydrator);
                            $questionKeyForm->bind($questionKey);

                            if (!$questionKeyForm->isValid()) {

                                $this->getResponse()->setStatusCode(
                                    Error::FormInvalid_code
                                );
                                return new JsonModel(
                                    array(
                                        'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $questionKeyForm->getMessages() 
				 ]
                                    )
                                );
                            }

                            $questionKey = $questionKeyForm->getData();
                            $questionKey->setQuestionId($question->getId());
                            $questionKey->setKeywordId($keywordId);
                            $objectManager->persist($questionKey);

                        }
                    }
                }
            }


            //delete previous feeds and notifications
            $objectManager->getRepository('Application\Entity\Notification')
                ->deleteBy(
                    array(
                        'fromId'     => $userId,
                        'questionId' => $question->getId(),
                        'verb'       => 'UpdateQuestion'
                    )
                );

            $objectManager->getRepository('Application\Entity\Feed')
                ->deleteBy(
                    array(
                        'actorId'    => $userId,
                        'questionId' => $question->getId(),
                        'verb'       => 'UpdateQuestion'
                    )
                );


            //Add to user's Activities
            $feed = new Feed();
            $feed->setUserId($userId);
            $feed->setActorId($userId);
            $feed->setQuestionId($question->getId());
            $feed->setPrivacy(max(array(
                $question->getPrivacy(),
                $asker->getVisibility()))
            );
            $feed->setVerb('UpdateQuestion');
            $feed->setCreatedOn($now);
            $objectManager->persist($feed);

            //update feed privacy
            if ($privacyIsChanged || $visibilityIsChanged) {
                //for CreateQuestion
                $feedPrivacy = max(array(
                    $question->getPrivacy(),
                    $asker->getVisibility()
                ));
                $objectManager->getRepository('Application\Entity\Feed')
                    ->updatePrivacy(array(
                        'actorId'    => $asker->getUserId(),
                        'questionId' => $question->getId(),
                        'verb'       => 'CreateQuestion'
                    ), $feedPrivacy);
            }

            if ($privacyIsChanged) {
                //for Vote(question)
                $objectManager->getRepository('Application\Entity\Feed')
                    ->updatePrivacy(array(
                        'questionId' => $question->getId(),
                        'answerId'   => null,
                        'verb'       => 'Vote'
                    ), $question->getPrivacy());

                //for CreateAnswer, UpdateAnswer, Vote(answer)
                $answers = $objectManager->getRepository('Qolve\Entity\Answer')
                    ->findByQuestionId($question->getId());
                foreach ($answers as $_answer) {
                    $feedPrivacy = max(array(
                        $question->getPrivacy(),
                        $_answer->getPrivacy(),
                        $_answer->getVisibility()
                    ));
                    $objectManager->getRepository('Application\Entity\Feed')
                        ->updatePrivacy(array(
                            'actorId'    => $_answer->getUserId(),
                            'questionId' => $question->getId(),
                            'answerId'   => $_answer->getId(),
                            'verb'       => 'CreateAnswer'
                        ), $feedPrivacy);
                    $objectManager->getRepository('Application\Entity\Feed')
                        ->updatePrivacy(array(
                            'actorId'    => $_answer->getUserId(),
                            'questionId' => $question->getId(),
                            'answerId'   => $_answer->getId(),
                            'verb'       => 'UpdateAnswer'
                        ), $feedPrivacy);

                    $feedPrivacy = max(array(
                        $question->getPrivacy(),
                        $_answer->getPrivacy()
                    ));
                    $objectManager->getRepository('Application\Entity\Feed')
                        ->updatePrivacy(array(
                            'questionId' => $question->getId(),
                            'answerId'   => $_answer->getId(),
                            'verb'       => 'Vote'
                        ), $feedPrivacy);
                }

                //for RaiseCredit, ReaskQuestion
                $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($question->getId());
                foreach ($askers as $_asker) {
                    $feedPrivacy = max(array(
                        $question->getPrivacy(),
                        $_asker->getVisibility()
                    ));
                    $objectManager->getRepository('Application\Entity\Feed')
                    ->updatePrivacy(array(
                        'actorId'    => $_asker->getUserId(),
                        'questionId' => $question->getId(),
                        'verb'       => 'RaiseCredit'
                    ), $feedPrivacy);
                    $objectManager->getRepository('Application\Entity\Feed')
                    ->updatePrivacy(array(
                        'actorId'    => $_asker->getUserId(),
                        'questionId' => $question->getId(),
                        'verb'       => 'ReaskQuestion'
                    ), $feedPrivacy);
                }
            }

            //update Question's Files
            $questionId = $question->getId();
            $preDocs    =  $objectManager
                ->getRepository('\Qolve\Entity\Document')
                ->findByQuestionId($questionId);

            if (isset($files[0])) {

                if (!empty($preDocs)) {

                    foreach ($preDocs as $preDoc) {
                        $preDocsId[] = $preDoc->getId();
                    }
                    foreach ($files as $file) {
                        !empty($file['id']) ? $docsId[] = $file['id'] : '';
                    }

                    if (!empty($docsId)) {
                        $sameDocs   = array_intersect($docsId, $preDocsId);
                        $deleteDocs = array_diff($preDocsId, $sameDocs);
                        foreach ($deleteDocs as $deleteId) {
                            $preDoc =  $objectManager
                                ->getRepository('\Qolve\Entity\Document')
                                ->find($deleteId);
                            if ($preDoc instanceof Document) {
                                unlink("./public".$preDoc->getLocation());
                                $objectManager->remove($preDoc);
                            }
                        }
                    } else {

                        foreach ($preDocs as $preDoc) {
                            unlink("./public".$preDoc->getLocation());
                            $objectManager->remove($preDoc);
                        }
                    }
                }

                foreach ($files as $file) {
                    if (empty($file['id']) ) {
                        $fileName    = $file['name'];
                        $tempPath    = $file['location'];
                        $location    = "/upfiles/users/$userId/questions/$questionId/$fileName";

                        if (!file_exists("./public/upfiles/users/$userId/questions/$questionId/")) {
                            mkdir("./public/upfiles/users/$userId/questions/$questionId/",
                                0777,
                                true
                            );
                        }
                        $destination = copy("./public".$tempPath ,
                            "./public/upfiles/users/$userId/questions/$questionId/$fileName"
                        );
                        if (!$destination) {

                            unlink("./public".$tempPath);
                            system('rm -rf ' . escapeshellarg("./public/upfiles/tempo/$userId"),
                                $retval
                            );

                            $this->getResponse()->setStatusCode(
                                Error::FileTransferingProblem_code
                            );
                            return new JsonModel(
                                array(
                                    'errors' => [
				 'code'    => Error::FileTransferingProblem_code,
				 'message' => Error::FileTransferingProblem_message 
				 ]
                                )
                            );
                        }
                        list($width, $height) =
                            getimagesize(PUBLIC_PATH.$location);
                        $type = substr($file['type'], 0, 5);
                        switch ($type) {
                        case 'image':
                            $type = Document::TYPE_IMAGE;
                            break;
                        case 'video':
                            $type = Document::TYPE_VIDEO;
                            break;
                        }
                        $docData = array(
                            'order'    => $file['order'],
                            'type'     => $type,
                            'location' => $location
                        );

                        $document = new Document();
                        $form     = $builder->createForm($document);
                        $form->setHydrator($hydrator);
                        $form->bind($document);
                        $form->setData($docData);

                        if (!$form->isValid()) {
                            $this->getResponse()->setStatusCode(
                                Error::FormInvalid_code
                            );
                            return new JsonModel(
                                array(
                                    'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]
                                )
                            );
                        }

                        $document = $form->getData();
                        $document->setQuestionId($question->getId());
                        $document->setWidth((int)$width);
                        $document->setHeight((int)$height);
                        $objectManager->persist($document);
                        $objectManager->flush($document);
                        unlink("./public".$tempPath);


                    } else {

                        $document = $objectManager
                            ->getRepository('\Qolve\Entity\Document')
                            ->find($file['id']);

                        if ($file['order'] != $document->getOrder()) {
                            $newOrder = array('order' => $file['order']);
                            $preData  = $hydrator->extract($document);
                            $newDoc   = array_merge($preData, $newOrder);
                            $builder  = new AnnotationBuilder();
                            $form     = $builder->createForm($document);
                            $form->setHydrator($hydrator);
                            $form->setBindOnValidate(false);
                            $form->bind($document);
                            $form->setData($newDoc);

                            if (!$form->isValid()) {
                                $this->getResponse()->setStatusCode(
                                    Error::FormInvalid_code
                                );
                                return new JsonModel(array(
                                    'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ])
                                );
                            }

                            $form->bindValues();
                        }
                    }
                }
                system('rm -rf ' .
                    escapeshellarg("./public/upfiles/tempo/$userId/"),
                    $retval
                );
            }
            else {
                if(isset($preDocs)) {
                    foreach ($preDocs as $preDoc) {
                        $objectManager->remove($preDoc);
                    }

                    system('rm -rf ' .
                        escapeshellarg(
                            "./public/upfiles/users/$userId/questions/$questionId/"
                        ),
                        $retval
                    );
                }
            }

            // Elastica Update //

            $elastica = $this->getServiceLocator()
                ->get('Elastica\Client');
            $elasticaClient = $elastica['client'];
            $index          = $elastica['index'];
            $elasticaIndex  = $elasticaClient->getIndex($index);
            $elasticaType   = $elasticaIndex->getType('question');

            $createdOn = $question->getCreatedOn()->format('Y-m-d H:i:s');
            $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));
            $levels    = $question->getLevels();
            $deadline  = $question->getDeadline();

            if (!empty($deadline)) {
                $deadline  = $question->getDeadline()->format('Y-m-d H:i:s');
                $deadline  = gmdate('Y-m-d H:i:s', strtotime($deadline));
            }

            if (!empty($levels)) {

                if (!preg_match('/\,/', $levels)) {

                    $levels = preg_replace('/\s/', '', $levels);
                }

                if (preg_match('/\,/', $levels)) {

                    $levels = explode(',', $levels);
                    $levels = array_map(function ($level) {
                        return preg_replace('/\s/', '', $level);},
                        $levels
                    );
                }
            }

            $keyNames = empty($keywords) ? $preKeyNames : $keywords;
            $param     = array (
                'levels'      => $levels,
                'credits'     => (float)$question->getCredits(),
                'keywords'    => $keyNames,
                'status'      => (int)$question->getStatus(),
                'paid'        => $question->getPaid(),
                'deadline'    => $deadline,
                'privacy'     => (int)$question->getPrivacy(),
            );

            $document = $elasticaType->getDocument($questionId);

            $document->setData($param);
            $elasticaType->updateDocument($document);
            $elasticaType->getIndex()->refresh();

            $objectManager->flush();
            // Update question activity in elasticsearch
            $params = array(
                'question_id' => $question->getId(),
                'keywords'    => $keywords,
                'deadline'    => $deadline,
                'credits'     => $question->getCredits()
            );

            $objectManager
                ->getRepository('Qolve\Entity\Question')
                ->setQuestionActivity(
                    $this->getServiceLocator(),
                    $params
            );

            //Add to Queue for send Notification
            $queue = $this->serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('notifications');
            $data = array(
                'questionId' => $question->getId(),
                'askerId'    => $user->getId()
            );

            $data['to'] = 'askers'; //high
            $job = new \Application\Job\SendNotification($this->serviceLocator);
            $job->setContent($data);
            $queue->push($job);

            $newMentions   = array();
            $newMentionsId = array();

            if (!empty($mentions)) {
                sort($preMentions);
                sort($mentions);

                $sameMentions = array_intersect($mentions, $preMentions);
                $newMentions  = array_diff($mentions, $sameMentions);
                $mentions     = $sameMentions;
            }

            foreach($newMentions as $mention) {

                $return = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->getOneByElastic($serviceLocator, 'username', $mention);

                $mention = array();
                if (!empty($return)) {
                    $mention = $objectManager
                        ->getRepository('Application\Entity\User')
                        ->find($return['user_id']);
                }

                if (!$mention instanceof User) {
                    continue;
                }
                $newMentionsId[] = $mention->getId();
            }

            if (!empty($newMentionsId)) {
                $data['verb']     = '\Qolve\Verb\CreateQuestion';
                $data['to']       = 'mentions';
                $data['mentions'] = $newMentionsId;
                $job = new \Application\Job\SendNotification($this->serviceLocator);
                $job->setContent($data);
                $queue->push($job);
            }

            $mentionsId = array();
            foreach($mentions as $mention) {

                $return = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->getOneByElastic($serviceLocator, 'username', $mention);

                $mention = array();
                if (!empty($return)) {
                    $mention = $objectManager
                        ->getRepository('Application\Entity\User')
                        ->find($return['user_id']);
                }

                if (!$mention instanceof User) {
                    continue;
                }
                $mentionsId[] = $mention->getId();
            }

            if (!empty($mentionsId)) {
                $data['verb']     = '\Qolve\Verb\UpdateQuestion';
                $data['to']       = 'mentions';
                $data['mentions'] = $mentionsId;
                $job = new \Application\Job\SendNotification($this->serviceLocator);
                $job->setContent($data);
                $queue->push($job);
            }

            if ($question->getPrivacy() == Question::PRIVACY_SHARED ) {

                $data['to'] = 'shareds'; //high

                if (empty($preShare)) {

                    $data['sharedUsers'] = isset($sharedUsers)
                        ? $sharedUsers
                        : array();
                } else {

                    $data['sharedUsers'] = isset($newShares)
                        ? $newShares
                        : array();
                }
                $job = new \Application\Job\SendNotification($this->serviceLocator);
                $job->setContent($data);
                $queue->push($job);
            }

            $data['to'] = 'questionFollowers';
            $job = new \Application\Job\SendNotification($this->serviceLocator);
            $job->setContent($data);
            $queue->push($job);

            return new JsonModel(array(
                'feed' => $hydrator->extract($feed, $info, $user)
            ));

        } else {

            $this->getResponse()->setStatusCode(Error::FormInvalid_code);

            return new JsonModel(array('errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]));
        }
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AuthenticationFailed_code,
				 'message' => Error::AuthenticationFailed_message 
				 ]
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserUnavailable_code,
				 'message' => Error::UserUnavailable_message 
				 ]
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');

        $user      = $this->identity();
        $userId    = $user->getId();
        $answer    = $objectManager
            ->getRepository('Qolve\Entity\Answer')
            ->findByQuestionId($id);
        $question  = $objectManager
            ->getRepository('Qolve\Entity\Question')
            ->find($id);
        $asker     = $objectManager
            ->getRepository('Qolve\Entity\Asker')
            ->findOneBy(array(
                'questionId' => $id,
                'status'     => Asker::STATUS_ASKER)
        );
        $preData   = $hydrator->extract($question);

        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                )
            );
        }

        if (!$asker instanceof Asker) {
            $this->getResponse()->setStatusCode(
                Error::AskerNotFound_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AskerNotFound_code,
				 'message' => Error::AskerNotFound_message 
				 ]
                )
            );
        }

        similar_text($asker->getUserId(), $user->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::PermissionDenied_code,
				 'message' => Error::PermissionDenied_message 
				 ]
                )
            );
        }

        if (!empty($answer)) {
            $this->getResponse()->setStatusCode(
                Error::QuestionHasAnswer_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionHasAnswer_code,
				 'message' => Error::QuestionHasAnswer_message 
				 ]
                )
            );
        }

        if ($preData['status'] != Question::STATUS_OPEN) {
            $this->getResponse()->setStatusCode(
                Error::QuestionIsNotOpen_code
            );
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionIsNotOpen_code,
				 'message' => Error::QuestionIsNotOpen_message 
				 ]
                )
            );
        }

        $objectManager->remove($question);

        $entitiesToDelete = array(
            'Qolve\Entity\Answer',
            'Qolve\Entity\Asker',
            'Qolve\Entity\Bookmark',
            'Qolve\Entity\Comment',
            'Qolve\Entity\Document',
            'Qolve\Entity\QuestionKeyword',
            'Qolve\Entity\Report',
            'Qolve\Entity\Share',
            'Qolve\Entity\Vote',
            'Application\Entity\Feed',
            'Application\Entity\Notification'
        );

        foreach ($entitiesToDelete as $entity) {
            $objectManager->getRepository($entity)
                ->deleteBy(array('questionId' => $question->getId()));
        }

        $questionFollows = $objectManager
            ->getRepository('Qolve\Entity\QuestionFollow')
            ->findByQuestionId($id);
        foreach ($questionFollows as $questionFollow) {
            $objectManager
                ->getRepository('Qolve\Entity\QuestionFollow')
                ->unfollow($questionFollow->getUserId(),
                    $questionFollow->getQuestionId()
            );
        }

        $objectManager->flush();

        // Elastica delete //

        $elastica = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $elasticaIndex  = $elasticaClient->getIndex($index);
        $elasticaType   = $elasticaIndex->getType('question');
        $elasticaType->deleteById($id);
        $elasticaType->getIndex()->refresh();

        return new JsonModel(array('success'));
    }
}
