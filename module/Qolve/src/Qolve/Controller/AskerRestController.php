<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Feed,
    Qolve\Entity\Question,
    Qolve\Entity\Answer,
    Qolve\Entity\Asker,
    Qolve\Entity\QuestionFollow,
    Qolve\Entity\Transaction,
    Application\Job\PublishFeed,
    Application\Job\SendNotification,
    Application\Entity\Error,
    Qolve\Job\Payment,
    Zend\Form\Annotation\AnnotationBuilder,
    Qolve\Entity\TransactionLog,
    Qolve\Library\Constants;

class AskerRestController extends AbstractRestfulController
{
    public function getList()
    {
        $criteria = $this->params()->fromQuery('criteria', array());
        $criteria['questionId'] = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');

        $limit  = $this->params()->fromQuery('limit', 5);
        $offset = $this->params()->fromQuery('offset', 0);

        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);

        $orderBy = $this->params()->fromQuery(
            'orderBy', array('askedOn' => 'ASC')
        );

        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $currentUser = $this->identity();

        $askers = $objectManager->getRepository('Qolve\Entity\Asker')
            ->findBy($criteria, $orderBy, $limit, $offset);
        $count  = $objectManager->getRepository('Qolve\Entity\Asker')
            ->countBy($criteria);

        $_askers = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($askers as $asker) {
            if ($asker instanceof Asker) {
                $user = $objectManager->getRepository('Application\Entity\User')
                    ->find($asker->getUserId());
                if ($user instanceof User) {
                    $_asker = $hydrator->extract($user, array(), $currentUser);

                    if ($currentUser) {
                        similar_text($currentUser->getId(), $user->getId(), $percent);
                    }

                    if (   $asker->getVisibility() == Asker::VISIBILITY_ANONYMOUS
                        && (!$currentUser || $percent < 100)
                    ) {
                        $_asker = array_map(function() {return null;}, $_asker);
                        $_asker['name'] = Asker::ANONYMOUS_NAME;
                    }

                    $_asker['invisible'] = (int) (
                        $asker->getVisibility() == Asker::VISIBILITY_ANONYMOUS
                    );

                    $_askers['list'][] = $_asker;
                }
            }
        }

        return new JsonModel(
            array(
                'askers' => $_askers
            )
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(401);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AuthenticationFailed_code,
				 'message' => Error::AuthenticationFailed_message 
				 ]
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserUnavailable_code,
				 'message' => Error::UserUnavailable_message 
				 ]
                )
            );
        }

        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question   = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);

        $editableProperties = array(
            'status',
            'userId',
            'questionId',
            'askedOn',
            'credit',
            'visibility',
            'answerId',
            'cardToken',
            'cardId',
            'device',
            'userPaid',
            'partnerName'
        );

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(404);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                )
            );
        }

        if ($question->getStatus() == Question::STATUS_CLOSED) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionIsClosed_code,
				 'message' => Error::QuestionIsClosed_message 
				 ]
                )
            );
        }

        $user = $this->identity();
        $now  = new \DateTime('now', new \DateTimeZone('UTC'));

        $asker = $objectManager->getRepository('\Qolve\Entity\Asker')
            ->findOneBy(
                array(
                    'questionId' => $questionId,
                    'userId'     => $user->getId()
                )
            );
        if ($asker instanceof Asker) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserHasReaskedThisQuestion_code,
				 'message' => Error::UserHasReaskedThisQuestion_message 
				 ]
                )
            );
        }

        if (!isset($data['credit'])) {
            $data['credit'] = (float) 0;
        } else {
            $data['credit'] = (float) $data['credit'];

        }
        if (isset($data['credit']) && $data['credit'] < 0) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::CreditIsNegative_code,
				 'message' => Error::CreditIsNegative_message 
				 ]
                )
            );
        }

        $partnerName  = null;
        $qolveShare   = null;
        $partnerShare = null;
        $userPaid     = null;
        $data['device'] = isset($data['device']) ? 
            $data['device'] : 
            null;

        if ($data['credit'] > 0) {
            if (   isset($data['device']) 
                && (   ($data['device'] == Constants::ANDROID_DEVICE)
                    || ($data['device'] == Constants::IOS_DEVICE)
                )
            ) {
                
                $qolveShare      = $data['credit'] * 0.1;
                $partnerShare    = $data['userPaid'] - $data['credit'];
                $data['credit']  = $data['credit'] * 0.9;
                $userPaid        = $data['userPaid'];

                if ($data['device'] == Constants::IOS_DEVICE) {
                    $partnerName = !isset($data['partnerName']) ? 
                        Constants::IOS_PARTNER :
                        $data['partnerName'];
                } else {
                    $partnerName = !isset($data['partnerName']) ? 
                        Constants::GOOGLE_PARTNER :
                        $data['partnerName'];
                }

            } else {
                $customerId = $user->getCustomerId();
                if (!isset($data['cardToken'])) {
                    $cardToken = null;
                } else {
                    $cardToken = $data['cardToken'];
                }
                if (empty($cardToken)) {
                    $this->getResponse()->setStatusCode(400);
                    return new JsonModel(
                        [
                            'errors' => [
                				'code'    => Error::CreditIsNegative_code,
                				'message' => Error::CreditIsNegative_message 
            				]
                        ]
                    );
                }

                $payment  = $this->getServiceLocator()
                    ->get('payment');

                //charge amount with stripe
                $charge = $payment->charge(
                    $user,
                    $data['cardToken'],
                    $data['cardId'],
                    $data['credit']
                );

                if(is_null($charge)) {

                    $this->getResponse()->setStatusCode(400);
                    return new JsonModel(
                        [
                            'error' => 'Token Id and Card Id not send'
                        ]
                    );
                }
                $data['credit'] = $charge;
            }
        }

        if (isset($data['visibility'])) {
            $data['visibility'] = (int) $data['visibility'];
            $visibility = $data['visibility'];
        }

        $userCredit = (float) $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->getUserKey($user->getId(), 'credit');

        $asker   = new Asker();
        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($asker);
        $form->setHydrator($hydrator);
        $form->bind($asker);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                [
                    'errors' => [
        				'code'    => Error::FormInvalid_code,
        				'message' => $form->getMessages() 
        			]
                ]
            );
        }

        $asker = $form->getData();
        $asker->setQuestionId($questionId);
        $asker->setUserId($user->getId());
        $asker->setStatus(Asker::STATUS_REASKER);
        $asker->setAskedOn($now);

        if (!$asker->getVisibility()) {
            $asker->setVisibility(Asker::VISIBILITY_PUBLIC);
        }

        $objectManager->persist($asker);

        $askerCount = $objectManager
            ->getRepository('\Qolve\Entity\Asker')
            ->getAskersCount($questionId);

        $question->setAskers($askerCount + 1);
        $question->setCredits($question->getCredits() + $asker->getCredit());

        $objectManager
            ->getRepository('Qolve\Entity\QuestionFollow')
            ->unfollow($user->getId(), $question->getId());

        // *** Create Transaction & Calculate User's Credit *** //
        if ($asker->getCredit() > 0) {
            $notes = json_encode(array(
                'questionId' => $question->getId()
            ));

            $transaction = new Transaction();
            $transaction->setUserId($asker->getUserId());
            $transaction->setDebit($asker->getCredit());
            $transaction->setStatus(Transaction::STATUS_BLOCKED);
            $transaction->setNotes($notes);
            $transaction->setCreatedOn($now);
            $objectManager->persist($transaction);

            if (   isset($data['device']) 
                && (   ($data['device'] == Constants::IOS_DEVICE)
                    || ($data['device'] == Constants::ANDROID_DEVICE)
                )
            ) {
                $transactionLog = new TransactionLog();
                $transactionLog->setUserId($asker->getUserId());
                $transactionLog->setUserPaid($userPaid);
                $transactionLog->setQolveShare($qolveShare);
                $transactionLog->setPartnerName($partnerName);
                $transactionLog->setPartnerShare($partnerShare);
                $transactionLog->setQuestionCost($asker->getCredit());
                $transactionLog->setQuestionId($question->getId());
                $transactionLog->setCreatedOn($now);

                if ($data['device'] == Constants::IOS_DEVICE) {
                    $transactionLog->setStatus(Constants::PAY_FROM_IOS_TO_QOLVE);
                } else {
                    $transactionLog->setStatus(Constants::PAY_FROM_GOOGLE_TO_QOLVE);
                }
                $objectManager->persist($transactionLog);
            }

        }

        //Add to user's Activities (and timeline)
        $feedPrivacy = max(
            array(
                $question->getPrivacy(),
                $asker->getVisibility()
            )
        );

        $feed = new Feed();
        $feed->setUserId($user->getId());
        $feed->setActorId($user->getId());
        $feed->setQuestionId($question->getId());
        $feed->setPrivacy($feedPrivacy);
        $feed->setVerb('ReaskQuestion');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);
        $objectManager->flush();

        // Update Question Activity in Elastic
        $askers = array();
        $askers = $objectManager
            ->getRepository('Qolve\Entity\Asker')
            ->findByQuestionId($question->getId());

        foreach($askers as $asker) {
            if ($asker instanceof Asker) {
                if ($asker->getStatus() == Asker::STATUS_REASKER) {
                    $_askers[] = $asker->getUserId();
                }
            }
        }

        $params = array(
            'question_id' => $question->getId(),
            'reaskers'    => $_askers,
            'credits'     => $question->getCredits()
        );

        $objectManager
            ->getRepository('Qolve\Entity\Question')
            ->setQuestionActivity(
                $this->getServiceLocator(),
                $params
        );

        //Add to Queue for publish Feeds
        if (   $question->getPrivacy() == Question::PRIVACY_PUBLIC
            && $asker->getVisibility() == Asker::VISIBILITY_PUBLIC) {

                $queue = $this->serviceLocator
                    ->get('\SlmQueue\Queue\QueuePluginManager')
                    ->get('feeds');

                $data = array(
                    'verb'       => '\Qolve\Verb\ReaskQuestion',
                    'questionId' => $question->getId(),
                    'askerId'    => $user->getId()
                );

                $data['to'] = 'userFollowers';
                $job = new PublishFeed($this->getServiceLocator());
                $job->setContent($data);
                $queue->push($job);
            }

        // Elastic Search //
        $this->elasticSearch($questionId);

        //Add to Queue for send Notification
        $queue = $this->serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'       => '\Qolve\Verb\ReaskQuestion',
            'questionId' => $question->getId(),
            'visibility' => $visibility,
            'askerId'    => $user->getId()
        );

        $data['to'] = 'askers'; //high
        $job = new SendNotification($this->getServiceLocator());
        $job->setContent($data);
        $queue->push($job);

        $data['to'] = 'questionFollowers';
        $job = new SendNotification($this->getServiceLocator());
        $job->setContent($data);
        $queue->push($job);

        return new JsonModel(
            $hydrator->extract(
                $asker,
                array(
                    'asker',
                    'answersList',
                    'commentsList',
                    'documentsList'
                ),
                $user
            )
        );
    }

    public function update($id, $data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(401);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AuthenticationFailed_code,
				 'message' => Error::AuthenticationFailed_message 
				 ]
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserUnavailable_code,
				 'message' => Error::UserUnavailable_message 
				 ]
                )
            );
        }

        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $info = [
            'asker',
            'answersList',
            'commentsList',
            'documentsList'
        ];

        $user = $this->identity();

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                )
            );
        }

        similar_text($user->getId(), $id, $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::PermissionDenied_code,
				 'message' => Error::PermissionDenied_message 
				 ]
                )
            );
        }

        $asker = $objectManager->getRepository('Qolve\Entity\Asker')
            ->findOneBy(
                array(
                    'questionId' => $question->getId(),
                    'userId'     => $user->getId()
                )
            );

        if (!$asker instanceof Asker) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::PermissionDenied_code,
				 'message' => Error::PermissionDenied_message 
				 ]
                )
            );
        }

        if ($question->getStatus() == Question::STATUS_CLOSED) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionIsClosed_code,
				 'message' => Error::QuestionIsClosed_message 
				 ]
                )
            );
        }

        $editableProperties = array(
            'credit',
            'visibility',
            'answerId',
            'cardToken',
            'cardId',
            'device',
            'userPaid',
            'partnerName'
        );

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (isset($data['visibility'])
            && $data['visibility'] != $asker->getVisibility()
        ) {
            $visibilityIsChanged = true;
        } else {
            $visibilityIsChanged = false;
        }

        if (isset($data['credit'])) {

            $qolveShare     = null;
            $partnerShare   = null;
            $partnerName    = null;
            $userPaid       = null;
            $data['device'] = isset($data['device']) ? 
                $data['device'] : 
                null;

            if (   ($data['device'] == Constants::IOS_DEVICE)
                    || ($data['device'] == Constants::ANDROID_DEVICE)
            ) {

                $qolveShare      = $data['credit'] * 0.1;
                $partnerShare    = $data['userPaid'] - $data['credit'];
                $data['credit']  = $data['credit'] * 0.9;
                $userPaid        = $data['userPaid'];
                if ($data['device'] == Constants::IOS_DEVICE) {
                    $partnerName = !isset($data['partnerName']) ? 
                        Constants::IOS_PARTNER :
                        $data['partnerName'];
                } else {
                    $partnerName = !isset($data['partnerName']) ? 
                        Constants::GOOGLE_PARTNER :
                        $data['partnerName'];
                }
            }

            empty($data['cardToken']) ? $data['cardToken'] = null : '';
            empty($data['cardId']) ? $data['cardId'] = null : '';

            $asker = $objectManager->getRepository('Qolve\Entity\Asker')
                ->raiseCredit(
                    $this->getServiceLocator(),
                    $data['credit'],
                    $question,
                    $user,
                    $data['cardToken'],
                    $data['cardId'],
                    $userPaid,
                    $qolveShare,
                    $partnerShare,
                    $partnerName,
                    $data['device']
            );

            if (is_array($asker) && isset($asker['error'])) {

                $this->getResponse()->setStatusCode(400);
                return new JsonModel($asker);
            }

            $charge         = $asker['charge'];
            $asker          = $asker['asker'];
            $info['charge'] = $charge;

        } else if (isset($data['answerId'])) {
            $answer = $objectManager->getRepository('Qolve\Entity\Answer')
                ->find($data['answerId']);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(404);

                return new JsonModel(
                    array(
                        'errors' => [
				 'code'    => Error::AnswerNotFound_code,
				 'message' => Error::AnswerNotFound_message 
				 ]
                    )
                );
            }

            $asker = $objectManager->getRepository('Qolve\Entity\Asker')
                ->pickBestAnswer(
                    $user,
                    $question,
                    $answer,
                    $this->getServiceLocator()
            );

        } else {
            unset($data['credit']);
            unset($data['answerId']);

            $preData = $hydrator->extract($asker);
            $data    = array_merge($preData, $data);
            $builder = new AnnotationBuilder();
            $form    = $builder->createForm($asker);
            $form->setHydrator($hydrator);
            $form->setBindOnValidate(false);
            $form->bind($asker);
            $form->setData($data);

            if (!$form->isValid()) {
                $this->getResponse()->setStatusCode(400);

                return new JsonModel(
                    array(
                        'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]
                    )
                );
            }

            $form->bindValues();
            $objectManager->flush();
        }

        if (is_array($asker) && isset($asker['error'])) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                array(
                    'code'  => 400,
                    'error' => $asker['error']['message']
                )
            );
        }

        //update feed privacy
        if ($visibilityIsChanged) {
            //for CreateQuestion, RaiseCredit, ReaskQuestion
                $feedPrivacy = max(array(
                    $question->getPrivacy(),
                    $asker->getVisibility()
                ));
                $objectManager->getRepository('Application\Entity\Feed')
                    ->updatePrivacy(array(
                        'actorId'    => $asker->getUserId(),
                        'questionId' => $question->getId(),
                        'verb'       => 'CreateQuestion'
                    ), $feedPrivacy);
                $objectManager->getRepository('Application\Entity\Feed')
                    ->updatePrivacy(array(
                        'actorId'    => $asker->getUserId(),
                        'questionId' => $question->getId(),
                        'verb'       => 'RaiseCredit'
                    ), $feedPrivacy);
                $objectManager->getRepository('Application\Entity\Feed')
                    ->updatePrivacy(array(
                        'actorId'    => $asker->getUserId(),
                        'questionId' => $question->getId(),
                        'verb'       => 'ReaskQuestion'
                    ), $feedPrivacy);
        }
        
        return new JsonModel(
            $hydrator->extract(
                $asker,
                $info,
                $user
            )
        );
    }

    /**
     * Elastic update
     */
    private function elasticSearch($questionId)
    {
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        $elastica       = $this->getServiceLocator()->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $param = array('credits' => $question->getCredits());

        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('question');
        $document      = $elasticaType->getDocument($questionId);

        $document->setData($param);
        $elasticaType->updateDocument($document);
        $elasticaType->getIndex()->refresh();
    }
}
