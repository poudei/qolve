<?php
namespace Qolve\Controller;

use Application\Controller\AbstractController,
    Zend\View\Model\ViewModel;

class GearmanWorkerController extends AbstractController
{

    public function workerAction()
    {
        if (!extension_loaded('gearman')) {
            die('Error! The PECL::gearman extension is required.');
        }
        $sm      = $this->getServiceLocator();
        $config  = $this->getServiceLocator()->get('Config');
        $gWorker = new \GearmanWorker();

        if ($config['gearman']['enable']) {
            if (isset($config['gearman']) && isset($config['gearman']['servers'])) {
                $gWorker->addServers($config['gearman']['servers']);
            } else {
                $gWorker->addServer();
            }
        }

        $workers = array (
            'CreateQuestion',
            'UpdateQuestion',
            'CreateAnswer',
            'UpdateAnswer',
            'ReaskQuestion',
            'RaiseCredit',
            'PickBestAnswer',
            'WriteComment',
            'Vote',
            'FollowQuestion',
            'FollowUser'
        );

        foreach ($workers as $worker) {
            $workerName = $worker;
            $gWorker->addFunction($worker, array($this, 'Gearman_dowork'), $workerName);
        }
        while ($gWorker->work()) {

            if ($gWorker->returnCode() != GEARMAN_SUCCESS) {
                break;
            }
        }

    }

    public function Gearman_dowork($job, $workerName)
    {

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $workerFile  = QOLVE_PATH . '/Workers/' . $workerName . '.php';
        $workerClass = 'Worker_' . $workerName;

        if (!file_exists($workerFile)) {
            throw new Exception('The worker file is not exist: ' . $workerFile);
        }
        include_once $workerFile;

        if (!class_exists($workerClass)) {
            throw new Exception(
                'The worker class: ' . $workerClass.
                ' is not exist in file: ' . $workerFile
            );
        }
        $worker = new $workerClass($sm);

        return $worker->work(unserialize($job->workload()), $objectManager);
    }
}
