<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel;

class QuestionAdminRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');
        
        $order    = $this->params()->fromQuery('order', 'createdOn');
        $limit    = $this->params()->fromQuery('limit', 20);
        $offset   = $this->params()->fromQuery('offset', 0);
        
        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);
        
        //TODO: get from elastic
        $questions = array();
        $count     = 0;
            
        $_questions = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($questions as $question) {
            $_questions[] = $hydrator->extract($question);
        }
        
        return new JsonModel(array("questions" => $_questions));
    }
}
