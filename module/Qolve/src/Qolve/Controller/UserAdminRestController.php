<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel;

class UserAdminRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');
        
        $order    = $this->params()->fromQuery('order', 'createdOn');
        $limit    = $this->params()->fromQuery('limit', 20);
        $offset   = $this->params()->fromQuery('offset', 0);
        
        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);
        
        //TODO: get from elastic
        $users = array();
        $count = 0;
            
        $_users = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($users as $user) {
            $_users[] = $hydrator->extract($user);
        }
        
        return new JsonModel(array("users" => $_users));
    }
}
