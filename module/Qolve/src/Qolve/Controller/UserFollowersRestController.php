<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\Error,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\UserFollow,

    Zend\Form\Annotation\AnnotationBuilder;

class UserFollowersRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        if ($this->params()->fromQuery('limit') == 'all') {

            $limit  = null;
            $offset = null;
        } else {

            $limit  = $this->normalize(
                'limit',
                $this->params()->fromQuery('limit', 10)
            );
            $offset = $this->normalize(
                'offset',
                $this->params()->fromQuery('offset', 0)
            );
        }

        $currentUser = $this->identity();

        $userId = $this->getEvent()->getRouteMatch()->getParam('user_id');
        if ($userId != null) {
            $user = $objectManager
                ->getRepository('\Application\Entity\User')
                ->find($userId);
        } else {
            $user = $currentUser;
        }
        if (!$user instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::UserNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserNotFound_message
                )
            );
        }

        $follows = $objectManager
            ->getRepository('Qolve\Entity\UserFollow')
            ->findBy(array(
                'userId' => $user->getId()
            ), null, $limit, $offset);
        $count   = (int) $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->getUserKey($user->getId(), 'followersCount');

        $_followers = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($follows as $follow) {
            $follower = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($follow->getFollowerId());
            if ($follower instanceof \Application\Entity\User) {
                $_followers['list'][] = $hydrator->extract(
                    $follower,
                    array(),
                    $currentUser
                );
            }
        }

        return new JsonModel(array('followers' => $_followers));
    }
}
