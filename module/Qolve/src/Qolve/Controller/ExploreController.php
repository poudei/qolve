<?php
namespace Qolve\Controller;

use Application\Controller\AbstractController,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\Keyword,
    Qolve\Entity\KeywordFollow,
    Application\Entity\User;

class ExploreController extends AbstractController
{
    const QUESTION_FREE = 0;
    const QUESTION_PAID = 1;
    const TOP_STORIES   = 0;
    const RECENT_DATE   = 1;

    public function searchAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $byComment  = $this->params()->fromQuery('comment', true);
        $byAnswer   = $this->params()->fromQuery('answer', true);
        $byDocument = $this->params()->fromQuery('document', true);

        $user = $this->identity();

        if ($user instanceof User) {
            $userId = $user->getId();
        }

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        if (!empty($data)) {
            $editableProperties = array(
                'limit',
                'offset',
                'deadline',
                'status',
                'levels',
                'keywords',
                'recent',
                'credits'
            );

            foreach ($data as $name => $value) {
                if (array_search($name, $editableProperties) === false) {
                    unset($data[$name]);
                }
            }

            !empty($data['limit'])  ? $limit  = $data['limit']  : $limit = 10;
            !empty($data['offset']) ? $offset = $data['offset'] : $offset = 0;

            $limit  = $this->normalize('limit', $limit);
            $offset = $this->normalize('offset', $offset);

            unset($data['limit']);
            unset($data['offset']);

            $info = array(
                'asker',
                'keywords',
                'userAnswerId'
            );

            if ($byAnswer) {
                $info[] = 'answersList';
            }

            if ($byComment) {
                $info[] = 'commentsList';
            }

            if ($byDocument) {
                $info[] = 'documentsList';
            }
        }

        $elastica = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $search        = new \Elastica\Search($elasticaClient);
        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('question');

        if (empty($data)) {

            $keywords = $objectManager
                ->getRepository('Qolve\Entity\Keyword')
                ->getKeywords();

            if (!empty($keywords)) {
                foreach ($keywords as $keyword) {
                    $search  = new \Elastica\Search($elasticaClient);

                    $keyword = $hydrator->extract(
                        $keyword, array(), !empty($user) ? $user : null
                    );

                    $elasticaBool = new \Elastica\Query\Bool();
                    $term         = new \Elastica\Query\Term();

                    $term->setParam(
                        'keywords', trim(strtolower($keyword['name']))
                    );
                    $elasticaBool->addMust($term);

                    $elasticaQuery = new \Elastica\Query($elasticaBool);

                    $count = $search->addIndex($index)
                        ->addType('question')
                        ->count($elasticaQuery);

                    $topKey[] = array(
                        'id'         => $keyword['id'],
                        'name'       => $keyword['name'],
                        'isFollowed' => $keyword['isFollowed'],
                        'count'      => $count
                    );
                }
            }

            foreach ($topKey as $key => $value) {
                $arr[$key]  = $value['count'];
            }

            array_multisort($arr, SORT_DESC, $topKey);
            $topKey = array_slice($topKey, 0, 7);

            foreach ($topKey as $key => $value) {
                $keyNames[] = array(
                    'id' => $value['id'],
                    'name' => $value['name'],
                    'count' => $value['count'],
                    'isFollowed' => $value['isFollowed']
                );
            }

            return new JsonModel(
                array(
                    "trendingKeywords" => $keyNames
                )
            );

        } elseif (!empty($data)) {
            $boolMust   = new \Elastica\Query\Bool();
            $keysResult = array();
            if (isset($data['keywords'])) {

                $boolKey = new \Elastica\Query\Bool();
                $keys = explode(' ', $data['keywords']);
                foreach($keys as $key => $value) {
                    if (empty($value)) {
                        unset($keys[$key]);
                    }
                }

                $keys = array_unique($keys);

                foreach ($keys as $key) {
                    $keyword = $objectManager
                        ->getRepository('Qolve\Entity\Keyword')
                        ->getKeyByName($key);
                    if ($keyword instanceof Keyword) {
                       $keysResult[] = $hydrator
                        ->extract($keyword, array(), $user);
                    } else {
                        $keysResult[] = array(
                            'id'         => null,
                            'name'       => $key,
                            'isFollowed' => 0
                        );
                    }

                    $term = new \Elastica\Query\Term();
                    $term->setParam('keywords', trim(strtolower($key)));
                    $boolKey->addShould($term);
                }
                $boolMust->addMust($boolKey);
            }

            if (isset($data['levels'])) {

                $boolLevel = new \Elastica\Query\Bool();
                
                if (!is_array($data['levels'])) {
                    $levels = explode(',', $data['levels']);
                } else {
                    $levels = $data['levels'];
                }
                
                foreach($levels as $key => $value) {
                    $value = preg_replace('/\s/', '', $value);

                    if (empty($value)) {
                        unset($levels[$key]);
                    }
                }
                $levels = array_unique($levels);

                foreach ($levels as $level) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('levels', trim(strtolower($level)));
                    $boolLevel->addShould($term);
                }
                $boolMust->addMust($boolLevel);
            }
            if (isset($data['credits'])
                && $data['credits'] == self::QUESTION_PAID
            ) {
                $term  = new \Elastica\Query\Term();
                $term->setParam('credits', self::QUESTION_FREE);
                $boolMust->addMustNot($term);

            } elseif ((   isset($data['credits'])
                       && $data['credits'] == self::QUESTION_FREE)
            ) {
                $term = new \Elastica\Query\Term();
                $term->setParam('credits', self::QUESTION_FREE);
                $boolMust->addMust($term);
            }

            if (isset($data['status'])) {
                $term  = new \Elastica\Query\Term();
                $term->setParam('status', $data['status']);
                $boolMust->addMust($term);
            }
            $term = new \Elastica\Query\Term();
            $term->setParam('privacy', 1);
            $boolMust->addMust($term);
            $elasticaQuery = new \Elastica\Query($boolMust);
            $elasticaQuery->setFrom($offset);
            $elasticaQuery->setSize($limit);

            if (   (isset($data['recent']))
                && ($data['recent'] == 1) ) {
                $elasticaQuery->setSort(
                    array('created_on' => array('order' => 'desc'))
                );
            } else if (   (isset($data['recent']))
                && ($data['recent'] == 0) ) {
                $elasticaQuery->setSort(
                    array('votes' => array('order' => 'desc'))
                );
            }

            $resultSet = $search->addIndex($index)
                ->addType('question')
                ->search($elasticaQuery);
            $resultCount = $search->addIndex($index)
                ->addType('question')
                ->count($elasticaQuery);
                
            foreach ($resultSet as $result) {
                $returns[] = $result->getData();
            }
            
            if (empty($returns)) {
                return new JsonModel(
                    array(
                        "questions"     => array(),
                        'count'         => 0,
                        'keywordFollow' => $keysResult

                    )
                );
            }
            foreach ($returns as $return) {
                $question = $objectManager
                    ->getRepository('Qolve\Entity\Question')
                    ->find($return['question_id']);
                if ($question instanceof Question) {
                    $questions[] = $hydrator
                        ->extract(
                            $question, $info, $user
                        );
                }
            }


            $returnSet['questions']     = 
                !empty($questions) ? $questions : array();
            $returnSet['count']         = $resultCount;
            $returnSet['keywordFollow'] = $keysResult;

            return new JsonModel(
                !empty($questions)
                    ? $returnSet
                    : array('questions'     => array(),
                            'count'         => 0,
                            'keywordFollow' => $keysResult
                      )
            );
        }
    }
}
