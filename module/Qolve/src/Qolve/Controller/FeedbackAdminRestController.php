<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\Feedback,
    Application\Entity\Error;

class FeedbackAdminRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');

        $type     = $this->params()->fromQuery('type');
        $rank     = $this->params()->fromQuery('rank');
        $orderBy  = $this->params()->fromQuery('orderBy', 'createdOn');
        $orderDir = $this->params()->fromQuery('orderDir', 'DESC');
        $limit    = $this->params()->fromQuery('limit', 20);
        $offset   = $this->params()->fromQuery('offset', 0);

        $order  = array($orderBy => $orderDir);
        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);

        $conditions = array();

        if (!is_null($type)) {
            if ((int)$type == 1 || (int)$type == 2) {
                $conditions['type'] = (int)$type;
            } else {
                switch (strtoupper($type)) {
                case 'BUG':
                    $conditions['type'] = Feedback::TYPE_BUG;
                    break;

                case 'ENHANCEMENT':
                    $conditions['type'] = Feedback::TYPE_ENHANCEMENT;
                    break;
                }
            }
        }

        if (!is_null($rank)) {
            if ((int)$rank >= 0 && (int)$rank <= 5) {
                $conditions['rank'] = (int)$rank;
            }
        }

        //TODO: get from different shards
        $feedbacks = $objectManager->getRepository('Application\Entity\Feedback')
            ->findBy($conditions, $order, $limit, $offset);
        $count = $objectManager->getRepository('Application\Entity\Feedback')
            ->countBy($conditions);

        $_feedbacks = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($feedbacks as $feedback) {
            $_feedbacks['list'][] = $hydrator->extract($feedback);
        }

        return new JsonModel(array("feedbacks" => $_feedbacks));
    }

    public function get($id)
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');
        $feedback = $objectManager
            ->getRepository('Application\Entity\Feedback')
            ->find($id);
        if (!$feedback instanceof Feedback) {
            $this->getResponse()->setStatusCode(
                Error::FeedbackNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::FeedbackNotFound_message
                )
            );

        }

        return new JsonModel(array("feedback" => $hydrator->extract($feedback)));


    }
}
