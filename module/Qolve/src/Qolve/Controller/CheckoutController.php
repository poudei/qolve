<?php
namespace Qolve\Controller;

use Application\Controller\AbstractController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\Asker,
    Application\Entity\User,
    Application\Entity\UserPref,
    Application\Entity\UserProfile,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;
    require_once(VENDOR_PATH . '/stripe-php/lib/Stripe.php');

class CheckoutController extends AbstractController
{
    public function checkoutAction()
    {
        $user = $this->identity();
        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($user->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        $config = $serviceLocator->get('config');
        $pref   = $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->findOneBy(
                [
                    'userId' => $user->getId(),
                    'key'    => 'credit'
                ]
        );

        $amount = ($pref instanceof UserPref) ?
            $pref->getValue() : 0;

        if ($amount >= $config['payment']['limit']) {

            $added = 0;
            if (($amount % 500) == 0) {
                $added = ((($amount / 500) + 1) * 30) + 10;
            }else {
                $added = (floor($amount / 500) * 30) + 40;
            }

            $amount += $added;
//            $amount -= $config['payment']['deduct'];
            \Stripe::setApiKey($config['payment']['apiKey']);

            if (!empty($data)) {
                $editableProperties = array(
                    'country',
                    'routingNumber',
                    'accountNumber',
                    'name',
                    'type',
                    'email'
                );

                foreach ($data as $name => $value) {
                    if (array_search($name, $editableProperties) === false) {
                        unset($data[$name]);
                    }
                }

                if (   empty($data['routingNumber'])
                    || empty($data['accountNumber'])
                    || empty($data['country'])
                    || empty($data['name'])
                ) {
                    $this->getResponse()->setStatusCode(401);
                    return new JsonModel(
                        array(
                            'error' => "Empty Data"
                        )
                    );
                }

                $data['type'] = !isset($data['type'])
                    ? 'individual' : $data['type'];

                $recipient = \Stripe_Recipient::create(
                    [
                        'name'         => $data['name'],
                        'type'         => $data['type'],
                        'bank_account' => [
                            'country'        => (string)$data['country'],
                            'account_number' => (string)$data['accountNumber'],
                            'routing_number' => (string)$data['routingNumber']
                        ],
                        "email"        => !empty($data['email']) ? $data['email'] : ''
                    ]
                );

                $recipientId = $recipient['id'];
                $transfer    = \Stripe_Transfer::create(
                    [
                        "amount"    => $amount, // amount in cents
                        "currency"  => "usd",
                        "recipient" => $recipientId,
                        "statement_description" => "Your Winning Pize"
                    ]
                );

                $pref->setValue(0);
                $objectManager->persist($pref);
                $objectManager->flush($pref);

            }

        } else {

            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'error' => "User does not have enough credit to checkout"
                )
            );

        }
        return new JsonModel(['Sucsess']);
    }

    public function deadlineAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        $config = $serviceLocator->get('config');

        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $now = $now->format('Ymd');
        $questions = $objectManager
            ->getRepository('Qolve\Entity\Question')
            ->getDeadlineQuestions(Question::PAID_NOTSTARTED, $now);
        foreach ($questions as $question) {
            $askers = [];
            $askers = $objectManager
                ->getRepository('Qolve\Entity\Asker')
                ->findByQuestionId($question->getId());
            foreach ($askers as $asker) {
                $userPref = $objectManager
                    ->getRepository('Application\Entity\UserPref')
                    ->findOneBy(
                        [
                            'userId' => $asker->getUserId(),
                            'key'    => 'credit'
                        ]
                );

                if ($userPref instanceof UserPref) {
                    $credit = 0;
                    $credit += $userPref->getCredit();
                    $userPref->setCredit($credit);
                    $objectManager->persist($userPref);
                    $objectManager->flush($userPref);
                }

            }
        }
    }
}
