<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Error,
    Qolve\Entity\Question,
    Qolve\Entity\Bookmark,
    Qolve\Entity\BookmarkList;

class BookmarkListQuestionsRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $limit      = $this->params()->fromQuery('limit', 5);
        $offset     = $this->params()->fromQuery('offset', 0);
        $byDocument = $this->params()->fromQuery('document', true);
        
        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);

        $user = $this->identity();

        $listId = $this->getEvent()->getRouteMatch()->getParam('list_id');
        if (strtolower($listId) 
            == strtolower(BookmarkList::NAME_UNCATEGORIZED)
        ) {
            if (!$user instanceof User) {
                $this->getResponse()->setStatusCode(
                    Error::AuthenticationFailed_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AuthenticationFailed_message
                    )
                );
            }

            $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
                ->findOneBy(
                    array(
                        'userId' => $user->getId(),
                        'name'   => BookmarkList::NAME_UNCATEGORIZED
                    )
                );
            if (!$list instanceof BookmarkList) {
                return new JsonModel(
                    array(
                        "questions" => array(
                            'list'  => array(),
                            'count' => 0
                        )
                    )
                );
            }
        } else {
            $list = $objectManager->getRepository('\Qolve\Entity\BookmarkList')
                ->find($listId);
        }

        if (!$list instanceof BookmarkList) {
            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }

        if ($user instanceof User) {
            similar_text($list->getUserId(), $user->getId(), $percent);
        } else {
            $percent = 0;
        }

        if ($list->getPrivacy() == BookmarkList::PRIVACY_PRIVATE
            && $percent < 100
        ) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $questions = $objectManager->getRepository('Qolve\Entity\Bookmark')
            ->getQuestionsOfList($list->getId(), $count, $offset, $limit);

        $info = array(
            'asker',
            'keywords',
            'userAnswerId'
        );
        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $_questions = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($questions as $question) {
            $_questions['list'][] = $hydrator
                ->extract($question, $info, $user);
        }

        return new JsonModel(
            array(
                "questions" => $_questions
            )
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );

            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $user = $this->identity();

        $listId = $this->getEvent()->getRouteMatch()->getParam('list_id');
        if (strtolower($listId) 
            == strtolower(BookmarkList::NAME_UNCATEGORIZED)
        ) {
            $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
                ->findOneBy(
                    array(
                        'userId' => $user->getId(),
                        'name'   => BookmarkList::NAME_UNCATEGORIZED
                    )
                );
            if (!$list instanceof BookmarkList) {
                $now = new \DateTime('now', new \DateTimeZone('UTC'));
                
                $list = new BookmarkList();
                $list->setName(BookmarkList::NAME_UNCATEGORIZED);
                $list->setUserId($user->getId());
                $list->setPrivacy(BookmarkList::PRIVACY_PRIVATE);
                $list->setQuestions(0);
                $list->setFollows(0);
                $list->setCreatedOn($now);

                $objectManager->persist($list);
            }
        } else {
            $list = $objectManager->getRepository('\Qolve\Entity\BookmarkList')
                ->find($listId);
        }

        if (!$list instanceof BookmarkList) {
            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }

        similar_text($list->getUserId(), $user->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        if (!isset($data['questionId'])) {
            $this->getResponse()->setStatusCode(
                Error::QuestionIdNotSent_code
            );

            return new JsonModel(
                array(
                    'error' => Error::QuestionIdNotSent_message
                )
            );
        }
        $questionId = strval($data['questionId']);
        $question   = $objectManager->getRepository('Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }
        if ($list->getPrivacy() == BookmarkList::PRIVACY_PUBLIC 
            && $question->getPrivacy() == Question::PRIVACY_SHARED
        ) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $bookmark = $objectManager->getRepository('\Qolve\Entity\Bookmark')
            ->findOneBy(
                array(
                    'listId'        => $list->getId(),
                    'questionId'    => $question->getId()
                )
            );
        if (!$bookmark instanceof Bookmark) {
            $bookmark = new Bookmark();
            $bookmark->setListId($list->getId());
            $bookmark->setQuestionId($question->getId());
            $objectManager->persist($bookmark);

            $questionsCount = $objectManager
                ->getRepository('Qolve\Entity\Bookmark')
                ->questionsCount($list->getId());
            $list->setQuestions($questionsCount + 1);
        }

        $objectManager->flush();

        $_bookmarkList = $hydrator->extract($list, array(), $user);
        return new JsonModel(
            array(
                'bookmarkList' => $_bookmarkList
            )
        );
    }

    public function delete($questionId)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $user = $this->identity();

        $listId = $this->getEvent()->getRouteMatch()->getParam('list_id');
        if (strtolower($listId) 
            == strtolower(BookmarkList::NAME_UNCATEGORIZED)
        ) {
            $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
                ->findOneBy(
                    array(
                        'userId' => $user->getId(),
                        'name'   => BookmarkList::NAME_UNCATEGORIZED
                    )
                );
        } else {
            $list = $objectManager->getRepository('\Qolve\Entity\BookmarkList')
                ->find($listId);
        }

        if (!$list instanceof BookmarkList) {
            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }

        similar_text($list->getUserId(), $user->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $bookmark = $objectManager->getRepository('Qolve\Entity\Bookmark')
            ->findOneBy(
                array(
                    'listId'        => $list->getId(),
                    'questionId'    => $question->getId()
                )
            );
        if (!$bookmark instanceof Bookmark) {
            $this->getResponse()->setStatusCode(
                Error::QuestionIsNotBookmarkedInList_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionIsNotBookmarkedInList_message
                )
            );
        }

        $objectManager->remove($bookmark);

        $questionsCount = $objectManager
            ->getRepository('Qolve\Entity\Bookmark')
            ->questionsCount($list->getId());

        $list->setQuestions(max($questionsCount - 1, 0));

        $objectManager->flush();

        return new JsonModel(array('Success'));
    }
}
