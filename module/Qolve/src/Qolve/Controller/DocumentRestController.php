<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Zend\File\Transfer\Adapter\Http,
    Application\Entity\User,
    Application\Entity\Error,
    Qolve\Entity\Question,
    Qolve\Entity\Answer,
    Qolve\Entity\Document,
    Zend\Form\Annotation\AnnotationBuilder;

class DocumentRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $limit    = $this->params()->fromQuery('limit', 10);
        $offset   = $this->params()->fromQuery('offset', 0);
        $criteria = $this->params()->fromQuery('criteria', array());
        $orderBy  = $this->params()->fromQuery(
            'orderBy', array('order' => 'ASC')
        );

        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        $criteria['questionId'] = $questionId;
        $criteria['answerId']   = $answerId;

        $documents = $objectManager->getRepository('Qolve\Entity\Document')
            ->findBy($criteria, $orderBy, $limit, $offset);
        $count = $objectManager->getRepository('Qolve\Entity\Document')
            ->countBy($criteria);

        $_documents = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($documents as $document) {
            $_documents['list'][] = $hydrator->extract($document);
        }

        return new JsonModel(
            array(
                'documents' => $_documents
            )
        );
    }

    public function get($id)
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        $document = $objectManager->getRepository('\Qolve\Entity\Document')
            ->find($id);

        if (!$document instanceof Document) {
            $this->getResponse()->setStatusCode(
                Error::DocumentNotFound_code
            );

            return new JsonModel(
                array(
                    'error' => Error::DocumentNotFound_message
                )
            );
        }

        similar_text($document->getQuestionId(), $questionId, $questionSim);
        similar_text($document->getAnswerId(), $answerId, $answerSim);
        if ($questionSim < 100 || $answerSim < 100) {
            $this->getResponse()->setStatusCode(
                Error::DocumentNotBelongsObject_code
            );
            return new JsonModel(
                array(
                    'error' => Error::DocumentNotBelongsObject_message
                )
            );
        }

        return new JsonModel(
            array(
                'document' => $hydrator->extract($document)
            )
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');
        $user     = $this->identity();
        $userId   = $user->getId();

        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        $doc   = $this->params()->fromFiles('doc');
        $order = (int) $this->getEvent()->getRouteMatch()->getParam('id')
            ? : 1;

        if (empty($doc)) {
            $this->getResponse()->setStatusCode(
                Error::FileNotSent_code
            );
            return new JsonModel(
                array(
                    'error' => Error::FileNotSent_message
                )
            );
        }

        if (!file_exists(
            "./public/upfiles/users/$userId/questions/$questionId")
        ) {
            mkdir(
                "./public/upfiles/users/$userId/questions/$questionId", 0777,
                true
            );
        }

        $userPath     = "users/$userId";
        $questionPath = "questions/$questionId";
        $answerPath   = "answers/$answerId";

        $location = "/upfiles/$userPath/$questionPath/";

        if ($answerId) {
            if (!file_exists(
                "./public/upfiles/$userPath/$questionPath/$answerPath")
            ) {
                mkdir(
                    "./public/upfiles/$userPath/$questionPath/$answerPath",
                    0777,
                    true
                );
            }

            $location .= "$answerPath/";
        }

        $adapter = new Http();
        $adapter->setDestination("./public$location");

        if (!$adapter->receive($doc['name'])) {
            $this->getResponse()->setStatusCode(
                Error::FileTransferingProblem_code
            );
            return new JsonModel(
                array(
                    'error' => Error::FileTransferingProblem_message
                )
            );
        }

        $document = new Document();
        $document->setQuestionId($questionId);
        $document->setAnswerId($answerId);
        $document->setOrder($order);
        $document->setLocation($location . $doc['name']);

        $type = substr($doc['type'], 0, 5);
        switch ($type) {
        case 'image':
            $document->setType(Document::TYPE_IMAGE);
            break;

        case 'video':
            $document->setType(Document::TYPE_VIDEO);
            break;
        }

        $objectManager->persist($document);
        $_document = $hydrator->extract($document);

        $objectManager->flush();

        return new JsonModel(
            array(
                "document" => $_document
            )
        );
    }

    public function update($id, $data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        $user   = $this->identity();
        $userId = $user->getId();

        $editableProperties = array(
            'description',
            'files'
        );

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);

        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );

                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        if ($answerId) {
            $preDocs =  $objectManager->getRepository('\Qolve\Entity\Document')
                ->findBy(
                    array(
                        'questionId' => $questionId,
                        'answerId' => $answerId
                    )
                );
        } else {
            $preDocs = $objectManager->getRepository('\Qolve\Entity\Document')
                ->findByQuestionId($questionId);
        }

        if (!empty($preDocs)) {
            foreach ($preDocs as $preDoc) {
                $preDocsId[] = $preDoc->getId();
            }

            foreach ($data['files'] as $file) {
                !empty($file['id']) ? $docsId[] = $file['id'] : '';
            }

            if (!empty($docsId)) {
                $sameDocs   = array_intersect($docsId, $preDocsId);
                $deleteDocs = array_diff($preDocsId, $sameDocs);
                foreach ($deleteDocs as $deleteId) {
                    $preDoc = $objectManager
                        ->getRepository('\Qolve\Entity\Document')
                        ->find($deleteId);
                    $objectManager->remove($preDoc);
                }
            } else {
                if ($answerId) {
                    $preDocs = $objectManager
                        ->getRepository('\Qolve\Entity\Document')
                        ->findBy(
                            array(
                                'questionId' => $questionId,
                                'answerId' => $answerId
                            )
                        );
                    if (!empty($preDocs)) {
                        foreach ($preDocs as $preDoc) {
                            $objectManager->remove($preDoc);
                        }
                    }
                } else {
                    $preDocs =  $objectManager
                        ->getRepository('\Qolve\Entity\Document')
                        ->findByQuestionId($questionId);
                    if (!empty($preDocs)) {
                        foreach ($preDocs as $preDoc) {
                            $objectManager->remove($preDoc);
                        }
                    }
                }
            }
        }

        $userPath     = "users/$userId";
        $questionPath = "questions/$questionId";
        $answerPath   = "answers/$answerId";
        $qPartialPath = "upfiles/$userPath/$questionPath";
        $aPartialPath = "upfiles/$userPath/$questionPath/$answerPath";

        foreach ($data['files'] as $file) {
            if (empty($file['id'])) {
                $fileName = $file['name'];
                $tempPath = $file['location'];
                if ($answerId) {
                    $location = "/$aPartialPath/$fileName";

                    if (!file_exists("./public/$aPartialPath/")) {
                        mkdir("./public/$aPartialPath/", 0777, true);
                    }
                    $destination = copy(
                        "./public/$tempPath" ,
                        "./public/$aPartialPath/$fileName"
                    );
                } else {
                    $location = "/$qPartialPath/$fileName";

                    if (!file_exists("./public/$qPartialPath/")) {
                        mkdir("./public/$qPartialPath/", 0777, true);
                    }
                    $destination = copy(
                        "./public/$tempPath" ,
                        "./public/$qPartialPath/$fileName"
                    );
                }
                if (!$destination) {
                    unlink("./public/$tempPath");
                    system(
                        'rm -rf '
                            . escapeshellarg("./public/upfiles/tempo/$userId"),
                        $retval
                    );

                    $this->getResponse()->setStatusCode(
                        Error::FileTransferingProblem_code
                    );

                    return new JsonModel(
                        array(
                            'error' => Error::FileTransferingProblem_message
                        )
                    );
                }

                $type = substr($file['type'], 0, 5);
                switch ($type) {
                case 'image':
                    $type = Document::TYPE_IMAGE;
                    break;

                case 'video':
                    $type = Document::TYPE_VIDEO;
                    break;
                }

                $data = array(
                    'order'    => $file['order'],
                    'type'     => $type,
                    'location' => $location
                );

                $builder  = new AnnotationBuilder();
                $document = new Document();
                $form     = $builder->createForm($document);
                $form->setHydrator($hydrator);
                $form->bind($document);
                $form->setData($data);

                if (!$form->isValid()) {
                    $this->getResponse()->setStatusCode(
                        Error::FormInvalid_code
                    );
                    return new JsonModel(
                        array(
                            'error' => $form->getMessages()
                        )
                    );
                }

                $document = $form->getData();
                $document->setQuestionId($question->getId());
                !empty($answerId) ? $document->setAnswerId($answerId) : '';
                $objectManager->persist($document);
                $objectManager->flush($document);
                unlink("./public".$tempPath);
                system(
                    'rm -rf '
                        . escapeshellarg("./public/upfiles/tempo/$userId"),
                    $retval
                );
            } else {
                $document = $objectManager
                    ->getRepository('\Qolve\Entity\Document')
                    ->find($file['id']);

                if ($file['order'] != $document->getOrder()) {
                    $newOrder = array('order' => $file['order']);
                    $preData  = $hydrator->extract($document);
                    $data     = array_merge($preData, $newOrder);
                    $builder  = new AnnotationBuilder();
                    $form     = $builder->createForm($document);
                    $form->setHydrator($hydrator);
                    $form->setBindOnValidate(false);
                    $form->bind($document);
                    $form->setData($data);

                    if (!$form->isValid()) {
                        $this->getResponse()->setStatusCode(
                            Error::FormInvalid_code
                        );
                        return new JsonModel(
                            array(
                                'error' => $form->getMessages()
                            )
                        );
                    }

                    $form->bindValues();
                }
            }
            $objectManager->flush();
        }

        $info = array('documentsList');

        if ($answerId) {
            $_answer = $hydrator->extract($answer, $info, $user);

            return new JsonModel(
                array(
                    'answer' => $_answer
                )
            );
        } else {
            $_question = $hydrator->extract($question, $info, $user);

            return new JsonModel(
                array(
                    'question' => $_question
                )
            );
        }
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        $document = $objectManager->getRepository('\Qolve\Entity\Document')
            ->find($id);

        if (!$document instanceof Document) {
            $this->getResponse()->setStatusCode(
                Error::DocumentNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::DocumentNotFound_message
                )
            );
        }

        similar_text($document->getQuestionId(), $questionId, $questionSim);
        similar_text($document->getAnswerId(), $answerId, $answerSim);
        if ($questionSim < 100 || $answerSim < 100) {
            $this->getResponse()->setStatusCode(
                Error::DocumentNotBelongsObject_code
            );
            return new JsonModel(
                array(
                    'error' => Error::DocumentNotBelongsObject_message
                )
            );
        }

        $objectManager->remove($document);
        $objectManager->flush();

        if (file_exists($document->getLocation())) {
            @rmdir($document->getLocation());
        }

        return new JsonModel(
            array(
                'success'
            )
        );
    }
}
