<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Share,
    Application\Entity\User,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

class ShareRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator    = $this->getServiceLocator()->get('Hydrator');

        $questionId  = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');

        $currentUser = $this->identity();

        $shares      = $objectManager
            ->getRepository('Qolve\Entity\Share')
            ->findByQuestionId($questionId);
        $count       = $objectManager
            ->getRepository('Qolve\Entity\Share')
            ->countByQuestionId($questionId);

        $_shares = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($shares as $share) {
            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($share->getUserId());
            if ($user instanceof User) {
                $_shares['list'][] = $hydrator->extract(
                    $user,
                    array(),
                    $currentUser
                );
            }
        }

        return new JsonModel(array("shareUsers" => $_shares));
    }

    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $hydrator    = $this->getServiceLocator()->get('Hydrator');
        $builder     = new AnnotationBuilder();
        $questionId  = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $data        = array_unique($data);

        foreach ($data['userId'] as $userId) {

            $share = $objectManager
                ->getRepository('Qolve\Entity\Share')
                ->findOneBy(array(
                    'userId'     => $userId,
                    'questionId' => $questionId)
            );

            if ($share instanceof Share) {
                $this->getResponse()->setStatusCode(
                    Error::DuplicateSharedUser_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::DuplicateSharedUser_message
                    )
                );
            }

            $shareData = array(
                'userId'     => $userId,
                'questionId' => $questionId
            );
            $share     = new Share();
            $form      = $builder->createForm($share);

            $form->setHydrator($hydrator);
            $form->bind($share);
            $form->setData($shareData);

            if (!$form->isValid()) {

                $this->getResponse()->setStatusCode(
                    Error::FormInvalid_code
                );
                return new JsonModel(
                    array(
                        'error' => $form->getMessages()
                    )
                );
            }

            $share = $form->getData();
            $objectManager->persist($share);
            $objectManager->flush();
            $return[] = $hydrator->extract($share);

        }

        return new JsonModel(array("SharedUsers" => $return));
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $share      = $objectManager
            ->getRepository('Qolve\Entity\Share')
            ->findOneBy(array(
                "userId"     => $id,
                "questionId" => $questionId)
        );

        if (!$share instanceof Share) {
            $this->getResponse()->setStatusCode(
                Error::ShareNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::ShareNotFound_message
                )
            );
        }

        $objectManager->remove($share);
        $objectManager->flush();

        return new JsonModel(array('success'));
    }

}
