<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\Error,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Qolve\Entity\Question,
    Qolve\Entity\Answer,
    Qolve\Entity\Transaction;

class ClaimRestController extends AbstractRestfulController
{
    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $user = $this->identity();

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $answerId = $this->getEvent()->getRouteMatch()
            ->getParam('answer_id');
        $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
            ->find($answerId);
        if (!$answer instanceof Answer) {
            $this->getResponse()->setStatusCode(
                Error::AnswerNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AnswerNotFound_message
                )
            );
        }

        similar_text($answer->getUserId(), $user->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(Error::PermissionDenied_code);
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        if ($question->getCredits() == 0) {
            $this->getResponse()->setStatusCode(
                Error::QuestionIsNotPaid_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionIsNotPaid_message
                )
            );
        }

        if ($objectManager->getRepository('Qolve\Entity\Answer')
            ->hasAcceptedAnswer($question->getId())
        ) {
            $this->getResponse()->setStatusCode(
                Error::QuestionHasAcceptedAnswer_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionHasAcceptedAnswer_message
                )
            );
        }

//        $debit      = (float) ($question->getCredits() * 0.5);
        $debit      = (int)$question->getCredits();
        $userCredit = (int) $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->getUserKey($user->getId(), 'credit');
        if ($userCredit < $debit) {
            $this->getResponse()->setStatusCode(
                Error::NotHaveEnoughCredit_code
            );
            return new JsonModel(
                array(
                    'error' => Error::NotHaveEnoughCredit_message
                )
            );
        }

        //create Transaction
        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        $notes = json_encode(
            array(
                'questionId' => $question->getId(),
                'answerId'   => $answer->getId()
            )
        );

        $transaction = new Transaction();
        $transaction->setUserId($user->getId());
        $transaction->setDebit($debit);
        $transaction->setStatus(Transaction::STATUS_BLOCKED);
        $transaction->setNotes($notes);
        $transaction->setCreatedOn($now);
        $objectManager->persist($transaction);

//        $userCredit = $objectManager
//            ->getRepository('Qolve\Entity\Transaction')
//            ->calculateUserCredit($user->getId());
        $userPref = $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->findOneBy(
                array(
                    'userId' => $user->getId(),
                    'key'    => 'credit'
                )
            );
        $userPref->setValue($userPref->getValue() - $debit);

        $objectManager->flush();

        $objectManager
            ->getRepository('Qolve\Entity\Question')
            ->setQuestionActivity(
                $this->getServiceLocator(),
                $params
        );

        return new JsonModel(
            array(
                'success' => true
            )
        );
    }
}
