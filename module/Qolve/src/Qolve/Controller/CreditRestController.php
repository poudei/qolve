<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\User,
    Application\Entity\UserPref,
    Application\Entity\Error,
    Qolve\Entity\Transaction,
    Zend\View\Model\JsonModel;

class CreditRestController extends AbstractRestfulController
{
    public function getList()
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $userCredit = $objectManager->getRepository('Qolve\Entity\Transaction')
            ->calculateUserCredit($user->getId());

        return new JsonModel(
            array(
                'credit' => $userCredit
            )
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $user = $this->identity();

        if (!isset($data['credit'])) {
            $this->getResponse()->setStatusCode(
                Error::CreditNotSent_code
            );
            return new JsonModel(
                array(
                    'error' => Error::CreditNotSent_message
                )
            );
        }

        if ($data['credit'] <= 0) {
            $this->getResponse()->setStatusCode(
                Error::CreditIsNegative_code
            );
            return new JsonModel(
                array(
                    'error' => Error::CreditIsNegative_message
                )
            );
        }

        if (!isset($data['action'])) {
            $this->getResponse()->setStatusCode(
                Error::ActionNotSent_code
            );
            return new JsonModel(
                array(
                    'error' => Error::ActionNotSent_message
                )
            );
        }

        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        $userCredit = $objectManager->getRepository('Qolve\Entity\Transaction')
            ->calculateUserCredit($user->getId());

        switch (strtolower($data['action'])) {
        case 'buy':
            $transaction = new Transaction();
            $transaction->setUserId($user->getId());
            $transaction->setDebit((float) $data['credit']);
            $transaction->setStatus(Transaction::STATUS_DONE);
            $transaction->setCreatedOn($now);
            $objectManager->persist($transaction);

            $userPref = $objectManager
                ->getRepository('Application\Entity\UserPref')
                ->find(
                    array(
                        'userId' => $user->getId(),
                        'key'    => 'credit'
                    )
                );
            if (!$userPref instanceof \Application\Entity\UserPref) {
                $userPref = new UserPref();
                $userPref->setUserId($user->getId());
                $userPref->setKey('credit');
                $userPref->setValue(0);
                
                $objectManager->persist($userPref);
            }
            
            $userCredit = (float) $userPref->getValue() 
                + (float) $data['credit'];
            $userPref->setValue($userCredit);

            $objectManager->flush();
            break;

        case 'sell':
            if ($userCredit < (float) $data['credit']) {
                $this->getResponse()->setStatusCode(
                    Error::NotHaveEnoughCredit_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::NotHaveEnoughCredit_message
                    )
                );
            }

            $transaction = new Transaction();
            $transaction->setUserId($user->getId());
            $transaction->setCredit((float) $data['credit']);
            $transaction->setStatus(Transaction::STATUS_DONE);
            $transaction->setCreatedOn($now);
            $objectManager->persist($transaction);

            $userPref = $objectManager
                ->getRepository('Application\Entity\UserPref')
                ->getUserPref($user->getId(), 'credit');
            $userCredit -= (float) $data['credit'];
            $userPref->setValue($userCredit);
            $objectManager->persist($userPref);

            $objectManager->flush();
            break;
        }

        return new JsonModel(
            array(
                'credit' => $userCredit
            )
        );
    }
}

