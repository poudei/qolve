<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Application\Entity\User,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;
    require_once(VENDOR_PATH . '/stripe-php/lib/Stripe.php');

class PaymentRestController extends AbstractRestfulController
{
    public function getList()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $user = $this->identity();
        $config = $this->getServiceLocator()
            ->get('config');

        \Stripe::setApiKey($config['payment']['apiKey']);

        if (is_null($user->getCustomerId())) {
            $return = [
                'list'  => [],
                'count' => 0
            ];
            return new JsonModel(['cards' => $return]);

        }

        $cards = [];
        $cardsList = \Stripe_Customer::retrieve($user->getCustomerId())->cards->all();
        $reflect   = new \ReflectionClass($cardsList);
        $reflect   = $reflect->getProperty('_values');
        $reflect->setAccessible(true);
        $cards = $reflect->getValue($cardsList);
        $cards = $cards['data'];
        foreach ($cards as $card) {
            $reflect = new \ReflectionClass($card);
            $reflect = $reflect->getProperty('_values');
            $reflect->setAccessible(true);
            $card = $reflect->getValue($card);

            $cardInfo[] = [
                'id'    => $card['id'],
                'last4' => $card['last4'],
                'type'  => $card['type']
            ];
        }

        $count = count($cards);
        $return = [
            'list'  => $cardInfo,
            'count' => $count
        ];

        return new JsonModel(['cards' => $return]);
    }
}
