<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Error;

class UserFollowRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $currentUser = $this->identity();

        $userId = $this->getEvent()->getRouteMatch()->getParam('user_id');
        if ($userId != null && strtoupper($userId) != 'ME') {
            $user = $objectManager
                ->getRepository('\Application\Entity\User')
                ->find($userId);
        } else {
            $user = $currentUser;
        }
        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::UserNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserNotFound_message
                )
            );
        }

        $users = $objectManager
            ->getRepository('Qolve\Entity\UserFollow')
            ->getRelatedUsers($user->getId());

        $_users = array();
        foreach ($users as $_user) {
            $_users[] = $hydrator->extract(
                $_user, array(), $currentUser
            );
        }

        return new JsonModel(
            array(
                'list' => $_users
            )
        );
    }
}
