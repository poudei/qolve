<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Keyword,
    Zend\Form\Annotation\AnnotationBuilder;

class KeywordRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');

        $limit  = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        
        $user = $this->identity();
        $objectManager
            ->getConnection()
            ->getShardManager()
            ->selectGlobal();

        $keywords = $objectManager
            ->getRepository('Qolve\Entity\Keyword')
            ->findBy(array(), array('name' => 'ASC'), $limit, $offset);
        $count = $objectManager
            ->getRepository('Qolve\Entity\Keyword')
            ->countAll();

        $_keywords = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($keywords as $keyword) {
            $_keywords['list'][] = $hydrator->extract(
                $keyword,
                array(),
                $user
            );
        }

        return new JsonModel(array('keyword' => $_keywords));
    }

    public function get($name)
    {
       
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');
        $config     = $this->getServiceLocator()->get('config');
        $limit      = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 10)
        );
        $offset     = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
        );

        $keywords = $objectManager
            ->getRepository('Qolve\Entity\Keyword')
            ->getSimilarKeywords($name, $offset, $limit, $config);

        $_keywords = array();
        foreach ($keywords as $keyword) {
            $_keywords[] = $keyword;
        }

        return new JsonModel(array('keyword' => $_keywords));
    }

    public function create($data) {
        $keys = $data['keywords'];
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        if (is_array($keys)) {
            foreach ($keys as $key) {
                $keyword = new Keyword();
                $keyword->setName($key);
                $objectManager->persist($keyword);
                $objectManager->flush();
            }
        }
        
        return new JsonModel(array('keyword' => 'success'));
    }
}
