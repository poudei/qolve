<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Feed,
    Application\Entity\Error,
    Qolve\Entity\Keyword,
    Qolve\Entity\KeywordFollow,
    Zend\Form\Annotation\AnnotationBuilder;

class KeywordFollowRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');

        $userId = $this->params()->fromQuery('userId');
//        $keyword   = $objectManager
//            ->getRepository('\Qolve\Entity\Keyword')
//            ->find($keywordId);
//
//        if (!$keyword instanceof Keyword) {
//            $this->getResponse()->setStatusCode(
//                Error::KeywordNotFound_code
//            );
//            return new JsonModel(array(
//                'error' => Error::KeywordNotFound_message
//                )
//            );
//        }
        
        $limit    = $this->normalize(
            'limit', 
            $this->params()->fromQuery('limit', null)
        );
        $offset   = $this->normalize(
            'offset', 
            $this->params()->fromQuery('offset', null));

//        $currentUser = $this->identity();
//
//        $followers = $objectManager
//            ->getRepository('Qolve\Entity\KeywordFollow')
//            ->getKeywordFollowers($keywordId, $count, $offset, $limit);
//
//        $_followers = array(
//            'list'  => array(),
//            'count' => $count, 
//        );
//        foreach ($followers as $follower) {
//            $_followers['list'][] = $hydrator
//                ->extract($follower, array(), $currentUser);
//        }

        $user = $this->identity();
        empty($userId) ? $userId = $user->getId() : '';
        $keyIds = $objectManager
            ->getRepository('Qolve\Entity\KeywordFollow')
            ->getuserkeywords($userId, $count, $offset, $limit);

        $keywords = array();
        foreach($keyIds as $keyId) {

            $keyword = $objectManager
                ->getRepository('Qolve\Entity\Keyword')
                ->find($keyId['keywordId']);
    
            if ($keyword instanceof Keyword) {
                $keywords[] = $hydrator->extract($keyword, array(), $user);
            } 
        }
        $return['keywords'] = array(
            'list'  => $keywords,
            'count' => $count

        );
        return new JsonModel($return);
    }

    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();

        $user     = $this->identity();
        $userId   = $user->getId();

        if (!empty($data)) {

            if (isset($data['levels'])) {

                if (preg_match('/\,$/', $data['levels'])) {

                    $data['levels'] = preg_replace(
                        '/\,$/',
                        '',
                        $data['levels']
                    );
                }

                if (preg_match('/\,/', $data['levels'])) {

                    $data['levels'] = explode(',', $data['levels']);
                    $data['levels'] = array_map(
                        function ($level) {return trim($level);},
                        $data['levels']
                    );
                    $data['levels'] = implode(',', $data['levels']);
                }
            } else {
                $data['levels'] = "General";
            }

            foreach ($data['keywords'] as $key) {

                if (empty($key['name'])) {
                    $this->getResponse()->setStatusCode(
                        Error::NameNotSent_code
                    );
                    return new JsonModel(
                        array(
                            'error' => Error::NameNotSent_message
                        )
                    );
                }
                $keyName = trim(strtolower($key['name']));
                $keyword = $objectManager
                    ->getRepository('\Qolve\Entity\Keyword')
                    ->getKeyByName($keyName);

                if ($keyword instanceof Keyword) {
                    $keywordId = $keyword->getId();

                } else {

                    $name    = trim(strtolower($key['name']));
                    $newKey  = new Keyword();
                    $keyForm = $builder->createForm($newKey);
                    $keyForm->setHydrator($hydrator);
                    $keyForm->bind($newKey);
                    $keyForm->setData(array('name' => $name));

                    if (!$keyForm->isValid()) {

                        $this->getResponse()->setStatusCode(
                            Error::FormInvalid_code
                        );
                        return new JsonModel(
                            array(
                                'error' => $keyForm->getMessages()
                            )
                        );
                    }

                    $newKey = $keyForm->getData();
                    $objectManager->persist($newKey);
                    $objectManager->flush($newKey);
                    $keywordId = $newKey->getId();
                }

                $kFollow = $objectManager
                    ->getRepository('\Qolve\Entity\KeywordFollow')
                    ->findOneBy(array(
                        'keywordId'  => $keywordId,
                        'userId'     => $userId
                    ));

                if ($kFollow instanceof KeywordFollow) {
                    continue;
                }

                $builder   = new AnnotationBuilder();
                $keyFollow = new KeywordFollow();
                $form      = $builder->createForm($keyFollow);

                $form->setHydrator($hydrator);
                $form->bind($keyFollow);
                $form->setData($data);

                if (!$form->isValid()) {

                    $this->getResponse()->setStatusCode(
                        Error::FormInvalid_code
                    );
                    return new JsonModel(
                        array(
                            'error' => $form->getMessages()
                        )
                    );
                }

                $keyFollow = $form->getData();
                $keyFollow->setKeywordId($keywordId);
                $keyFollow->setUserId($userId);
                $objectManager->persist($keyFollow);
                $now  = new \DateTime('now', new \DateTimeZone('UTC'));

                $objectManager->flush();

                $keyFollow          = $hydrator->extract($keyFollow);
                $keyFollows[]       = $keyFollow;
                $info['keywords'][] = $keyFollow['keywordId'];
            }

            if (!empty($keyFollows)) {
                $info['userId'] = $userId;
                \Qolve\Verb\FollowKeyword::publishFeed(
                    $info, $this->serviceLocator
                );
            }
        }

        return new JsonModel(array(
            'keywordFollow' => !empty($keyFollows) ?
             $keyFollows : array()
             )
        );
    }

    public function delete($name)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $keyword   = $objectManager
            ->getRepository('\Qolve\Entity\Keyword')
            ->getKeyByName($name);

        if (!$keyword instanceof Keyword) {
            $this->getResponse()->setStatusCode(
                Error::KeywordNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::KeywordNotFound_message
                )
            );
        }
        $keyId = $keyword->getId();
        $user  = $this->identity();

        $kFollow = $objectManager
            ->getRepository('\Qolve\Entity\KeywordFollow')
            ->findOneBy(array(
                'keywordId'  => $keyId,
                'userId'     => $user->getId()
            ));
        if (!$kFollow instanceof KeywordFollow) {
            $this->getResponse()->setStatusCode(
                Error::UserHasNotFollowedThisObject_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserHasNotFollowedThisObject_message
                )
            );
        }

        $objectManager->remove($kFollow);
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'verb' => 'FollowKeyword',
                    'content' => $keyword->getName(),
                    'actorId' => $user->getId()
                )
            );
        
        $objectManager->flush();

        return new JsonModel(array('Success'));
    }
}
