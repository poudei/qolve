<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\Answer,
    Qolve\Entity\Vote,
    Qolve\Entity\QuestionKeyword,
    Qolve\Entity\Keyword,
    Application\Entity\Feed,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder,
    Application\Service\Gearman;

class VoteRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator   = $this->getServiceLocator()->get('Hydrator');

        $userId     = $this->getEvent()->getRouteMatch()->getParam('user_id');
        $questionId = $this->getEvent()->getRouteMatch()->getParam('question_id');
        $answerId   = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        if ($userId) {
            return $this->get($userId);
        }

        $question = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        if ($answerId) {
            $answer = $objectManager
                ->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        $criteria = $this->params()->fromQuery('criteria', array());
        $criteria['questionId'] = $questionId;
        $criteria['answerId']   = $answerId;
        $limit    = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 5)
        );
        $offset   = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
        );
        $orderBy  = $this->params()
            ->fromQuery('orderBy', array('votedOn' => 'DESC'));

        $votes = $objectManager
            ->getRepository('Qolve\Entity\Vote')
            ->findBy($criteria, $orderBy, $limit, $offset);
        $count = $objectManager
            ->getRepository('Qolve\Entity\Vote')
            ->countBy($criteria);

        $_votes = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($votes as $vote) {
            $_votes['list'][] = $hydrator->extract($vote);
        }

        return new JsonModel(array('votes' => $_votes));
    }

    public function get($userId)
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $questionId = $this->getEvent()->getRouteMatch()->getParam('question_id');
        $answerId   = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        $question   = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        if ($answerId) {
            $answer = $objectManager
                ->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error'=> Error::AnswerNotFound_code
                    )
                );
            }
        }

        $vote = $objectManager
            ->getRepository('\Qolve\Entity\Vote')
            ->findOneBy(array(
                'questionId' => $questionId,
                'answerId'   => $answerId,
                'userId'     => $userId
            ));
        if ($vote instanceof Vote) {
            $score = $vote->getScore();
        } else {
            $score = 0;
        }

        return new JsonModel(array('vote' => $score));
    }

    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $userId     = $this->getEvent()->getRouteMatch()
            ->getParam('user_id');
        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $answerId   = $this->getEvent()->getRouteMatch()
            ->getParam('answer_id');

        $question   = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);

        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }
        $answer = null;
        if ($answerId) {
            $answer = $objectManager
                ->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }

            $_answer = $hydrator->extract($answer, array(), $this->identity());
            if ($_answer['invisible'] == 1) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $user = $this->identity();
        if ($userId) {
            similar_text($userId, $user->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }
        if (!$userId) {
            $userId = $user->getId();
        }
        //TODO: remove this comment
        /*
        if ($answerId == null) {
            $asker = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findOneBy(array(
                        'questionId' => $question->getId(),
                        'status'     => \Qolve\Entity\Asker::STATUS_ASKER
                    ));

            similar_text($asker->getUserId(), $user->getId(), $percent);
            if ($percent == 100) {
                $this->getResponse()->setStatusCode(403);

                return new JsonModel(array('error' => "Permission Denied : User does not have permission to vote his own question."));
            }
        } else {
            similar_text($answer->getUserId(), $user->getId(), $percent);
            if ($percent == 100) {
                $this->getResponse()->setStatusCode(403);

                return new JsonModel(array('error' => "Permission Denied : User does not have permission to vote his own answer."));
            }
        }
         */
        if (isset($data['score'])) {
            $score = (int) $data['score'];
            if ($score != -1 && $score != 1) {
                $this->getResponse()->setStatusCode(
                    Error::ScoreInvalid_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::ScoreInvalid_message
                    )
                );
            }
        }

        $preVote = $objectManager->getRepository('\Qolve\Entity\Vote')
            ->findOneBy(array(
                'questionId' => $questionId,
                'answerId'   => $answerId,
                'userId'     => $user->getId()
            )
        );
        if ($preVote instanceof Vote) {
            return $this->update($preVote->getId(), $data);
        }

        $builder  = new AnnotationBuilder();
        $vote     = new Vote();
        $form     = $builder->createForm($vote);
        $form->setHydrator($hydrator);
        $form->bind($vote);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(
                Error::FormInvalid_code
            );
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $vote = $form->getData();
        $now  = new \DateTime('now', new \DateTimeZone('UTC'));

        $vote->setQuestionId($questionId);
        $vote->setAnswerId($answerId);
        $vote->setUserId($user->getId());
        $vote->setVotedOn($now);

        $objectManager->persist($vote);
        $objectManager->flush($vote);

        $votes  = $objectManager
            ->getRepository('\Qolve\Entity\Vote')
            ->getTotalScores($questionId, $answerId);
        $voters = $objectManager
            ->getRepository('\Qolve\Entity\Vote')
            ->getVotersCount($questionId, $answerId);

        $object = isset($answerId) ? $answer : $question;
        $object->setVotes($votes);
        $object->setVoters($voters);
        $objectManager->persist($object);
        $objectManager->flush($object);

        if ($object instanceof Question) {
            //Create one record for user in follow table, if there is not
            $objectManager
                ->getRepository('Qolve\Entity\QuestionFollow')
                ->follow($user->getId(), $object->getId());
        }

        // Elastic Append vote

        if ($object instanceof Question) {
            $date = $vote->getVotedOn()->format('Y-m-d H:i:s');
            $this->elasticSearch($questionId, $date, null);
        }

        //Add to user's Activities
        if (!$answer instanceof Answer) {
            $feedPrivacy = $question->getPrivacy();
        } else {
            $feedPrivacy = max(
                array(
                    $question->getPrivacy(), $answer->getPrivacy()
                )
            );
        }

        $feed = new Feed();
        $feed->setUserId($user->getId());
        $feed->setActorId($user->getId());
        $feed->setQuestionId($questionId);
        $feed->setAnswerId($answerId);
        $feed->setContent((string) $vote->getScore());
        $feed->setPrivacy($feedPrivacy);
        $feed->setVerb('Vote');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);

        $objectManager->flush();

        //Add to Queue for publish Feeds
        if ($feedPrivacy == Feed::PRIVACY_PUBLIC) {
            $queue = $this->serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('feeds');

            $data = array(
                'verb'   => '\Qolve\Verb\Vote',
                'voteId' => $vote->getId()
            );

            $data['to'] = array('userFollowers');
            $job = new \Application\Job\PublishFeed($this->serviceLocator);
            $job->setContent($data);
            $queue->push($job);
        }

        //Add to Queue for send Notification
        $queue = $this->serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'      => '\Qolve\Verb\Vote',
            'voteId'    => $vote->getId()
        );

        if ($answerId == null) {
            $data['to'] = array('askers');
        } else {
            $data['to'] = array('answerWriter');
        }

        $job = new \Application\Job\SendNotification($this->serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        return new JsonModel(array(
            'vote'    => $hydrator->extract($vote),
            'votes'   => $object->getVotes(),
            'success' => true
        ));
    }

    public function update($id, $data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $answerId   = $this->getEvent()->getRouteMatch()
            ->getParam('answer_id');

        $question   = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        if ($answerId) {
            $answer = $objectManager
                ->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }

            $_answer = $hydrator->extract($answer, array(), $this->identity());
            if ($_answer['invisible'] == 1) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $editableProperties = array('score');

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        $user = $this->identity();

        if (isset($data['score'])) {
            $score = (int) $data['score'];
            if ($score != -1 && $score != 1) {
                $this->getResponse()->setStatusCode(
                    Error::ScoreInvalid_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::ScoreInvalid_message
                    )
                );
            }
        }

        $vote = $objectManager->getRepository('\Qolve\Entity\Vote')
            ->findOneBy(array(
                'id'         => $id,
                'questionId' => $questionId,
                'answerId'   => $answerId
         )
        );

        if (!$vote instanceof Vote) {
            $this->getResponse()->setStatusCode(
                Error::VoteNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::VoteNotFound_message
                )
            );
        }

        similar_text($vote->getUserId(), $user->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }
        $now  = new \DateTime('now', new \DateTimeZone('UTC'));

        $preData  = $hydrator->extract($vote);
        $data     = array_merge($preData, $data);
        $builder  = new AnnotationBuilder();
        $form     = $builder->createForm($vote);
        $form->setHydrator($hydrator);
        $form->setBindOnValidate(false);
        $form->bind($vote);
        $form->setData($data);
        $vote->setVotedOn($now);
        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(
                Error::FormInvalid_code
            );
            return new JsonModel(
                array(
                    'error' => $form->getMessages()
                )
            );
        }

        $form->bindValues();
        $objectManager->flush($vote);

        $votes  = $objectManager
            ->getRepository('\Qolve\Entity\Vote')
            ->getTotalScores($questionId, $answerId);
        $voters = $objectManager
            ->getRepository('\Qolve\Entity\Vote')
            ->getVotersCount($questionId, $answerId);

        $object = !empty($answerId) ? $answer : $question;
        $object->setVotes($votes);
        $object->setVoters($voters);
        $objectManager->flush($object);

        // Update Elastic
        if (empty($answerId)) {
            $date = $vote->getVotedOn()->format('Y-m-d H:i:s');
            $this->elasticSearch($questionId, $date, null);
        }
        
        //delete previous feeds and notifications
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'fromId'     => $user->getId(),
                    'questionId' => $question->getId(),
                    'answerId'   => $answerId,
                    'verb'       => 'Vote'
                )
            );
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'actorId'    => $user->getId(),
                    'questionId' => $question->getId(),
                    'answerId'   => $answerId,
                    'verb'       => 'Vote'
                )
            );
        
        $objectManager->flush();
        
        //Add to user's Activities
        if (!$answerId) {
            $feedPrivacy = $question->getPrivacy();
        } else {
            $feedPrivacy = max(
                array(
                    $question->getPrivacy(), $answer->getPrivacy()
                )
            );
        }

        $feed = new Feed();
        $feed->setUserId($vote->getUserId());
        $feed->setActorId($vote->getUserId());
        $feed->setQuestionId($vote->getQuestionId());
        $feed->setAnswerId($vote->getAnswerId());
        $feed->setContent((string) $vote->getScore());
        $feed->setPrivacy($feedPrivacy);
        $feed->setVerb('Vote');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);

        $objectManager->flush();

        //Add to Queue for send Notification
        $queue = $this->serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'   => '\Qolve\Verb\Vote',
            'voteId' => $vote->getId()
        );
        
        if ($answerId == null) {
            $data['to'] = array('askers');
        } else {
            $data['to'] = array('answerWriter');
        }
        
        $job = new \Application\Job\SendNotification($this->serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        return new JsonModel(array(
            'vote'  => $hydrator->extract($vote),
            'votes' => $object->getVotes()
        ));
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()->getParam('question_id');
        $answerId   = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        $question   = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);

        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        if ($answerId) {
            $answer = $objectManager
                ->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }

            $_answer = $hydrator->extract($answer, array(), $this->identity());
            if ($_answer['invisible'] == 1) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $user = $this->identity();

        $vote = $objectManager
            ->getRepository('\Qolve\Entity\Vote')
            ->findOneBy(array(
                'userId'     => $user->getId(),
                'questionId' => $questionId,
                'answerId'   => $answerId
            )
        );
        if (!$vote instanceof Vote) {
            $this->getResponse()->setStatusCode(
                Error::VoteNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::VoteNotFound_message
                )
            );
        }

        $objectManager->remove($vote);
        $objectManager->flush($vote);

        $votes  = $objectManager
            ->getRepository('\Qolve\Entity\Vote')
            ->getTotalScores($questionId, $answerId);
        $voters = $objectManager
            ->getRepository('\Qolve\Entity\Vote')
            ->getVotersCount($questionId, $answerId);

        $object = isset($answerId) ? $answer : $question;
        $object->setVotes($votes);
        $object->setVoters($voters);

        $objectManager->flush($object);

        empty($answerId) ? $answerId = NULL : '';

        // delete feed and notification
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'fromId'     => $user->getId(),
                    'questionId' => $question->getId(),
                    'answerId'   => $answerId,
                    'verb'       => 'Vote'
                )
            );
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'actorId'    => $user->getId(),
                    'questionId' => $question->getId(),
                    'answerId'   => $answerId,
                    'verb'       => 'Vote'
                )
            );
        
        $objectManager->flush();

        // Elastic delete //

        if ($object instanceof Question) {
            $date = $vote->getVotedOn()->format('Y-m-d H:i:s');
            $mode = 'delete';
            $this->elasticSearch($questionId, $date, $mode);
        }

        return new JsonModel(array(
            'votes'     => $object->getVotes(),
            'success'   => true
        ));
    }

    public function elasticSearch($questionId, $date = null, $mode = null)
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $question       = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        $elastica = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $elasticaIndex  = $elasticaClient->getIndex($index);
        $elasticaType   = $elasticaIndex->getType('question');

        if ($mode == 'delete') {

            $param = array(
                'votes'       => $question->getVotes(),
                'last_update' => $date
            );

            $document = $elasticaType->getDocument($questionId);

            $document->setData($param);
            $elasticaType->updateDocument($document);
            $elasticaType->getIndex()->refresh();

        } else {
            $param = array(
                'votes'       => $question->getVotes(),
                'last_update' => $date
            );

            $document = $elasticaType->getDocument($questionId);

            $document->setData($param);
            $elasticaType->updateDocument($document);
            $elasticaType->getIndex()->refresh();

            $test = new \Qolve\Job\VotingStats($this->getServiceLocator());
            $test->execute();
        }
    }
}
