<?php
namespace Qolve\Controller;

use Application\Controller\AbstractController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\RegRequest,
    Qolve\Entity\Keyword,
    Qolve\Entity\KeywordFollow,
    Application\Entity\Password,
    Application\Entity\User,
    Application\Entity\UserPref,
    Application\Entity\UserProfile,
    Application\Entity\Error,
    Qolve\Entity\Transaction,
    Zend\Crypt\Password\Bcrypt,
    Zend\Filter\HtmlEntities,
    Zend\Form\Annotation\AnnotationBuilder;


class RegisterRequestController extends AbstractController
{
    public function getlistAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');
        $registers = array();
        $objectManager->getConnection()
            ->getShardManager()
            ->selectGlobal();

        $registers = $objectManager
            ->getRepository('Qolve\Entity\RegRequest')
            ->findAll();

        $returns = array();

        foreach ($registers as $register) {
            $returns[] = $hydrator->extract($register);
        }

        return new JsonModel(array("users" => $returns));

    }

    public function getAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');
        $objectManager->getConnection()
            ->getShardManager()
            ->selectGlobal();

        $email = $this->params()->fromQuery('email');

        if(!empty($email)) {

            $objectManager
                ->getConnection()
                ->getShardManager()
                ->selectGlobal();

            $register = $objectManager
                ->getRepository('Qolve\Entity\RegRequest')
                ->findOneByEmail($email);

            if ($register instanceof RegRequest) {

                $this->getResponse()->setStatusCode(200);
                return new JsonModel($hydrator->extract($register));
            } else {
                $this->getResponse()->setStatusCode(
                    Error::RegisterRequestNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::RegisterRequestNotFound_message
                    )
                );
            }
        }
    }

    public function demandAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        $editableProperties = array(
            'id',
            'email',
            'name',
            'gender',
            'education',
            'age',
            'type',
            'timezone',
            'keywords'
        );

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }
        if (!empty($data['email'])) {
            if (!preg_match(
                    '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/',
                    $data['email']
                )
            ) {
                $this->getResponse()->setStatusCode(
                    Error::EmailInvalid_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::EmailInvalid_message
                    )
                );
            }

            $objectManager
                ->getConnection()
                ->getShardManager()
                ->selectGlobal();

            $request = $objectManager
                ->getRepository('Qolve\Entity\RegRequest')
                ->findOneByEmail($data['email']);

            if ($request instanceof RegRequest) {
                $this->getResponse()->setStatusCode(
                    Error::DuplicateEmail_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::DuplicateEmail_message
                    )
                );
            }
        }

        if (  ($data['gender'] < 1)
            ||
              ($data['gender'] > 2)
        ) {
            $this->getResponse()->setStatusCode(
                Error::Error_code
            );
            return new JsonModel(
                array(
                    'error' => Error::Error_message
                )
            );
        }

        if (isset($data['keywords'])) {
            $keys = explode(',', $data['keywords']);
            $keys = array_unique($keys);
            
            foreach ($keys as $key => $value) {
                $filter = new HtmlEntities();
                $keys[$key] = $filter->filter($value);
            }
            $data['keywords'] = implode(',', $keys);
        }
        $builder     = new AnnotationBuilder();
        $regRequest  = new RegRequest();
        $form        = $builder->createForm($regRequest);

        $form->setHydrator($hydrator);
        $form->bind($regRequest);
        $form->setData($data);
        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $regRequest = $form->getData();
        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $regRequest->setRequestTime($now);
        $objectManager->persist($regRequest);
        $objectManager->flush();
        return new JsonModel(array(
            'user' => $hydrator->extract($regRequest)
            )
        );
    }

    public function inviteAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        $editableProperties = array('email');

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (!empty($data['email'])) {
            if (!preg_match(
                    '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/',
                    $data['email']
                )
            ) {
                $this->getResponse()->setStatusCode(
                    Error::EmailInvalid_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::EmailInvalid_message
                    )
                );
            }

            $objectManager
                ->getConnection()
                ->getShardManager()
                ->selectGlobal();

            $request = $objectManager
                ->getRepository('Qolve\Entity\RegRequest')
                ->findOneByEmail($data['email']);

            if (!$request instanceof RegRequest) {
                $this->getResponse()->setStatusCode(
                    Error::RegisterRequestNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::RegisterRequestNotFound_message
                    )
                );
            }

            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($request->getUserId());

            if (!$user instanceof User) {
                $this->getResponse()->setStatusCode(
                    Error::UserNotRegistered_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::UserNotRegistered_message
                    )
                );
            }

            $now      = new \DateTime('now', new \DateTimeZone('UTC'));

            $bcrypt   = new Bcrypt();
            $length   = rand(6,8);
            $bytes    = openssl_random_pseudo_bytes($length, $cstrong);
            $password = bin2hex($bytes);
            $cryPass  = $bcrypt->create($password);

            $length   = rand(15,20);
            $bytes    = openssl_random_pseudo_bytes($length, $cstrong);
            $hashKey  = bin2hex($bytes);

            $password = $objectManager
                ->getRepository('Application\Entity\Password')
                ->find($user->getId());

            if ($password instanceof Password) {
                $objectManager->remove($password);
                $objectManager->flush($password);
            }

            $password = new Password();
            $password->setRequestKey($hashKey);
            $password->setUserId($user->getId());
            $password->setRequestTime($now);
            $objectManager->persist($password);
            $objectManager->flush();

            $config     = $serviceLocator->get('config');
            $emailQueue = $serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('email');

            $mail = new \Application\Job\SendEmail($serviceLocator);
            $mail->setContent(array(
                'to'        => $data['email'],
                'subject'   => $user->getName() . ", You're chosen to help us and win the prize!",
                'template'  => 'Register',
                'params'    => array(
                'resetpath' => $config['email']['address'].$hashKey,
                'name'      => $user->getName(),
                'username'  => $user->getUsername()
                )
            ));
            $mail->execute();
            return new jsonModel(array('Success'));

        }
        
        $this->getResponse()->setStatusCode(
            Error::EmailNotSent_code
        );
        return new jsonModel(
            array(
                'error' => Error::EmailNotSent_message
            )
        );
    }

    public function registerAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');
        $builder   = new AnnotationBuilder();
        $config    = $this->getServiceLocator()->get('config');

        $data   = $this->getRequest()->getContent();
        $data   = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        $emails = $data['email'];
        $users = array();
        if(!empty($emails)) {
            !is_array($emails) ? $emails = array($emails) : '';
            foreach ($emails as $email) {
                $objectManager
                    ->getConnection()
                    ->getShardManager()
                    ->selectGlobal();

                $register = $objectManager
                    ->getRepository('Qolve\Entity\RegRequest')
                    ->findOneByEmail($email);

                if (!$register instanceof RegRequest) {
                    $this->getResponse()->setStatusCode(
                        Error::RegisterRequestNotFound_code
                    );
                    return new JsonModel(
                        array(
                            'error' => Error::RegisterRequestNotFound_message
                        )
                    );

                }

                $return = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->getOneByElastic(
                        $serviceLocator,
                        'email',
                        $email
                );

                $user = array();

                if (!empty($return)) {
                    $user = $objectManager
                        ->getRepository('Application\Entity\User')
                        ->find($return['user_id']);
                }

                if ($user instanceof User) {
                    $this->getResponse()->setStatusCode(
                        Error::DuplicateEmail_code
                    );
                    return new JsonModel(
                        array(
                            'error' => Error::DuplicateEmail_message
                        )
                    );
                }

                $data = array(
                    'email'  => $register->getEmail(),
                    'name'   => $register->getName(),
                    'gender' => (int)$register->getGender()
                );

                $user = new User();
                $form = $builder->createForm($user);
                $form->setHydrator($hydrator);
                $form->bind($user);
                $form->setData($data);

                if (!$form->isValid()) {
                    $this->getResponse()->setStatusCode(
                        Error::FormInvalid_code
                    );
                    return new JsonModel(array(
                        'error' => $form->getMessages()
                        )
                    );
                }

                $name = $register->getName();
                $name = str_ireplace('_', ' ', $name);
                $name = str_ireplace(',', ' ', $name);
                $words = explode(' ', $name);

                $pieces = array();
                foreach ($words as $word) {
                    if ($word != '') {
                        $pieces[] = strtolower($word);
                    }
                }
                $username = join('.', $pieces);

                $_username = $username;

                $return = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->getOneByElastic($serviceLocator, 'username', $_username);

                $similarUser = array();
                if (!empty($return)) {
                    $similarUser = $objectManager
                        ->getRepository('Application\Entity\User')
                        ->find($return['user_id']);
                }

                $index = 0;
                while ($similarUser instanceof User) {
                    $index++;
                    $_username = $username . $index;

                    $similarUser = $objectManager
                        ->getRepository('Application\Entity\User')
                        ->getOneByElastic($serviceLocator, 'username', $_username);

                    if (!empty($similarUser)) {
                        $similarUser = $objectManager
                            ->getRepository('Application\Entity\User')
                            ->find($similarUser['user_id']);
                    }
                 }

                $username = $_username;

                $bcrypt   = new Bcrypt();
                $length   = rand(6,8);
                $bytes    = openssl_random_pseudo_bytes($length, $cstrong);
                $password = bin2hex($bytes);
                $cryPass  = $bcrypt->create($password);

                $length   = rand(15,20);
                $bytes    = openssl_random_pseudo_bytes($length, $cstrong);
                $hashKey  = bin2hex($bytes);
                $now      = new \DateTime('now', new \DateTimeZone('UTC'));

                $user->setUsername($username);
                $user->setPassword($cryPass);
                $user->setStatus(1);
                $user->setDeleted(0);
                $user->setCreatedOn($now);
                $user->setModifiedOn($now);

                $objectManager->persist($user);
                $register->setUserId($user->getId());

                $transaction = new Transaction();
                $transaction->setUserId($user->getId());
                $transaction->setDebit(100);
                $transaction->setStatus(Transaction::STATUS_DONE);
                $transaction->setCreatedOn($now);
                $objectManager->persist($transaction);

                $userPref = new UserPref();
                $userPref->setUserId($user->getId());
                $userPref->setKey('credit');
                $userPref->setValue(100);
                $objectManager->persist($userPref);

                $UserProfile = new UserProfile();
                $UserProfile->setUserId($user->getId());
                $UserProfile->setTimezone($register->getTimezone());
                $UserProfile->setOccupation(null);
                $UserProfile->setSchool(null);
                $UserProfile->setLocation(null);
                $UserProfile->setDeleted(0);
                $UserProfile->setCreatedOn($now);
                $UserProfile->setModifiedBy(null);
                $UserProfile->setModifiedOn(null);
                $objectManager->persist($UserProfile);

                $defaultListPref = $objectManager
                    ->getRepository('Application\Entity\UserPref')
                    ->find(
                        array(
                            'userId' => $user->getId(),
                            'key'    => 'defaultBookmarkListId'
                        )
                );

                if (!$defaultListPref instanceof UserPref) {
                    $defaultListPref = new UserPref();
                    $defaultListPref->setUserId($user->getId());
                    $defaultListPref->setKey('defaultBookmarkListId');

                    $objectManager->persist($defaultListPref);
                }

                $keywords = $register->getKeywords();

                if(!empty($keywords)) {
                    $keys = explode(',', $keywords);
                    $keys = array_unique($keys);
                    foreach ($keys as $key) {
                        $objectManager
                            ->getConnection()
                            ->getShardManager()
                            ->selectGlobal();
                        $keyword = $objectManager
                            ->getRepository('Qolve\Entity\Keyword')
                            ->findOneByName($key);

                        if (!$keyword instanceof Keyword) {

                            $keyword = new Keyword();
                            $keyword->setName($key);
                            $objectManager->persist($keyword);
                        }

                        $keyFollow = new KeywordFollow();
                        $keyFollow->setKeywordId($keyword->getId());
                        $keyFollow->setUserId($user->getId());
                        $keyFollow->setLevels('General');
                        $objectManager->persist($keyFollow);

                    }
                }

                $password = new Password();
                $password->setRequestKey($hashKey);
                $password->setUserId($user->getId());
                $password->setRequestTime($now);
                $objectManager->persist($password);
                $objectManager->flush();

                $createdOn = $user->getCreatedOn()->format('Y-m-d H:i:s');
                $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));

                //Add user to Elastic
                $elastica = $this->getServiceLocator()
                    ->get('Elastica\Client');
                $elasticaClient = $elastica['client'];
                $index          = $elastica['index'];

                $item = array (
                    'user_id'    => $user->getId(),
                    'name'       => $user->getName(),
                    'username'   => $user->getUsername(),
                    'email'      => $user->getEmail(),
                    'status'     => $user->getStatus(),
                    'deleted'    => $user->getDeleted(),
                    'created_on' => $createdOn
                );
                $elasticaIndex = $elasticaClient->getIndex($index);
                $elasticaType  = $elasticaIndex->getType('user');
                $content       = new \Elastica\Document($user->getId(), $item);

                $elasticaType->addDocument($content);
                $elasticaType->getIndex()->refresh();

                $mail = new \Application\Job\SendEmail($serviceLocator);
                $mail->setContent(array(
                    'to'        => $email,
                    'subject'   => $user->getName() . ", You're chosen to help us and win the prize!",
                    'template'  => 'Register',
                    'params'    => array(
                    'resetpath' => $config['email']['address'].$hashKey,
                    'name'      => $user->getName(),
                    'username'  => $user->getUsername()
                    )
                ));
                $mail->execute();

                $users[] = $hydrator->extract($user);
            }
            return new JsonModel(array("users" => $users));
        }
    }
}
