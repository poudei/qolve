<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Error,
    Qolve\Entity\Question,
    Qolve\Entity\Answer,
    Qolve\Entity\Report,
    Zend\Form\Annotation\AnnotationBuilder;

class ReportRestController extends AbstractRestfulController
{
    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $user = $this->identity();

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        if (!empty($answerId)) {
            $answer = $objectManager
                ->getRepository('\Qolve\Entity\Answer')
                ->find($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        $report = $objectManager
            ->getRepository('Qolve\Entity\Report')
            ->getUserReport($user->getId(), $questionId, $answerId);

        if (!$report instanceof Report) {
            $builder  = new AnnotationBuilder();
            $report   = new Report();
            $form     = $builder->createForm($report);
            $form->setHydrator($hydrator);
            $form->bind($report);
            $form->setData($data);

            if (!$form->isValid()) {
                $this->getResponse()->setStatusCode(
                    Error::FormInvalid_code
                );
                return new JsonModel(
                    array(
                        'error' => $form->getMessages()
                    )
                );
            }

            $now = new \DateTime('now', new \DateTimeZone('UTC'));

            $report = $form->getData();
            $report->setReporterId($user->getId());
            $report->setQuestionId($questionId);
            $report->setAnswerId($answerId);
            $report->setCreatedOn($now);
            $objectManager->persist($report);

            $objectManager->flush();

            // Update Question Activity in Elastic

            if (!empty($answerId)) {
                $reports = $objectManager
                    ->getRepository('Qolve\Entity\Report')
                    ->getReports($questionId, $answerId);
                foreach($reports as $rep) {
                    $params['answer_report'][] = array(
                        'reporter' => $rep['reporterId'],
                        'answerId' => $rep['answerId']
                    );
                }
            } else {
                $reports = $objectManager
                    ->getRepository('Qolve\Entity\Report')
                    ->getReports($questionId);
                foreach($reports as $rep) {
                    $params['question_report'][] = $rep['reporterId'];
                }
            }

            $params['question_id'] = $questionId;

            $objectManager
                ->getRepository('Qolve\Entity\Question')
                ->setQuestionActivity(
                    $this->getServiceLocator(),
                    $params
            );

        }

        return new JsonModel(
            array(
                'report' => $hydrator->extract($report, array(), $user)
            )
        );
    }
}
