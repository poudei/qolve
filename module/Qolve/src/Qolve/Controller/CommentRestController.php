<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\Answer,
    Qolve\Entity\Comment,
    Application\Entity\Feed,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

class CommentRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $limit  = $this->params()->fromQuery('limit', 5);
        $offset = $this->params()->fromQuery('offset', 0);
        
        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }
        
        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');
        
        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        $comments = $objectManager->getRepository('Qolve\Entity\Comment')
            ->getComments($questionId, $answerId, $count, $offset, $limit);

        $_comments = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($comments as $comment) {
            $_comments['list'][] = $hydrator
                ->extract($comment, array(), $this->identity());
        }

        return new JsonModel(
            array(
                'comments' => $_comments
            )
        );
    }

    public function get($id)
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }
        
        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');
        
        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }
        }

        $comment = $objectManager->getRepository('Qolve\Entity\Comment')
            ->findOneBy(
                array(
                    'id'            => $id,
                    'questionId'    => $questionId,
                    'answerId'      => $answerId
                )
            );
        if (!$comment instanceof Comment) {
            $this->getResponse()->setStatusCode(
                Error::CommentNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::CommentNotFound_message
                )
            );
        }

        $_comment = $hydrator->extract($comment, array(), $this->identity());

        return new JsonModel(
            array(
                'comment' => $_comment
            )
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }
        
        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }

            $_answer = $hydrator->extract($answer, array(), $this->identity());
            if ($_answer['invisible'] == 1) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $builder = new AnnotationBuilder();
        $comment = new Comment();
        $form    = $builder->createForm($comment);
        $form->setHydrator($hydrator);
        $form->bind($comment);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(
                array(
                    'error' => $form->getMessages()
                )
            );
        }

        $comment = $form->getData();
        $user    = $this->identity();
        $now     = new \DateTime('now', new \DateTimeZone('UTC'));

        $comment->setQuestionId($questionId);
        $comment->setAnswerId($answerId);
        $comment->setUserId($user->getId());
        $comment->setWrittenOn($now);

        $objectManager->persist($comment);
        $commentsCount = $objectManager->getRepository('Qolve\Entity\Comment')
            ->getCommentsCount($questionId, $answerId);

        if (isset($answerId)) {
            $answer->setComments($commentsCount + 1);
        } else {
            $question->setComments($commentsCount + 1);
        }

        //Add to user's Activities
        if (!isset($answer)) {
            $feedPrivacy = $question->getPrivacy();
        } else {
            $feedPrivacy = max(
                array(
                    $question->getPrivacy(), $answer->getPrivacy()
                )
            );
        }
        
        $feed = new Feed();
        $feed->setUserId($user->getId());
        $feed->setActorId($user->getId());
        $feed->setQuestionId($questionId);
        $feed->setAnswerId($answerId);
        $feed->setPrivacy($feedPrivacy);
        $feed->setVerb('WriteComment');
        $feed->setContent($comment->getId());
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);

        $objectManager->flush();

        //Add to Queue for send Notification
        $queue = $this->serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'      => '\Qolve\Verb\WriteComment',
            'commentId' => $comment->getId()
        );

        if ($answerId == null) {
            $data['to'] = array('askers');
        } else {
            $data['to'] = array('answerWriter', 'askers');
        }

        $job = new \Application\Job\SendNotification($this->serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        return new JsonModel(
            array(
                'comment' => $hydrator->extract($comment, array(), $user)
            )
        );
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }
        
        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');

        if ($answerId) {
            $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($answerId);
            if (!$answer instanceof Answer) {
                $this->getResponse()->setStatusCode(
                    Error::AnswerNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AnswerNotFound_message
                    )
                );
            }

            $_answer = $hydrator->extract($answer, array(), $this->identity());
            if ($_answer['invisible'] == 1) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $comment = $objectManager->getRepository('Qolve\Entity\Comment')
            ->findOneBy(
                array(
                    'id'         => $id,
                    'questionId' => $questionId,
                    'answerId'   => $answerId
                )
            );
        if (!$comment instanceof Comment) {
            $this->getResponse()->setStatusCode(
                Error::CommentNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::CommentNotFound_message
                )
            );
        }

        $user = $this->identity();

        similar_text($user->getId(), $comment->getUserId(), $cWriterSim);
        if ($cWriterSim < 100) {
            
            if ($answerId) {
                similar_text(
                    $user->getId(), $answer->getUserId(), $aWriterSim
                );
                if ($aWriterSim < 100) {
                    $this->getResponse()->setStatusCode(
                        Error::PermissionDenied_code
                    );
                    return new JsonModel(
                        array(
                            'error' => Error::PermissionDenied_message
                        )
                    );
                }
            } else {
                $asker = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findOneBy(
                        array(
                            'questionId' => $question->getId(),
                            'userId'     => $user->getId(),
                            'status'     => \Qolve\Entity\Asker::STATUS_ASKER
                        )
                    );
                if (!$asker instanceof \Qolve\Entity\Asker) {
                    $this->getResponse()->setStatusCode(
                        Error::PermissionDenied_code
                    );
                    return new JsonModel(
                        array(
                            'error' => Error::PermissionDenied_message
                        )
                    );
                }
            }
        }

        $commentsCount = $objectManager->getRepository('Qolve\Entity\Comment')
            ->getCommentsCount($questionId, $answerId);

        if (isset($answerId)) {
            $answer->setComments($commentsCount - 1);
        } else {
            $question->setComments($commentsCount - 1);
        }

        $objectManager->remove($comment);
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'verb'    => 'WriteComment',
                    'content' => $comment->getId()
                )
            );
        
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'verb'    => 'WriteComment',
                    'content' => $comment->getId()
                )
            );
        
        $objectManager->flush();

        return new JsonModel(
            array(
                'success'
            )
        );
    }
}
