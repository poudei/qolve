<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Qolve\Entity\Question,
    Qolve\Entity\QuestionFollow,
    Application\Entity\Feed,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder,
    Application\Service\Gearman;

class QuestionFollowRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question   = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);

        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $limit    = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 5)
        );
        $offset   = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
        );

        $currentUser = $this->identity();

        $followers = $objectManager
            ->getRepository('Qolve\Entity\QuestionFollow')
            ->getQuestionFollowers($questionId, $count, $offset, $limit);

        $_followers = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($followers as $follower) {
            $_followers['list'][] = $hydrator->extract(
                $follower,
                array(),
                $currentUser
            );
        }

        return new JsonModel(array('followers' => $_followers));
    }

    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()->getParam('question_id');
        $question   = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $user = $this->identity();

        $asker = $objectManager
            ->getRepository('Qolve\Entity\Asker')
            ->findOneBy(array(
                'questionId' => $question->getId(),
                'userId'     => $user->getId()
            )
        );
        if ($asker instanceof \Qolve\Entity\Asker) {
            $this->getResponse()->setStatusCode(
                Error::UserIsAsker_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserIsAsker_message
                )
            );
        }

        $qFollow = $objectManager
            ->getRepository('Qolve\Entity\QuestionFollow')
            ->follow($user->getId(), $question->getId());

        //Add to user's Activities
        $now  = new \DateTime('now', new \DateTimeZone('UTC'));
        $feed = new Feed();
        $feed->setUserId($user->getId());
        $feed->setActorId($user->getId());
        $feed->setQuestionId($question->getId());
        $feed->setPrivacy($question->getPrivacy());
        $feed->setVerb('FollowQuestion');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);
        $objectManager->flush();

        //Add to Queue for send Notification
        $queue = $this->serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'       => '\Qolve\Verb\FollowQuestion',
            'userId'     => $qFollow->getUserId(),
            'questionId' => $qFollow->getQuestionId()
        );

        $data['to'] = 'askers';
        $job = new \Application\Job\SendNotification($this->serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        return new JsonModel(array(
            'questionFollow' => $hydrator->extract($qFollow))
        );
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question   = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($questionId);

        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $user = $this->identity();

        $objectManager
            ->getRepository('Qolve\Entity\QuestionFollow')
            ->unfollow($user->getId(), $question->getId());
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'verb'       => 'FollowQuestion',
                    'questionId' => $question->getId(),
                    'actorId'    => $user->getId()
                )
            );
        
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'verb'       => 'FollowQuestion',
                    'questionId' => $question->getId(),
                    'fromId'     => $user->getId()
                )
            );

        return new JsonModel(array('Success'));
    }
}
