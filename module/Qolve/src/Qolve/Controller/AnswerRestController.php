<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Feed,
    Qolve\Entity\Question,
    Qolve\Entity\Asker,
    Qolve\Entity\Answer,
    Qolve\Entity\Report,
    Qolve\Entity\Document,
    Application\Job\SendNotification,
    Application\Job\PublishFeed,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

class AnswerRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $limit      = $this->params()->fromQuery('limit', 10);
        $offset     = $this->params()->fromQuery('offset', 0);
        $byComment  = $this->params()->fromQuery('comment', false);
        $byDocument = $this->params()->fromQuery('document', true);

        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);

        $info = array(
            'isVoted'
        );

        if ($byComment) {
            $info[] = 'commentsList';
        }

        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $user = $this->identity();

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                )
            );
        }

        $answers = $objectManager->getRepository('Qolve\Entity\Answer')
            ->getAnswers($questionId, $offset, $limit);

        $_answers = array(
            'list'  => array(),
            'count' => $question->getAnswers()
        );
        foreach ($answers as $answer) {
            if ($answer instanceof Answer) {
                $_answers['list'][] = $hydrator->extract($answer, $info, $user);
            }
        }

        return new JsonModel(
            array(
                'answers' => $_answers
            )
        );
    }

    public function get($id)
    {
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $byComment  = $this->params()->fromQuery('comment', true);
        $byDocument = $this->params()->fromQuery('document', true);

        $info = array(
            'isVoted'
        );

        if ($byComment) {
            $info[] = 'commentsList';
        }

        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $user = $this->identity();

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                )
            );
        }

        $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
            ->find($id);
        if (!$answer instanceof Answer) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AnswerNotFound_code,
				 'message' => Error::AnswerNotFound_message 
				 ]
                )
            );
        }

        $_answer = $hydrator->extract($answer, $info, $user);

        return new JsonModel(
            array(
                'answer' => $_answer
            )
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(401);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AuthenticationFailed_code,
				 'message' => Error::AuthenticationFailed_message 
				 ]
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserUnavailable_code,
				 'message' => Error::UserUnavailable_message 
				 ]
                )
            );
        }

        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        if (isset($data['files'])) {
            $files = $data['files'];
            foreach ($files as $file) {
                if (!file_exists('public/'.$file['location'])) {
                    $this->getResponse()->setStatusCode(404);

                    return new JsonModel(
                        array(
                            'errors' => [
				 'code'    => Error::FileNotFound_code,
				 'message' => Error::FileNotFound_message 
				 ]
                        )
                    );
                }
            }
        }

        $mentions = array();
        if (isset($data['description'])) {

            preg_match_all('/(?<=\@)(.+?)((?=\s+)|$)/',
                $data['description'], $mentions
            );
            $mentions = array_map('strtolower', $mentions[1]);
        }

        $user = $this->identity();

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                )
            );
        }

        $asker = $objectManager->getRepository('Qolve\Entity\Asker')
            ->findOneBy(
                array(
                    'questionId' => $question->getId(),
                    'userId'     => $user->getId()
                )
            );
 
        if ($asker instanceof Asker) {
            $this->getResponse()->setStatusCode(401);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::PermissionDenied_code,
				 'message' => Error::PermissionDenied_message 
				 ]
                )
            );
        }

        $answer = $objectManager->getRepository('Qolve\Entity\Answer')
            ->findOneBy(
                array(
                    'questionId' => $question->getId(),
                    'userId'     => $user->getId()
                )
            );
        if ($answer instanceof Answer) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserHasAnsweredThisQuestion_code,
				 'message' => Error::UserHasAnsweredThisQuestion_message 
				 ]
                )
            );
        }
        
        if ($question->getPrivacy() == Question::PRIVACY_SHARED) {
            $data['privacy'] = Answer::PRIVACY_PUBLIC;
        } else {
            if ($question->getCredits() > 0) {
                $data['privacy'] = $question->getAnswerPrivilege();
            } else {
                $data['privacy'] = isset($data['privacy'])
                    ? $data['privacy'] : Answer::PRIVACY_PRIVATE;
            }
        }

        $builder = new AnnotationBuilder();
        $answer  = new Answer();
        $form    = $builder->createForm($answer);
        $form->setHydrator($hydrator);
        $form->bind($answer);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]
                )
            );
        }

        //Create one record in Answer table
        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        $answer = $form->getData();
        $answer->setQuestionId($questionId);
        $answer->setUserId($user->getId());
        $answer->setStatus(Answer::STATUS_NORMAL);
        $answer->setVotes(0);
        $answer->setComments(0);
        $answer->setVoters(0);
        $answer->setDeleted(false);
        $answer->setCreatedOn($now);

        if (!$answer->getVisibility()) {
            $answer->setVisibility(Answer::VISIBILITY_PUBLIC);
        }
        $objectManager->persist($answer);

        //Update question record in Question table
        $count = $objectManager
            ->getRepository('Qolve\Entity\Answer')
            ->answersCount($questionId);
        $count += $answer->getId() ? 1 : 0;
        $question->setAnswers($count);

        //Create one record for user in follow table, if there is not
        $objectManager
            ->getRepository('Qolve\Entity\QuestionFollow')
            ->follow($user->getId(), $question->getId());

        //Add to user's Activities (and timeline)
        $feedPrivacy = max(
            array(
                $question->getPrivacy(), 
                $answer->getPrivacy(),
                $answer->getVisibility()
            )
        );

        $feed = new Feed();
        $feed->setUserId($user->getId());
        $feed->setActorId($user->getId());
        $feed->setQuestionId($question->getId());
        $feed->setAnswerId($answer->getId());
        $feed->setPrivacy($feedPrivacy);
        $feed->setVerb('CreateAnswer');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);
        $objectManager->flush();

//        $params = array( 
//            'question_id' => $question->getId(),
//            'answer'
//        );
//        $objectManager
//            ->getRepository('Qolve\Entity\Question')
//            ->setQuestionActivity(
//                $this->getServiceLocator(),
//                $params
//        );

        //Add to Queue for publish Feeds
        if (   $question->getPrivacy() == Question::PRIVACY_PUBLIC
            && $answer->getPrivacy() == Answer::PRIVACY_PUBLIC
            && $answer->getVisibility() == Answer::VISIBILITY_PUBLIC
        ) {
            $feedsQueue = $this->serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('feeds');

            $data = array(
                'verb'       => '\Qolve\Verb\CreateAnswer',
                'questionId' => $questionId,
                'answerId'   => $answer->getId()
            );

            $data['to'] = 'userFollowers';
            $job = new PublishFeed($this->serviceLocator);
            $job->setContent($data);
            $feedsQueue->push($job);
        }

        //Add to Queue for send Notification
        $queue = $this->serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'     => '\Qolve\Verb\CreateAnswer',
            'answerId' => $answer->getId()
        );

        $data['to'] = 'askers'; //high
        $job = new SendNotification($this->getServiceLocator());
        $job->setContent($data);
        $queue->push($job);

        $mentionsId = array();
        foreach($mentions as $mention) {

            $return = $objectManager
                ->getRepository('Application\Entity\User')
                ->getOneByElastic($serviceLocator, 'username', $mention);

            $mention = array();
            if (!empty($return)) {
                $mention = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($return['user_id']);
            }

            if (!$mention instanceof User) {
                continue;
            }

            $mentionsId[] = $mention->getId();
        }
        if (!empty($mentionsId)) {

            $data['to']       = 'mentions';
            $data['mentions'] = $mentionsId;

            $job = new SendNotification($this->getServiceLocator());
            $job->setContent($data);
            $queue->push($job);
        }
        
        $data['to'] = 'questionFollowers';
        $job = new SendNotification($this->getServiceLocator());
        $job->setContent($data);
        $queue->push($job);

        $userId     = $user->getId();
        $questionId = $question->getId();
        $answerId   = $answer->getId();

        // upload file for web-browser
        $userPath     = "users/$userId";
        $questionPath = "questions/$questionId";
        $answerPath   = "answers/$answerId";
        $partialPath  = "upfiles/$userPath/$questionPath/$answerPath";

        if (!empty($files)) {
            foreach ($files as $file) {
                $fileName = $file['name'];
                $tempPath = $file['location'];
                $location = "/$partialPath/$fileName";

                if (!file_exists("./public/$partialPath/")) {
                    mkdir("./public/$partialPath/", 0777, true);
                }

                $destination = copy(
                    "./public/$tempPath", "./public/$partialPath/$fileName"
                );
                if (!$destination) {
                    unlink("./public/$tempPath");
                    system(
                        'rm -rf ' . escapeshellarg(
                            "./public/upfiles/tempo/$userId"
                        ),
                        $retval
                    );
                    $this->getResponse()->setStatusCode(500);
                    return new JsonModel(
                        array(
                            'errors' => [
				 'code'    => Error::FileTransferingProblem_code,
				 'message' => Error::FileTransferingProblem_message 
				 ]
                        )
                    );
                }

                $type = substr($file['type'], 0, 5);
                switch ($type) {
                case 'image':
                    $type = Document::TYPE_IMAGE;
                    break;

                case 'video':
                    $type = Document::TYPE_VIDEO;
                    break;
                }

                $data = array(
                    'order'    => $file['order'],
                    'type'     => $type,
                    'location' => $location
                );
                list($width, $height) =
                    getimagesize(PUBLIC_PATH.$location);

                $document = new Document();
                $form     = $builder->createForm($document);
                $form->setHydrator($hydrator);
                $form->bind($document);
                $form->setData($data);

                if (!$form->isValid()) {
                    $this->getResponse()->setStatusCode(400);

                    return new JsonModel(
                        array(
                            'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]
                        )
                    );
                }

                $document = $form->getData();
                $document->setQuestionId($question->getId());
                $document->setAnswerId($answerId);
                $document->setWidth((int)$width);
                $document->setHeight((int)$height);
                $objectManager->persist($document);
                $objectManager->flush($document);
                unlink("./public".$tempPath);
            }
            system(
                'rm -rf ' . escapeshellarg(
                    "./public/upfiles/tempo/$userId"
                ),
                $retval
            );
        }

        $info = array(
            'documentsList',
            'commentsList'
        );

        return new JsonModel(
            array(
                'answer' => $hydrator->extract($answer, $info, $user)
            )
        );
    }

    public function update($id, $data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(401);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AuthenticationFailed_code,
				 'message' => Error::AuthenticationFailed_message 
				 ]
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserUnavailable_code,
				 'message' => Error::UserUnavailable_message 
				 ]
                )
            );
        }

        $info = array(
            'documentsList',
            'commentsList'
        );
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(404);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                )
            );
        }

        $answer = $objectManager->getRepository('Qolve\Entity\Answer')
            ->findOneById($id);
        if (!$answer instanceof Answer) {
            $this->getResponse()->setStatusCode(404);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AnswerNotFound_code,
				 'message' => Error::AnswerNotFound_message 
				 ]
                )
            );
        }

        $user   = $this->identity();
        $userId = $user->getId();
        similar_text($answer->getUserId(), $userId, $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(401);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::PermissionDenied_code,
				 'message' => Error::PermissionDenied_message 
				 ]
                )
            );
        }

        if ($answer->getStatus() != Answer::STATUS_NORMAL) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AnswerIsNotNormal_code,
				 'message' => Error::AnswerIsNotNormal_message 
				 ]
                )
            );
        }

        $editableProperties = array(
            'files',
            'description',
            'privacy',
            'visibility'
        );
        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (empty($data)) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::EmptyData_code,
				 'message' => Error::EmptyData_message 
				 ]
                )
            );
        }
        
        if (isset($data['privacy']) 
            && $data['privacy'] != $answer->getPrivacy()
        ) {
            $privacyIsChanged = true;
        } else {
            $privacyIsChanged = false;
        }
        
        if (isset($data['visibility']) 
            && $data['visibility'] != $answer->getVisibility()
        ) {
            $visibilityIsChanged = true;
        } else {
            $visibilityIsChanged = false;
        }

        $mentions = array();
        if (isset($data['description'])) {

            preg_match_all('/(?<=\@)(.+?)((?=\s+)|$)/',
                $data['description'], $mentions
            );
            preg_match_all('/(?<=\@)(.+?)((?=\s+)|$)/',
                $answer->getDescription(), $preMentions
            );
            
            $preMentions = array_map('strtolower', $preMentions[1]);
            $mentions    = array_map('strtolower', $mentions[1]);
        }
        
        $preDocs =  $objectManager
            ->getRepository('\Qolve\Entity\Document')
            ->findBy(array(
                'questionId' => $questionId,
                'answerId'   => $id
        ));
        if (isset($data['files'][0])) {
            if (!empty($preDocs)) {
                foreach ($preDocs as $preDoc) {
                    $preDocsId[] = $preDoc->getId();
                }

                foreach ($data['files'] as $file) {
                    !empty($file['id']) ? $docsId[] = $file['id'] : '';
                }

                if (!empty($docsId)) {
                    $sameDocs   = array_intersect($docsId, $preDocsId);
                    $deleteDocs = array_diff($preDocsId, $sameDocs);

                    foreach ($deleteDocs as $deleteId) {
                        $preDoc = $objectManager
                            ->getRepository('\Qolve\Entity\Document')
                            ->find($deleteId);
                        if ($preDoc instanceof Document) {
                            unlink("./public".$preDoc->getLocation());
                            $objectManager->remove($preDoc);
                        }
                    }
                } else {
                    foreach ($preDocs as $preDoc) {
                        unlink("./public".$preDoc->getLocation());
                        $objectManager->remove($preDoc);
                    }
                }
            }

            $userPath     = "users/$userId";
            $questionPath = "questions/$questionId";
            $answerPath   = "answers/$id";
            $partialPath  = "upfiles/$userPath/$questionPath/$answerPath";

            foreach ($data['files'] as $file) {
                if (empty($file['id'])) {
                    $fileName = $file['name'];
                    $tempPath = $file['location'];

                    $location = "/$partialPath/$fileName";

                    if (!file_exists("./public/$partialPath/")) {
                        mkdir("./public/$partialPath/", 0777, true);
                    }
                    $destination = copy(
                        "./public/$tempPath",
                        "./public/$partialPath/$fileName"
                    );

                    if (!$destination) {
                        unlink("./public/$tempPath");
                        system(
                            'rm -rf ' . escapeshellarg(
                                "./public/upfiles/tempo/$userId"
                            ),
                            $retval
                        );

                        $this->getResponse()->setStatusCode(500);
                        return new JsonModel(
                            [
                                'errors' => [
                    				'code'    => Error::FileTransferingProblem_code,
                    				'message' => Error::FileTransferingProblem_message 
                    		    ]
                            ]
                        );
                    }

                    $type = substr($file['type'], 0, 5);
                    switch ($type) {
                    case 'image':
                        $type = Document::TYPE_IMAGE;
                        break;

                    case 'video':
                        $type = Document::TYPE_VIDEO;
                        break;
                    }

                    $docData = array(
                        'order'    => $file['order'],
                        'type'     => $type,
                        'location' => $location
                    );
                    list($width, $height) =
                        getimagesize(PUBLIC_PATH.$location);
                    $document = new Document();
                    $form     = $builder->createForm($document);
                    $form->setHydrator($hydrator);
                    $form->bind($document);
                    $form->setData($docData);

                    if (!$form->isValid()) {
                        $this->getResponse()->setStatusCode(400);
                        return new JsonModel(
                            [
                                'errors' => [
                    				'code'    => Error::FormInvalid_code,
                    				'message' => $form->getMessages() 
                    		    ]
                            ]
                        );
                    }

                    $document = $form->getData();
                    $document->setQuestionId($question->getId());
                    $document->setAnswerId($id);
                    $document->setWidth((int)$width);
                    $document->setHeight((int)$height);
                    $objectManager->persist($document);
                    $objectManager->flush($document);
                    unlink("./public".$tempPath);

                } else {
                    $document = $objectManager
                        ->getRepository('\Qolve\Entity\Document')
                        ->find($file['id']);

                    if ($file['order'] != $document->getOrder()) {
                        $newOrder = array('order' => $file['order']);
                        $preData  = $hydrator->extract($document);
                        $newDoc   = array_merge($preData, $newOrder);
                        $builder  = new AnnotationBuilder();
                        $form     = $builder->createForm($document);
                        $form->setHydrator($hydrator);
                        $form->setBindOnValidate(false);
                        $form->bind($document);
                        $form->setData($newDoc);

                        if (!$form->isValid()) {
                            $this->getResponse()->setStatusCode(400);
                            return new JsonModel(
                                array(
                                    'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]
                                )
                            );
                        }

                        $form->bindValues();
                    }
                }
            }
        } else {
            if(isset($preDocs)) {
                foreach ($preDocs as $preDoc) {
                    $objectManager->remove($preDoc);
                }
                system('rm -rf ' .
                    escapeshellarg(
                        "./public/upfiles/users/$userId/questions/$questionId/answers/"
                    ),
                    $retval
                );
            }
        }

        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        if (   isset($data['description'])
            || isset($data['privacy'])
            || isset($data['visibility'])
        ) {
            isset($data['description'])
                ? $newData['description'] =  $data['description'] : '';
            isset($data['privacy'])
                ? $newData['privacy']     =  $data['privacy'] : '';
            isset($data['visibility'])
                ? $newData['visibility']  =  $data['visibility'] : '';

            $preData  = $hydrator->extract($answer);
            $data     = array_merge($preData, $newData);
            $builder  = new AnnotationBuilder();
            $form     = $builder->createForm($answer);

            $form->setHydrator($hydrator);
            $form->setBindOnValidate(false);
            $form->bind($answer);
            $form->setData($newData);

            if (!$form->isValid()) {
                $this->getResponse()->setStatusCode(400);

                return new JsonModel(
                    array(
                        'errors' => [
				 'code'    => Error::FormInvalid_code,
				 'message' => $form->getMessages() 
				 ]
                    )
                );
            }

            $form->bindValues();

            $answer->setModifiedOn($now);
        }
        
        //delete previous feeds and notifications
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'fromId'     => $answer->getUserId(),
                    'questionId' => $answer->getQuestionId(),
                    'answerId'   => $answer->getId(),
                    'verb'       => 'UpdateAnswer'
                )
            );
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'actorId'    => $answer->getUserId(),
                    'questionId' => $answer->getQuestionId(),
                    'answerId'   => $answer->getId(),
                    'verb'       => 'UpdateAnswer'
                )
            );

        $objectManager->flush();
        
        //Add to user's Activities
        $feedPrivacy = max(
            array(
                $question->getPrivacy(), 
                $answer->getPrivacy(),
                $answer->getVisibility()
            )
        );

        $feed = new Feed();
        $feed->setUserId($user->getId());
        $feed->setActorId($user->getId());
        $feed->setQuestionId($question->getId());
        $feed->setAnswerId($answer->getId());
        $feed->setPrivacy($feedPrivacy);
        $feed->setVerb('UpdateAnswer');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);
        $objectManager->flush();

        //Add to Queue for publish Feeds
        if (   $question->getPrivacy() == Question::PRIVACY_PUBLIC
            && $answer->getPrivacy() == Answer::PRIVACY_PUBLIC
        ) {
            $feedsQueue = $this->serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('feeds');

            $data = array(
                'verb'       => '\Qolve\Verb\UpdateAnswer',
                'questionId' => $question->getId(),
                'answerId'   => $answer->getId()
            );

            $data['to'] = 'userFollowers';
            $job = new PublishFeed($this->serviceLocator);
            $job->setContent($data);
            $feedsQueue->push($job);
        }

        //Add to Queue for send Notification
        $queue = $this->serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'     => '\Qolve\Verb\UpdateAnswer',
            'answerId' => $answer->getId()
        );

        $data['to'] = 'askers'; //high
        $job = new SendNotification($this->getServiceLocator());
        $job->setContent($data);
        $queue->push($job);

        if (!empty($mentions)) {
            sort($preMentions);
            sort($mentions);

            $sameMentions = array_intersect($mentions, $preMentions);
            $mentions     = array_diff($mentions, $sameMentions);
        }
        $mentionsId = array();
        foreach($mentions as $mention) {

            $return = $objectManager
                ->getRepository('Application\Entity\User')
                ->getOneByElastic($serviceLocator, 'username', $mention);

            $mention = array();
            if (!empty($return)) {
                $mention = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($return['user_id']);
            }

            if (!$mention instanceof User) {
                continue;
            }

            $mentionsId[] = $mention->getId();
        }
        if (!empty($mentionsId)) {

            $data['to']       = 'mentions';
            $data['mentions'] = $mentionsId;

            $job = new SendNotification($this->getServiceLocator());
            $job->setContent($data);
            $queue->push($job);
        }
        
        $data['to'] = 'questionFollowers';
        $job = new SendNotification($this->getServiceLocator());
        $job->setContent($data);
        $queue->push($job);
        
        //update feed privacy
        if ($privacyIsChanged || $visibilityIsChanged) {
            //for CreateAnswer, UpdateAnswer
            $feedPrivacy = max(array(
                $question->getPrivacy(),
                $answer->getPrivacy(),
                $answer->getVisibility()
            ));
            $objectManager->getRepository('Application\Entity\Feed')
                ->updatePrivacy(array(
                    'actorId'    => $answer->getUserId(),
                    'questionId' => $question->getId(),
                    'answerId'   => $answer->getId(),
                    'verb'       => 'CreateAnswer'
                ), $feedPrivacy);
            $objectManager->getRepository('Application\Entity\Feed')
                ->updatePrivacy(array(
                    'actorId'    => $answer->getUserId(),
                    'questionId' => $question->getId(),
                    'answerId'   => $answer->getId(),
                    'verb'       => 'UpdateAnswer'
                ), $feedPrivacy);
        }
        
        if ($privacyIsChanged) {
            //for Vote(answer)
            $feedPrivacy = max(array(
                $question->getPrivacy(),
                $answer->getPrivacy()
            ));
            $objectManager->getRepository('Application\Entity\Feed')
                ->updatePrivacy(array(
                    'questionId' => $question->getId(),
                    'answerId'   => $answer->getId(),
                    'verb'       => 'Vote'
                ), $feedPrivacy);
        }

        return new JsonModel(
            array(
                'answer' => $hydrator->extract($answer, $info, $user)
            )
        );
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'error' => "Authentication Failed"
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::UserUnavailable_code,
				 'message' => Error::UserUnavailable_message 
				 ]
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $questionId = $this->getEvent()->getRouteMatch()
            ->getParam('question_id');
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::QuestionNotFound_code,
				 'message' => Error::QuestionNotFound_message 
				 ]
                )
            );
        }

        $answer = $objectManager->getRepository('Qolve\Entity\Answer')
            ->findOneById($id);
        if (!$answer instanceof Answer) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AnswerNotFound_code,
				 'message' => Error::AnswerNotFound_message 
				 ]
                )
            );
        }

        $user = $this->identity();

        similar_text($answer->getUserId(), $user->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::PermissionDenied_code,
				 'message' => Error::PermissionDenied_message 
				 ]
                )
            );
        }

        if ($answer->getStatus() != Answer::STATUS_NORMAL) {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(
                array(
                    'errors' => [
				 'code'    => Error::AnswerIsNotNormal_code,
				 'message' => Error::AnswerIsNotNormal_message 
				 ]
                )
            );
        }

        $answerCount = $objectManager->getRepository('Qolve\Entity\Answer')
            ->answersCount($questionId);

        $objectManager->remove($answer);
        $question->setAnswers($answerCount - 1);


        $entitiesToDelete = array(
            'Qolve\Entity\AnswerPrivilege',
            'Qolve\Entity\Comment',
            'Qolve\Entity\Document',
            'Qolve\Entity\Report',
            'Qolve\Entity\Vote',
            'Application\Entity\Feed',
            'Application\Entity\Notification'
        );

        foreach ($entitiesToDelete as $entity) {
            $objectManager->getRepository($entity)
                ->deleteBy(array('answerId' => $answer->getId()));
        }

        $objectManager->flush();

        return new JsonModel(
            array(
                'Success'
            )
        );
    }
}
