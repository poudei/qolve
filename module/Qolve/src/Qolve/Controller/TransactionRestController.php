<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\Error,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Zend\Form\Annotation\AnnotationBuilder;

class TransactionRestController extends AbstractRestfulController
{
    public function getList()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $limit  = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 10)
        );
        $offset = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
        );

        $user   = $this->identity();

        $transactions = $objectManager
            ->getRepository('Qolve\Entity\Transaction')
            ->findBy(array(
                'userId' => $user->getId()
            ), array(
                'createdOn' => 'DESC'
            ), $limit, $offset
        );
        $count = $objectManager
            ->getRepository('Qolve\Entity\Transaction')
            ->countBy(array(
                'userId' => $user->getId()
            )
        );

        $_transactions = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($transactions as $transaction) {
            $_transactions['list'][] = $hydrator->extract($transaction);
        }

        return new JsonModel(array("transactions" => $_transactions));
    }
}
