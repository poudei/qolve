<?php
namespace Qolve\Controller;

use Application\Controller\AbstractController,
    Application\Entity\Error,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Zend\Json\Json as Json,
    Zend\File\Transfer\Adapter\Http,
    Zend\Validator\File\IsImage,
    Zend\Form\Annotation\AnnotationBuilder;

class UploadFileController extends AbstractController
{
    public function uploadAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $user     = $this->identity();
        $userId   = $user->getId();
        $file     = $this->getRequest()->getFiles();

        if (isset($file['post'])) {

            $file   = $file['post'];
            $order  = (int) $this->getRequest()->getQuery('order' , 1);
            if (empty($order)) {
                $this->getResponse()->setStatusCode(
                    Error::OrderNotSent_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::OrderNotSent_message
                    )
                );
            }

            if (empty($file)) {
                $this->getResponse()->setStatusCode(
                    Error::FileNotSent_code
                );
                return new JsonModel(
                    array('error' => Error::FileNotSent_message
                    )
                );
            }

            $validator = new IsImage();
            if (!$validator->isValid($file)) {
                $this->getResponse()->setStatusCode(
                    Error::NotImage_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::NotImage_message
                    )
                );
            }

            if (!file_exists("./public/upfiles/tempo/$userId")) {
                mkdir("./public/upfiles/tempo/$userId", 0777, true);
            }

            $location = "/upfiles/tempo/$userId/";
            $adapter  = new Http();

            $adapter->setDestination(PUBLIC_PATH.$location);

            if (!$adapter->receive($file['name'])) {
                $this->getResponse()->setStatusCode(
                    Error::FileTransferingProblem_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::FileTransferingProblem_message
                    )
                );
            }

            if (preg_match('/\s/', $file['name'])) {
                chmod(PUBLIC_PATH.$location.$file['name'], 0777);
                $newName = preg_replace('/\s/','-',$file['name']);
               rename(
                    PUBLIC_PATH.$location.$file['name'], 
                    PUBLIC_PATH.$location.$newName
                );
                $file['name'] = $newName;
            }
            list($width, $height) =
                getimagesize(PUBLIC_PATH.$location.$file['name']);

            $fileInfo['name']     = $file['name'];
            $fileInfo['location'] = $location.$file['name'];
            $fileInfo['order']    = $order;
            $fileInfo['type']     = $file['type'];
            $fileInfo['width']    = $width;
            $fileInfo['height']   = $height;

            return new JsonModel(array('file' => $fileInfo));

        } elseif (isset($file['profile'])) {

            $file = $file['profile'];
            if (empty($file)) {
                $this->getResponse()->setStatusCode(
                    Error::FileNotSent_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::FileNotSent_message
                    )
                );
            }

            $validator = new IsImage();
            if (!$validator->isValid($file)) {
                $this->getResponse()->setStatusCode(
                    Error::NotImage_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::NotImage_message
                    )
                );
            }

            $location = "/upfiles/users/$userId/profile/";

            if (!file_exists(PUBLIC_PATH . $location)) {
                mkdir(PUBLIC_PATH . $location, 0777, true);
            }

            
            $adapter  = new Http();
            $adapter->setDestination(PUBLIC_PATH.$location);

            if (!$adapter->receive($file['name'])) {
                $this->getResponse()->setStatusCode(
                    Error::FileTransferingProblem_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::FileTransferingProblem_message
                    )
                );
            }

            if (preg_match('/\s/', $file['name'])) {
                chmod(PUBLIC_PATH.$location.$file['name'], 0777);
                $newName = preg_replace('/\s/','-',$file['name']);
               rename(
                    PUBLIC_PATH.$location.$file['name'], 
                    PUBLIC_PATH.$location.$newName
                );
                $file['name'] = $newName;
            }

            $user->setImagePath($location.$file['name']);
            $objectManager->flush();

            return new JsonModel(array('user' => $hydrator->extract($user)));
        }
    }
}
