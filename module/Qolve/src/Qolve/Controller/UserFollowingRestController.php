<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\UserFollow,
    Application\Entity\Feed,
    Application\Entity\User,
    Application\Job\SendNotification,
    Application\Job\PublishFeed,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder,
    Application\Service\Gearman;

class UserFollowingRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $limit  = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 10)
        );
        $offset = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
        );

        $currentUser = $this->identity();

        $userId = $this->getEvent()->getRouteMatch()->getParam('user_id');
        if ($userId != null) {
            $user = $objectManager
                ->getRepository('\Application\Entity\User')
                ->find($userId);
        } else {
            $user = $currentUser;
        }
        if (!$user instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::UserNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserNotFound_message
                )
            );
        }

        $follows = $objectManager
            ->getRepository('Qolve\Entity\UserFollow')
            ->findBy(array(
                'followerId' => $user->getId()
            ), null, $limit, $offset);
        $count   = $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->getUserKey($user->getId(), 'userFollowingsCount');

        $_followings = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($follows as $follow) {
            $following = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($follow->getUserId());
            if ($following instanceof User) {
                $_followings['list'][] = $hydrator->extract(
                    $following,
                    array(),
                    $currentUser
                );
            }
        }

        return new JsonModel(array('followings' => $_followings));
    }

    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }
    
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        if (!isset($data['userId'])) {
            $this->getResponse()->setStatusCode(
                Error::UserIdNotSent_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserIdNotSent_message
                )
            );
        }

        !is_array($data['userId']) ?
            $userIds = array($data['userId']) :
            $userIds = $data['userId'];
        foreach ($userIds as $userId) {

            $following = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($userId);
            if (!$following instanceof User) {
                $this->getResponse()->setStatusCode(
                    Error::UserNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::UserNotFound_message
                    )
                );
            }

            $user = $this->identity();
            
            similar_text($user->getId(), $following->getId(), $percent);
            if ($percent == 100) {
                continue;
            }

            $userFollow = $objectManager
                ->getRepository('Qolve\Entity\UserFollow')
                ->findOneBy(array(
                    'userId'     => $following->getId(),
                    'followerId' => $user->getId()
                )
            );

            if ($userFollow instanceof UserFollow) {
                continue;
            }
            $userFollow = new UserFollow();
            $userFollow->setUserId($following->getId());
            $userFollow->setFollowerId($user->getId());
            $objectManager->persist($userFollow);

            //Update UserPref
            $followerPref = $objectManager
                ->getRepository('\Application\Entity\UserPref')
                ->getUserPref($userFollow->getFollowerId(), 'userFollowingsCount');
            $followingsCount = $objectManager
                ->getRepository('\Qolve\Entity\UserFollow')
                ->followingsCount($userFollow->getFollowerId()) + 1;
            $followerPref->setValue($followingsCount);
            $objectManager->persist($followerPref);

            $followingPref   = $objectManager
                ->getRepository('\Application\Entity\UserPref')
                ->getUserPref($userFollow->getUserId(), 'followersCount');
            $followersCount  = $objectManager
                ->getRepository('\Qolve\Entity\UserFollow')
                ->followersCount($userFollow->getUserId()) + 1;

            $followingPref->setValue($followersCount);
            $objectManager->persist($followingPref);

            $usersFollow[] = $userFollow;
            $now           = new \DateTime('now', new \DateTimeZone('UTC'));

            $objectManager->flush();

            //Add to user's Activities
            $feed = new Feed();
            $feed->setUserId($userFollow->getFollowerId());
            $feed->setActorId($userFollow->getFollowerId());
            $feed->setContent($userFollow->getUserId());
            $feed->setPrivacy(Feed::PRIVACY_PRIVATE);
            $feed->setVerb('FollowUser');
            $feed->setCreatedOn($now);
            $objectManager->persist($feed);

            //Set feeds based on user's folloyees
            $preFeed = $objectManager
                ->getRepository('Application\Entity\Feed')
                ->setPreFeeds($user->getId(), $userId);

            $feedsQueue = $this->serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('feeds');
            $data = array(
                'verb'       => '\Qolve\Verb\FollowUser',
                'followerId' => $userFollow->getFollowerId(),
                'folloyeeId' => $userFollow->getUserId()
            );

            $job = new PublishFeed($this->serviceLocator);
            $job->setContent($data);
            $feedsQueue->push($job);

            //Add to Queue for send Notification
            $queue = $this->serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('notifications');

            $data['to'] = 'folloyee';
            $job = new SendNotification($this->serviceLocator);
            $job->setContent($data);
            $queue->push($job);

            //Add to Queue for send Email
            $isFollowed = $objectManager
                ->getRepository('Qolve\Entity\UserFollow')
                ->isFollowed($user->getId(), $following->getId());
            $config =  $this->getServiceLocator()->get('config');
            $emailQueue = $this->getServiceLocator()
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('email');
            $job = new \Application\Job\SendEmail($this->getServiceLocator());
            $job->setContent(array(
                'to'        => $following->getEmail(),
                'from'      => $user->getUsername() . " (via Qolve)",
                'subject'   => $user->getUsername() . " is now following you on Qolve!",
                'template'  => 'Follow',
                'params'    => array(
                    'follower'         => $user->getName(),
                    'followerUsername' => $user->getUsername(),
                    'folloyee'         => $following->getName(),
                    'isFollowed'       => $isFollowed,
                    'baseUrl'          => $config['baseUrl']
                    )
            ));
            $emailQueue->push(
                $job,
                array('delay' => new \DateInterval("PT30S"))
            );
        }

        return new JsonModel(array('Success'));
    }

    public function delete($userId)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $following = $objectManager
            ->getRepository('Application\Entity\User')
            ->find($userId);
        if (!$following instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::UserNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserNotFound_message
                )
            );
        }

        $user = $this->identity();

        $follow = $objectManager
            ->getRepository('Qolve\Entity\UserFollow')
            ->findOneBy(array(
                'userId'     => $following->getId(),
                'followerId' => $user->getId()
            )
        );
        if (!$follow instanceof \Qolve\Entity\UserFollow) {
            $this->getResponse()->setStatusCode(
                Error::UserHasNotFollowedThisObject_code
            );
            return new JsonModel(
                array('error' => Error::UserHasNotFollowedThisObject_message
                )
            );
        }

        $objectManager->remove($follow);
        
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'verb'   => 'FollowUser',
                    'fromId' => $user->getId(),
                    'toId'   => $following->getId()
                )
            );
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'verb'    => 'FollowUser',
                    'actorId' => $user->getId(),
                    'content' => $following->getId()
                )
            );
        
        $objectManager->flush();

        //Update UserPref
        $followerPref = $objectManager
            ->getRepository('\Application\Entity\UserPref')
            ->getUserPref($user->getId(), 'userFollowingsCount');
        $followingsCount = $objectManager
            ->getRepository('\Qolve\Entity\UserFollow')
            ->followingsCount($user->getId());
        $followerPref->setValue($followingsCount);
        $objectManager->persist($followerPref);

        $followingPref = $objectManager
            ->getRepository('\Application\Entity\UserPref')
            ->getUserPref($userId, 'followersCount');
        $followersCount  = $objectManager
            ->getRepository('\Qolve\Entity\UserFollow')
            ->followersCount($userId);
        $followingPref->setValue($followersCount);
        $objectManager->persist($followingPref);

        $objectManager->flush();

        return new JsonModel(array('success'));

    }
}
