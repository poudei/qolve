<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Feed,
    Qolve\Entity\BookmarkList,
    Qolve\Entity\BookmarkListFollow,
    Application\Job\SendNotification,
    Application\Entity\Error;

class BookmarkListFollowRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $listId = $this->getEvent()->getRouteMatch()->getParam('list_id');
        $list   = $objectManager->getRepository('\Qolve\Entity\BookmarkList')
            ->find($listId);
        if (!$list instanceof BookmarkList) {
            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );

            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }

        $limit  = $this->params()->fromQuery('limit', 5);
        $offset = $this->params()->fromQuery('offset', 0);
        
        $limit  = $this->normalize('limit', $limit);
        $offset = $this->normalize('offset', $offset);

        $currentUser = $this->identity();

        $followers = $objectManager
            ->getRepository('Qolve\Entity\BookmarkListFollow')
            ->getBookmarkListFollowers($listId, $count, $offset, $limit);

        $_followers = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($followers as $follower) {
            $_followers['list'][] = $hydrator
                ->extract($follower, array(), $currentUser);
        }

        return new JsonModel(
            array(
                'followers' => $_followers
            )
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $listId = $this->getEvent()->getRouteMatch()->getParam('list_id');
        $list   = $objectManager->getRepository('\Qolve\Entity\BookmarkList')
            ->find($listId);
        if (!$list instanceof BookmarkList) {
            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }

        $user = $this->identity();

        similar_text($list->getUserId(), $user->getId(), $percent);
        if ($percent == 100) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        if ($list->getPrivacy() == BookmarkList::PRIVACY_PRIVATE) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $follow = $objectManager
            ->getRepository('\Qolve\Entity\BookmarkListFollow')
            ->findOneBy(
                array(
                    'listId'    => $listId,
                    'userId'    => $user->getId()
                )
            );
        if ($follow instanceof BookmarkListFollow) {
            $this->getResponse()->setStatusCode(
                Error::UserHasFollowedThisObject_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserHasFollowedThisObject_message
                )
            );
        }

        $follow = new BookmarkListFollow();
        $follow->setListId($listId);
        $follow->setUserId($user->getId());
        $objectManager->persist($follow);

        $followsCount = $objectManager
            ->getRepository('\Qolve\Entity\BookmarkList')
            ->followsCount($listId);
        $list->setFollows($followsCount + 1);

        //Add to user's Activities (and timeline)
        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        
        $feed = new Feed();
        $feed->setUserId($user->getId());
        $feed->setActorId($user->getId());
        $feed->setContent($list->getId());
        $feed->setPrivacy(Feed::PRIVACY_PRIVATE);
        $feed->setVerb('FollowBookmarkList');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);
        
        $objectManager->flush();

        //Add to Queue for send Notification
        $queue = $this->serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'   => '\Qolve\Verb\FollowBookmarkList',
            'listId' => $follow->getListId(),
            'userId' => $follow->getUserId()
        );

        $data['to'] = 'user';
        $job = new SendNotification($this->serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        return new JsonModel(
            array(
                'listFollow' => $hydrator->extract($follow)
            )
        );
    }

    public function delete($content)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );

            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $listId = $this->getEvent()->getRouteMatch()->getParam('list_id');
        $list   = $objectManager->getRepository('\Qolve\Entity\BookmarkList')
            ->find($listId);
        if (!$list instanceof BookmarkList) {
            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }

        $user = $this->identity();

        $follow = $objectManager
            ->getRepository('\Qolve\Entity\BookmarkListFollow')
            ->findOneBy(array(
                'listId'    => $listId,
                'userId'    => $user->getId()
            ));
        if (!$follow instanceof BookmarkListFollow) {
            $this->getResponse()->setStatusCode(
                Error::UserHasNotFollowedThisObject_code
            );

            return new JsonModel(
                array(
                    'error' => Error::UserHasNotFollowedThisObject_message
                )
            );
        }

        $objectManager->remove($follow);

        $followsCount = $objectManager
            ->getRepository('\Qolve\Entity\BookmarkList')
            ->followsCount($listId);
        $list->setFollows(max(array($followsCount - 1, 0)));
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'actorId' => $user->getId(),
                    'content' => $list->getId(),
                    'verb'    => 'FollowBookmarkList'
                )
            );
        
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'fromId'  => $user->getId(),
                    'content' => $list->getId(),
                    'verb'    => 'FollowBookmarkList'
                )
            );
        
        $objectManager->flush();

        return new JsonModel(
            array(
                'Success'
            )
        );
    }
}
