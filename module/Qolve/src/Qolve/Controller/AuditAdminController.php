<?php
namespace Qolve\Controller;

use Application\Controller\AbstractController,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\Keyword,
    Qolve\Entity\Asker,
    Qolve\Entity\KeywordFollow,
    Application\Entity\User;

class AuditAdminController extends AbstractController
{
    const QUESTION_FREE = 0;
    const QUESTION_PAID = 1;
    const TOP_STORIES   = 0;
    const RECENT_DATE   = 1;

    public function copyAllAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $elastica = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $search        = new \Elastica\Search($elasticaClient);
        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('question');
        $elasticaQuery = new \Elastica\Query(new \Elastica\Query\MatchAll());
        $resultCount   = $search->addIndex($index)
            ->addType('question')
            ->count($elasticaQuery);

        $elasticaQuery->setSize($resultCount);

        $resultSet = $search->addIndex($index)
            ->addType('question')
            ->search($elasticaQuery);

        foreach ($resultSet as $result) {
            $returns[] = $result->getData();
        }

        if (empty($returns)) {
            return new JsonModel(
                array(
                    'questions'     => array(),
                    'count'         => 0,
                )
            );
        }

        foreach ($returns as $return) {

            $term = new \Elastica\Query\Term();

            $term->setParam('question_id', $return['question_id']);

            $search        = new \Elastica\Search($elasticaClient);
            $elasticaQuery = new \Elastica\Query($term);

            $resultSet = array();
            $resultSet = $search->addIndex($index)
                ->addType('questionLists')
                ->search($elasticaQuery);

            $resp = null;
            foreach ($resultSet as $result) {
                $resp = $result->getData();
            }
            if (empty($resp)) {


                $question = $objectManager
                    ->getRepository('Qolve\Entity\Question')
                    ->find($return['question_id']);
                if ($question instanceof Question) {

                    $asker = $objectManager
                        ->getRepository('Qolve\Entity\Asker')
                        ->findOneBy(array(
                            'questionId' => $return['question_id'],
                            'status'     => 1
                        )
                    );

                    $userId = null;
                    if ($asker instanceof Asker) {
                        $userId = $asker->getUserId();
                    }
                    $createdOn = null;
                    $createdOn = $question->getCreatedOn()->format('Y-m-d H:i:s');
                    $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));

                    $deadline  = $question->getDeadline();

                    if (!empty($deadline)) {
                        $deadline  = $question->getDeadline()->format('Y-m-d H:i:s');
                    }
                    //Create question activity in elasticsearch
                    $params = array(
                        'question_id'     => $question->getId(),
                        'user_id'         => $userId,
                        'reaskers'        => null,
                        'claimed'         => null,
                        'accepted'        => null,
                        'best_answer'     => null,
                        'question_report' => null,
                        'answer_report'   => null,
                        'bought'          => null,
                        'keywords'        => $return['keywords'],
                        'credits'         => $question->getCredits(),
                        'paid'            => $question->getPaid(),
                        'status'          => $question->getStatus(),
                        'deadline'        => $deadline,
                        'created_on'      => $createdOn
                    );

                    $objectManager
                        ->getRepository('Qolve\Entity\Question')
                        ->setQuestionActivity(
                            $this->getServiceLocator(),
                            $params
                    );
                }
            }
        }
        return new JsonModel(array('success'));
    }

    public function questionAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        if (!empty($data)) {
            $editableProperties = array(
                'limit',
                'offset',
                'deadline',
                'questionId',
                'userId',
                'reaskers',
                'claimed',
                'accepted',
                'bestAnswer',
                'questionReport',
                'answerReport',
                'bought',
                'keywords',
                'credits',
                'paid',
                'status',
                'recent'
            );

            foreach ($data as $name => $value) {
                if (array_search($name, $editableProperties) === false) {
                    unset($data[$name]);
                }
            }

            !empty($data['limit'])  ? $limit  = $data['limit']  : $limit = 10;
            !empty($data['offset']) ? $offset = $data['offset'] : $offset = 0;

            $limit  = $this->normalize('limit', $limit);
            $offset = $this->normalize('offset', $offset);

            unset($data['limit']);
            unset($data['offset']);

        }

        $elastica = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $search        = new \Elastica\Search($elasticaClient);
        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('questionLists');

        if (empty($data)) {
            $resultCount = $search->addIndex($index)
                ->addType('questionLists')
                ->count(new \Elastica\Query\MatchAll());

            return new JsonModel(array('questionCount' => $resultCount));

        } elseif (!empty($data)) {
            $boolMust   = new \Elastica\Query\Bool();
            $keysResult = array();
            if (isset($data['keywords'])) {

                $boolKey = new \Elastica\Query\Bool();
                $keys = explode(' ', $data['keywords']);
                foreach($keys as $key => $value) {
                    if (empty($value)) {
                        unset($keys[$key]);
                    }
                }

                $keys = array_unique($keys);

                foreach ($keys as $key) {
//                    $keyword = $objectManager
//                        ->getRepository('Qolve\Entity\Keyword')
//                        ->getKeyByName($key);
//                    if ($keyword instanceof Keyword) {
//                       $keysResult[] = $hydrator
//                        ->extract($keyword, array(), $user);
//                    } else {
//                        $keysResult[] = array(
//                            'id'         => null,
//                            'name'       => $key,
//                            'isFollowed' => 0
//                        );
//                    }

                    $term = new \Elastica\Query\Term();
                    $term->setParam('keywords', trim(strtolower($key)));
                    $boolKey->addShould($term);
                }
                $boolMust->addMust($boolKey);
            }

            if (   isset($data['claimed'])
                && (   ($data['claimed'] == 1)
                    || ($data['claimed'] == 0)
                   )
            ) {
                if ($data['claimed'] == 1) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('claimed', 'na');
                    $boolMust->addMustNot($term);

                } elseif ($data['claimed'] == 0) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('claimed', 'na');
                    $boolMust->addMust($term);
                }

            } elseif (isset($data['claimed'])) {

                $boolClaimed = new \Elastica\Query\Bool();

                if (!is_array($data['claimed'])) {
                    $claimed = explode(',', $data['claimed']);
                } else {
                    $claimed = $data['claimed'];
                }

                foreach($claimed as $key => $value) {
                    $value = preg_replace('/\s/', '', $value);

                    if (empty($value)) {
                        unset($claimed[$key]);
                    }
                }
                $claimed = array_unique($claimed);

                foreach ($claimed as $_claimed) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('claimed', trim(strtolower($_claimed)));
                    $boolClaimed->addShould($term);
                }
                $boolMust->addMust($boolClaimed);
            }

            if (   isset($data['accepted'])
                && (   ($data['accepted'] == 1)
                    || ($data['accepted'] == 0)
                   )
            ) {
                if ($data['accepted'] == 1) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('accepted', 'na');
                    $boolMust->addMustNot($term);

                } elseif ($data['accepted'] == 0) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('accepted', 'na');
                    $boolMust->addMust($term);
                }

            } elseif (isset($data['accepted'])) {
                $boolAccepted = new \Elastica\Query\Bool();

                if (!is_array($data['accepted'])) {
                    $accepted = explode(',', $data['accepted']);
                } else {
                    $accepted = $data['accepted'];
                }

                foreach($accepted as $key => $value) {
                    $value = preg_replace('/\s/', '', $value);

                        if (empty($value)) {
                        unset($accepted[$key]);
                    }
                }
                $accepted = array_unique($accepted);

                foreach ($accepted as $_accepted) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('accepted', trim(strtolower($_accepted)));
                    $boolAccepted->addShould($term);
                }
                $boolMust->addMust($boolAccepted);
            }

            if (   isset($data['questionReport'])
                && (   ($data['questionReport'] == 1)
                    || ($data['questionReport'] == 0)
                   )
            ) {
                if ($data['questionReport'] == 1) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('question_report', 'na');
                    $boolMust->addMustNot($term);

                } elseif ($data['questionReport'] == 0) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('question_report', 'na');
                    $boolMust->addMust($term);
                }

            } elseif (isset($data['questionReport'])) {

                $boolQuestionReport = new \Elastica\Query\Bool();

                if (!is_array($data['questionReport'])) {
                    $questionReport = explode(',', $data['questionReport']);
                } else {
                    $questionReport = $data['questionReport'];
                }

                foreach($questionReport as $key => $value) {
                    $value = preg_replace('/\s/', '', $value);

                        if (empty($value)) {
                        unset($questionReport[$key]);
                    }
                }
                $questionReport = array_unique($questionReport);

                foreach ($questionReport as $_questionReport) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('question_report', trim(strtolower($_questionReport)));
                    $boolQuestionReport->addShould($term);
                }
                $boolMust->addMust($boolQuestionReport);
            }

            if (   isset($data['answerReport'])
                && (   ($data['answerReport'] == 1)
                    || ($data['answerReport'] == 0)
                   )
            ) {
                if ($data['answerReport'] == 1) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('answer_report', 'na');
                    $boolMust->addMustNot($term);
                } elseif ($data['answerReport'] == 0) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('answer_report', 'na');
                    $boolMust->addMust($term);

                }

            } elseif (isset($data['answerReport'])) {

                $boolAnswerReport= new \Elastica\Query\Bool();

                if (!is_array($data['answerReport'])) {
                    $answerReport = explode(',', $data['answerReport']);
                } else {
                    $answerReport = $data['answerReport'];
                }

                foreach($answerReport as $key => $value) {
                    $value = preg_replace('/\s/', '', $value);

                        if (empty($value)) {
                        unset($answerReport[$key]);
                    }
                }
                $answerReport = array_unique($answerReport);

                foreach ($answerReport as $_answerReport) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('answer_report', trim(strtolower($_answerReport)));
                    $boolAnswerReport->addShould($term);
                }
                $boolMust->addMust($boolAnswerReport);
            }

            if (   isset($data['bought'])
                && (   ($data['bought'] == 1)
                    || ($data['bought'] == 0)
                   )
            ) {
                if ($data['bought'] == 1) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('bought', 'na');
                    $boolMust->addMustNot($term);
                } elseif ($data['bought'] == 0) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('bought', 'na');
                    $boolMust->addMust($term);

                }

            } elseif (isset($data['bought'])) {

                $boolBought = new \Elastica\Query\Bool();

                if (!is_array($data['answerReport'])) {
                    $bought = explode(',', $data['bought']);
                } else {
                    $bought = $data['bought'];
                }

                foreach($bought as $key => $value) {
                    $value = preg_replace('/\s/', '', $value);

                        if (empty($value)) {
                        unset($bought[$key]);
                    }
                }
                $bought = array_unique($bought);

                foreach ($bought as $_bought) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('bought', trim(strtolower($_bought)));
                    $boolBought->addShould($term);
                }
                $boolMust->addMust($boolBought);
            }

            if (   isset($data['reaskers'])
                && (   ($data['reaskers'] == 1)
                    || ($data['reaskers'] == 0)
                   )
            ) {
                if ($data['reaskers'] == 1) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('reaskers', 'na');
                    $boolMust->addMustNot($term);

                } elseif ($data['reaskers'] == 0) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('reaskers', 'na');
                    $boolMust->addMust($term);

                }

            } elseif (isset($data['reaskers'])) {

                $boolReaskers = new \Elastica\Query\Bool();

                if (!is_array($data['reaskers'])) {
                    $reaskers = explode(',', $data['reaskers']);
                } else {
                    $reaskers = $data['reaskers'];
                }

                foreach($reaskers as $key => $value) {
                    $value = preg_replace('/\s/', '', $value);

                        if (empty($value)) {
                        unset($reaskers[$key]);
                    }
                }
                $reaskers = array_unique($reaskers);

                foreach ($reaskers as $reasker) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('reaskers', trim(strtolower($reasker)));
                    $boolReaskers->addShould($term);
                }
                $boolMust->addMust($boolReaskers);
            }

            if (isset($data['credits'])
                && $data['credits'] == self::QUESTION_PAID
            ) {
                $term  = new \Elastica\Query\Term();
                $term->setParam('credits', self::QUESTION_FREE);
                $boolMust->addMustNot($term);

            } elseif ((   isset($data['credits'])
                       && $data['credits'] == self::QUESTION_FREE)
            ) {
                $term = new \Elastica\Query\Term();
                $term->setParam('credits', self::QUESTION_FREE);
                $boolMust->addMust($term);
            }

            if (isset($data['userId'])) {
                $term  = new \Elastica\Query\Term();
                $term->setParam('user_id', $data['userId']);
                $boolMust->addMust($term);
            }

            if (isset($data['questionId'])) {
                $term  = new \Elastica\Query\Term();
                $term->setParam('question_id', $data['questionId']);
                $boolMust->addMust($term);
            }

            if (isset($data['paid'])) {
                $term  = new \Elastica\Query\Term();
                $term->setParam('paid', $data['paid']);
                $boolMust->addMust($term);
            }


            if (isset($data['status'])) {
                $term  = new \Elastica\Query\Term();
                $term->setParam('status', $data['status']);
                $boolMust->addMust($term);
            }

            if (   isset($data['bestAnswer'])
                && (   ($data['bestAnswer'] == 1)
                    || ($data['bestAnswer'] == 0)
                   )
            ) {
                if ($data['bestAnswer'] == 1) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('best_answer', 1.0);
                    $boolMust->addMustNot($term);

                } elseif ($data['bestAnswer'] == 0) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('best_answer', 1.0);
                    $boolMust->addMust($term);

                }

            } elseif (isset($data['bestAnswer'])) {

                $term = new \Elastica\Query\Term();
                $term->setParam('best_answer', $data['bestAnswer']);
                $boolMust->addMust($term);
            }

            if (   isset($data['deadline'])
                && (   ($data['deadline'] == 1)
                    || ($data['deadline'] == 0)
                   )
            ) {
                if ($data['deadline'] == 1) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('deadline', 1.0);
                    $boolMust->addMustNot($term);

                } elseif ($data['deadline'] == 0) {
                    $term  = new \Elastica\Query\Term();
                    $term->setParam('deadline', 1.0);
                    $boolMust->addMust($term);

                }
            }

            $elasticaQuery = new \Elastica\Query($boolMust);

            if (   (!empty($data['recent']))
                && ($data['recent'] == 1) ) {
                $elasticaQuery->setSort(
                    array('created_on' => array('order' => 'desc'))
                );
            } else if (   (isset($data['recent']))
                && ($data['recent'] == 0) ) {
                $elasticaQuery->setSort(
                    array('votes' => array('order' => 'desc'))
                );
            }

            if (   (count($data) == 1)
                && isset($data['recent'])
            ) {
                $elasticaQuery = new \Elastica\Query(
                    new \Elastica\Query\MatchAll()
                );
            }

            $elasticaQuery->setFrom($offset);
            $elasticaQuery->setSize($limit);
            $resultSet = $search->addIndex($index)
                ->addType('questionLists')
                ->search($elasticaQuery);
            $resultCount = $search->addIndex($index)
                ->addType('questionLists')
                ->count($elasticaQuery);

            foreach ($resultSet as $result) {
                $returns[] = $result->getData();
            }

            if (empty($returns)) {
                return new JsonModel(
                    array(
                        'questions'     => array(),
                        'count'         => 0,

                    )
                );
            }

            $returnSet['questions']     = $returns;
            $returnSet['count']         = $resultCount;

            return new JsonModel($returnSet);
        }
    }

    public function userAction()
    {
        return new JsonModel(array('Success'));
    }
}

