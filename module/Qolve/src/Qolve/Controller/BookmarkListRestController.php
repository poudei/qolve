<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Error,
    Qolve\Entity\Question,
    Qolve\Entity\Bookmark,
    Qolve\Entity\BookmarkList,
    Qolve\Entity\BookmarkListFollow,
    Zend\Form\Annotation\AnnotationBuilder;

class BookmarkListRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $byQuestion = $this->params()->fromQuery('question', true);
        $byComment  = $this->params()->fromQuery('comment', true);
        $byAnswer   = $this->params()->fromQuery('answer', true);
        $byDocument = $this->params()->fromQuery('document', true);
        $userId     = $this->params()->fromQuery('userId');
        $mode       = $this->params()->fromQuery('mode', 'all');
        
        $mode = strtolower($mode);

        $info = array(
            'asker',
            'keywords',
            'userAnswerId',
            'listKeywords'
        );
        if ($byQuestion) {
            $info[] = 'questionsList';
        }
        if ($byAnswer) {
            $info[] = 'answersList';
        }
        if ($byComment) {
            $info[] = 'commentsList';
        }
        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $currentUser = $this->identity();

        if (!isset($userId)) {
            if (!$currentUser instanceof User) {
                $this->getResponse()->setStatusCode(
                    Error::AuthenticationFailed_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AuthenticationFailed_message
                    )
                );
            }
        }

        if (!empty($userId)) {
            $user = $objectManager->getRepository('Application\Entity\User')
                ->find($userId);
            if (!$user instanceof User) {
                $this->getResponse()->setStatusCode(
                    Error::UserNotFound_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::UserNotFound_message
                    )
                );
            }
        }

        $_lists = array();

        if ($mode != 'follow') { //mode = create or all
            empty($userId) 
                ? $creatorId = $currentUser->getId() : $creatorId = $userId;

            $createLists = $objectManager
                ->getRepository('Qolve\Entity\BookmarkList')
                ->getLists($creatorId, array('name' => 'ASC'), $currentUser);

            $_lists['create'] = array();
            foreach ($createLists as  $list) {
                $_lists['create'][] = $hydrator
                    ->extract($list, $info, $currentUser);
            }
        }

        if ($mode != 'create') { //mode = follow or all
            $listFollows = $objectManager
                ->getRepository('Qolve\Entity\BookmarkListFollow')
                ->findBy(
                    array(
                        'userId' => empty($userId) 
                            ? $currentUser->getId() : $userId
                    ), 
                    array()
                );

            $_lists['follow'] = array();
            foreach ($listFollows as $follow) {
                $list = $objectManager
                    ->getRepository('Qolve\Entity\BookmarkList')
                    ->find($follow->getListId());
                if ($list instanceof BookmarkList) {
                    $_lists['follow'][] = $hydrator
                        ->extract($list, $info, $currentUser);
                }
            }

            $arr = array();
            foreach ($_lists['follow'] as $key => $value) {
                $arr[$key]  = $value['name'];
            }

            array_multisort($arr, SORT_ASC, $_lists['follow']);
        }

        return new JsonModel(
            array(
                "bookmarkLists" => $_lists
            )
        );
    }

    public function get($id)
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $byQuestion = $this->params()->fromQuery('question', true);
        $byComment  = $this->params()->fromQuery('comment', true);
        $byAnswer   = $this->params()->fromQuery('answer', true);
        $byDocument = $this->params()->fromQuery('document', true);
        
        $currentUser = $this->identity();

        if (strtolower($id) == strtolower(BookmarkList::NAME_UNCATEGORIZED)) {
            if (!$currentUser instanceof User) {
                $this->getResponse()->setStatusCode(
                    Error::AuthenticationFailed_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::AuthenticationFailed_message
                    )
                );
            }

            $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
                ->getUserUncatList($currentUser->getId());
        } else {
            $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
                ->find($id);
        }

        if (!$list instanceof BookmarkList) {

            if (strtolower($id) 
                == strtolower(BookmarkList::NAME_UNCATEGORIZED)
            ) {
                $list = new BookmarkList();
                $list->setId(BookmarkList::NAME_UNCATEGORIZED);
                $list->setName(BookmarkList::NAME_UNCATEGORIZED);

                return new JsonModel(
                    array(
                        'bookmarkList' => $hydrator
                            ->extract($list, array(), $currentUser)
                    )
                );
            }

            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }

        if ($currentUser instanceof User) {
            similar_text($currentUser->getId(), $list->getUserId(), $percent);
        }

        if ($list->getPrivacy() == BookmarkList::PRIVACY_PRIVATE 
            && (!$currentUser || $percent < 100)
        ) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $info = array(
            'asker',
            'keywords',
            'userAnswerId',
            'listKeywords'
        );
        if ($byQuestion) {
            $info[] = 'questionsList';
        }
        if ($byAnswer) {
            $info[] = 'answersList';
        }
        if ($byComment) {
            $info[] = 'commentsList';
        }
        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $_list = $hydrator->extract($list, $info, $currentUser);

        return new JsonModel(
            array(
                "bookmarkList" => $_list
            )
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        if (!isset($data['name'])) {
            $data['name'] = BookmarkList::NAME_UNCATEGORIZED;
        }

        if (!isset($data['privacy'])) {
            $data['privacy'] = BookmarkList::PRIVACY_PUBLIC;
        }

        $user = $this->identity();

        $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
            ->findOneBy(
                array(
                    'userId'        => $user->getId(),
                    'name'          => $data['name']
                )
            );

        if (!$list instanceof BookmarkList) {
            
            $builder = new AnnotationBuilder();
            $list    = new BookmarkList();
            $form    = $builder->createForm($list);
            $form->setHydrator($hydrator);
            $form->bind($list);
            $form->setData($data);

            if (!$form->isValid()) {
                $this->getResponse()->setStatusCode(Error::FormInvalid_code);

                return new JsonModel(
                    array(
                        'error' => $form->getMessages()
                    )
                );
            }

            $list = $form->getData();
            $now  = new \DateTime('now', new \DateTimeZone('UTC'));

            $list->setUserId($user->getId());
            $list->setQuestions(0);
            $list->setFollows(0);
            $list->setCreatedOn($now);

            if (strtolower($list->getName()) 
                == strtolower(BookmarkList::NAME_UNCATEGORIZED)
            ) {
                $list->setPrivacy(BookmarkList::PRIVACY_PRIVATE);
            }

            $objectManager->persist($list);
        }

        if (isset($data['questionId'])) {
            $questionId = strval($data['questionId']);
            $question   = $objectManager
                ->getRepository('Qolve\Entity\Question')
                ->find($questionId);
            if (!$question instanceof Question) {
                $this->getResponse()->setStatusCode(
                    Error::QuestionNotFound_code
                );

                return new JsonModel(
                    array(
                        'error' => Error::QuestionNotFound_message
                    )
                );
            }

            $bookmark = $objectManager->getRepository('Qolve\Entity\Bookmark')
                ->findOneBy(
                    array(
                        'listId'        => $list->getId(),
                        'questionId'    => $questionId
                    )
                );
            if (!$bookmark instanceof Bookmark) {
                $bookmark = new Bookmark();
                $bookmark->setListId($list->getId());
                $bookmark->setQuestionId($data['questionId']);
                $objectManager->persist($bookmark);

                $questionsCount = $objectManager
                    ->getRepository('Qolve\Entity\Bookmark')
                    ->questionsCount($list->getId());
                $list->setQuestions($questionsCount + 1);
            }
        }

        $objectManager->flush();
        
        return new JsonModel(
            array(
                'bookmarkList' => $hydrator->extract($list, array(), $user)
            )
        );
    }

    public function update($id, $data)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        if (strtolower($id) == strtolower(BookmarkList::NAME_UNCATEGORIZED)) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
            ->find($id);
        if (!$list instanceof BookmarkList) {
            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }
        if ($list->getName() == BookmarkList::NAME_UNCATEGORIZED) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $user = $this->identity();
        similar_text($list->getUserId(), $user->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        if (isset($data['privacy']) 
            && $data['privacy'] == BookmarkList::PRIVACY_PUBLIC 
            && $list->getPrivacy() == BookmarkList::PRIVACY_PRIVATE
        ) {
            if ($objectManager->getRepository('Qolve\Entity\BookmarkList')
                ->hasPrivateQuestion($list->getId())
            ) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );

                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $preData = $hydrator->extract($list);

        $editableProperties = array(
            'name',
            'privacy',
        );
        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        $data    = array_merge($preData, $data);
        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($list);
        $form->setHydrator($hydrator);
        $form->setBindOnValidate(false);
        $form->bind($list);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);

            return new JsonModel(
                array(
                    'error' => $form->getMessages()
                )
            );
        }

        $form->bindValues();

        if ($preData['privacy'] == BookmarkList::PRIVACY_PUBLIC 
            && $list->getPrivacy() == BookmarkList::PRIVACY_PRIVATE
        ) {
            $follows = $objectManager
                ->getRepository('Qolve\Entity\BookmarkListFollow')
                ->findByListId($list->getId());
            foreach ($follows as $follow) {
                if ($follow instanceof BookmarkListFollow) {
                    $objectManager->remove($follow);
                }
            }
        }

        $objectManager->flush();

        return new JsonModel(
            array(
                'bookmarkList' => $hydrator->extract($list, array(), $user)
            )
        );
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        if (strtolower($id) == strtolower(BookmarkList::NAME_UNCATEGORIZED)) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
            ->find($id);
        if (!$list instanceof BookmarkList) {
            $this->getResponse()->setStatusCode(
                Error::BookmarkListNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::BookmarkListNotFound_message
                )
            );
        }
        if ($list->getName() == BookmarkList::NAME_UNCATEGORIZED) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $user = $this->identity();
        similar_text($list->getUserId(), $user->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );

            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $bookmarks = $objectManager->getRepository('Qolve\Entity\Bookmark')
            ->findByListId($list->getId());
        foreach ($bookmarks as $bookmark) {
            if ($bookmark instanceof Bookmark) {
                $objectManager->remove($bookmark);
            }
        }

        $follows = $objectManager
            ->getRepository('Qolve\Entity\BookmarkListFollow')
            ->findByListId($list->getId());
        foreach ($follows as $follow) {
            if ($follow instanceof BookmarkListFollow) {
                $objectManager->remove($follow);
            }
        }

        $objectManager->remove($list);
        
        $entitiesToDelete = array(
            'Qolve\Entity\Bookmark',
            'Qolve\Entity\BookmarkListFollow'
        );
        
        foreach ($entitiesToDelete as $entity) {
            $objectManager->getRepository($entity)
                ->deleteBy(array('listId' => $list->getId()));
        }
        
        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'verb'    => 'FollowBookmarkList',
                    'content' => $list->getId()
                )
            );
        
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'verb'    => 'FollowBookmarkList',
                    'content' => $list->getId()
                )
            );
        
        $objectManager->flush();

        return new JsonModel(
            array(
                'Success'
            )
        );
    }
}
