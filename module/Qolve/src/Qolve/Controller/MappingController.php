<?php
namespace Qolve\Controller;

use Application\Controller\AbstractController,

    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel;

class MappingController extends AbstractController
{

    public function questionAction()
    {
        $index   = $this->getEvent()->getRouteMatch()->getParam('index');
        $elastica  = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient  = $elastica['client'];
        
        $elasticaIndex = $elasticaClient->getIndex($index);

        $elasticaType = $elasticaIndex->getType('question');

        $mapping = new \Elastica\Type\Mapping();
        $mapping->setType($elasticaType);

        $mapping->setProperties(array(
            'question_id' => array(
                'store' => 'yes',
                'type'  => 'long'
            ),

            'keywords' => array(
                'store' => 'yes',
                'index' => 'analyzed',
                'type'  => 'string',
            ),

            'votes' => array(
                'store' => 'yes',
                'type'  => 'integer'
            ),

            'answers' => array(
                'store' => 'yes',
                'type'  => 'integer'
            ),

            'levels' => array(
                'store' => 'yes',
                'index' => 'analyzed',
                'type'  => 'string'
            ),
            'status' => array(
                'store' => 'yes',
                'type'  => 'integer'
            ),
            'credits' => array(
                'store' => 'yes',
                'type'  => 'float'
            ),

            'recent_votes' => array(
                'store' => 'yes',
                'type'  => 'integer'
            ),

            'paid' => array(
                'store' => 'yes',
                'type'  => 'integer'
            ),

            'privacy' => array(
                'store' => 'yes',
                'type'  => 'integer'
            ),

            'deadline' => array(
                'store'  => 'yes',
                'type'   => 'date',
                'format' => "yyyy-MM-dd HH:mm:ss",
            ),

            'last_update' => array(
                'store'  => 'yes',
                'type'   => 'date',
                'format' => "yyyy-MM-dd HH:mm:ss",
            ),

            'created_on' => array(
                'store'  => 'yes',
                'type'   => 'date',
                'format' => "yyyy-MM-dd HH:mm:ss"
            )
        ));

        // Send mapping to type
        $mapping->send();

        return new JsonModel(array("success"));

    }

    public function userAction()
    {
        $index   = $this->getEvent()->getRouteMatch()->getParam('index');
        $config  = $this->getServiceLocator()->get('Config');
        $elastica  = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient  = $elastica['client'];

        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('user');

        $mapping = new \Elastica\Type\Mapping();
        $mapping->setType($elasticaType);

        $mapping->setProperties(array(
            'user_id' => array(
                'store' => 'yes',
                'type'  => 'long'
            ),

            'name' => array(
                'store'      => 'yes',
                'index'      => 'not_analyzed',
                'type'       => 'string',
            ),

            'username' => array(
                'store'      => 'yes',
                'index'      => 'not_analyzed',
                'type'       => 'string',
            ),

            'email' => array(
                'store'      => 'yes',
                'index'      => 'not_analyzed',
                'type'       => 'string',
            ),

            'status' => array(
                'store' => 'yes',
                'type'  => 'integer'
            ),

            'deleted' => array(
                'store' => 'yes',
                'type'  => 'integer'
            ),

            'created_on' => array(
                'store'  => 'yes',
                'type'   => 'date',
                'format' => "yyyy-MM-dd HH:mm:ss"
            )
        ));

        // Send mapping to type
        $mapping->send();

        return new JsonModel(array("success"));

    }

    public function questionListAction()
    {
        $index   = $this->getEvent()->getRouteMatch()->getParam('index');
        $config  = $this->getServiceLocator()->get('Config');
        $elastica  = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient  = $elastica['client'];

        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('questionLists');

        $mapping = new \Elastica\Type\Mapping();
        $mapping->setType($elasticaType);

        $mapping->setProperties(array(
            
            'question_id' => array(
                'store' => 'yes',
                'type'  => 'long'
            ),

            'user_id' => array(
                'store' => 'yes',
                'type'  => 'long'
            ),

            'reaskers' => array(
                'store'      => 'yes',
                'index'      => 'analyzed',
                'null_value' => 'na',
                'type'       => 'string',
            ),

            'claimed' => array(
                'store'      => 'yes',
                'index'      => 'analyzed',
                'null_value' => 'na',
                'type'       => 'string'
            ),

            'accepted' => array(
                'store'      => 'yes',
                'index'      => 'analyzed',
                'null_value' => 'na',
                'type'       => 'string',
            ),
            
            'best_answer' => array(
                'store'      => 'yes',
                'type'       => 'long',
                'null_value' => 1.0
                
            ),

            'question_report' => array(
                'store'      => 'yes',
                'index'      => 'analyzed',
                'null_value' => 'na',
                'type'       => 'string'
            ),

            'answer_report' => array(
                'properties' => array(
                    'reporter' => array(
                        'store'      => 'yes',
                        'index'      => 'not_analyzed',
                        'null_value' => 'na',
                        'type'       => 'string'
                    ),

                    'answerId' => array(
                        'store'      => 'yes',
                        'index'      => 'not_analyzed',
                        'null_value' => 'na',
                        'type'       => 'string'
                    )
                )
            ),

            'bought' => array(
                'store' => 'yes',
                'index' => 'analyzed',
                'null_value' => 'na',
                'type'  => 'string'
            ),

            'keywords' => array(
                'store' => 'yes',
                'index' => 'analyzed',
                'null_value' => 'na',
                'type'  => 'string'
            ),

            'credits' => array(
                'store' => 'yes',
                'type'  => 'short'
            ),

            'paid' => array(
                'store' => 'yes',
                'type'  => 'short'
            ),

            'status' => array(
                'store' => 'yes',
                'type'  => 'short'
            ),

            'deadline' => array(
                'store'  => 'yes',
                'type'   => 'date',
                'format' => "yyyy-MM-dd HH:mm:ss",
                'null_value' => 1.0
                
            ),

            'created_on' => array(
                'store'  => 'yes',
                'type'   => 'date',
                'format' => "yyyy-MM-dd HH:mm:ss"
            )
        ));

        // Send mapping to type
        $mapping->send();

        return new JsonModel(array("success"));

    }


    public function createAction()
    {
        $index   = $this->getEvent()->getRouteMatch()->getParam('index');
        $elastica  = $this->getServiceLocator()
                ->get('Elastica\Client');
        $elasticaClient  = $elastica['client'];

        $index = new \Elastica\Index($elasticaClient, $index);
        $index->create();

        return new JsonModel(array("success"));

    }

    public function deleteAction()
    {
        $index   = $this->getEvent()->getRouteMatch()->getParam('index');
        $config  = $this->getServiceLocator()->get('Config');
        $elastica  = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient  = $elastica['client'];

        $index = new \Elastica\Index($elasticaClient, $index);
        $index->delete();

        return new JsonModel(array("success"));

    }

    public function shardAction()
    {
        $elasticaClient = new \Elastica\Client(
            array(
                'host' => '127.0.0.1',
                'port' => '9200'
            )
        );
        $shardData = array(

            'status'              => 'jimbo',
            'primary_active'      => true,
            'active_shards'       => 1,
            'relocating_shards'   => 0,
            'initializing_shards' => 0,
            'unassigned_shards'   => 1,
        );

        $this->_shard = new \Elastica\Cluster\Health($elasticaClient, $shardData);

        return new JsonModel(array("success"));

    }
}
