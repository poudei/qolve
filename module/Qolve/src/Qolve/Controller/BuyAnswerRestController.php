<?php
namespace Qolve\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\Error,
    Zend\View\Model\JsonModel,
    Qolve\Entity\Question,
    Qolve\Entity\Answer;

class BuyAnswerRestController extends AbstractRestfulController
{
    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(
                Error::UserUnavailable_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $user = $this->identity();

        $questionId = $this->getEvent()->getRouteMatch()->getParam('question_id');
        $question   = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        if (!$question instanceof Question) {
            $this->getResponse()->setStatusCode(
                Error::QuestionNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::QuestionNotFound_message
                )
            );
        }

        $answerId = $this->getEvent()->getRouteMatch()->getParam('answer_id');
        $answer   = $objectManager->getRepository('\Qolve\Entity\Answer')
            ->find($answerId);
        if (!$answer instanceof Answer) {
            $this->getResponse()->setStatusCode(
                Error::AnswerNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AnswerNotFound_message
                )
            );
        }

        if ($answer->getPrivacy() == Answer::PRIVACY_PUBLIC) {
            $this->getResponse()->setStatusCode(
                Error::AnswerIsNotPrivate_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AnswerIsNotPrivate_message
                )
            );
        }

        if ($answer->getStatus() == Answer::STATUS_NORMAL) {
            $this->getResponse()->setStatusCode(
                Error::AnswerIsNotAccepted_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AnswerIsNotAccepted_message
                )
            );
        }

        if ($question->getCredits() > 0) {
            $price = (float) ($question->getCredits() * 0.5);

            $buyerCredit = (int) $objectManager->getRepository('Application\Entity\UserPref')
                ->getUserKey($user->getId(), 'credit');
            if ($buyerCredit < $price) {
                $this->getResponse()->setStatusCode(
                    Error::NotHaveEnoughCredit_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::NotHaveEnoughCredit_message
                    )
                );
            }

            $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                ->findByQuestionId($question->getId());

            //TODO: complete
        }

        return new JsonModel(array('success' => true));
    }
}
