<?php

namespace Qolve\Library;

class Constants {

    const IOS_DEVICE     = 1;
    const ANDROID_DEVICE = 2;
    const IOS_PARTNER 	 = '1';
    const GOOGLE_PARTNER = '2';
    
    const PAY_FROM_IOS_TO_QOLVE     = 1;
    const PAY_FROM_GOOGLE_TO_QOLVE  = 2;
    const PAY_FROM_QOLVE_TO_USER    = 3;

}
