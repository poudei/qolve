<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation as Form;

/**
 * Transaction
 *
 * @ORM\Table(name="`transaction`")
 * @ORM\Entity(repositoryClass="Qolve\Entity\TransactionRepository")
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="transaction", lifetime="3600")

 */
class Transaction
{
    const STATUS_BLOCKED    = 1;
    const STATUS_DONE       = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var decimal
     *
     * @ORM\Column(name="debit", type="float", nullable=true)
     * @Form\Validator({"name":"\Zend\I18n\Validator\Float"})
     * @Form\Exclude()
     */
    private $debit;

    /**
     * @var decimal
     *
     * @ORM\Column(name="credit", type="float", nullable=true)
     * @Form\Validator({"name":"\Zend\I18n\Validator\Float"})
     * @Form\Exclude()
     */
    private $credit;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     * @Form\Validator({"name":"Digits"})
     * @Form\Exclude()
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=1000, nullable=true)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "1", "max" : "1000"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Exclude()
     
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     * @Form\Exclude()
     */
    private $createdOn;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param  integer     $userId
     * @return Transaction
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set debit
     *
     * @param  integer     $debit
     * @return Transaction
     */
    public function setDebit($debit)
    {
        $this->debit = $debit;

        return $this;
    }

    /**
     * Get debit
     *
     * @return integer
     */
    public function getDebit()
    {
        return $this->debit;
    }

    /**
     * Set credit
     *
     * @param  integer     $credit
     * @return Transaction
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return integer
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set status
     *
     * @param  integer     $status
     * @return Transaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set notes
     *
     * @param  string      $notes
     * @return Transaction
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set createdOn
     *
     * @param  \DateTime   $createdOn
     * @return Transaction
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set id
     *
     * @param  integer     $id
     * @return Transaction
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
