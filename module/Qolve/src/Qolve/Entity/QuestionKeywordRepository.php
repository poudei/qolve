<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    Qolve\Entity\QuestionKeyword,
    Qolve\Entity\Keyword;

class QuestionKeywordRepository extends EntityRepository
{
//    public function questionKeyMatch($questionId, $keywordId)
//    {
//        $objectManager = $this->getEntityManager();
//        $objectManager
//            ->getConnection()
//            ->getShardManager()
//            ->selectShard($questionId);
//
//        $query = $objectManager->createQuery(
//            "SELECT k
//                FROM \Qolve\Entity\QuestionKeyword k
//                WHERE k.questionId = '$questionId'
//                AND   k.keywordId  = '$keywordId'"
//        );
//        $result = $query->getResult();
//        isset($result[0]) ? $result = $result[0] : $result = array()
//    }

    public function getKeywordsOfQuestion($questionId)
    {
        //TODO: cashe
        $query = $this->getEntityManager()->createQuery("
            SELECT key FROM \Qolve\Entity\QuestionKeyword qkey
                JOIN \Qolve\Entity\Keyword key
                WITH qkey.keywordId = key.id
                WHERE qkey.questionId = '$questionId'"
        );

        $query->useResultCache(true, LIFE_TIME, 'question_'. $questionId .
            '_keywords'
        );

        $result   = $query->getResult();
        $keywords = array();

        foreach ($result as $keyword) {
            $key['id']   = $keyword->getId();
            $key['name'] = $keyword->getName();
            $keywords[]  = $key;
        }

        return $keywords;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "qk.$key IS NULL";
            } else {
                $conditions[] = "qk.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\QuestionKeyword qk
                WHERE $conditions"
        );
        return $query->execute();
    }
}
