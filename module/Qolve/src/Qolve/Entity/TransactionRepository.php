<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository;

class TransactionRepository extends EntityRepository
{
    public function calculateUserCredit($userId)
    {
        $askDebits      = $this->_calculateAskDebits($userId);
        $sellDebits     = $this->_calculateSellDebits($userId);
        $answerCredits  = $this->_calculateAnswerCredits($userId);
        $buyCredits     = $this->_calculateBuyCredits($userId);

        return ($answerCredits + $buyCredits) - ($askDebits + $sellDebits);
    }

    public function findByQuestionId($questionId)
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT transaction
                FROM \Qolve\Entity\Transaction transaction
                WHERE transaction.notes
                LIKE '%\"questionId\":\"$questionId\"%'"
        );

        return $query->getResult();
    }

    public function countBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $property => $value) {
            if ($value === null) {
                $conditions[] = "transaction.$property IS NULL";
            } else {
                $conditions[] = "transaction.$property = '$value'";
            }
        }

        $queryStr = "SELECT COUNT(transaction) num
            FROM \Qolve\Entity\Transaction transaction ";
        if (!empty($conditions)) {
            $queryStr .= "WHERE " . join(' AND ', $conditions);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);
        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    private function _calculateAskDebits($userId)
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT SUM(transaction.debit) debits
                FROM \Qolve\Entity\Transaction transaction
                WHERE transaction.userId = '$userId'
                AND transaction.notes LIKE '%questionId%'"
        );

        $query->setFirstResult(0)
            ->setMaxResults(1);

        $result = $query->getResult();

        return $result[0]['debits'] ? $result[0]['debits'] : 0;
    }

    private function _calculateAnswerCredits($userId)
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT SUM(transaction.credit) credits
                FROM \Qolve\Entity\Transaction transaction
                WHERE transaction.userId = '$userId'
                AND transaction.notes LIKE '%questionId%'"
        );

        $query->setFirstResult(0)
            ->setMaxResults(1);

        $result = $query->getResult();

        return $result[0]['credits'] ? $result[0]['credits'] : 0;
    }

    private function _calculateSellDebits($userId)
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT SUM(transaction.credit) debits
                FROM \Qolve\Entity\Transaction transaction
                WHERE transaction.userId = '$userId'
                AND transaction.notes IS NULL"
        );

        $query->setFirstResult(0)
            ->setMaxResults(1);

        $result = $query->getResult();

        return $result[0]['debits'] ? $result[0]['debits'] : 0;
    }

    private function _calculateBuyCredits($userId)
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT SUM(transaction.debit) credits
                FROM \Qolve\Entity\Transaction transaction
                WHERE transaction.userId = '$userId'
                AND transaction.notes IS NULL"
        );

        $query->setFirstResult(0)
            ->setMaxResults(1);

        $result = $query->getResult();

        return $result[0]['credits'] ? $result[0]['credits'] : 0;
    }
}
