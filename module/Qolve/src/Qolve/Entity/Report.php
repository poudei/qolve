<?php

namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="Qolve\Entity\ReportRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @Application\ORM\Cacheable\Cacheable(type="report", lifetime="3600")
 * 
 * @Form\Name("report")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Report
{    
    const REASON_FORMATTINGISSUES = 1;
    const REASON_PERSONALATTACK   = 2;
    const REASON_SPAM             = 3;
    const REASON_NOTREADABLE      = 4;
    const REASON_INACCURATETAGS   = 5;
    const REASON_DUPLICATE        = 6; 
    const REASON_OTHER            = 7;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="report_id_seq", allocationSize=1, initialValue=1)
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="reporter_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Exclude()
     */
    private $reporterId;

    /**
     * @var integer
     *
     * @ORM\Column(name="reason", type="smallint", nullable=false)
     * @Form\Validator({"name":"Digits"})
     */
    private $reason;

    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Exclude()
     */
    private $answerId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Exclude()
     */
    private $createdOn;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reporterId
     *
     * @param  integer $reporterId
     * @return Report
     */
    public function setReporterId($reporterId)
    {
        $this->reporterId = $reporterId;

        return $this;
    }

    /**
     * Get reporterId
     *
     * @return integer
     */
    public function getReporterId()
    {
        return $this->reporterId;
    }

    /**
     * Set reason
     *
     * @param  integer $reason
     * @return Report
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return integer
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set questionId
     *
     * @param  integer $questionId
     * @return Report
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set answerId
     *
     * @param  integer $answerId
     * @return Report
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * Set createdOn
     *
     * @param  \DateTime $createdOn
     * @return Report
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * clearCache
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        
        if ($this->getAnswerId() == null) {
            $cache->delete(
                'user_' . $this->getReporterId() 
                    . '_question_' . $this->getQuestionId() 
                    . '_report'
            );
        } else {
            $cache->delete(
                'user_' . $this->getReporterId() 
                    . '_answer_' . $this->getAnswerId() 
                    . '_report'
            );
        }
    }
}
