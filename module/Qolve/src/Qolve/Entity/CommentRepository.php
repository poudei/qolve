<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    Qolve\Entity\Comment;

class CommentRepository extends EntityRepository
{
    public function getCommentsCount($questionId, $answerId = null)
    {
        if (empty($answerId)) {
            $query = $this->getEntityManager()->createQuery(
                "SELECT COUNT(comment.id) comments
                    FROM \Qolve\Entity\Comment comment
                    WHERE comment.answerId is NULL
                    AND comment.questionId = '$questionId'"
            );
        } else {
            $query = $this->getEntityManager()->createQuery(
                "SELECT COUNT(comment.id) comments
                    FROM \Qolve\Entity\Comment comment
                    WHERE comment.questionId = '$questionId'
                    AND comment.answerId   = '$answerId'"
            );

        }

        $result = $query->getResult();

        return isset($result[0]['comments']) ? $result[0]['comments'] : 0;
    }

    public function getComments($questionId, $answerId, &$count = null,
        $offset = 0, $limit = 3
    ) {
        if (isset($answerId)) {
            $queryStr = "SELECT comment
                FROM \Qolve\Entity\Comment comment
                    WHERE comment.questionId = '$questionId'
                    AND comment.answerId = '$answerId'
                    ORDER BY comment.writtenOn DESC";
        } else {
            $queryStr = "SELECT comment
                FROM \Qolve\Entity\Comment comment
                    WHERE comment.questionId = '$questionId'
                    AND comment.answerId is NULL
                    ORDER BY comment.writtenOn DESC";
        }

        $query = $this->getEntityManager()->createQuery($queryStr)
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $comments = $query->getResult();

        $countQueryStr = str_ireplace('SELECT comment',
            'SELECT COUNT(comment) num', $queryStr
        );
        $countQueryStr = str_ireplace(stristr($countQueryStr,
            'ORDER BY'), '', $countQueryStr
        );
        $countQuery    = $this->getEntityManager()
            ->createQuery($countQueryStr);
        $countResult   = $countQuery->getResult();
        $count         = isset($countResult[0]['num']) ?
             $countResult[0]['num'] : 0;

        return array_reverse($comments);
    }

    public function getPartialComments($questionId, $answerId = null)
    {
        $results  = array();
        $conds[]  = "c.questionId = '$questionId'";
        !empty($answerId) 
            ? $conds[]= "c.answerId = '$answerId'" 
            : $conds[]= 'c.answerId IS NULL';
        $queryStr = "SELECT c from Qolve\Entity\Comment c";
        $queryStr .= " WHERE " . join(' AND ', $conds)
            . " ORDER BY c.writtenOn DESC";
        $query    = $this->getEntityManager()
            ->createQuery($queryStr)
            ->setMaxResults(7);
        if ($answerId) {
            $query->useResultCache(true, LIFE_TIME, 'question_'.
                $questionId . '_answer_' . $answerId . '_comments'
            );
        } else {
            $query->useResultCache(true, LIFE_TIME,
                'question_'. $questionId . '_comments'
            );
        }
        $results = $query->getResult();
        return isset($results[0]) ? array_reverse($results) : array();
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "c.$key IS NULL";
            } else {
                $conditions[] = "c.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Comment c
                WHERE $conditions"
        );
        return $query->execute();
    }
}
