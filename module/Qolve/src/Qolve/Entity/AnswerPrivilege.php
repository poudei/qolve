<?php

namespace Qolve\Entity;

use Zend\Form\Annotation as Form,
    Doctrine\ORM\Mapping as ORM;

/**
 * AnswerPrivilege
 *
 * @ORM\Table(name="answer_privilege")
 * @ORM\Entity(repositoryClass="Qolve\Entity\AnswerPrivilegeRepository")
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="answerId")
 * @Application\ORM\Cacheable\Cacheable(type="answerPrivilege", lifetime="3600")
 */
class AnswerPrivilege
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $answerId;

    /**
     * Set userId
     *
     * @param  integer         $userId
     * @return AnswerPrivilege
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set answerId
     *
     * @param  integer         $answerId
     * @return AnswerPrivilege
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }
}
