<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    Qolve\Entity\KeywordFollow;

class KeywordFollowRepository extends EntityRepository
{
    public function getKeywordFollowers($keywordId, &$count,
        $offset = 0, $limit = 5
    ) {
//selecting shard : userId
//        $objectManager = $this->getEntityManager();
//        $objectManager
//            ->getConnection()
//            ->getShardManager()
//            ->selectShard($userId);

        $query = $this->getEntityManager()->createQuery(
            "SELECT  user
                FROM \Qolve\Entity\KeywordFollow keyFollow
                JOIN \Application\Entity\User user
                WITH keyFollow.userId = user.id
                WHERE keyFollow.keywordId = '$keywordId'"
        )
        ->setFirstResult($offset)
        ->setMaxResults($limit);

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT  COUNT(keyFollow) num
                FROM \Qolve\Entity\KeywordFollow keyFollow
                WHERE keyFollow.keywordId = '$keywordId'"
        );
        $countResult = $countQuery->getResult();
        $count       = isset($countResult[0]['num']) ?
            $countResult[0]['num'] : 0;

        return $query->getResult();
    }

    public function isFollowed($keywordId, $userId)
    {
        $result = array();
        $query  = $this->getEntityManager()->createQuery(
            "SELECT k FROM  Qolve\Entity\KeywordFollow k
                WHERE k.userId = '$userId'
                AND k.keywordId = '$keywordId'"
        );

        $query->useResultCache(true, LIFE_TIME,
            'user_'. $userId . '_keyword_' . $keywordId . '_follow'
        );
        $result = $query->getResult();
        if (   isset($result[0])
            && $result[0] instanceof KeywordFollow
        ) {
            $return = 1;
        } else {
            $return = 0;
        }

        return $return;
    }

    public function getuserkeywords($userId, &$count, $offset = null, $limit = null)
    {
        //TODO:add shard

        $query = $this->getEntityManager()->createQuery(
            "SELECT k.keywordId FROM Qolve\Entity\KeywordFollow k
                WHERE k.userId = '$userId'"
        )
        ->setFirstResult($offset)
        ->setMaxResults($limit);

        $result = $query->getResult();

        $query->useResultCache(true, LIFE_TIME,
            'user_'. $userId . '_keyword_follows'
        );

        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(k.keywordId) num FROM Qolve\Entity\KeywordFollow k
                WHERE k.userId = '$userId'"
        );

        $count = $query->getResult();
        $count = $count[0]['num'];
        return $result;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "f.$key IS NULL";
            } else {
                $conditions[] = "f.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\KeywordFollow f
                WHERE $conditions"
        );
        return $query->execute();
    }
}
