<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    Application\Entity\User;

class ShareRepository extends EntityRepository
{
    public function countByQuestionId($questionId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(share) num
                FROM \Qolve\Entity\Share share
                WHERE share.questionId = '$questionId'"
        );

        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function getQuestionShared($questionId)
    {
        $results = array();
        $return  = array();

        $query  = $this->getEntityManager()->createQuery(
            "SELECT share.userId FROM  Qolve\Entity\Share share
                WHERE share.questionId = '$questionId'"
        );

        $query->useResultCache(true, LIFE_TIME, 'question_'.
            $questionId . '_shares'
        );
        $results = $query->getResult();
        foreach ($results as $result) {
            $user = $this->getEntityManager()
                ->getRepository('Application\Entity\User')
                ->find($result['userId']);
            if ($user instanceof User) {
                $return[] = $user;
            }
        }
        return $return;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "s.$key IS NULL";
            } else {
                $conditions[] = "s.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Share s
                WHERE $conditions"
        );
        return $query->execute();
    }
}
