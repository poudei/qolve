<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Share
 *
 * @ORM\Table(name="share")
 * @ORM\Entity(repositoryClass="Qolve\Entity\ShareRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="questionId")
 * @Application\ORM\Cacheable\Cacheable(type="share", lifetime="3600")
 *
 * @Form\Name("share")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Share
{
    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $userId;

    /**
     * Set questionId
     *
     * @param  integer $questionId
     * @return Share
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set userId
     *
     * @param  integer $userId
     * @return Share
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        $cache->delete('question_' . $this->getQuestionId() . '_shares');
    }
}
