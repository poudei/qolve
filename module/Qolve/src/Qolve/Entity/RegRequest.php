<?php

namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation as Form;


/**
 * RegRequest
 *
 * @ORM\Table(name="reg_request")
 * @ORM\Entity(repositoryClass="Qolve\Entity\RegRequestRepository")
 */
class RegRequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="reg_request_id_seq")
     */
    private $id;

    /**
     * @var integer
     * @Form\Required(false)
     *
     * @ORM\Column(name="user_id", type="bigint",  nullable=true, unique=false)
     * @Form\Exclude()     
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"EmailAddress"})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "1", "max" : "100"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Filter({"name":"StringTrim"})          
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="education", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "1", "max" : "100"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Filter({"name":"StringTrim"})          
     */
    private $education;

    /**
     * @var integer
     *
     * @ORM\Column(name="gender", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"Digits"})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $gender;
    
    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "1", "max" : "255"}})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $timezone;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Validator({"name":"Digits"})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"Digits"})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=1000, precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "1", "max" : "1000"}})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $keywords;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="request_time", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"Date", "options":{"format" : "Y-m-d H:i:s"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Exclude()
     */
    private $requestTime;

    /**
     * Set id
     *
     * @param integer $id
     * @return RegRequest
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return RegRequest
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return RegRequest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RegRequest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set education
     *
     * @param string $education
     * @return RegRequest
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get education
     *
     * @return string 
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     * @return RegRequest
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer 
     */
    public function getGender()
    {
        return $this->gender;
    }
    
    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }
    
    /**
     * Set timezone
     *
     * @param strine $timezone
     * @return RegRequest
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return RegRequest
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return RegRequest
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }


    /**
     * Set keywords 
     *
     * @param string $keywords
     * @return RegRequest
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string 
     */
    public function getKeywords()
    {
        return $this->keywords;
    
    }

    /**
     * Set requestTime
     *
     * @param \DateTime $requestTime
     * @return RegRequest
     */
    public function setRequestTime($requestTime)
    {
        $this->requestTime = $requestTime;

        return $this;
    }

    /**
     * Get requestTime
     *
     * @return \DateTime 
     */
    public function getRequestTime()
    {
        return $this->requestTime;
    }
}
