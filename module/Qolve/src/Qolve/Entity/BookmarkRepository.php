<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository;

class BookmarkRepository extends EntityRepository
{
    public function getQuestionsOfList($listId, &$count, $offset = 0,
        $limit = 10
    ) {
        $bookmarks = $this->findBy(array('listId' => $listId), null,
            $limit, $offset
        );

        $questions = array();
        foreach ($bookmarks as $bookmark) {
            $questions[] = $this->getEntityManager()
                ->getRepository('Qolve\Entity\Question')
                ->find($bookmark->getQuestionId());
        }

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT COUNT(bookmark) num
                FROM \Qolve\Entity\Bookmark bookmark
                WHERE bookmark.listId = '$listId'"
        );

        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $questions;
    }

    public function getQuestionsOfBookmarks($userId, &$count,
        $offset = 0, $limit = 10
    ) {
        $query = $this->getEntityManager()->createQuery(
            "SELECT  bookmark
                FROM \Qolve\Entity\Bookmark bookmark
                JOIN \Qolve\Entity\BookmarkList list
                WITH bookmark.listId = list.id
                WHERE list.name = '" . BookmarkList::NAME_UNCATEGORIZED . "'
                AND list.userId = '$userId'"
        )
        ->setFirstResult($offset)
        ->setMaxResults($limit);

        $bookmarks = $query->getResult();

        $questions = array();
        foreach ($bookmarks as $bookmark) {
            $questions[] = $this->getEntityManager()
                ->getRepository('Qolve\Entity\Question')
                ->find($bookmark->getQuestionId());
        }

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT  COUNT(bookmark) num
                FROM \Qolve\Entity\Bookmark bookmark
                JOIN \Qolve\Entity\BookmarkList list
                WITH bookmark.listId = list.id
                WHERE list.name = '" . BookmarkList::NAME_UNCATEGORIZED . "'
                AND list.userId = '$userId'"
        );

        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $questions;
    }

    public function questionsCount($listId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(bookmark.questionId) questions
                FROM \Qolve\Entity\Bookmark bookmark
                WHERE bookmark.listId = '$listId'"
        );

        $result = $query->getResult();

        return isset($result[0]['questions']) ? $result[0]['questions'] : 0;
    }

    public function getPartialQuestionsOfList($listId)
    {
        $bookmarks = array();
        $query = $this->getEntityManager()->createQuery(
            "SELECT b from Qolve\Entity\Bookmark b
                WHERE b.listId = '$listId'"
        )
        ->setMaxResults(5);
        $query->useResultCache(true, LIFE_TIME,
            'list_'. $listId . '_bookmarks'
        );

        $bookmarks = $query->getResult();
        $questions = array();

        foreach ($bookmarks as $bookmark) {
            $questions[] = $this->getEntityManager()
                ->getRepository('Qolve\Entity\Question')
                ->find($bookmark->getQuestionId()
            );
        }

        return $questions;
    }

    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "b.$key IS NULL";
            } else {
                $conditions[] = "b.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Bookmark b
                WHERE $conditions"
        );
        return $query->execute();
    }
}
