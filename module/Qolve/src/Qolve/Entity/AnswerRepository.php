<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class AnswerRepository extends EntityRepository
{
    public function getAnswers($questionId, $offset = 0, $limit = 3)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT answer FROM \Qolve\Entity\Answer answer
                WHERE answer.questionId = '$questionId'
                ORDER BY answer.status DESC,
                answer.votes DESC,
                answer.createdOn ASC"
         )
         ->setFirstResult($offset)
         ->setMaxResults($limit);

        return $query->getResult();
    }

    //like getAnswers without limit and offset
    public function getPartialAnswers($questionId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT answer FROM \Qolve\Entity\Answer answer
                WHERE answer.questionId = '$questionId'
                ORDER BY answer.status DESC,
                answer.votes DESC,
                answer.createdOn ASC"
        )
        ->setFirstResult(0)
        ->setMaxResults(5);
        $query->useResultCache(true, LIFE_TIME, 'question_'.
            $questionId . '_answers'
        );

        return $query->getResult();
    }

    public function answersCount($questionId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(answer.id) answers
                FROM \Qolve\Entity\Answer answer
                WHERE answer.questionId = '$questionId'"
        );

        $result = $query->getResult();

        return isset($result[0]['answers']) ? $result[0]['answers'] : 0;
    }

    public function getUserAnswer($questionId, $userId)
    {
        $results = array();
        $status  = Asker::STATUS_ASKER;
        $query = $this->getEntityManager()->createQuery(
            "SELECT answer FROM  Qolve\Entity\Answer answer
                WHERE answer.questionId = '$questionId'
                AND answer.userId = '$userId'"
        );

        $query->useResultCache(true, LIFE_TIME, 'user_' . $userId . 
            '_question_'. $questionId . '_answer'
        );
        $result = $query->getResult();

        return isset($result[0]) ? $result[0] : null;

    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "a.$key IS NULL";
            } else {
                $conditions[] = "a.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Answer a
                WHERE $conditions"
        );
        return $query->execute();
    }
}
