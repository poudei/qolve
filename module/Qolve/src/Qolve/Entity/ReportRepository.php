<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    Application\Entity\User;

class ReportRepository extends EntityRepository
{
    public function getUserReport($userId, $questionId, $answerId = null)
    {
        if ($answerId == null) {
            $query = $this->getEntityManager()->createQuery(
                "SELECT r from Qolve\Entity\Report r
                    WHERE r.reporterId = '$userId'
                    AND r.questionId = '$questionId'
                    AND r.answerId IS NULL"
            );
            $query->useResultCache(true, LIFE_TIME,
                'user_' . $userId . '_question_'. $questionId . '_report'
            );
        } else {
            $query = $this->getEntityManager()->createQuery(
                "SELECT r from Qolve\Entity\Report r
                    WHERE r.reporterId = '$userId'
                    AND r.questionId = '$questionId'
                    AND r.answerId = '$answerId'"
            );
            $query->useResultCache(true, LIFE_TIME,
                'user_' . $userId . '_answer_'. $answerId . '_report'
            );
        }

        $result = $query->getResult();

        return isset($result[0]) ? $result[0] : null;
    }

    public function getReports($questionId, $answerId = null)
    {
        if ($answerId == null) {
            $query = $this->getEntityManager()->createQuery(
                "SELECT r.reporterId, r.questionId from Qolve\Entity\Report r
                    WHERE r.questionId = '$questionId'
                    AND r.answerId IS NULL"
            );
            $query->useResultCache(true, LIFE_TIME,
                'question_'. $questionId . '_reports'
            );
        } else {
            $query = $this->getEntityManager()->createQuery(
                "SELECT r.reporterId, r.answerId from Qolve\Entity\Report r
                    WHERE r.questionId = '$questionId'
                    AND r.answerId  IS NOT NULL"
            );
            $query->useResultCache(true, LIFE_TIME,
                'answer_'. $answerId . '_report'
            );
        }

        $result = $query->getResult();
        return isset($result[0]) ? $result : array();
    }

    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "r.$key IS NULL";
            } else {
                $conditions[] = "r.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Report r
                WHERE $conditions"
        );
        return $query->execute();
    }
}
