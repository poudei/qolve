<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository;

class QuestionFollowRepository extends EntityRepository
{
    public function getQuestionFollowers($questionId, &$count, $offset = 0,
        $limit = 5
    ) {
        $qFollows = $this->findBy(array('questionId' => $questionId),
            null, $limit, $offset
        );

        $_followers = array();
        foreach ($qFollows as $qFollow) {
            $_followers[] = $this->getEntityManager()
                ->getRepository('Application\Entity\User')
                ->find($qFollow->getUserId());
        }

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT  COUNT(questionFollow) num
                FROM \Qolve\Entity\QuestionFollow questionFollow
                WHERE  questionFollow.questionId = '$questionId'"
        );
        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $_followers;
    }

    public function followingsCount($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(follow.questionId) num
                FROM \Qolve\Entity\QuestionFollow follow
                WHERE follow.userId = '$userId'"
        );

        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function follow($userId, $questionId)
    {
        $follow = $this->find(array(
            'userId'     => $userId,
            'questionId' => $questionId
        ));
        if ($follow instanceof QuestionFollow) {
            return;
        }
        
        $isAsked = $this->getEntityManager()
            ->getRepository('Qolve\Entity\Question')
            ->isAsked($questionId, $userId);
        if ($isAsked) {
            return;
        }

        $follow = new QuestionFollow();
        $follow->setUserId($userId);
        $follow->setQuestionId($questionId);

        $this->getEntityManager()->persist($follow);
        $this->getEntityManager()->flush($follow);

        //Update UserPref
        $followerPref = $this->getEntityManager()
            ->getRepository('\Application\Entity\UserPref')
            ->getUserPref($userId, 'questionFollowingsCount');
        $followingsCount = $this->followingsCount($userId);
        $followerPref->setValue($followingsCount);

        $this->getEntityManager()->persist($followerPref);
        $this->getEntityManager()->flush($followerPref);

        return $follow;
    }

    public function unfollow($userId, $questionId)
    {
        $follow = $this->find(array(
            'userId'     => $userId,
            'questionId' => $questionId
        ));

        if (!$follow instanceof QuestionFollow) {
            return;
        }

        $this->getEntityManager()->remove($follow);
        $this->getEntityManager()->flush($follow);

        //Update UserPref
        $followerPref = $this->getEntityManager()
            ->getRepository('\Application\Entity\UserPref')
            ->getUserPref($userId, 'questionFollowingsCount');

        $followingsCount = $this->getEntityManager()
            ->getRepository('\Qolve\Entity\QuestionFollow')
            ->followingsCount($userId);

        $followerPref->setValue($followingsCount);

        $this->getEntityManager()->persist($followerPref);
        $this->getEntityManager()->flush($followerPref);

        return true;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "f.$key IS NULL";
            } else {
                $conditions[] = "f.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\QuestionFollow f
                WHERE $conditions"
        );
        return $query->execute();
    }
}
