<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository;

class RegRequestRepository extends EntityRepository
{
    public function getNotRegisteredRequests($offset = 0, $limit = 20, &$count)
    {
        //TODO: get from global shard
        
        $query = $this->getEntityManager()->createQuery("
            SELECT r FROM \Qolve\Entity\RegRequest r 
            WHERE  r.userId IS NULL
            ORDER BY r.requestTime DESC
        ");
        $query->setFirstResult($offset)
              ->setMaxResults($limit);
        
        $countQuery = $this->getEntityManager()->createQuery("
            SELECT count(r) num FROM \Qolve\Entity\RegRequest r 
            WHERE  r.userId IS NULL
        ");
        $countResult = $countQuery->getResult();
        
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;
        return $query->getResult();
    }
}
