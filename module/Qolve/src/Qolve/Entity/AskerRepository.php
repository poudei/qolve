<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    Qolve\Entity\Transaction,
    Qolve\Entity\TransactionLog,
    Application\Job\PublishFeed,
    Application\Job\SendNotification,
    Qolve\Job\Payment,
    Qolve\Entity\Asker,
    Application\Entity\User,
    Application\Entity\Feed,
    Application\Entity\Error,
    Qolve\Library\Constants;

class AskerRepository extends EntityRepository
{
    public function calculateFinalCredit($questionId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT SUM(asker.credit) credits
                FROM \Qolve\Entity\Asker asker
                WHERE asker.questionId = '$questionId'"
        );

        $result = $query->getResult();

        return isset($result[0]['credits']) ? $result[0]['credits'] : 0;
    }

    public function calculateFinalAnswer($questionId)
    {
        $askers = $this->getEntityManager()
            ->getRepository('\Qolve\Entity\Asker')
            ->findByQuestionId($questionId);

        $answers = array();
        foreach ($askers as $asker) {
            if ($asker->getAnswerId()) {
                $answerId = $asker->getAnswerId();

                if (!isset($answers[$answerId])) {
                    $answers[$answerId] = 0;
                }
                !is_null($asker->getCredit()) ?
                    $credit = $asker->getCredit() :
                    $credit = 0;
                $answers[$answerId] += $credit;
            }
        }

        $finalAnswer = null;
        $maxScore   = -1;
        foreach ($answers as $answerId => $score) {
            if ($score > $maxScore) {
                $finalAnswer = $answerId;
                $maxScore    = $score;
            }
        }

        return $finalAnswer;
    }

    public function getAskersCount($questionId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(asker.questionId) askers
                FROM \Qolve\Entity\Asker asker
                WHERE asker.questionId = '$questionId'"
        );

        $result = $query->getResult();

        return isset($result[0]['askers']) ? $result[0]['askers'] : 0;
    }

    public function getQuestionsOfUser($userId, &$count, $offset = 0,
        $limit = 10
    ) {
        $query = $this->getEntityManager()->createQuery(
            "SELECT  question
                FROM \Qolve\Entity\Asker asker
                JOIN \Qolve\Entity\Question question
                WITH asker.questionId = question.id
                WHERE  asker.userId = '$userId'"
        )
        ->setFirstResult($offset)
        ->setMaxResults($limit);

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT  COUNT(asker) num
                FROM \Qolve\Entity\Asker asker
                WHERE  asker.userId = '$userId'"
        );
        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $query->getResult();
    }

    public function countAcceptsOfAnswer($answerId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(a) num
                FROM \Qolve\Entity\Asker a
                    WHERE a.answerId='$answerId'"
        );

        $result = $query->getResult();

        return isset($result[0]['num']) ? (int) $result[0]['num'] : 0;
    }

    public function countBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $property => $value) {
            if ($value === null) {
                $conditions[] = "asker.$property IS NULL";
            } else {
                $conditions[] = "asker.$property = '$value'";
            }
        }

        $queryStr = "SELECT COUNT(asker) num
            FROM \Qolve\Entity\Asker asker ";
        if (!empty($conditions)) {
            $queryStr .= "WHERE " . join(' AND ', $conditions);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);
        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "a.$key IS NULL";
            } else {
                $conditions[] = "a.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);

        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Asker a
                WHERE $conditions"
        );
        return $query->execute();
    }

    public function raiseCredit(
        $serviceLocator, 
        $credit, 
        $question, 
        $user, 
        $cardToken, 
        $cardId,
        $userPaid,
        $qolveShare,
        $partnerShare,
        $partnerName,
        $device = 1
    ) {
        $questionId = $question->getId();
        $objectManager = $this->getEntityManager();

        $hydrator = $serviceLocator->get('Hydrator');
        $userId   = $user->getId();

        $asker = $objectManager->getRepository('Qolve\Entity\Asker')
            ->findOneBy(
                [
                    'questionId' => $questionId,
                    'userId'     => $userId
                ]
            );
       
       $preData = $hydrator->extract($asker);

       if ($credit <= $preData['credit']) {
            return [
                'error' => [
                    'message' => Error::CreditIsLow_message,
                    'code'    => Error::CreditIsLow_code
                ]
            ];
        }

        //charge money for stripe
        if ($credit < 0) {
            return [
                'error' => [
                    'message' => Error::NotHaveEnoughCredit_message,
                    'code'    => Error::NotHaveEnoughCredit_code
                ]
            ];
        }

        $added = $credit - $preData['credit'];

        $customerId = $user->getCustomerId();
        $now  = new \DateTime('now', new \DateTimeZone('UTC'));

        if (   ($device != Constants::IOS_DEVICE)
            && ($device != Constants::ANDROID_DEVICE)
        ) {

            $payment    = $serviceLocator->get('payment');

            //charge amount with stripe
            $charge = $payment->charge(
                $user,
                $cardToken,
                $cardId,
                $added
            );

            if(!$charge) {
              return [
                    'error' => [
                        'message' => Error::ChargeProblem_message,
                        'code'    => Error::ChargeProblem_code
                    ]
                ];
            }
        } else {

            $transactionLog = $objectManager
                ->getRepository('Qolve\Entity\TransactionLog')
                ->findOneBy(
                    [
                        'questionId' => $questionId,
                        'userId'     => $userId
                    ]
            );  

            if (!$transactionLog instanceof TransactionLog) {
                  return [
                    'error' => [
                        'message' => Error::Problem_in_tranaction_history_message,
                        'code'    => Error::Problem_in_tranaction_history_code
                    ]
                ];  
            }
            
            $transactionLog->setUserId($asker->getUserId());
            $transactionLog->setUserPaid($userPaid);
            $transactionLog->setQolveShare($qolveShare);
            $transactionLog->setPartnerName($partnerName);
            $transactionLog->setPartnerShare($partnerShare);
            $transactionLog->setQuestionCost($credit);
            $transactionLog->setQuestionId($questionId);
            $transactionLog->setCreatedOn($now);

            if ($device == Constants::IOS_DEVICE) {
                $transactionLog->setStatus(Constants::PAY_FROM_IOS_TO_QOLVE);
            } else {
                $transactionLog->setStatus(Constants::PAY_FROM_GOOGLE_TO_QOLVE);
            }

            $objectManager->persist($transactionLog);
        }

        $asker->setCredit($credit);
        $objectManager->persist($asker);

        $plus = $asker->getCredit() - $preData['credit'];

        $finalCredit = $objectManager
            ->getRepository('\Qolve\Entity\Asker')
            ->calculateFinalCredit($questionId) + $plus;
        $question->setCredits($finalCredit);

        // *** Create Transaction & Calculate User's Credit *** //
        if ($plus > 0) {
            $notes = json_encode(
                array(
                    'questionId' => $question->getId()
                )
            );

            $transaction = new Transaction();
            $transaction->setUserId($asker->getUserId());
            $transaction->setDebit($plus);
            $transaction->setStatus(Transaction::STATUS_BLOCKED);
            $transaction->setNotes($notes);
            $transaction->setCreatedOn($now);
            $objectManager->persist($transaction);
//            $userCredit = $objectManager
//                ->getRepository('Qolve\Entity\Transaction')
//                ->calculateUserCredit($asker->getUserId());
//            $userPref = $objectManager
//                ->getRepository('Application\Entity\UserPref')
//                ->findOneBy(
//                    array(
//                        'userId' => $asker->getUserId(),
//                        'key'    => 'credit'
//                    )
//                );
//            $userPref->setValue($userCredit - $plus);
        }

        $this->elasticSearch($questionId, $serviceLocator);

        $objectManager->flush();

        $preBestAnswer = $objectManager
            ->getRepository('\Qolve\Entity\Answer')
            ->findOneBy(array(
                'questionId' => $questionId,
                'status'     => Answer::STATUS_BEST
            )
        );
        if ($preBestAnswer instanceof Answer) {
            $accepts = $objectManager
                ->getRepository('Qolve\Entity\Asker')
                ->countAcceptsOfAnswer($preBestAnswer->getId());
            if ($accepts == 0) {
                $preBestAnswer->setStatus(Answer::STATUS_NORMAL);
            } else {
                $preBestAnswer->setStatus(Answer::STATUS_ACCEPTED);
            }
        }

        $finalAnswerId = $objectManager
            ->getRepository('\Qolve\Entity\Asker')
            ->calculateFinalAnswer($questionId);
        $question->setAnswerId($finalAnswerId);

        if (!is_null($finalAnswerId)) {
            $finalAnswer = $objectManager
                ->getRepository('\Qolve\Entity\Answer')
                ->find($finalAnswerId);
            if ($finalAnswer instanceof Answer) {
                $finalAnswer->setStatus(Answer::STATUS_BEST);
            }
        }


        //delete previous feeds and notifications
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'fromId'     => $asker->getUserId(),
                    'questionId' => $question->getId(),
                    'verb'       => 'RaiseCredit'
                )
            );

        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'actorId'    => $asker->getUserId(),
                    'questionId' => $question->getId(),
                    'verb'       => 'RaiseCredit'
                )
            );

        $objectManager->flush();


        //Add to user's Activities
        $feedPrivacy = max(
            array(
                $question->getPrivacy(), $asker->getVisibility()
            )
        );

        $feed = new Feed();
        $feed->setUserId($userId);
        $feed->setActorId($userId);
        $feed->setQuestionId($question->getId());
        $feed->setPrivacy($feedPrivacy);
        $feed->setVerb('RaiseCredit');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);
        $objectManager->flush();

        // Update question activity in elasticsearch
        $params = array(
            'question_id' => $question->getId(),
            'credits'     => $question->getCredits()
        );

        $objectManager
            ->getRepository('Qolve\Entity\Question')
            ->setQuestionActivity(
                $serviceLocator,
                $params
        );
        //Add to Queue for publish Feeds
        if (   $question->getPrivacy() == Question::PRIVACY_PUBLIC
            && $asker->getVisibility() == Asker::VISIBILITY_PUBLIC
        ) {
            $queue = $serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('feeds');

            $data = array(
                'verb'       => '\Qolve\Verb\RaiseCredit',
                'userId'     => $userId,
                'questionId' => $question->getId()
            );

            $data['to'] = 'userFollowers';
            $job = new PublishFeed($serviceLocator);
            $job->setContent($data);
            $queue->push($job);
        }

        //Add to Queue for send Notification
        $queue = $serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'       => '\Qolve\Verb\RaiseCredit',
            'userId'     => $userId,
            'questionId' => $question->getId()
        );

        $data['to'] = 'askers'; //high
        $job = new SendNotification($serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        $data['to'] = 'questionFollowers';
        $job = new SendNotification($serviceLocator);
        $job->setContent($data);
        $queue->push($job);
        $return = [
            'asker'  => $asker,
            'charge' => $added
        ];
        return $return;
    }

    public function pickBestAnswer($user, $question, $answer, $serviceLocator)
    {
        $objectManager = $this->getEntityManager();

        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        $asker = $objectManager->getRepository('Qolve\Entity\Asker')
            ->find(array(
                'userId'     => $user->getId(),
                'questionId' => $question->getId()
            ));
        if (!$asker instanceof Asker) {
            return array(
                'error' => array(
                    'code'    => Error::PermissionDenied_code,
                    'message' => Error::PermissionDenied_message
                )
            );
        }
        $visibility  = $asker->getVisibility();
        $preAnswerId = $asker->getAnswerId();

        if (!is_null($preAnswerId)) {
            similar_text($answer->getId(), $asker->getAnswerId(), $percent);
            if ($percent == 100) {
                return $asker;
            }
        }

        $asker->setAnswerId($answer->getId());
        $objectManager->flush();

        // Update Question Activity in Elastic
        $askers   = array();
        $accepted = array();
        $askers   = $objectManager
            ->getRepository('Qolve\Entity\Asker')
            ->findByQuestionId($question->getId());
        foreach ($askers as $asker) {
            if ($asker instanceof Asker) {
                if (!is_null($asker->getAnswerId())) {
                    $accepted[] = $asker->getAnswerId();
                }
            }
        }

        $params = array(
            'question_id' => $question->getId(),
            'accepted'    => $accepted
        );

        $objectManager
            ->getRepository('Qolve\Entity\Question')
            ->setQuestionActivity(
                $serviceLocator,
                $params
        );

        if (!is_null($preAnswerId)) {
            $accepts = $this->countAcceptsOfAnswer($preAnswerId);
            if ($accepts == 0) {
                $preAnswer = $objectManager
                    ->getRepository('Qolve\Entity\Answer')
                    ->find($preAnswerId);
                if ($preAnswer instanceof Answer) {
                    $preAnswer->setStatus(Answer::STATUS_NORMAL);
                }
            }
        }

        $answer->setStatus(Answer::STATUS_ACCEPTED);

        $preBestAnswer = $objectManager
            ->getRepository('\Qolve\Entity\Answer')
            ->findOneBy(
                array(
                    'questionId'    => $question->getId(),
                    'status'        => Answer::STATUS_BEST
                )
            );
        if ($preBestAnswer instanceof Answer) {
            $accepts = $this->countAcceptsOfAnswer($preBestAnswer->getId());
            if ($accepts == 0) {
                $preBestAnswer->setStatus(Answer::STATUS_NORMAL);
            } else {
                $preBestAnswer->setStatus(Answer::STATUS_ACCEPTED);
            }
        }

        $askerNotPickedBestAnswer = $this->findOneBy(
            array(
                'questionId' => $question->getId(),
                'answerId'   => null
            )
        );

        if (!$askerNotPickedBestAnswer instanceof Asker) {
            $question->setStatus(Question::STATUS_CLOSED);
            $question->setPaid(Question::PAID_INPROGRESS);
            // Update Elastic
            $objectManager->persist($question);
            $objectManager->flush($question);

            $elastica = $serviceLocator
                ->get('Elastica\Client');

            $elasticaClient = $elastica['client'];
            $index          = $elastica['index'];
            $elasticaIndex  = $elasticaClient->getIndex($index);
            $elasticaType   = $elasticaIndex->getType('question');

            $param = array (
                'status' => $question->getStatus(),
                'paid'   => $question->getPaid(),
            );

            $document = $elasticaType->getDocument($question->getId());

            $document->setData($param);
            $elasticaType->updateDocument($document);
            $elasticaType->getIndex()->refresh();

            // Update Question Activity in Elastic

            $params = array(
                'question_id' => $question->getId(),
                'status'      => $question->getStatus(),
                'paid'        => $question->getPaid()

            );

            $objectManager
                ->getRepository('Qolve\Entity\Question')
                ->setQuestionActivity(
                    $serviceLocator,
                    $params
            );

            //Add to Queue for payment
            $job = new Payment($serviceLocator);
            $job->setContent(
                array(
                    'questionId' => $question->getId()
                )
            );

            $queue = $serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('payment');
            $queue->push($job);
        }

        $finalAnswerId = $this->calculateFinalAnswer($question->getId());
        $question->setAnswerId($finalAnswerId);

        if (!is_null($finalAnswerId)) {
            $finalAnswer = $objectManager
                ->getRepository('\Qolve\Entity\Answer')
                ->find($finalAnswerId);
            if ($finalAnswer instanceof Answer) {
                $finalAnswer->setStatus(Answer::STATUS_BEST);

                //Update Question Activity IN Elastic
                $params = array(
                    'question_id' => $question->getId(),
                    'best_answer' => (string)$finalAnswerId
                );

                $objectManager
                    ->getRepository('Qolve\Entity\Question')
                    ->setQuestionActivity(
                        $serviceLocator,
                        $params
                );

                $transactionLog = $objectManager
                    ->getRepository('Qolve\Entity\TransactionLog')
                    ->findByQuestionId($question->getId());

                foreach($transactionLog as $row) {
                    $row->setSolverId($finalAnswer->getUserId());
                    $objectManager->persist($row);
                }
            }
        }


        //delete previous feeds and notifications
        $objectManager->getRepository('Application\Entity\Notification')
            ->deleteBy(
                array(
                    'fromId'     => $asker->getUserId(),
                    'questionId' => $asker->getQuestionId(),
                    'answerId'   => $asker->getAnswerId(),
                    'verb'       => 'PickBestAnswer'
                )
            );

        $objectManager->getRepository('Application\Entity\Feed')
            ->deleteBy(
                array(
                    'actorId'    => $asker->getUserId(),
                    'questionId' => $asker->getQuestionId(),
                    'answerId'   => $asker->getAnswerId(),
                    'verb'       => 'PickBestAnswer'
                )
            );

        $objectManager->flush();


        //Add to user's Activities
        $feedPrivacy = max(
            array(
                $question->getPrivacy(), $asker->getVisibility()
            )
        );

        $feed = new Feed();
        $feed->setUserId($user->getId());
        $feed->setActorId($user->getId());
        $feed->setQuestionId($question->getId());
        $feed->setAnswerId($answer->getId());
        $feed->setPrivacy($feedPrivacy);
        $feed->setVerb('PickBestAnswer');
        $feed->setCreatedOn($now);
        $objectManager->persist($feed);

        $objectManager->flush();

        //Add to Queue for send Notification
        $queue = $serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('notifications');

        $data = array(
            'verb'       => '\Qolve\Verb\PickBestAnswer',
            'userId'     => $user->getId(),
            'questionId' => $question->getId(),
            'answerId'   => $answer->getId()
        );

        $data['to'] = 'askers'; //high
        $job = new SendNotification($serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        $data['to'] = 'questionFollowers';
        $job = new SendNotification($serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        $data['to'] = 'answerWriter';
        $job = new SendNotification($serviceLocator);
        $job->setContent($data);
        $queue->push($job);

        $userId = $answer->getUserId();
        if (!empty($userId)) {

            $to = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($userId);
            $document = $objectManager
                ->getRepository('Qolve\Entity\Document')
                ->findBy(array(
                    'questionId' => $question->getId(),
                    'answerId'     => $answer->getId()
            ));

            if (   $visibility
                == Asker::VISIBILITY_ANONYMOUS
               ) {
                $askerName = Asker::ANONYMOUS_NAME;
                $subject   = Asker::ANONYMOUS_NAME;

            } else {
                $askerName = $user->getName();
                $subject   = $user->getUsername();
            }

            $config = $serviceLocator->get('config');
            $emailQueue = $serviceLocator
                ->get('\SlmQueue\Queue\QueuePluginManager')
                ->get('email');

            //Add to Queue for send Email
            $job = new \Application\Job\SendEmail($serviceLocator);
            $job->setContent(array(
                'to'        => $to->getEmail(),
                'subject'   => $subject . " accepted your solution on his problem",
                'template'  => 'AcceptSolution',
                'params'    => array(
                    'solver'     => $to->getName(),
                    'asker'      => $askerName,
                    'gender'     => $user->getGender(),
                    'questionId' => $question->getId(),
                    'answerId'   => $answer->getId(),
                    'baseUrl'    => $config['baseUrl']
                    )
            ));
            $emailQueue->push(
                $job,
                array('delay' => new \DateInterval("PT30S"))
            );

        }
        return $asker;
    }

    private function elasticSearch($questionId, $serviceLocator)
    {
        $objectManager = $this->getEntityManager();

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
            ->find($questionId);
        $elastica       = $serviceLocator->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $param = array('credits' => $question->getCredits());

        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('question');
        $document      = $elasticaType->getDocument($questionId);

        $document->setData($param);
        $elasticaType->updateDocument($document);
        $elasticaType->getIndex()->refresh();

    }

}
