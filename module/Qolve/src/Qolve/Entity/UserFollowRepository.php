<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    Qolve\Entity\UserFollow,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class UserFollowRepository extends EntityRepository
{
    public function followersCount($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(follow.followerId) num
                FROM \Qolve\Entity\UserFollow follow
                WHERE follow.userId = '$userId'"
        );

        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function followingsCount($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(follow.userId) num
                FROM \Qolve\Entity\UserFollow follow
                WHERE follow.followerId = '$userId'"
        );

        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function isFollowed($userId, $currentUserId)
    {
        $result = array();

        $query = $this->getEntityManager()->createQuery(
            "SELECT userFollow FROM Qolve\Entity\UserFollow userFollow
                 WHERE userFollow.userId = '$userId'
                 AND userFollow.followerId = '$currentUserId'"
        );
        $query->useResultCache(true, LIFE_TIME, 'user_'. $currentUserId .
            '_following_' . $userId . '_follow'
        );

        $result = $query->getResult();
        if (   isset($result[0])
            && $result[0] instanceof UserFollow) {
            $return = 1;
        } else {
            $return = 0;
        }

        return $return;
    }

    public function getFollowers($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT f.followerId FROM Qolve\Entity\UserFollow f
                 WHERE f.userId = '$userId'"
        );
        $query->useResultCache(true, LIFE_TIME, 'user_'.
            $userId . '_followers'
        );

        $result = $query->getResult();

        $followers = array();
        foreach ($result as $follow) {
            $followers[] = $this->getEntityManager()
                ->getRepository('Application\Entity\User')
                ->find($follow['followerId']);
        }

        return $followers;
    }

    public function getFollowings($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT f.userId FROM Qolve\Entity\UserFollow f
                 WHERE f.followerId = '$userId'"
        );
        $query->useResultCache(true, LIFE_TIME,
            'user_'. $userId . '_followings'
        );

        $result = $query->getResult();

        $followings = array();
        foreach ($result as $follow) {
            $followings[] = $this->getEntityManager()
                ->getRepository('Application\Entity\User')
                ->find($follow['userId']);
        }

        return $followings;
    }

    public function getRelatedUsers($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT f.userId, f.followerId
                FROM Qolve\Entity\UserFollow f
                WHERE f.userId  = '$userId'
                OR f.followerId = '$userId'"
        );

        $query->useResultCache(true, LIFE_TIME,
            'user_'. $userId . '_follows'
        );
        $result = $query->getResult();

        $ids = array();
        foreach ($result as $follow) {
            similar_text($follow['userId'], $userId, $percent);
            if ($percent == 100) {
                $ids[] = $follow['followerId'];
            } else {
                $ids[] = $follow['userId'];
            }
        }
        $ids = array_unique($ids);

        $users = array();
        foreach ($ids as $id) {
            $users[] = $this->getEntityManager()
                ->getRepository('Application\Entity\User')
                ->find($id);
        }

        return $users;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "f.$key IS NULL";
            } else {
                $conditions[] = "f.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\UserFollow f
                WHERE $conditions"
        );
        return $query->execute();
    }
}
