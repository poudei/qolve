<?php
namespace Qolve\Entity;

use Application\Entity\User,
    Doctrine\ORM\EntityRepository;

class BookmarkListRepository extends EntityRepository
{
    public function getLists($userId, $order = null, $currentUser = null)
    {
        if (!is_null($userId) && $currentUser instanceof User) {
            similar_text($userId, $currentUser->getId(), $percent);
            if ($percent == 100) {
                return $this->getSelfLists($userId, $order);
            }
        }
        
        $public = BookmarkList::PRIVACY_PUBLIC;

        $conditions = array();
        if ($userId) {
            $conditions[] = "list.userId = '$userId'";
        }
        if ($currentUser instanceof User) {
            $conditions[] = "(list.privacy = $public
                OR list.userId = '".$currentUser->getId()."')";
        } else {
            $conditions[] = "list.privacy = $public";
        }
        $conditions[] = "list.name <> '" .
            BookmarkList::NAME_UNCATEGORIZED . "'";
        $where = join(' AND ', $conditions);

        $_order = array();
        foreach ($order as $orderBy => $orderDir) {
            switch ($orderDir) {
            case -1:
            case 'desc':
            case 'DESC':
                $dir = 'DESC';
                break;
            case 1:
            case 'asc':
            case 'ASC':
                $dir = 'ASC';
                break;
            default:
                $dir = 'DESC';
            }

            $_order[] = "list.$orderBy $dir";
        }

        $queryStr = "SELECT list
            FROM \Qolve\Entity\BookmarkList list WHERE $where";

        if (!empty($_order)) {
            $queryStr .= " ORDER BY " . join(',', $_order);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);

        return $query->getResult();
    }
    
    public function getUserUncatList($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT l FROM \Qolve\Entity\BookmarkList l 
                WHERE l.userId = '$userId' 
                    AND l.name = '" . BookmarkList::NAME_UNCATEGORIZED . "'"
        );
        $query->setMaxResults(1);
        
        $query->useResultCache(
            true, LIFE_TIME, 'user_' . $userId . '_list_uncategorized'
        );
        
        $result = $query->getResult();
        return isset($result[0]) ? $result[0] : null;
    }

    public function followsCount($listId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT COUNT(follow.listId) follows
                FROM \Qolve\Entity\BookmarkListFollow follow
                WHERE follow.listId = '$listId'"
        );

        $result = $query->getResult();

        return isset($result[0]['follows']) ? $result[0]['follows'] : 0;
    }

    public function hasPrivateQuestion($listId)
    {
        $bookmarks = $this->getEntityManager()
            ->getRepository('Qolve\Entity\Bookmark')
            ->findByListId($listId);

        foreach ($bookmarks as $bookmark) {
            $question = $this->getEntityManager()
                ->getRepository('Qolve\Entity\Question')
                ->fine($bookmark->getQuestionId());
            if ($question->getPrivacy() == Question::PRIVACY_SHARED) {
                return true;
            }
        }

        return false;
    }

    public function getListKeywords($listId)
    {
        //TODO: cache
        $_keywords = array();

        $query = $this->getEntityManager()->createQuery("
            SELECT bookmark
                FROM \Qolve\Entity\Bookmark bookmark
                WHERE bookmark.listId = '$listId'"
        );

        $query->useResultCache(true, LIFE_TIME, 'list_'. $listId .
            '_bookmarks'
        );

        $bookmarks = $query->getResult();
        foreach ($bookmarks as $bookmark) {
            $query = $this->getEntityManager()->createQuery("
                SELECT keyword
                    FROM \Qolve\Entity\Keyword keyword
                    JOIN \Qolve\Entity\QuestionKeyword questionKey
                    WITH keyword.id = questionKey.keywordId
                    WHERE questionKey.questionId = '" .
                        $bookmark->getQuestionId() . "'"
            );

            $query->useResultCache(true, LIFE_TIME, 'question_'.
                $bookmark->getQuestionId() . '_keywords'
            );

            $keywords = $query->getResult();
            foreach ($keywords as $keyword) {
                $_keywords[] = $keyword->getName();
            }
        }

        $_keywords = array_values(array_unique($_keywords));
        sort($_keywords);

        return $_keywords;
    }

    public function countTotalListsOfUser($userId)
    {
        //TODO: cache
        $query = $this->getEntityManager()->createQuery("
            SELECT COUNT(list) num
                FROM \Qolve\Entity\BookmarkList list
                WHERE list.userId = '$userId'"
        );
        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function countPublicListsOfUser($userId)
    {
        //TODO: cache
        $query = $this->getEntityManager()->createQuery("
            SELECT COUNT(list) num
                FROM \Qolve\Entity\BookmarkList list
                WHERE list.userId = '$userId'
                AND list.privacy = " . BookmarkList::PRIVACY_PUBLIC
        );
        $query->useResultCache(true, LIFE_TIME,
            'user_'. $userId . '_countPublicLists'
        );

        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }
    
    
    private function getSelfLists($userId, $order = null)
    {   
        $objectManager = $this->getEntityManager();
        
        $defaultListId = $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->getUserKey($userId, 'defaultBookmarkListId');
        
        if (!is_null($defaultListId)) {
            $defaultList = $objectManager
                ->getRepository('Qolve\Entity\BookmarkList')
                ->find($defaultListId);
        } else {
            $defaultListId = '';
            $defaultList   = null;
        }
        
        $_order = array();
        foreach ($order as $orderBy => $orderDir) {
            switch ($orderDir) {
            case -1:
            case 'desc':
            case 'DESC':
                $dir = 'DESC';
                break;
            case 1:
            case 'asc':
            case 'ASC':
                $dir = 'ASC';
                break;
            default:
                $dir = 'DESC';
            }

            $_order[] = "l.$orderBy $dir";
        }

        $queryStr = "SELECT l
            FROM \Qolve\Entity\BookmarkList l
                WHERE l.userId  = '$userId' 
                    AND l.id   <> '$defaultListId' 
                    AND l.name <> '" . BookmarkList::NAME_UNCATEGORIZED . "'";

        if (!empty($_order)) {
            $queryStr .= " ORDER BY " . join(' , ', $_order);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);
        
        $lists = $query->getResult();
        
        //Add uncategorized list
        $uncatList = $objectManager->getRepository('Qolve\Entity\BookmarkList')
            ->getUserUncatList($userId);
        if (!$uncatList instanceof BookmarkList) {
            $uncatList = new BookmarkList();
            $uncatList->setId(
                BookmarkList::NAME_UNCATEGORIZED
            );
            $uncatList->setName(
                BookmarkList::NAME_UNCATEGORIZED
            );
        }
        array_unshift($lists, $uncatList);
        
        //Add default list
        if ($defaultList instanceof BookmarkList) {
            similar_text($defaultList->getId(), $uncatList->getId(), $percent);
            if ($percent < 100) {
                array_unshift($lists, $defaultList);
            }
        }
        
        return $lists;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "l.$key IS NULL";
            } else {
                $conditions[] = "l.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\BookmarkList l
                WHERE $conditions"
        );
        return $query->execute();
    }
}
