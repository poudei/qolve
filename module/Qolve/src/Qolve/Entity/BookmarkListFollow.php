<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Bookmark List Follow
 *
 * @ORM\Table(name="bookmark_list_follow")
 * @ORM\Entity(repositoryClass="Qolve\Entity\BookmarkListFollowRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="bookmarkListFollow", lifetime="3600")

 * @Form\Name("bookmarkListFollow")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class BookmarkListFollow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="list_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $listId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * Set listId
     *
     * @param  integer            $listId
     * @return BookmarkListFollow
     */
    public function setListId($listId)
    {
        $this->listId = $listId;

        return $this;
    }

    /**
     * Get listId
     *
     * @return integer
     */
    public function getListId()
    {
        return $this->listId;
    }

    /**
     * Set userId
     *
     * @param  integer        $userId
     * @return QuestionFollow
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        $cache->delete('user_' . $this->getUserId() . '_list_' . $this->getListId() . '_follow');
    }
}
