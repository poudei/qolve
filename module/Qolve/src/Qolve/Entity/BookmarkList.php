<?php

namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Bookmark List
 *
 * @ORM\Table(name="bookmark_list")
 * @ORM\Entity(repositoryClass="Qolve\Entity\BookmarkListRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="bookmarkList", lifetime="3600")
 * 
 * @Form\Name("bookmarkList")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class BookmarkList
{
    const NAME_UNCATEGORIZED = 'Uncategorized';

    const PRIVACY_PUBLIC  = 1;
    const PRIVACY_PRIVATE = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="`name`", type="string", length=255, nullable=false)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "0", "max" : "255"}})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="privacy", type="smallint", nullable=false)
     * @Form\Validator({"name":"Digits"})
     */
    private $privacy;

    /**
     * @var integer
     *
     * @ORM\Column(name="questions", type="integer", nullable=false)
     *
     * @Form\Exclude()
     */
    private $questions;

    /**
     * @var integer
     *
     * @ORM\Column(name="follows", type="integer", nullable=false)
     *
     * @Form\Exclude()
     */
    private $follows;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     *
     * @Form\Exclude()
     */
    private $createdOn;

    /**
     * Set id
     *
     * @param  integer      $id
     * @return BookmarkList
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string       $name
     * @return BookmarkList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set userId
     *
     * @param  integer      $userId
     * @return BookmarkList
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set privacy
     *
     * @param  integer      $privacy
     * @return BookmarkList
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return integer
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set questions
     *
     * @param  integer      $questions
     * @return BookmarkList
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Get questions
     *
     * @return integer
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set follows
     *
     * @param  integer      $follows
     * @return BookmarkList
     */
    public function setFollows($follows)
    {
        $this->follows = $follows;

        return $this;
    }

    /**
     * Get follows
     *
     * @return integer
     */
    public function getFollows()
    {
        return $this->follows;
    }

    /**
     * Set createdOn
     *
     * @param  \DateTime    $createdOn
     * @return BookmarkList
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        
        $cache->delete('user_' . $this->getUserId() . '_countPublicLists');
        
        if ($this->getName() == self::NAME_UNCATEGORIZED) {
            $cache->delete(
                'user_' . $this->getUserId() . '_list_uncategorized'
            );
        }
    }
}
