<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository;
class DocumentRepository extends EntityRepository
{
    public function countBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $property => $value) {
            if ($value === null) {
                $conditions[] = "doc.$property IS NULL";
            } else {
                $conditions[] = "doc.$property = '$value'";
            }
        }

        $queryStr = "SELECT COUNT(doc) num
            FROM \Qolve\Entity\Document doc ";
        if (!empty($conditions)) {
            $queryStr .= "WHERE " . join(' AND ', $conditions);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);
        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function getDocuments($questionId, $answerId = null)
    {
        if (empty($answerId)) {
            $query = $this->getEntityManager()->createQuery(
                "SELECT d from Qolve\Entity\Document d
                    WHERE d.questionId = '$questionId'
                    AND d.answerId IS NULL 
                    ORDER BY d.order"
            );
            $query->useResultCache(true, LIFE_TIME,
                'question_'. $questionId . '_documents'
            );
        } else {
            $query = $this->getEntityManager()->createQuery(
                "SELECT d from Qolve\Entity\Document d
                    WHERE d.questionId = '$questionId'
                    AND d.answerId = '$answerId' 
                    ORDER BY d.order"
            );
            $query->useResultCache(true, LIFE_TIME,
                'answer_'. $answerId . '_documents'
            );
        }

        $results = $query->getResult();

        return isset($results[0]) ? $results : array();
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "d.$key IS NULL";
            } else {
                $conditions[] = "d.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Document d
                WHERE $conditions"
        );
        return $query->execute();
    }
}
