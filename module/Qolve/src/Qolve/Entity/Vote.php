<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Vote
 *
 * @ORM\Table(name="vote")
 * @ORM\Entity(repositoryClass="Qolve\Entity\VoteRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="questionId")
 * @Application\ORM\Cacheable\Cacheable(type="vote", lifetime="3600")
 *
 * @Form\Name("vote")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Vote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $answerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="smallint", nullable=false)
     * @Form\Validator({"name":"\Zend\I18n\Validator\Int"})
     * @Form\Required(true)
     */
    private $score;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="voted_on", type="datetime", nullable=true)
     *
     * @Form\Exclude()
     */
    private $votedOn;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionId
     *
     * @param  integer $questionId
     * @return Vote
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set answerId
     *
     * @param  integer $answerId
     * @return Vote
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * Set userId
     *
     * @param  integer $userId
     * @return Vote
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set score
     *
     * @param  integer $score
     * @return Vote
     */
    public function setScore($score)
    {
        if ($score == -1 || $score == 1) {
            $this->score = $score;
        }

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set votedOn
     *
     * @param  \DateTime $votedOn
     * @return Vote
     */
    public function setVotedOn($votedOn)
    {
        $this->votedOn = $votedOn;

        return $this;
    }

    /**
     * Get votedOn
     *
     * @return \DateTime
     */
    public function getVotedOn()
    {
        return $this->votedOn;
    }

    /**
     * Set id
     *
     * @param  integer $id
     * @return Vote
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * clearCache
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        
        if ($this->getAnswerId() == null) {
            $cache->delete(
                'user_' . $this->getUserId() 
                    . '_question_' . $this->getQuestionId() 
                    . '_vote'
            );
        } else {
            $cache->delete(
                'user_' . $this->getUserId() 
                    . '_answer_' . $this->getAnswerId() 
                    . '_vote'
            );
        }
    }
}
