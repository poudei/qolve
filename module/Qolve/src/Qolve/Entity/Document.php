<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="Qolve\Entity\DocumentRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="questionId")
 * @Application\ORM\Cacheable\Cacheable(type="document", lifetime="3600")
 *
 * @Form\Name("Document")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Document
{
    const TYPE_IMAGE = 1;
    const TYPE_VIDEO = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $questionId;

     /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $answerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
     *
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="`order`", type="smallint", nullable=false)
     */
    private $order;
    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=true)
     *
     * @Form\Exclude()
     */
    private $width;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", nullable=true)
     *
     * @Form\Exclude()
     */
    private $height;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     *
     * @Form\Required(false)
     */
    private $location;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionId
     *
     * @param  integer  $questionId
     * @return Document
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set answerId
     *
     * @param  integer  $answerId
     * @return Document
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

     /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * Set type
     *
     * @param  integer  $type
     * @return Document
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set width
     *
     * @param  integer  $width
     * @return Document
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param  integer  $height
     * @return Document
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * Set order
     *
     * @param  integer  $order
     * @return Document
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set location
     *
     * @param  string   $location
     * @return Document
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set id
     *
     * @param  integer  $id
     * @return Document
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * clearCache
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        
        if ($this->getAnswerId() == null) {
            $cache->delete(
                'question_' . $this->getQuestionId() . '_documents'
            );
        } else {
            $cache->delete(
                'answer_' . $this->getAnswerId() . '_documents'
            );
        }
    }
}
