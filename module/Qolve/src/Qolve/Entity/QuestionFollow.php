<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation as Form;

/**
 * Question Follow
 *
 * @ORM\Table(name="question_follow")
 * @ORM\Entity(repositoryClass="Qolve\Entity\QuestionFollowRepository")
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="questionId")
 * @Application\ORM\Cacheable\Cacheable(type="questionFollow", lifetime="3600")
 * @Form\Name("questionFollow")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class QuestionFollow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * Set questionId
     *
     * @param  integer        $questionId
     * @return QuestionFollow
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set userId
     *
     * @param  integer        $userId
     * @return QuestionFollow
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
