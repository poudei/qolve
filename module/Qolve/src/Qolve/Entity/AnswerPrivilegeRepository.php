<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class AnswerPrivilegeRepository extends EntityRepository
{
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "ap.$key IS NULL";
            } else {
                $conditions[] = "ap.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\AnswerPrivilege ap
                WHERE $conditions"
        );
        return $query->execute();
    }
}
