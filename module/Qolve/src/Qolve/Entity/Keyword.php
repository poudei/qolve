<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Comment
 *
 * @ORM\Table(name="keyword")
 * @ORM\HasLifecycleCallbacks
 * 
 * @ORM\Entity(repositoryClass="Qolve\Entity\KeywordRepository")
 * @Application\ORM\Cacheable\Cacheable(type="keyword", lifetime="3600")
 *
 * @Form\Name("keyword")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */

class Keyword
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="keyword_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length = 255, nullable=false)
     * @Form\Validator({"name":"StringLength", "options": {"min" : "1", "max" : "255"}})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $name;  

    /**
     * Set id
     *
     * @param  integer $id
     * @return integer
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string  $name
     * @return Keyword
     */
    public function setName($name)
    {
        $this->name = strtolower(trim($name));

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        $cache->delete('keyword_' . $this->getName());
        $cache->delete('keywords');
    }

}
