<?php

namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Bookmark
 *
 * @ORM\Table(name="bookmark")
 * @ORM\Entity(repositoryClass="Qolve\Entity\BookmarkRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="listId")
 * @Application\ORM\Cacheable\Cacheable(type="bookmark", lifetime="3600")
 *
 * @Form\Name("bookmark")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Bookmark
{
    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $listId;

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set questionId
     *
     * @param  integer  $questionId
     * @return Bookmark
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get listId
     *
     * @return integer
     */
    public function getListId()
    {
        return $this->listId;
    }

    /**
     * Set listId
     *
     * @param  integer  $listId
     * @return Bookmark
     */
    public function setListId($listId)
    {
        $this->listId = $listId;

        return $this;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        $cache->delete('list_' . $this->getListId() . '_bookmarks');
    }
}
