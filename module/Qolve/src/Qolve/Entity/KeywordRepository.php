<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository;

class KeywordRepository extends EntityRepository
{
    public function getKeywordList($key)
    {
        $objectManager = $this->getEntityManager();
        $objectManager
            ->getConnection()
            ->getShardManager()
            ->selectGlobal();

        $query = $objectManager->createQuery(
            "SELECT keyword.name
                FROM \Qolve\Entity\Keyword keyword
                WHERE keyword.name LIKE '$key%'"
        );

        if ($keys = $query->getResult()) {

            foreach ($keys as $key) {
                $keywords['name'][] = $key['name'];
            }

            return $keywords;
        }

    }

    public function countAll()
    {
        $objectManager = $this->getEntityManager();
        $objectManager
            ->getConnection()
            ->getShardManager()
            ->selectGlobal();

        $query = $objectManager->createQuery(
            "SELECT COUNT (key) num
                FROM \Qolve\Entity\Keyword key"
        );

        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function getKeysName()
    {
        $objectManager = $this->getEntityManager();
        $objectManager
            ->getConnection()
            ->getShardManager()
            ->selectGlobal();

        $query = $objectManager->createQuery(
            "SELECT keyword.name
                FROM \Qolve\Entity\Keyword keyword"
        );

        $result = $query->getResult();

        return isset($result[0]) ? $result : 0;
    }

    public function getSimilarKeywords($name, $offset = 0, $limit = 10, $config = null)
    {
        $dbParams = $config['doctrine']
            ['connection']
            ['orm_default']
            ['params']['global'];

        $host     = $dbParams['host'];
        $port     = $dbParams['port'];
        $dbname   = $dbParams['dbname'];
        $user     = $dbParams['user'];
        $password = $dbParams['password'];

        //TODO : DO it by Doctrine !!!
        $conn = pg_pconnect(
            "host='$host' port='$port' dbname='$dbname'
             user='$user'  password='$password'"
        );
        $result = pg_query($conn, "SELECT id, name,
            similarity(name, '$name') AS sim 
            FROM keyword
            WHERE name LIKE '%$name%'
            ORDER BY sim desc
            LIMIT $limit offset $offset"
        );
        while ($row = pg_fetch_assoc($result)) {
            if (($row['sim'] > 0)) {

                $results[] = array(
                    'id'   => $row['id'],
                    'name' => $row['name']
                );
            }
        }
        empty($results) ? $results = array() : '';

        return $results;
    }

    public function insertKey($name)
    {
        if (preg_match('/^[a-zA-Z0-9][a-zA-Z0-9_]+$/', $name)) {
            $objectManager = $this->getEntityManager();
            $keyword       = new Keyword();
            $keyword->setName($name);
            $objectManager
                ->getConnection()
                ->getShardManager()
                ->selectGlobal();

            $objectManager->persist($keyword);
            $objectManager->flush($keyword);
            
            return $keyword;
        }
    }

    public function getKeywords()
    {
        $objectManager = $this->getEntityManager();
        $objectManager
            ->getConnection()
            ->getShardManager()
            ->selectGlobal();
        $query = $objectManager->createQuery(
            "SELECT keyword
                FROM \Qolve\Entity\Keyword keyword"
        );

        $query->useResultCache(true, LIFE_TIME, 'keywords');
        $keys = array();
        $keys = $query->getResult();
        return $keys;
    }

    public function getKeyByName($name)
    {
        $objectManager = $this->getEntityManager();
        $objectManager
            ->getConnection()
            ->getShardManager()
            ->selectGlobal();
        $query = $objectManager->createQuery(
            "SELECT keyword
                FROM \Qolve\Entity\Keyword keyword
                WHERE keyword.name='$name'"
        );

        $query->useResultCache(true, LIFE_TIME, 'keyword_' . $name);
        $key = $query->getResult();
        isset($key[0]) ? $key = $key[0] : $key = array();
        return $key;
    }

    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "k.$key IS NULL";
            } else {
                $conditions[] = "k.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Keyword k
                WHERE $conditions"
        );
        return $query->execute();
    }
}
