<?php

namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TransactionLog
 *
 * @ORM\Table(name="transaction_log")
 * @ORM\Entity
 */
class TransactionLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="transaction_log_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="solver_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $solverId;

    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $questionId;

    /**
     * @var float
     *
     * @ORM\Column(name="user_paid", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $userPaid;

    /**
     * @var string
     *
     * @ORM\Column(name="partner_name", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $partnerName;

    /**
     * @var float
     *
     * @ORM\Column(name="partner_share", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $partnerShare;

    /**
     * @var float
     *
     * @ORM\Column(name="qolve_share", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $qolveShare;

    /**
     * @var float
     *
     * @ORM\Column(name="question_cost", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $questionCost;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdOn;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return TransactionLog
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set solverId
     *
     * @param integer $solverId
     * @return TransactionLog
     */
    public function setSolverId($solverId)
    {
        $this->solverId = $solverId;

        return $this;
    }

    /**
     * Get solverId
     *
     * @return integer 
     */
    public function getSolverId()
    {
        return $this->solverId;
    }

    /**
     * Set questionId
     *
     * @param integer $questionId
     * @return TransactionLog
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer 
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set userPaid
     *
     * @param float $userPaid
     * @return TransactionLog
     */
    public function setUserPaid($userPaid)
    {
        $this->userPaid = $userPaid;

        return $this;
    }

    /**
     * Get userPaid
     *
     * @return float 
     */
    public function getUserPaid()
    {
        return $this->userPaid;
    }

    /**
     * Set partnerName
     *
     * @param string $partnerName
     * @return TransactionLog
     */
    public function setPartnerName($partnerName)
    {
        $this->partnerName = $partnerName;

        return $this;
    }

    /**
     * Get partnerName
     *
     * @return string 
     */
    public function getPartnerName()
    {
        return $this->partnerName;
    }

    /**
     * Set partnerShare
     *
     * @param float $partnerShare
     * @return TransactionLog
     */
    public function setPartnerShare($partnerShare)
    {
        $this->partnerShare = $partnerShare;

        return $this;
    }

    /**
     * Get partnerShare
     *
     * @return float 
     */
    public function getPartnerShare()
    {
        return $this->partnerShare;
    }

    /**
     * Set qolveShare
     *
     * @param float $qolveShare
     * @return TransactionLog
     */
    public function setQolveShare($qolveShare)
    {
        $this->qolveShare = $qolveShare;

        return $this;
    }

    /**
     * Get qolveShare
     *
     * @return float 
     */
    public function getQolveShare()
    {
        return $this->qolveShare;
    }

    /**
     * Set questionCost
     *
     * @param float $questionCost
     * @return TransactionLog
     */
    public function setQuestionCost($questionCost)
    {
        $this->questionCost = $questionCost;

        return $this;
    }

    /**
     * Get questionCost
     *
     * @return float 
     */
    public function getQuestionCost()
    {
        return $this->questionCost;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return TransactionLog
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return TransactionLog
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}
