<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    Qolve\Entity\bookmarkListFollow;

class BookmarkListFollowRepository extends EntityRepository
{
    public function getBookmarkListFollowers($listId, &$count, 
        $offset = 0, $limit = 5
    ) {
        $query = $this->getEntityManager()->createQuery(
            "SELECT  user
                FROM \Qolve\Entity\BookmarkListFollow follow
                JOIN \Application\Entity\User user
                WITH follow.userId = user.id
                WHERE follow.listId = '$listId'"
        )
        ->setFirstResult($offset)
        ->setMaxResults($limit);

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT  COUNT(follow) num
                FROM \Qolve\Entity\BookmarkListFollow follow
                WHERE follow.listId = '$listId'"
        );
        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $query->getResult();
    }

    public function countFollowedLists($userId)
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT COUNT(follow) num
                FROM \Qolve\Entity\BookmarkListFollow follow
                WHERE follow.userId = '$userId'"
        );
        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }
    
    public function isFollowed($listId, $userId)
    {
        $result = array();
        $query  = $this->getEntityManager()->createQuery(
            "SELECT b FROM  Qolve\Entity\bookmarkListFollow b
                WHERE b.listId = '$listId'
                AND b.userId = '$userId'"
        );

        $query->useResultCache(true, LIFE_TIME, 'user_' . $userId .
            '_list_'. $listId . '_follow'
        );
        $result = $query->getResult();
        if (   isset($result[0])
            && $result[0] instanceof bookmarkListFollow) {
            $return = 1;
        } else {
            $return = 0;
        }

        return $return;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "f.$key IS NULL";
            } else {
                $conditions[] = "f.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\BookmarkListFollow f
                WHERE $conditions"
        );
        return $query->execute();
    }
}
