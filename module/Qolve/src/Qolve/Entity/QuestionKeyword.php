<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Question Keyword
 *
 * @ORM\Table(name="question_keyword")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Qolve\Entity\QuestionKeywordRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="questionId")
 * @Application\ORM\Cacheable\Cacheable(type="questionKeyword", lifetime="3600")
 *
 * @Form\Name("questionKeyword")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class QuestionKeyword
{
    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="keyword_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $keywordId;

    /**
     * Set questionId
     *
     * @param  integer         $questionId
     * @return QuestionKeyword
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set keywordId
     *
     * @param  integer         $keywordId
     * @return QuestionKeyword
     */
    public function setKeywordId($keywordId)
    {
        $this->keywordId = $keywordId;

        return $this;
    }

    /**
     * Get keywordId
     *
     * @return integer
     */
    public function getKeywordId()
    {
        return $this->keywordId;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        $cache->delete('question_' . $this->getQuestionId() . '_keywords');
    }
}
