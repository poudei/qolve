<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Asker
 *
 * @ORM\Table(name="asker")
 * @ORM\Entity(repositoryClass="Qolve\Entity\AskerRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="questionId")
 * @Application\ORM\Cacheable\Cacheable(type="asker", lifetime="3600")

 * @Form\Name("asker")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Asker
{
    const STATUS_ASKER      = 1;
    const STATUS_REASKER    = 2;

    const VISIBILITY_PUBLIC     = 1;
    const VISIBILITY_ANONYMOUS  = 2;
    const ANONYMOUS_NAME = "ANONYMOUS";

    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     * @Form\Validator({"name":"Digits"})
     * @Form\Exclude()
     *
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="credit", type="float", nullable=false)
     * @Form\Validator({"name":"\Zend\I18n\Validator\Float"})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $credit;

    /**
     * @var integer
     *
     * @ORM\Column(name="visibility", type="smallint", nullable=false)
     * @Form\Validator({"name":"Digits"})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $visibility;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", nullable=true)
     *
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)
     */
    private $answerId;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="asked_on", type="datetime", nullable=true)
     *
     * @Form\Exclude()
     */
    private $askedOn;

    /**
     * Set questionId
     *
     * @param  integer $questionId
     * @return Asker
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set userId
     *
     * @param  integer $userId
     * @return Asker
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set status
     *
     * @param  integer $status
     * @return Asker
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set credit
     *
     * @param  integer $credit
     * @return Asker
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return integer
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set visibility
     *
     * @param  integer $visibility
     * @return Asker
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return integer
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set answerId
     *
     * @param  integer $answerId
     * @return Asker
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

/**
     * Set askedOn
     *
     * @param  integer $askedOn
     * @return Asker
     */
    public function setAskedOn($askedOn)
    {
        $this->askedOn = $askedOn;

        return $this;
    }

    /**
     * Get askedOn
     *
     * @return integer
     */
    public function getAskedOn()
    {
        return $this->askedOn;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        
        if ($this->getStatus() == self::STATUS_ASKER) {
            $cache->delete('question_' . $this->getQuestionId() . '_asker');
        }
        
        $cache->delete(
            'user_' . $this->getUserId() 
                . '_question_'. $this->getQuestionId() 
                . '_askStatus'
        );
    }
}
