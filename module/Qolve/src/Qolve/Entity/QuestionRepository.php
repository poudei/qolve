<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator,
    Qolve\Entity\Question,
    Qolve\Entity\Asker,
    Application\Entity\User;

class QuestionRepository extends EntityRepository
{
    public function getAskerId($questionId)
    {
        $questionQuery = $this->getEntityManager()->createQuery(
            "SELECT asker.userId, asker.status
                FROM \Qolve\Entity\Asker asker
                WHERE asker.questionId = $questionId"
        );

        if ($askers = $questionQuery->getResult()) {

            if (count($askers) > 1) {

                foreach ($askers as $asker) {
                    if ($asker['status'] == Asker::STATUS_ASKER) {
                        return $asker['userId'];
                    }
                }
            }

            $askerId = $askers[0]['userId'];

            return $askerId;
        } else {
            return false;
        }
    }

    public function getAsker($questionId)
    {
        if ($questionId) {
            $status  = Asker::STATUS_ASKER;
            $query = $this->getEntityManager()->createQuery(
                "SELECT asker.userId FROM  Qolve\Entity\Asker asker
                    WHERE asker.questionId = '$questionId'
                    AND asker.status = '$status'"
            );

            $query->useResultCache(true, LIFE_TIME, 'question_'. $questionId .
                '_asker'
            );
            $result = $query->getResult();

            $userId = !empty($result[0]['userId']) ? $result[0]['userId'] : null;
            if (is_null($userId)) {
                return array();
            }
            $user   = $this->getEntityManager()
                ->getRepository('Application\Entity\User')
                ->find($userId);
            return $user;
        }
    }

    public function isAsked($questionId, $userId)
    {
        $result = array();
        $query  = $this->getEntityManager()->createQuery(
            "SELECT u.status FROM Qolve\Entity\Asker u
                WHERE u.questionId = '$questionId'
                AND u.userId = '$userId'"
        );
        $query->useResultCache(true, LIFE_TIME, 'user_' . $userId .
            '_question_'. $questionId . '_askStatus'
        );

        $result = $query->getResult();
        return isset($result[0]['status']) ? true : false;
    }

    public function getList(array $criteria, array $orderBy = null,
        $limit = null, $offset = null, $currentUser = null
    ) {
        if (empty($criteria)) {
            $criteria = array();
        }
        if (!is_array($criteria)) {
            $criteria = array($criteria);
        }

        $conditions = array();
        foreach ($criteria as $property => $value) {
            if (is_array($value)) {
                foreach ($value as $op => $_value) {
                    switch ($op) {
                        case 'greaterThan':
                            $conditions[] = "question.$property > $_value";
                            break;
                        case 'greaterThanOrEqual':
                            $conditions[] = "question.$property >= $_value";
                            break;
                        case 'lessThan':
                            $conditions[] = "question.$property < $_value";
                            break;
                        case 'lessThanOrEqual':
                            $conditions[] = "question.$property <= $_value";
                            break;
                    }
                }
            } else {
                if ($value === null) {
                    $conditions[] = "question.$property IS NULL";
                } else {
                    $conditions[] = "question.$property = '$value'";
                }
            }
        }

        if (!$currentUser instanceof \Application\Entity\User) {
            $conditions[] = "question.privacy = " . Question::PRIVACY_PUBLIC;
        } else {
            $conditions[] = "(question.privacy = " . Question::PRIVACY_PUBLIC .
                " OR share.userId = " . $currentUser->getId() . ")";
        }

        if ($orderBy) {
            $orders = array();
            foreach ($orderBy as $by => $dir) {
                switch (strtoupper($dir)) {
                    case 'ASC':
                    case '1':
                        $orders[] = "question.$by ASC";
                        break;

                    case 'DESC':
                    case '-1':
                        $orders[] = "question.$by DESC";
                        break;
                }
            }
        }

        $queryStr = "SELECT distinct question
            FROM \Qolve\Entity\Question question
            LEFT JOIN \Qolve\Entity\Share share
            WITH share.questionId = question.id ";
        if (!empty($conditions)) {
            $queryStr .= " WHERE " . join(' AND ', $conditions);
        }
        if (!empty($orders)) {
            $queryStr .= " ORDER BY " . join(', ', $orders);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);

        if ($offset) {
            $query->setFirstResult($offset);
        }
        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query->getResult();
    }

    public function countBy(array $criteria, $currentUser = null)
    {
        $conditions = array();
        foreach ($criteria as $property => $value) {
            if (is_array($value)) {
                foreach ($value as $op => $_value) {
                    switch ($op) {
                        case 'greaterThan':
                            $conditions[] = "question.$property > $_value";
                            break;
                        case 'greaterThanOrEqual':
                            $conditions[] = "question.$property >= $_value";
                            break;
                        case 'lessThan':
                            $conditions[] = "question.$property < $_value";
                            break;
                        case 'lessThanOrEqual':
                            $conditions[] = "question.$property <= $_value";
                            break;
                    }
                }
            } else {
                if ($value === null) {
                    $conditions[] = "question.$property IS NULL";
                } else {
                    $conditions[] = "question.$property = '$value'";
                }
            }
        }

        if (!$currentUser instanceof \Application\Entity\User) {
            $conditions[] = "question.privacy = " . Question::PRIVACY_PUBLIC;
        } else {
            $conditions[] = "(question.privacy = " . Question::PRIVACY_PUBLIC .
                "OR share.userId = " . $currentUser->getId() . ")";
        }

        $queryStr = "SELECT COUNT(distinct question.id) num
            FROM \Qolve\Entity\Question question
            LEFT JOIN \Qolve\Entity\Share share
            WITH share.questionId = question.id ";
        if (!empty($conditions)) {
            $queryStr .= "WHERE " . join(' AND ', $conditions);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);
        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function setQuestionActivity($serviceLocator, $params) {
       // *** Insert into Elastica *** //
        $elastica  = $serviceLocator
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];
        $elasticaIndex  = $elasticaClient->getIndex($index);
        $elasticaType   = $elasticaIndex->getType('questionLists');
        $search         = new \Elastica\Search($elasticaClient);

        $questionId = $params['question_id'];

        $term = new \Elastica\Query\Term();
        $term->setParam('_id', $questionId);

        $elasticaQuery = new \Elastica\Query($term);
        $resultSet = $search->addIndex($index)
            ->addType('questionLists')
            ->search($elasticaQuery);

        $return = array();
        foreach ($resultSet as $result) {
            $return = $result->getData();
        }

        if (!empty($return)) {

            $document = $elasticaType->getDocument($questionId);
            $document->setData($params);
            $elasticaType->updateDocument($document);
            $elasticaType->getIndex()->refresh();

        } else {

            $elasticaIndex = $elasticaClient->getIndex($index);
            $elasticaType  = $elasticaIndex->getType('questionLists');
            $content       = new \Elastica\Document($questionId, $params);

            $elasticaType->addDocument($content);
            $elasticaType->getIndex()->refresh();
        }
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "q.$key IS NULL";
            } else {
                $conditions[] = "q.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Question q
                WHERE $conditions"
        );
        return $query->execute();
    }
}
