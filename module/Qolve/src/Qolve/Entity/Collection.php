<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation as Form;


/**
 * Collection
 *
 * @ORM\Table(name="collection")
 * @Application\ORM\Cacheable\Cacheable(type="collection", lifetime="3600")
 * @ORM\Entity
 */
class Collection
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="owner_id", type="bigint", nullable=false)
     */
    private $ownerId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "0", "max" : "64"}})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="smallint", nullable=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ownerId
     *
     * @param  integer    $ownerId
     * @return Collection
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * Get ownerId
     *
     * @return integer
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * Set name
     *
     * @param  string     $name
     * @return Collection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set deleted
     *
     * @param  integer    $deleted
     * @return Collection
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set createdBy
     *
     * @param  integer    $createdBy
     * @return Collection
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdOn
     *
     * @param  \DateTime  $createdOn
     * @return Collection
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set id
     *
     * @param  integer    $id
     * @return Collection
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
