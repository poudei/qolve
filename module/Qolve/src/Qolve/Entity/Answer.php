<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Answer
 *
 * @ORM\Table(name="answer")
 * @ORM\Entity(repositoryClass="Qolve\Entity\AnswerRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="questionId")
 * @Application\ORM\Cacheable\Cacheable(type="answer", lifetime="3600")
 *
 * @Form\Name("answer")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Answer
{
    const PRIVACY_PUBLIC    = 1;
    const PRIVACY_PRIVATE   = 2;

    const VISIBILITY_PUBLIC     = 1;
    const VISIBILITY_ANONYMOUS  = 2;
    const ANONYMOUS_NAME = "ANONYMOUS";

    const STATUS_NORMAL     = 1;
    const STATUS_ACCEPTED   = 2;
    const STATUS_BEST       = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="privacy", type="smallint", nullable=false)
     * @Form\Validator({"name":"Digits"})
     */
    private $privacy;

    /**
     * @var integer
     *
     * @ORM\Column(name="visibility", type="smallint", nullable=false)
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $visibility;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="votes", type="smallint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $votes;

    /**
     * @var integer
     *
     * @ORM\Column(name="comments", type="integer", nullable=false)
     *
     * @Form\Exclude()
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(name="voters", type="integer", nullable=false)
     *
     * @Form\Exclude()
     */
    private $voters;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=10000, nullable=true)
     * @Form\Validator({"name":"StringLength", "options": {"min" : "0", "max" : "10000"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="smallint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     *
     * @Form\Exclude()
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_by", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_on", type="datetime", nullable=true)
     *
     * @Form\Exclude()
     */
    private $modifiedOn;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionId
     *
     * @param  integer $questionId
     * @return Answer
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set userId
     *
     * @param  integer $userId
     * @return Answer
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set privacy
     *
     * @param  integer  $privacy
     * @return Question
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return integer
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set visibility
     *
     * @param  integer $visibility
     * @return Answer
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return integer
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set status
     *
     * @param  integer $status
     * @return Answer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set votes
     *
     * @param  integer $votes
     * @return Answer
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * Get votes
     *
     * @return integer
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set comments
     *
     * @param  integer $comments
     * @return Answer
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return integer
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set voters
     *
     * @param  integer $voters
     * @return Answer
     */
    public function setVoters($voters)
    {
        $this->voters = $voters;

        return $this;
    }

    /**
     * Get voters
     *
     * @return integer
     */
    public function getVoters()
    {
        return $this->voters;
    }

    /**
     * Set description
     *
     * @param  string $description
     * @return Answer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set deleted
     *
     * @param  integer $deleted
     * @return Answer
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set createdOn
     *
     * @param  \DateTime $createdOn
     * @return Answer
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedBy
     *
     * @param  integer $modifiedBy
     * @return Answer
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set modifiedOn
     *
     * @param  \DateTime $modifiedOn
     * @return Answer
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modifiedOn = $modifiedOn;

        return $this;
    }

    /**
     * Get modifiedOn
     *
     * @return \DateTime
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    /**
     * Set id
     *
     * @param  integer $id
     * @return Answer
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();

        $cache->delete(
            'user_' . $this->getUserId()
                . '_question_' . $this->getQuestionId()
                . '_answer'
        );

        $cache->delete('question_' . $this->getQuestionId() . '_answers');
    }

}
