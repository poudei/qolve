<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="Qolve\Entity\CommentRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 * @Application\ORM\Cacheable\Cacheable(type="comment", lifetime="3600")
 *
 * @Form\Name("comment")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $answerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=1000, nullable=true)
     * @Form\Validator({"name":"StringLength", "options": {"min" : "0", "max" : "1000"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="written_on", type="datetime", nullable=false)
     *
     * @Form\Exclude()
     */
    private $writtenOn;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionId
     *
     * @param  integer $questionId
     * @return Comment
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set answerId
     *
     * @param  integer $answerId
     * @return Comment
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * Set userId
     *
     * @param  integer $userId
     * @return Comment
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set content
     *
     * @param  string  $content
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set writtenOn
     *
     * @param  \DateTime $writtenOn
     * @return Comment
     */
    public function setWrittenOn($writtenOn)
    {
        $this->writtenOn = $writtenOn;

        return $this;
    }

    /**
     * Get writtenOn
     *
     * @return \DateTime
     */
    public function getWrittenOn()
    {
        return $this->writtenOn;
    }

    /**
     * Set id
     *
     * @param  integer $id
     * @return Comment
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        
        if ($this->getAnswerId()) {
            $cache->delete(
                'question_'. $this->getQuestionId() 
                    . '_answer_' . $this->getAnswerId() 
                    . '_comments'
            );
        } else {
            $cache->delete('question_'. $this->getQuestionId() . '_comments');
        }
    }

}
