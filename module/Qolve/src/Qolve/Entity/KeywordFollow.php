<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Keyword Follow
 *
 * @ORM\Table(name="keyword_follow")
 * @ORM\Entity(repositoryClass="Qolve\Entity\KeywordFollowRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="keywordFollow", lifetime="3600")
 *
 * @Form\Name("keywordFollow")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class KeywordFollow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="keyword_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $keywordId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="levels", type="string", length=500, nullable=false)
     * @Form\Validator({"name":"StringLength", "options": {"min" : "1", "max" : "255"}})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $levels;

    /**
     * Set keywordId
     *
     * @param  integer       $keywordId
     * @return KeywordFollow
     */
    public function setKeywordId($keywordId)
    {
        $this->keywordId = $keywordId;

        return $this;
    }

    /**
     * Get keywordId
     *
     * @return integer
     */
    public function getKeywordId()
    {
        return $this->keywordId;
    }

    /**
     * Set userId
     *
     * @param  integer       $userId
     * @return KeywordFollow
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set levels
     *
     * @param  string        $levels
     * @return KeywordFollow
     */
    public function setLevels($levels)
    {
        $this->levels = $levels;

        return $this;
    }

    /**
     * Get levels
     *
     * @return string
     */
    public function getLevels()
    {
        return $this->levels;
    }


    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        $cache->delete(
                'user_'. $this->getUserId()
                    . '_keyword_' . $this->getKeywordId()
                    . '_follow'
            );

        $cache->delete(
                'user_'. $this->getUserId()
                    . '_keyword_follows'
            );

    }
}
