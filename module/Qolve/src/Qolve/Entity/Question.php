<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="Qolve\Entity\QuestionRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 * @Application\ORM\Cacheable\Cacheable(type="question", lifetime="3600")

 * @Form\Name("question")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Question
{
    const STATUS_OPEN              = 1;
    const STATUS_RESOLVED          = 2;
    const STATUS_CLOSED            = 3;

    const PRIVACY_PUBLIC           = 1;
    const PRIVACY_SHARED           = 2;

    const ANSWER_PRIVILEGE_PUBLIC    = 1;
    const ANSWER_PRIVILEGE_PROTECTED = 2;

    const PAID_NOTSTARTED          = 1;
    const PAID_INPROGRESS          = 2;
    const PAID_DONE                = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime", nullable=true)
     * @Form\Validator({"name":"Date", "options":{"format" : "Y-m-d H:i:s"}})
     * @Form\Required(false)
     */
    private $deadline;

    /**
     * @var string
     *
     * @ORM\Column(name="levels", type="string", length=500, nullable=false)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "5"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)
     */
    private $levels;

    /**
     * @var integer
     *
     * @ORM\Column(name="privacy", type="smallint", nullable=false)
     * @Form\Validator({"name":"Regex", "options":{"pattern" : "/^[0-9]$/"}})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $privacy;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_privilege", type="smallint", nullable=false)
     * @Form\Validator({"name":"Regex", "options":{"pattern" : "/^[0-9]$/"}})
     */
    private $answerPrivilege;

    /**
     * @var integer
     *
     * @ORM\Column(name="votes", type="smallint", nullable=false)
     * @Form\Exclude()
     *
     */
    private $votes;

    /**
     * @var integer
     *
     * @ORM\Column(name="credits", type="float", nullable=false)
     * @Form\Validator({"name":"\Zend\I18n\Validator\Float"})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)
     */
    private $credits;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5, nullable=false)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "2", "max" : "5"}})
     * @Form\Filter({"name" : "htmlentities" })
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="askers", type="integer", nullable=false)
     * @Form\Exclude()
     *
     */
    private $askers;

    /**
     * @var integer
     *
     * @ORM\Column(name="voters", type="integer", nullable=false)
     * @Form\Exclude()
     *
     */
    private $voters;

    /**
     * @var integer
     *
     * @ORM\Column(name="comments", type="integer", nullable=false)
     * @Form\Exclude()
     *
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(name="answers", type="integer", nullable=false)
     * @Form\Exclude()
     *
     */
    private $answers;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=10000, nullable=true)
     * @Form\Validator({"name":"StringLength", "options": {"min" : "0", "max" : "10000"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $answerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     *
     * @Form\Exclude()
     * @Form\Required(false)
     */
    private $status;

    /**
     *@var integer
     *
     *@ORM\Column(name="paid", type="smallint", nullable=false)
     *
     * @Form\Exclude()
     * @Form\Required(false)
     */
    private $paid;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="smallint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     * @Form\Validator({"name":"Date", "options":{"format" : "Y-m-d H:i:s"}})
     * @Form\Exclude()
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_by", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_on", type="datetime", nullable=true)
     * @Form\Validator({"name":"Date", "options":{"format" : "Y-m-d H:i:s"}})
     * @Form\Exclude()
     */
    private $modifiedOn;

    /**
     * Set id
     *
     * @param  integer  $id
     * @return Question
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deadline
     *
     * @param  \DateTime $deadline
     * @return Question
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set levels
     *
     * @param  string   $levels
     * @return Question
     */
    public function setLevels($levels)
    {
        $this->levels = $levels;

        return $this;
    }

    /**
     * Get levels
     *
     * @return string
     */
    public function getLevels()
    {
        return $this->levels;
    }

    /**
     * Set privacy
     *
     * @param  integer  $privacy
     * @return Question
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return integer
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set answerPrivilege
     *
     * @param  integer  $answerPrivilege
     * @return Question
     */
    public function setAnswerPrivilege($answerPrivilege)
    {
        $this->answerPrivilege = $answerPrivilege;

        return $this;
    }

    /**
     * Get answerPrivilege
     *
     * @return integer
     */
    public function getAnswerPrivilege()
    {
        return $this->answerPrivilege;
    }
    /**
     * Set votes
     *
     * @param  integer  $votes
     * @return Question
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * Get votes
     *
     * @return integer
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set credits
     *
     * @param  integer  $credits
     * @return Question
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;

        return $this;
    }

    /**
     * Get credits
     *
     * @return integer
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * Set language
     *
     * @param  string   $language
     * @return Question
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set askers
     *
     * @param  integer  $askers
     * @return Question
     */
    public function setAskers($askers)
    {
        $this->askers = $askers;

        return $this;
    }

    /**
     * Get askers
     *
     * @return integer
     */
    public function getAskers()
    {
        return $this->askers;
    }

    /**
     * Set voters
     *
     * @param  integer  $voters
     * @return Question
     */
    public function setVoters($voters)
    {
        $this->voters = $voters;

        return $this;
    }

    /**
     * Get voters
     *
     * @return integer
     */
    public function getVoters()
    {
        return $this->voters;
    }

    /**
     * Set comments
     *
     * @param  integer  $comments
     * @return Question
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return integer
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set answers
     *
     * @param  integer  $answers
     * @return Question
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;

        return $this;
    }

    /**
     * Get answers
     *
     * @return integer
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set description
     *
     * @param  string   $description
     * @return Question
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set answerId
     *
     * @param  integer  $answerId
     * @return Question
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * Set status
     *
     * @param  integer  $status
     * @return Question
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set paid
     *
     * @param  integer  $paid
     * @return Question
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return integer
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set deleted
     *
     * @param  integer  $deleted
     * @return Question
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set createdOn
     *
     * @param  \DateTime $createdOn
     * @return Question
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedBy
     *
     * @param  integer  $modifiedBy
     * @return Question
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set modifiedOn
     *
     * @param  \DateTime $modifiedOn
     * @return Question
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modifiedOn = $modifiedOn;

        return $this;
    }

    /**
     * Get modifiedOn
     *
     * @return \DateTime
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    /**
     * @ORM\PostLoad
     * testPostLoad
     *
     * @access public
     * @return void
     */
    public function testPostLoad()
    {
        //var_dump($args->getObjectManager()->getConfiguration()->getResultCacheImpl());
        //var_dump($args->getObject());exit;
    }
}
