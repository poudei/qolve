<?php
namespace Qolve\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * User Follow
 *
 * @ORM\Table(name="user_follow")
 * @ORM\Entity(repositoryClass="Qolve\Entity\UserFollowRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="userFollow", lifetime="3600")
 *
 * @Form\Name("userFollow")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class UserFollow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="follower_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $followerId;

    /**
     * Set userId
     *
     * @param  integer    $userId
     * @return UserFollow
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set followerId
     *
     * @param  integer    $followerId
     * @return UserFollow
     */
    public function setFollowerId($followerId)
    {
        $this->followerId = $followerId;

        return $this;
    }

    /**
     * Get followerId
     *
     * @return integer
     */
    public function getFollowerId()
    {
        return $this->followerId;
    }
    
    /**
     * @ORM\PostPersist
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        
        $cache->delete(
            'user_' . $this->getFollowerId() 
                . '_following_' . $this->getUserId() 
                . '_follow'
        );
        
        $cache->delete('user_' . $this->getFollowerId() . '_followings');
        $cache->delete('user_' . $this->getFollowerId() . '_follows');
        $cache->delete('user_' . $this->getUserId()     . '_followers');
        $cache->delete('user_' . $this->getUserId()     . '_follows');
    }
}
