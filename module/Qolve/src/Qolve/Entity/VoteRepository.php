<?php
namespace Qolve\Entity;

use Doctrine\ORM\EntityRepository;
class VoteRepository extends EntityRepository
{
    public function getTotalScores($questionId, $answerId)
    {
        if ($answerId) {
            $query = $this->getEntityManager()->createQuery(
                "SELECT SUM(vote.score) scores
                    FROM \Qolve\Entity\Vote vote
                    WHERE vote.questionId = '$questionId'
                    AND vote.answerId     = '$answerId'"
            );
        } else {
            $query = $this->getEntityManager()->createQuery(
                "SELECT SUM(vote.score) scores
                    FROM \Qolve\Entity\Vote vote
                    WHERE vote.questionId='$questionId'
                    AND vote.answerId IS NULL"
            );
        }

        $result = $query->getResult();

        return isset($result[0]['scores']) ? $result[0]['scores'] : 0;
    }

    public function getVotersCount($questionId, $answerId = null)
    {
        if ($answerId) {
            $query = $this->getEntityManager()->createQuery(
                "SELECT COUNT(vote.userId) voters
                    FROM \Qolve\Entity\Vote vote
                    WHERE vote.questionId = '$questionId'
                    AND   vote.answerId   = '$answerId'"
            );
        } else {
            $query = $this->getEntityManager()->createQuery(
                "SELECT COUNT(vote.userId) voters
                    FROM \Qolve\Entity\Vote vote
                    WHERE vote.questionId = '$questionId'
                    AND   vote.answerId IS NULL"
            );
        }

        $result = $query->getResult();

        return isset($result[0]['voters']) ? $result[0]['voters'] : 0;
    }

    public function countBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $property => $value) {
            if ($value === null) {
                $conditions[] = "vote.$property IS NULL";
            } else {
                $conditions[] = "vote.$property = '$value'";
            }
        }

        $queryStr = "SELECT COUNT(vote) num
            FROM \Qolve\Entity\Vote vote ";
        if (!empty($conditions)) {
            $queryStr .= "WHERE " . join(' AND ', $conditions);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);
        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function getUserVote($userId, $questionId, $answerId = null)
    {
        if ($answerId == null) {
            $query = $this->getEntityManager()->createQuery(
                "SELECT v FROM \Qolve\Entity\Vote v
                    WHERE v.userId = '$userId'
                    AND v.questionId = '$questionId'
                    AND v.answerId IS NULL"
            );

            $query->useResultCache(true, LIFE_TIME,
                'user_'. $userId . '_question_' . $questionId . '_vote'
            );

        } else {
            $query = $this->getEntityManager()->createQuery(
                "SELECT v FROM \Qolve\Entity\Vote v
                    WHERE v.userId = '$userId'
                    AND v.questionId = '$questionId'
                    AND v.answerId = '$answerId'"
            );

            $query->useResultCache(true, LIFE_TIME,
                'user_'. $userId . '_answer_' . $answerId . '_vote'
            );
        }
        $result = $query->getResult();
        return isset($result[0]) ? $result[0] : array();
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "v.$key IS NULL";
            } else {
                $conditions[] = "v.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Qolve\Entity\Vote v
                WHERE $conditions"
        );
        return $query->execute();
    }
}
