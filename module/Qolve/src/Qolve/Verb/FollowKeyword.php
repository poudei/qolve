<?php
namespace Qolve\Verb;

use Application\Entity\Notification,
    Application\Entity\Feed,
    Qolve\Entity\Keyword,
    Qolve\Entity\Question,
    Qolve\Entity\Asker;

class FollowKeyword
{
    public static function publishFeed($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        
        $elastica       = $serviceLocator->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $userId         = $data['userId'];
        $keywordFollows = $data['keywords'];
        $now            = new \DateTime('now', new \DateTimeZone('UTC'));

        if (!empty($keywordFollows)) {
            foreach ($keywordFollows as $keywordFollow) {

                $keyword = $objectManager
                    ->getRepository('Qolve\Entity\Keyword')
                    ->find($keywordFollow);

                if ($keyword instanceof Keyword) {

                    $preFeed = $objectManager
                        ->getRepository('Application\Entity\Feed')
                        ->findOneBy(array(
                            'userId'  => $userId,
                            'actorId' => $userId,
                            'verb'    => 'FollowKeyword',
                            'content' => $keyword->getName()
                        ));
                    if (!$preFeed instanceof \Application\Entity\Feed) {
                    
                        //Add to user's Activities
                        $feed = new Feed();
                        $feed->setUserId($userId);
                        $feed->setActorId($userId);
                        $feed->setPrivacy(\Application\Entity\Feed::PRIVACY_PRIVATE);
                        $feed->setVerb('FollowKeyword');
                        $feed->setContent($keyword->getName());
                        $feed->setCreatedOn($now);
                        $objectManager->persist($feed);
                        $objectManager->flush();
                    }

                    $keywords[] = $keyword->getName();
                }
            }

            $elasticaQuery     = new \Elastica\Query();
            $search            = new \Elastica\Search($elasticaClient);
            $elasticaBool      = new \Elastica\Query\Bool();
            $elasticaIndex     = $elasticaClient->getIndex($index);
            $elasticaType      = $elasticaIndex->getType('question');

            if (!empty($keywords)) {
                foreach ($keywords as $key) {

                    $term = new \Elastica\Query\Term();
                    $term->setParam('keywords', trim(strtolower($key)));
                    $elasticaBool->addShould($term);
                }

                $elasticaQuery = new \Elastica\Query($elasticaBool);

                $elasticaQuery->setSort(array('votes' => array('order' => 'desc')));

                $resultSet = $search->addIndex($index)
                    ->addType('question')
                    ->search($elasticaQuery);
                $count = $search->addIndex($index)
                    ->addType('question')
                    ->count($elasticaQuery);
                foreach ($resultSet as $result) {

                    $returns[] = $result->getData();
                }
            }
            if (empty($returns)) {
                return;
            }
            foreach ($returns as $return) {

                $question = $objectManager->getRepository('Qolve\Entity\Question')
                    ->find($return['question_id']);

                if ($question instanceof Question) {
                    $asker = $objectManager->getRepository('Qolve\Entity\Asker')
                        ->findOneByQuestionId($question->getId());
                    if ($asker instanceof Asker) {

                        $feedPrivacy = max(array(
                                    $question->getPrivacy(),
                                    $asker->getVisibility()
                                    ));
                        
                        $preFeed = $objectManager
                            ->getRepository('Application\Entity\Feed')
                            ->findOneBy(array(
                                'userId'     => $userId,
                                'actorId'    => $asker->getUserId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'CreateQuestion'
                            ));
                        if (!$preFeed instanceof Feed) {
                            $feed = new Feed();
                            $feed->setUserId($userId);
                            $feed->setActorId($asker->getUserId());
                            $feed->setQuestionId($question->getId());
                            $feed->setPrivacy($feedPrivacy);
                            $feed->setVerb('CreateQuestion');
                            $feed->setCreatedOn($now);
                            $objectManager->persist($feed);
                        }
                    }
                }
            }
            $objectManager->flush();
        }

        return 'Done';

    }
}
