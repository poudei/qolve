<?php
namespace Qolve\Verb;

use Application\Entity\Notification;

class UpdateQuestion
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if (!isset($data['askerId'])) {
            return false;
        }

        $asker = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['askerId']);
        if (!$asker instanceof \Application\Entity\User) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'shareds':

                if (!isset($data['sharedUsers'])) {
                    return false;
                }

                if (!is_array($data['sharedUsers'])) {
                    $data['sharedUsers'] = array($data['sharedUsers']);
                }

                foreach ($data['sharedUsers'] as $_sharedId) {

                    $share = $objectManager->getRepository('Qolve\Entity\Share')
                        ->findOneByUserId($_sharedId);

                    if (!$share instanceof \Qolve\Entity\Share) {
                       return false;
                    }

                    similar_text($_sharedId, $asker->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId' => $_sharedId,
                                'fromId' => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb' => 'UpdateQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_sharedId);
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setTo($to);
                            $notif->setVerb('UpdateQuestion');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;

            case 'askers':

                $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($question->getId());

                foreach ($askers as $_asker) {
                    similar_text($_asker->getUserId(), $asker->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_asker->getUserId(),
                                'fromId'     => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'UpdateQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_asker->getUserId());
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setTo($to);
                            $notif->setVerb('UpdateQuestion');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);

                            //Add to Queue for send Email
                            $queue = $serviceLocator->get('\SlmQueue\Queue\QueuePluginManager')
                                    ->get('email');

                            $user = $objectManager->getRepository('Application\Entity\User')
                                ->find($_asker->getUserId());

                            $job = new \Application\Job\SendNotification($serviceLocator);
                            $job->setContent(array('email' => $user->getEmail()));
                            $queue->push($job);
                        }
                    }
                }

                break;

            case 'questionFollowers':

                $qFollows = $objectManager->getRepository('Qolve\Entity\QuestionFollow')
                    ->findByQuestionId($question->getId());

                foreach ($qFollows as $_qFollow) {
                    similar_text($_qFollow->getUserId(), $asker->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_qFollow->getUserId(),
                                'fromId'     => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'UpdateQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_qFollow->getUserId());
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setTo($to);
                            $notif->setVerb('UpdateQuestion');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;

            case 'mentions':

                if (!isset($data['mentions'])) {
                    return false;
                }

                if (!is_array($data['mentions'])) {
                    $data['mentions'] = array($data['mentions']);
                }
                foreach ($data['mentions'] as $_mentionId) {

                    similar_text($_mentionId, $asker->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_mentionId,
                                'fromId'     => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'UpdateQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_mentionId);
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setVerb('UpdateQuestion');
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
