<?php
namespace Qolve\Verb;

use Application\Entity\Notification;

class WriteComment
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['commentId'])) {
            return false;
        }

        $comment = $objectManager->getRepository('\Qolve\Entity\Comment')
                ->findOneById($data['commentId']);
        if (!$comment instanceof \Qolve\Entity\Comment) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'askers':

                $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($comment->getQuestionId());

                foreach ($askers as $_asker) {
                    similar_text($_asker->getUserId(), $comment->getUserId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_asker->getUserId(),
                                'fromId'     => $comment->getUserId(),
                                'questionId' => $comment->getQuestionId(),
                                'answerId'   => $comment->getAnswerId(),
                                'content'    => $comment->getId(),
                                'verb'       => 'WriteComment'
                            ));
                        if (!$preNotif instanceof Notification) {
                            
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_asker->getUserId());
                            $notif->setFromId($comment->getUserId());
                            $notif->setQuestionId($comment->getQuestionId());
                            $notif->setAnswerId($comment->getAnswerId());
                            $notif->setContent($comment->getId());
                            $notif->setTo($to);
                            $notif->setVerb('WriteComment');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;

            case 'answerWriter':

                if ($comment->getAnswerId() != null) {
                    $answer = $objectManager->getRepository('Qolve\Entity\Answer')
                            ->find($comment->getAnswerId());

                    similar_text($answer->getUserId(), $comment->getUserId(), $percent);
                    if ($percent < 100) {
                        
                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $answer->getUserId(),
                                'fromId'     => $comment->getUserId(),
                                'questionId' => $comment->getQuestionId(),
                                'answerId'   => $comment->getAnswerId(),
                                'content'    => $comment->getId(),
                                'verb'       => 'WriteComment'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($answer->getUserId());
                            $notif->setFromId($comment->getUserId());
                            $notif->setQuestionId($comment->getQuestionId());
                            $notif->setAnswerId($comment->getAnswerId());
                            $notif->setContent($comment->getId());
                            $notif->setTo($to);
                            $notif->setVerb('WriteComment');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
