<?php
namespace Qolve\Verb;

use Application\Entity\Notification;

class PickBestAnswer
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if (!isset($data['answerId'])) {
            return false;
        }

        $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($data['answerId']);
        if (!$answer instanceof \Qolve\Entity\Answer) {
            return false;
        }

        if (!isset($data['userId'])) {
            return false;
        }

        $user = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['userId']);
        if (!$user instanceof \Application\Entity\User) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'askers':

                $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($question->getId());

                foreach ($askers as $_asker) {
                    similar_text($_asker->getUserId(), $user->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_asker->getUserId(),
                                'fromId'     => $user->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'PickBestAnswer'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_asker->getUserId());
                            $notif->setFromId($user->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setVerb('PickBestAnswer');
                            $notif->setContent($answer->getId());
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;

            case 'questionFollowers':

                $qFollows = $objectManager->getRepository('Qolve\Entity\QuestionFollow')
                    ->findByQuestionId($question->getId());

                foreach ($qFollows as $_qFollow) {
                    similar_text($_qFollow->getUserId(), $user->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_qFollow->getUserId(),
                                'fromId'     => $user->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'PickBestAnswer'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_qFollow->getUserId());
                            $notif->setFromId($user->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setVerb('PickBestAnswer');
                            $notif->setContent($answer->getId());
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;

            case 'answerWriter':

                $writer = $objectManager->getRepository('Application\Entity\User')
                    ->find($answer->getUserId());
                if (!$writer instanceof \Application\Entity\User) {
                    return false;
                }

                similar_text($writer->getId(), $user->getId(), $percent);
                if ($percent < 100) {
                    
                    $preNotif = $objectManager
                        ->getRepository('Application\Entity\Notification')
                        ->findOneBy(array(
                            'toId'       => $writer->getId(),
                            'fromId'     => $user->getId(),
                            'questionId' => $question->getId(),
                            'verb'       => 'PickBestanswer'
                        ));
                    if (!$preNotif instanceof Notification) {
                        
                        $notif = new \Application\Entity\Notification();
                        $notif->setToId($writer->getId());
                        $notif->setFromId($user->getId());
                        $notif->setQuestionId($question->getId());
                        $notif->setVerb('PickBestAnswer');
                        $notif->setContent($answer->getId());
                        $notif->setTo($to);
                        $notif->setSeen(false);
                        $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                        $objectManager->persist($notif);

                        //Add to Queue for send Email
    //                    $emailQueue = $serviceLocator->get('\SlmQueue\Queue\QueuePluginManager')
    //                            ->get('email');
    //
    //                    $job = new \Application\Job\SendEmail($serviceLocator);
    //                    $job->setContent(array(
    //                        'to'        => $writer->getEmail(),
    //                        'subject'   => 'Pick Best Answer',
    //                        'template'  => 'PickBestAnswer',
    //                        'params'    => array(
    //                            'user'      => $writer,
    //                            'question'  => $question,
    //                            'answer'    => $answer,
    //                            'asker'     => $user
    //                        )
    //                    ));
    //                    $emailQueue->push($job);
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
