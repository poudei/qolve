<?php
namespace Qolve\Verb;

use Application\Entity\Feed,
    Application\Entity\User,
    Application\Entity\Notification,    
    Qolve\Entity\Asker,
    Qolve\Entity\Question;

class RaiseCredit
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof Question) {
            return false;
        }

        if (!isset($data['userId'])) {
            return false;
        }

        $user = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['userId']);
        if (!$user instanceof User) {
            return false;
        }

        $asker = $objectManager->getRepository('\Qolve\Entity\Asker')
                ->findOneBy(array(
                    'userId'        => $user->getId(),
                    'questionId'    => $question->getId()
                ));
        if (!$asker instanceof Asker) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'askers':

                $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($question->getId());

                foreach ($askers as $_asker) {
                    similar_text($_asker->getUserId(), $user->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId' => $_asker->getUserId(),
                                'fromId' => $user->getId(),
                                'questionId' => $question->getId(),
                                'verb' => 'RaiseCredit'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new Notification();
                            $notif->setToId($_asker->getUserId());
                            $notif->setFromId($user->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setVerb('RaiseCredit');
                            $notif->setContent($asker->getCredit());
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);

                            //Add to Queue for send Email
                            $emailQueue = $serviceLocator
                                ->get('\SlmQueue\Queue\QueuePluginManager')
                                ->get('email');

                            $user = $objectManager
                                ->getRepository('Application\Entity\User')
                                ->find($_asker->getUserId());

                            $job = new \Application\Job\SendEmail($serviceLocator);
                            $job->setContent(array('email' => $user->getEmail()));
                            $emailQueue->push(
                                $job,
                                array('delay' => new \DateInterval("PT30S"))
                            );
                        }
                    }
                }

                break;

            case 'questionFollowers':

                $qFollows = $objectManager->getRepository('Qolve\Entity\QuestionFollow')
                    ->findByQuestionId($question->getId());

                foreach ($qFollows as $_qFollow) {
                    similar_text($_qFollow->getUserId(), $user->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_qFollow->getUserId(),
                                'fromId'     => $user->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'RaiseCredit'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_qFollow->getUserId());
                            $notif->setFromId($user->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setVerb('RaiseCredit');
                            $notif->setContent($asker->getCredit());
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }

    public static function publishFeed($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof Question) {
            return false;
        }

        if (!isset($data['userId'])) {
            return false;
        }

        $user = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['userId']);
        if (!$user instanceof User) {
            return false;
        }

        $asker = $objectManager->getRepository('\Qolve\Entity\Asker')
                ->findOneBy(array(
                    'userId'        => $user->getId(),
                    'questionId'    => $question->getId()
                ));
        if (!$asker instanceof Asker) {
            return false;
        }

        $feedPrivacy = max(array(
            $question->getPrivacy(),
            $asker->getVisibility()
        ));

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {

            case 'userFollowers':

                $follows = $objectManager->getRepository('Qolve\Entity\UserFollow')
                            ->findByUserId($asker->getUserId());
                foreach ($follows as $_follow) {
                    similar_text($_follow->getFollowerId(), $asker->getUserId(), $percent);
                    if ($percent < 100) {

                        $preFeed = $objectManager
                            ->getRepository('Application\Entity\Feed')
                            ->findOneBy(array(
                                'userId'     => $_follow->getFollowerId(),
                                'actorId'    => $asker->getUserId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'RaiseCredit'
                            ));
                        if (!$preFeed instanceof Feed) {
                        
                            $feed = new Feed();
                            $feed->setUserId($_follow->getFollowerId());
                            $feed->setActorId($asker->getUserId());
                            $feed->setQuestionId($question->getId());
                            $feed->setPrivacy($feedPrivacy);
                            $feed->setVerb('RaiseCredit');
                            $feed->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));
                            $objectManager->persist($feed);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
