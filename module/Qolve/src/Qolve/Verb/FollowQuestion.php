<?php
namespace Qolve\Verb;

use Application\Entity\Notification;

class FollowQuestion
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['userId'])) {
            return false;
        }

        $user = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['userId']);
        if (!$user instanceof \Application\Entity\User) {
            return false;
        }

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'askers':

                $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($question->getId());

                foreach ($askers as $_asker) {
                    similar_text($_asker->getUserId(), $user->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_asker->getUserId(),
                                'fromId'     => $user->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'FollowQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_asker->getUserId());
                            $notif->setFromId($user->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setTo($to);
                            $notif->setVerb('FollowQuestion');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
