<?php
namespace Qolve\Verb;

use Application\Entity\Notification;

class Vote
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['voteId'])) {
            return false;
        }

        $vote = $objectManager->getRepository('\Qolve\Entity\Vote')
                ->find($data['voteId']);
        if (!$vote instanceof \Qolve\Entity\Vote) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'askers':

                $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($vote->getQuestionId());

                foreach ($askers as $_asker) {
                    similar_text($_asker->getUserId(), $vote->getUserId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_asker->getUserId(),
                                'fromId'     => $vote->getUserId(),
                                'questionId' => $vote->getQuestionId(),
                                'answerId'   => $vote->getAnswerId(),
                                'verb'       => 'Vote'
                        ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_asker->getUserId());
                            $notif->setFromId($vote->getUserId());
                            $notif->setQuestionId($vote->getQuestionId());
                            $notif->setAnswerId($vote->getAnswerId());
                            $notif->setContent((string) $vote->getScore());
                            $notif->setTo($to);
                            $notif->setVerb('Vote');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;

            case 'answerWriter':

                if ($vote->getAnswerId() != null) {
                    $answer = $objectManager->getRepository('Qolve\Entity\Answer')
                        ->find($vote->getAnswerId());

                    similar_text($answer->getUserId(), $vote->getUserId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $answer->getUserId(),
                                'fromId'     => $vote->getUserId(),
                                'questionId' => $vote->getQuestionId(),
                                'answerId'   => $vote->getAnswerId(),
                                'verb'       => 'Vote'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($answer->getUserId());
                            $notif->setFromId($vote->getUserId());
                            $notif->setQuestionId($vote->getQuestionId());
                            $notif->setAnswerId($vote->getAnswerId());
                            $notif->setContent((string) $vote->getScore());
                            $notif->setTo($to);
                            $notif->setVerb('Vote');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }

    public static function publishFeed($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['voteId'])) {
            return false;
        }

        $vote = $objectManager->getRepository('Qolve\Entity\Vote')
                ->findOneById($data['voteId']);
        if (!$vote instanceof \Qolve\Entity\Vote) {
            return false;
        }

        $question = $objectManager->getRepository('Qolve\Entity\Question')
                ->findOneById($vote->getQuestionId());
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if ($vote->getAnswerId() != null) {
            $answer = $objectManager->getRepository('Qolve\Entity\Answer')
                    ->findOneById($vote->getAnswerId());
            if (!$answer instanceof \Qolve\Entity\Answer) {
                return false;
            }

            $feedPrivacy = max(array(
                $question->getPrivacy(),
                $answer->getPrivacy()
            ));
        } else {
            $feedPrivacy = $question->getPrivacy();
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'userFollowers':

                $follows = $objectManager->getRepository('Qolve\Entity\UserFollow')
                        ->findByUserId($vote->getUserId());

                foreach ($follows as $_follow) {
                    similar_text($_follow->getFollowerId(), $vote->getUserId(), $percent);
                    if ($percent < 100) {
//TODO :  ADD $objectManager->getConnection()->getShardManager()->selectShard($_follow->getFollowerId);
                        $preFeed = $objectManager
                            ->getRepository('Application\Entity\Feed')
                            ->findOneBy(array(
                                'userId'     => $_follow->getFollowerId(),
                                'actorId'    => $vote->getUserId(),
                                'questionId' => $vote->getQuestionId(),
                                'answerId'   => $vote->getAnswerId(),
                                'content'    => (string) $vote->getScore(),
                                'verb'       => 'Vote'
                            )
                        );
                        if (!$preFeed instanceof \Application\Entity\Feed) {

                            $feed = new \Application\Entity\Feed();
                            $feed->setUserId($_follow->getFollowerId());
                            $feed->setActorId($vote->getUserId());
                            $feed->setQuestionId($vote->getQuestionId());
                            $feed->setAnswerId($vote->getAnswerId());
                            $feed->setContent((string) $vote->getScore());
                            $feed->setPrivacy($feedPrivacy);
                            $feed->setVerb('Vote');
                            $feed->setCreatedOn($now);

                            $objectManager->persist($feed);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
