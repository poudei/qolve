<?php
namespace Qolve\Verb;

use Application\Entity\Feed,
    Application\Entity\Notification;

class UpdateAnswer
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['answerId'])) {
            return false;
        }

        $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($data['answerId']);
        if (!$answer instanceof \Qolve\Entity\Answer) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'askers':

                $askers = $objectManager->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($answer->getQuestionId());

                foreach ($askers as $_to) {

                    similar_text($_to->getUserId(), $answer->getUserId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_to->getUserId(),
                                'fromId'     => $answer->getUserId(),
                                'questionId' => $answer->getQuestionId(),
                                'answerId'   => $answer->getId(),
                                'verb'       => 'UpdateAnswer'
                            ));
                        if (!$preNotif instanceof Notification) {

                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_to->getUserId());
                            $notif->setFromId($answer->getUserId());
                            $notif->setQuestionId($answer->getQuestionId());
                            $notif->setAnswerId($answer->getId());
                            $notif->setTo($to);
                            $notif->setVerb('UpdateAnswer');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);

                            //Add to Queue for send Email
                            $emailQueue = $serviceLocator
                                ->get('\SlmQueue\Queue\QueuePluginManager')
                                ->get('email');

                            $user = $objectManager->getRepository('Application\Entity\User')
                                ->find($_to->getUserId());

                            $job = new \Application\Job\SendEmail($serviceLocator);
                            $job->setContent(array('email' => $user->getEmail()));
                            $emailQueue->push(
                                $job,
                                array('delay' => new \DateInterval("PT30S"))
                            );
                        }
                    }
                }

                break;

            case 'questionFollowers':

                $followers = $objectManager->getRepository('Qolve\Entity\QuestionFollow')
                    ->findByQuestionId($answer->getQuestionId());

                foreach ($followers as $_to) {
                    similar_text($_to->getUserId(), $answer->getUserId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_to->getUserId(),
                                'fromId'     => $answer->getUserId(),
                                'questionId' => $answer->getQuestionId(),
                                'answerId'   => $answer->getId(),
                                'verb'       => 'UpdateAnswer'
                            ));
                        if (!$preNotif instanceof Notification) {

                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_to->getUserId());
                            $notif->setFromId($answer->getUserId());
                            $notif->setQuestionId($answer->getQuestionId());
                            $notif->setAnswerId($answer->getId());
                            $notif->setTo($to);
                            $notif->setVerb('UpdateAnswer');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;

                case 'mentions':

                if (!isset($data['mentions'])) {
                    return false;
                }

                if (!is_array($data['mentions'])) {
                    $data['mentions'] = array($data['mentions']);
                }
                foreach ($data['mentions'] as $_mentionId) {

                    similar_text($_mentionId, $answer->getUserId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_mentionId,
                                'fromId'     => $answer->getUserId(),
                                'questionId' => $answer->getQuestionId(),
                                'answerId'   => $answer->getId(),
                                'verb'       => 'UpdateAnswer'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_mentionId);
                            $notif->setFromId($answer->getUserId());
                            $notif->setQuestionId($answer->getQuestionId());
                            $notif->setAnswerId($answer->getId());
                            $notif->setVerb('UpdateAnswer');
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }

    public static function publishFeed($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if (!isset($data['answerId'])) {
            return false;
        }
        $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($data['answerId']);
        if (!$answer instanceof \Qolve\Entity\Answer) {
            return false;
        }

        $feedPrivacy = max(array(
            $question->getPrivacy(),
            $answer->getPrivacy(),
            $answer->getVisibility()
        ));

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {

            case 'userFollowers':

                $followers = $objectManager->getRepository('Qolve\Entity\UserFollow')
                            ->findByUserId($answer->getUserId());

                foreach ($followers as $_to) {
                    similar_text($_to->getFollowerId(), $answer->getUserId(), $percent);
                    if ($percent < 100) {

                        $preFeed = $objectManager
                            ->getRepository('Application\Entity\Feed')
                            ->findOneBy(array(
                                'userId'     => $_to->getFollowerId(),
                                'actorId'    => $answer->getUserId(),
                                'questionId' => $question->getId(),
                                'answerId'   => $answer->getId(),
                                'verb'       => 'UpdateAnswer'
                            ));
                        if (!$preFeed instanceof Feed) {

                            $feed = new Feed();
                            $feed->setUserId($_to->getFollowerId());
                            $feed->setActorId($answer->getUserId());
                            $feed->setQuestionId($question->getId());
                            $feed->setAnswerId($answer->getId());
                            $feed->setPrivacy($feedPrivacy);
                            $feed->setVerb('UpdateAnswer');
                            $feed->setCreatedOn($answer->getCreatedOn());
                            $objectManager->persist($feed);
                        }
                    }
                }

                break;

            case 'shareds':

                $shares = $objectManager->getRepository('Qolve\Entity\Share')
                    ->findByQuestionId($question->getId());

                foreach ($shares as $_to) {
                    similar_text($_to->getUserId(), $answer->getUserId(), $percent);
                    if ($percent < 100) {

                        $preFeed = $objectManager
                            ->getRepository('Application\Entity\Feed')
                            ->findOneBy(array(
                                'userId'     => $_to->getUserId(),
                                'actorId'    => $answer->getUserId(),
                                'questionId' => $question->getId(),
                                'answerId'   => $answer->getId(),
                                'verb'       => 'UpdateAnswer'
                            ));
                        if (!$preFeed instanceof Feed) {

                            $feed = new Feed();
                            $feed->setUserId($_to->getUserId());
                            $feed->setActorId($answer->getUserId());
                            $feed->setQuestionId($question->getId());
                            $feed->setAnswerId($answer->getId());
                            $feed->setPrivacy($feedPrivacy);
                            $feed->setVerb('UpdateAnswer');
                            $feed->setCreatedOn($answer->getCreatedOn());
                            $objectManager->persist($feed);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
