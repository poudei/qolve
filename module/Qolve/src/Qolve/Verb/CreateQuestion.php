<?php
namespace Qolve\Verb;

use Application\Entity\Notification,
    Application\Entity\Feed;

class CreateQuestion
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if (!isset($data['askerId'])) {
            return false;
        }

        $asker = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['askerId']);
        if (!$asker instanceof \Application\Entity\User) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'shareds':

                if (!isset($data['sharedUsers'])) {
                    return false;
                }

                if (!is_array($data['sharedUsers'])) {
                    $data['sharedUsers'] = array($data['sharedUsers']);
                }

                foreach ($data['sharedUsers'] as $_sharedId) {

                    $share = $objectManager->getRepository('Qolve\Entity\Share')
                        ->findOneByUserId($_sharedId);

                    if (!$share instanceof \Qolve\Entity\Share) {
                       return false;
                    }

                    similar_text($_sharedId, $asker->getId(), $percent);
                    if ($percent < 100) {
                        
                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_sharedId,
                                'fromId'     => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'CreateQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_sharedId);
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setVerb('CreateQuestion');
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
                

            case 'mentions':

                if (!isset($data['mentions'])) {
                    return false;
                }

                if (!is_array($data['mentions'])) {
                    $data['mentions'] = array($data['mentions']);
                }

                foreach ($data['mentions'] as $_mentionId) {

                    similar_text($_mentionId, $asker->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_mentionId,
                                'fromId'     => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'CreateQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_mentionId);
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setVerb('CreateQuestion');
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
                
                
            case 'userFollowers':

                $follows = $objectManager->getRepository('Qolve\Entity\UserFollow')
                            ->findByUserId($asker->getId());
                foreach ($follows as $_follow) {
                    similar_text($_follow->getFollowerId(), $asker->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_follow->getFollowerId(),
                                'fromId'     => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'CreateQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_follow->getFollowerId());
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setTo($to);
                            $notif->setVerb('CreateQuestion');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();
        $apns = $serviceLocator->get('apns');

        return 'Done';
    }

    public static function publishFeed($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if (!isset($data['askerId'])) {
            return false;
        }

        $asker = $objectManager->getRepository('\Qolve\Entity\Asker')
                ->findOneByUserId($data['askerId']);
        if (!$asker instanceof \Qolve\Entity\Asker) {
            return false;
        }

        $feedPrivacy = max(array(
            $question->getPrivacy(),
            $asker->getVisibility()
        ));

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {

            case 'userFollowers':

                $follows = $objectManager->getRepository('Qolve\Entity\UserFollow')
                            ->findByUserId($asker->getUserId());
                foreach ($follows as $_follow) {
                    similar_text($_follow->getFollowerId(), $asker->getUserId(), $percent);
                    if ($percent < 100) {

                        $preFeed = $objectManager
                            ->getRepository('Application\Entity\Feed')
                            ->findOneBy(array(
                                'userId'     => $_follow->getFollowerId(),
                                'actorId'    => $asker->getUserId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'CreateQuestion'
                            ));
                        if (!$preFeed instanceof Feed) {
                            $feed = new Feed();
                            $feed->setUserId($_follow->getFollowerId());
                            $feed->setActorId($asker->getUserId());
                            $feed->setQuestionId($question->getId());
                            $feed->setPrivacy($feedPrivacy);
                            $feed->setVerb('CreateQuestion');
                            $feed->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));
                            $objectManager->persist($feed);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }

    public static function publishStatus($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if (!isset($data['askerId'])) {
            return false;
        }

        $asker = $objectManager->getRepository('\Qolve\Entity\Asker')
                ->findOneByUserId($data['askerId']);
        if (!$asker instanceof \Qolve\Entity\Asker) {
            return false;
        }

        $feedPrivacy = max(array(
            $question->getPrivacy(),
            $asker->getVisibility()
        ));

        $message = $question->getDescription();
        
        
        //todo
        $hybridauth = new \Hybrid_Auth(dirname(__DIR__) . "/../../../../config/autoload/hybridauth.php");
        $provider = $hybridauth->authenticate($userProvider->getProvider());
        $provider->setUserStatus($message);

        return 'Done';
    }
}
