<?php
namespace Qolve\Verb;

use Application\Entity\Notification;

class FollowUser
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['followerId'])) {
            return false;
        }

        $follower = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['followerId']);
        if (!$follower instanceof \Application\Entity\User) {
            return false;
        }

        if (!isset($data['folloyeeId'])) {
            return false;
        }

        $folloyee = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['folloyeeId']);
        if (!$folloyee instanceof \Application\Entity\User) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'folloyee':
                
                similar_text($folloyee->getId(), $follower->getId(), $percent);
                if ($percent < 100) {
                    
                    $preNotif = $objectManager
                        ->getRepository('Application\Entity\Notification')
                        ->findOneBy(array(
                            'toId' => $folloyee->getId(),
                            'fromId' => $follower->getId(),
                            'verb' => 'FollowUser'
                        ));
                    if (!$preNotif instanceof Notification) {
                        $notif = new \Application\Entity\Notification();
                        $notif->setToId($folloyee->getId());
                        $notif->setFromId($follower->getId());
                        $notif->setTo($to);
                        $notif->setVerb('FollowUser');
                        $notif->setContent($folloyee->getId());
                        $notif->setSeen(false);
                        $notif->setCreatedOn($now);

                        $objectManager->persist($notif);
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }

    public static function publishFeed($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['followerId'])) {
            return false;
        }

        $follower = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['followerId']);

        if (!$follower instanceof \Application\Entity\User) {
            return false;
        }

        if (!isset($data['folloyeeId'])) {
            return false;
        }

        $folloyee = $objectManager->getRepository('\Application\Entity\User')
            ->findOneById($data['folloyeeId']);

        if (!$folloyee instanceof \Application\Entity\User) {
            return false;
        }

        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        //Add to user's Activities
        
        $preFeed = $objectManager->getRepository('Application\Entity\Feed')
            ->findOneBy(array(
                'userId'  => $follower->getId(),
                'actorId' => $follower->getId(),
                'verb'    => 'FollowUser',
                'content' => $folloyee->getId()
            ));
        if (!$preFeed instanceof \Application\Entity\Feed) {
            $feed = new \Application\Entity\Feed();
            $feed->setUserId($follower->getId());
            $feed->setActorId($follower->getId());
            $feed->setPrivacy(\Application\Entity\Feed::PRIVACY_PRIVATE);
            $feed->setVerb('FollowUser');
            $feed->setContent($folloyee->getId());
            $feed->setCreatedOn($now);
            $objectManager->persist($feed);
            $objectManager->flush();
        }
    }
}
