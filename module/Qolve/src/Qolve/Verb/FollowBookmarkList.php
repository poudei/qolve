<?php
namespace Qolve\Verb;

use Application\Entity\Notification;

class FollowBookmarkList
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['userId'])) {
            return false;
        }

        $user = $objectManager->getRepository('\Application\Entity\User')
                ->findOneById($data['userId']);
        if (!$user instanceof \Application\Entity\User) {
            return false;
        }

        if (!isset($data['listId'])) {
            return false;
        }

        $list = $objectManager->getRepository('\Qolve\Entity\BookmarkList')
                ->findOneById($data['listId']);
        if (!$list instanceof \Qolve\Entity\BookmarkList) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'user':
                
                similar_text($list->getUserId(), $user->getId(), $percent);
                if ($percent < 100) {
                    
                    $preNotif = $objectManager
                        ->getRepository('Application\Entity\Notification')
                        ->findOneBy(array(
                            'toId'    => $list->getUserId(),
                            'fromId'  => $user->getId(),
                            'content' => $list->getId(),
                            'verb'    => 'FollowBookmarkList'
                        ));
                    if (!$preNotif instanceof Notification) {
                        
                        $notif = new \Application\Entity\Notification();
                        $notif->setToId($list->getUserId());
                        $notif->setFromId($user->getId());
                        $notif->setContent($list->getId());
                        $notif->setTo($to);
                        $notif->setVerb('FollowBookmarkList');
                        $notif->setSeen(false);
                        $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                        $objectManager->persist($notif);
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
