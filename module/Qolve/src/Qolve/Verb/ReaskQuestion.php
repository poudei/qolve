<?php
namespace Qolve\Verb;

use Qolve\Entity\Asker,
    Qolve\Entity\Question,
    Application\Entity\Notification,
    Application\Entity\User,
    Application\Job\SendEmail,
    Application\Entity\Feed;

class ReaskQuestion
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof Question) {
            return false;
        }

        if (!isset($data['askerId'])) {
            return false;
        }

        $asker = $objectManager
            ->getRepository('\Application\Entity\User')
            ->find($data['askerId']);

        if (!$asker instanceof User) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'askers':

                $askers = $objectManager
                    ->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($question->getId());

                foreach ($askers as $_asker) {
                    similar_text($_asker->getUserId(), $asker->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_asker->getUserId(),
                                'fromId'     => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'ReaskQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            
                            $notif = new Notification();
                            $notif->setToId($_asker->getUserId());
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setTo($to);
                            $notif->setVerb('ReaskQuestion');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);

                            if (   $data['visibility']
                                == Asker::VISIBILITY_ANONYMOUS
                            ) {
                                $reasker = Asker::ANONYMOUS_NAME;
                                $subject = Asker::ANONYMOUS_NAME; 

                            } else {
                                $reasker = $asker->getName();
                                $subject = $asker->getUsername();
                            }

                            $user = $objectManager
                                ->getRepository('\Application\Entity\User')
                                ->find($_asker->getUserId());

                            if ($user instanceof User) {

                                $config = $serviceLocator->get('config');
                                $emailQueue = $serviceLocator
                                    ->get('\SlmQueue\Queue\QueuePluginManager')
                                    ->get('email');

                                //Add to Queue for send Email
                                $job = new SendEmail($serviceLocator);
                                $job->setContent(array(
                                    'to'        => $user->getEmail(),
                                    'subject'   => $subject . " reasked your problem",
                                    'template'  => 'Reask',
                                    'params'    => array(
                                        'asker'      => $user->getName(),
                                        'reasker'    => $reasker,
                                        'questionId' => $question->getId(),
                                        'baseUrl'    => $config['baseUrl']
                                        )
                                ));

                                $emailQueue->push(
                                    $job,
                                    array('delay' => new \DateInterval("PT30S"))
                                );
                            }
                        }
                    }
                }

                break;
                
            case 'questionFollowers':

                $qFollows = $objectManager->getRepository('Qolve\Entity\QuestionFollow')
                    ->findByQuestionId($question->getId());

                foreach ($qFollows as $_qFollow) {
                    similar_text($_qFollow->getUserId(), $asker->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_qFollow->getUserId(),
                                'fromId'     => $asker->getId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'ReaskQuestion'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new Notification();
                            $notif->setToId($_qFollow->getUserId()); 
                            $notif->setFromId($asker->getId());
                            $notif->setQuestionId($question->getId());
                            $notif->setTo($to);
                            $notif->setVerb('ReaskQuestion');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;

            case 'userFollowers':

                $askers = $objectManager
                    ->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($question->getId());

                foreach ($askers as $_asker) {
                    $follows = $objectManager
                        ->getRepository('Qolve\Entity\UserFollow')
                        ->findByUserId($_asker->getUserId());

                    foreach ($follows as $_follow) {
                        similar_text($_follow->getFollowerId(), $asker->getId(), $percent);
                        if ($percent < 100) {

                            $preNotif = $objectManager
                                ->getRepository('Application\Entity\Notification')
                                ->findOneBy(array(
                                    'toId'       => $_follow->getFollowerId(),
                                    'fromId'     => $asker->getId(),
                                    'questionId' => $question->getId(),
                                    'verb'       => 'ReaskQuestion'
                                ));
                            if (!$preNotif instanceof Notification) {
                                $notif = new Notification();
                                $notif->setToId($_follow->getFollowerId());
                                $notif->setFromId($asker->getId());
                                $notif->setQuestionId($question->getId());
                                $notif->setTo($to);
                                $notif->setVerb('ReaskQuestion');
                                $notif->setSeen(false);
                                $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                                $objectManager->persist($notif);
                            }
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }

    public static function publishFeed($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }

        $question = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->findOneById($data['questionId']);
        if (!$question instanceof Question) {
            return false;
        }

        if (!isset($data['askerId'])) {
            return false;
        }

        $asker = $objectManager
            ->getRepository('\Qolve\Entity\Asker')
            ->findOneByUserId($data['askerId']);
        if (!$asker instanceof Asker) {
            return false;
        }

        $feedPrivacy = max(array(
            $question->getPrivacy(),
            $asker->getVisibility()
        ));

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'userFollowers':

                $follows = $objectManager
                    ->getRepository('Qolve\Entity\UserFollow')
                    ->findByUserId($asker->getUserId());

                foreach ($follows as $_follow) {
                    similar_text($_follow->getFollowerId(), $asker->getUserId(), $percent);
                    if ($percent < 100) {
                        
                        $preFeed = $objectManager
                            ->getRepository('Application\Entity\Feed')
                            ->findOneBy(array(
                                'userId'     => $_follow->getFollowerId(),
                                'actorId'    => $asker->getUserId(),
                                'questionId' => $question->getId(),
                                'verb'       => 'ReaskQuestion'
                            ));
                        if (!$preFeed instanceof Feed) {

                            $feed = new \Application\Entity\Feed();
                            $feed->setUserId($_follow->getFollowerId());
                            $feed->setActorId($asker->getUserId());
                            $feed->setQuestionId($question->getId());
                            $feed->setPrivacy($feedPrivacy);
                            $feed->setVerb('ReaskQuestion');
                            $feed->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($feed);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
