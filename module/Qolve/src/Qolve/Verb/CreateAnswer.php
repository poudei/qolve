<?php
namespace Qolve\Verb;

use Application\Entity\Notification,
    Qolve\Entity\Question,
    Qolve\Entity\Answer,
    Application\Entity\User,
    Application\Job\SendEmail,
    Application\Entity\Feed;

class CreateAnswer
{
    public static function sendNotification($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['answerId'])) {
            return false;
        }

        $answer = $objectManager
            ->getRepository('\Qolve\Entity\Answer')
            ->find($data['answerId']);

        if (!$answer instanceof Answer) {
            return false;
        }

        $question = $objectManager
            ->getRepository('\Qolve\Entity\Question')
            ->find($answer->getQuestionId());

        if (!$question instanceof Question) {
            return false;
        }

        $writer = $objectManager
            ->getRepository('Application\Entity\User')
            ->find($answer->getUserId());

        if (!$writer instanceof User) {
            return false;
        }

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        $emailQueue = $serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('email');

        foreach ($data['to'] as $to) {
            switch ($to) {
            case 'askers':

                $askers = $objectManager
                    ->getRepository('Qolve\Entity\Asker')
                    ->findByQuestionId($answer->getQuestionId());

                foreach ($askers as $_asker) {
                    $_to = $objectManager
                        ->getRepository('Application\Entity\User')
                        ->find($_asker->getUserId());

                    if ($_to instanceof User) {
                        similar_text($_to->getId(), $writer->getId(), $percent);
                        if ($percent < 100) {

                            $preNotif = $objectManager
                                ->getRepository('Application\Entity\Notification')
                                ->findOneBy(array(
                                    'toId'       => $_to->getId(),
                                    'fromId'     => $writer->getId(),
                                    'questionId' => $answer->getQuestionId(),
                                    'answerId'   => $answer->getId(),
                                    'verb'       => 'CreateAnswer'
                                ));
                            if (!$preNotif instanceof Notification) {

                                $notif = new \Application\Entity\Notification();
                                $notif->setToId($_to->getId());
                                $notif->setFromId($writer->getId());
                                $notif->setQuestionId($answer->getQuestionId());
                                $notif->setAnswerId($answer->getId());
                                $notif->setTo($to);
                                $notif->setVerb('CreateAnswer');
                                $notif->setSeen(false);
                                $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                                $objectManager->persist($notif);

                                $config = $serviceLocator->get('config');
                                $emailQueue = $serviceLocator
                                    ->get('\SlmQueue\Queue\QueuePluginManager')
                                    ->get('email');

                                //Add to Queue for send Email
                                if (   $answer->getVisibility()
                                    == Answer::VISIBILITY_ANONYMOUS
                                ) {
                                    $writerName  = Answer::ANONYMOUS_NAME;
                                    $subject     = Answer::ANONYMOUS_NAME;
                                } else {
                                    $writerName  = $writer->getName();
                                    $subject     = $writer->getUsername();
                                }

                                $job = new SendEmail($serviceLocator);
                                $job->setContent(array(
                                    'to'        => $_to->getEmail(),
                                    'subject'   => $subject
                                        . " posted a solution on your problem",
                                    'template'  => 'CreateAnswer',
                                    'params'    => array(
                                        'name'       => $_to->getName(),
                                        'questionId' => $question->getId(),
                                        'answerId'   => $answer->getId(),
                                        'baseUrl'    => $config['baseUrl'],
                                        'writer'     => $writerName
                                    )
                                ));

                                $emailQueue->push(
                                    $job,
                                    array('delay' => new \DateInterval("PT30S"))
                                );

            //                    $apns = $serviceLocator()->get('apns');
            //
            //                    ($data['notifType'] == 'android') ?
            //                        $apns->androidApns($registerationId, $message) :
            //                        $apns->appleApns($registerationId, $message);
                            }
                        }
                    }
                }

                break;
            case 'mentions':

                if (!isset($data['mentions'])) {
                    return false;
                }

                if (!is_array($data['mentions'])) {
                    $data['mentions'] = array($data['mentions']);
                }
                foreach ($data['mentions'] as $_mentionId) {

                    similar_text($_mentionId, $writer->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId' => $_mentionId,
                                'fromId' => $writer->getId(),
                                'questionId' => $answer->getQuestionId(),
                                'answerId' => $answer->getId(),
                                'verb' => 'CreateAnswer'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new \Application\Entity\Notification();
                            $notif->setToId($_mentionId);
                            $notif->setFromId($writer->getId());
                            $notif->setQuestionId($answer->getQuestionId());
                            $notif->setAnswerId($answer->getId());
                            $notif->setVerb('CreateAnswer');
                            $notif->setTo($to);
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;


            case 'questionFollowers':

                $followers = $objectManager->getRepository('Qolve\Entity\QuestionFollow')
                    ->findByQuestionId($answer->getQuestionId());

                foreach ($followers as $_to) {
                    similar_text($_to->getUserId(), $writer->getId(), $percent);
                    if ($percent < 100) {

                        $preNotif = $objectManager
                            ->getRepository('Application\Entity\Notification')
                            ->findOneBy(array(
                                'toId'       => $_to->getUserId(),
                                'fromId'     => $writer->getId(),
                                'questionId' => $answer->getQuestionId(),
                                'answerId'   => $answer->getId(),
                                'verb'       => 'CreateAnswer'
                            ));
                        if (!$preNotif instanceof Notification) {
                            $notif = new Notification();
                            $notif->setToId($_to->getUserId());
                            $notif->setFromId($writer->getId());
                            $notif->setQuestionId($answer->getQuestionId());
                            $notif->setAnswerId($answer->getId());
                            $notif->setTo($to);
                            $notif->setVerb('CreateAnswer');
                            $notif->setSeen(false);
                            $notif->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));

                            $objectManager->persist($notif);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }

    public static function publishFeed($data, $serviceLocator)
    {
        $objectManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        if (!isset($data['questionId'])) {
            return false;
        }
        $question = $objectManager->getRepository('\Qolve\Entity\Question')
                ->findOneById($data['questionId']);
        if (!$question instanceof \Qolve\Entity\Question) {
            return false;
        }

        if (!isset($data['answerId'])) {
            return false;
        }
        $answer = $objectManager->getRepository('\Qolve\Entity\Answer')
                ->findOneById($data['answerId']);
        if (!$answer instanceof \Qolve\Entity\Answer) {
            return false;
        }

        $feedPrivacy = max(array(
            $question->getPrivacy(),
            $answer->getPrivacy(),
            $answer->getVisibility()
        ));

        if (!is_array($data['to'])) {
            $data['to'] = array($data['to']);
        }

        foreach ($data['to'] as $to) {
            switch ($to) {

            case 'userFollowers':

                $followers = $objectManager->getRepository('Qolve\Entity\UserFollow')
                            ->findByUserId($answer->getUserId());

                foreach ($followers as $_to) {
                    similar_text($_to->getFollowerId(), $answer->getUserId(), $percent);
                    if ($percent < 100) {

                        $preFeed = $objectManager
                            ->getRepository('Application\Entity\Feed')
                            ->findOneBy(array(
                                'userId'     => $_to->getFollowerId(),
                                'actorId'    => $answer->getUserId(),
                                'questionId' => $question->getId(),
                                'answerId'   => $answer->getId(),
                                'verb'       => 'CreateAnswer'
                            ));
                        if (!$preFeed instanceof Feed) {
                            $feed = new Feed();
                            $feed->setUserId($_to->getFollowerId());
                            $feed->setActorId($answer->getUserId());
                            $feed->setQuestionId($question->getId());
                            $feed->setAnswerId($answer->getId());
                            $feed->setPrivacy($feedPrivacy);
                            $feed->setVerb('CreateAnswer');
                            $feed->setCreatedOn($answer->getCreatedOn());
                            $objectManager->persist($feed);
                        }
                    }
                }

                break;
            }
        }

        $objectManager->flush();

        return 'Done';
    }
}
