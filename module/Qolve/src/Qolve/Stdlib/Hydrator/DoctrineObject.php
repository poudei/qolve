<?php
namespace Qolve\Stdlib\Hydrator;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as BaseHydrator;
use Qolve\Entity\Asker;
use Application\Entity\User;
use Application\Entity\UserPref;
use Qolve\Entity\KeywordFollow;
use Qolve\Entity\Answer;
use Qolve\Entity\Question;
use Qolve\Entity\Comment;
use Qolve\Entity\AnswerPrivilege;
use Qolve\Entity\Vote;
use Qolve\Entity\Keyword;
use Qolve\Entity\BookmarkList;
use Qolve\Entity\BookmarkListFollow;
use Application\Entity\Notification;
use Application\Entity\Feed;

class DoctrineObject extends BaseHydrator
{
    /**
     * Extract values from an object
     *
     * @param  object $object
     * @return array
     */
    public function extract($object, $info = [], $currentUser = null)
    {
        if (!is_object($object)) {
            return $object;
        }
        
        $data = parent::extract($object);

        if (property_exists($object, 'id')
            && (!isset($data['id']) || !is_numeric($data['id']))
        ) {
            return $data;
        }

        if ($object instanceof \Application\Entity\User) {
            unset($data['password']);
            unset($data['email']);
        }

        if ($object instanceof \Application\Entity\UserProfile) {
            unset($data['userId'], $data['deleted'], $data['createdOn'],
                $data['modifiedBy'], $data['modifiedOn']);
        }

        if ($object instanceof \Qolve\Entity\Answer) {
            unset($data['questionId']);
        }

        if ($object instanceof \Qolve\Entity\Comment) {
            unset($data['questionId'], $data['answerId']);
        }

        if ($object instanceof \Qolve\Entity\Document) {
            unset($data['questionId'], $data['answerId']);
        }

        if ($object instanceof \Qolve\Entity\Vote) {
            unset($data['questionId'], $data['answerId']);
        }

        if ($object instanceof \Application\Entity\Notification) {
            unset($data['toId']);
        }

        if (isset($data['userId']) && !empty($data['userId']) && !isset($data['user'])) {
            $user = $this->objectManager->getRepository('Application\Entity\User')
                ->find($data['userId']);
            if ($user instanceof \Application\Entity\User) {
                $data['user'] = $this->extract($user);
            }
        }

        if (isset($data['actorId']) && !empty($data['actorId']) && !isset($data['actor'])) {
            $actor = $this->objectManager->getRepository('Application\Entity\User')
                ->find($data['actorId']);
            if ($actor instanceof \Application\Entity\User) {
                $data['actor'] = $this->extract($actor);
            }
        }

        if (isset($data['fromId']) && !empty($data['fromId']) && !isset($data['from'])) {
            $from = $this->objectManager->getRepository('Application\Entity\User')
                ->find($data['fromId']);
            if ($from instanceof \Application\Entity\User) {
                $data['from'] = $this->extract($from);
            }
        }

        if (isset($data['followerId']) && !empty($data['followerId']) && !isset($data['follower'])) {
            $follower = $this->objectManager->getRepository('Application\Entity\User')
                ->find($data['followerId']);
            if ($follower instanceof \Application\Entity\User) {
                $data['follower'] = $this->extract($follower);
            }
        }

        if (isset($data['folloyeeId']) && !empty($data['folloyeeId']) && !isset($data['folloyee'])) {
            $folloyee = $this->objectManager->getRepository('Application\Entity\User')
                ->find($data['folloyeeId']);
            if ($folloyee instanceof \Application\Entity\User) {
                $data['folloyee'] = $this->extract($folloyee);
            }
        }

        if (isset($data['questionId']) && !empty($data['questionId']) && !isset($data['question'])) {
            $question = $this->objectManager->getRepository('Qolve\Entity\Question')
                ->find($data['questionId']);
            if ($question instanceof \Qolve\Entity\Question) {
                $data['question'] = $this->extract($question, $info, $currentUser);
            }
        }

        if (isset($data['answerId']) && !empty($data['answerId']) && !isset($data['answer'])) {
            $answer = $this->objectManager->getRepository('Qolve\Entity\Answer')
                ->find($data['answerId']);
            if ($answer instanceof \Qolve\Entity\Answer) {
                $data['answer'] = $this->extract($answer, $info, $currentUser);
            }
        }

        if (isset($data['keywordId']) && !empty($data['keywordId']) && !isset($data['keyword'])) {
            $keyword = $this->objectManager->getRepository('Qolve\Entity\Keyword')
                ->find($data['keywordId']);
            if ($keyword instanceof \Qolve\Entity\Keyword) {
                $data['keyword'] = $keyword->getName();
            }
        }


        if ($object instanceof \Application\Entity\User) {

            if (array_search('listsCount', $info) !== false) {
                if ($currentUser instanceof \Application\Entity\User) {
                    similar_text($currentUser->getId(), $object->getId(), $percent);
                    if ($percent == 100) {
                        $data['listsCount'] = $this->objectManager
                            ->getRepository('Qolve\Entity\BookmarkList')
                            ->countTotalListsOfUser($object->getId());
                    } else {
                        $data['listsCount'] = $this->objectManager
                            ->getRepository('Qolve\Entity\BookmarkList')
                            ->countPublicListsOfUser($object->getId());
                    }
                } else {
                    $data['listsCount'] = $this->objectManager
                        ->getRepository('Qolve\Entity\BookmarkList')
                        ->countPublicListsOfUser($object->getId());
                }
            }

            if (array_search('followersList', $info) !== false) {
                $followers = $this->objectManager
                    ->getRepository('Qolve\Entity\UserFollow')
                    ->getFollowers($object->getId());

                $data['followersList'] = array();
                foreach ($followers as $follower) {
                    if ($follower instanceof User) {
                        $data['followersList'][] = $this->extract($follower);
                    }
                }
            }

            if (array_search('followingsList', $info) !== false) {
                $followings = $this->objectManager->getRepository('Qolve\Entity\UserFollow')
                    ->getFollowings($object->getId());

                $data['followingsList'] = array();
                foreach ($followings as $following) {
                    if ($following instanceof User) {
                        $data['followingsList'][] = $this->extract($following);
                    }
                }
            }

            if ($currentUser instanceof \Application\Entity\User) {
                $data['isFollowed'] = $this->objectManager
                        ->getRepository('Qolve\Entity\UserFollow')
                        ->isFollowed($object->getId(), $currentUser->getId());
            } else {
                $data['isFollowed'] = 0;
            }

            if ($currentUser instanceof \Application\Entity\User) {
                similar_text($currentUser->getId(), $object->getId(), $percent);
                if ($percent == 100) {
                    $data['providers'] = $this->objectManager
                        ->getRepository('Application\Entity\UserProvider')
                        ->getUserProviders($object->getId());
                }
            }

            $prefs = $this->objectManager
                ->getRepository('Application\Entity\UserPref')
                ->getUserPref($object->getId());

            if (isset($prefs['credit'])) {

                $percent = 0;
                if ($currentUser instanceof User) {
                    similar_text($currentUser->getId(), $object->getId(), $percent);
                }

                if ($percent < 100) {
                    unset($prefs['credit']);
                }
            }

            $data = array_merge($data, $prefs);

            $profile = $this->objectManager
                ->getRepository('Application\Entity\UserProfile')
                ->getUserProfile($object->getId());

            if ($profile instanceof \Application\Entity\UserProfile) {
                $_profile = $this->extract($profile, array(), $currentUser);
                $data     = array_merge($data, $_profile);
            }
        }

        if ($object instanceof \Qolve\Entity\Question) {
//            $escaper = new \Zend\Escaper\Escaper('utf-8');
//            $data['description'] = $escaper->escapeJs($data['description']);
//            $data['description'] = $escaper->escapeJs($data['description']);
            $amount = $object->getCredits();
            $data['charge'] = 0;

            if ($amount > 0 ) {
                $added = 0;
                if (($amount % 500) == 0) {
                    $added = ((($amount / 500) + 1) * 50) + 10;
                }else {
                    $added = (floor($amount / 500) * 50) + 60;
                }

                $data['charge'] = $amount + $added;

                if (!empty($info['charge'])) {
                    $amount = $info['charge'];
                    if (($amount % 500) == 0) {
                        $added = ((($amount / 500) + 1) * 50) + 10;
                    }else {
                        $added = (floor($amount / 500) * 50) + 60;
                    }

                    $data['charge'] = $amount + $added;
                }
            }

            $asker = $this->objectManager->getRepository('Qolve\Entity\Question')
                ->getAsker($object->getId());

            if ($asker instanceof User) {
                $ask   = $this->objectManager->getRepository('Qolve\Entity\Asker')
                    ->find(array(
                                'userId'     => $asker->getId(),
                                'questionId' => $object->getId()
                                ));

                if (array_search('asker', $info) !== false) {
                    $_asker = $this->extract($asker);

                    $percent = 0;
                    if ($currentUser instanceof User) {
                        similar_text($currentUser->getId(), $asker->getId(), $percent);
                    }

                    if (   $ask->getVisibility() == Asker::VISIBILITY_ANONYMOUS
                            && $percent < 100
                       ) {
                        $_asker = array_map(function () {return null;}, $_asker);
                        $_asker['name'] = Asker::ANONYMOUS_NAME;

                        $_asker['invisible'] = 1;
                    } else {
                        $_asker['invisible'] = 0;
                    }

                    $data['asker'] = $_asker;
                }
            }

            if (array_search('keywords', $info) !== false) {
                $keywords = $this->objectManager
                    ->getRepository('Qolve\Entity\QuestionKeyword')
                    ->getKeywordsOfQuestion($object->getId());

                if (!empty($keywords)) {
                    foreach ($keywords as $keyword) {
                        $key = array(
                                'name' => $keyword['name']
                                );

                        if ($currentUser instanceof User) {
                            $key['isFollowed'] = $this->objectManager
                                ->getRepository('Qolve\Entity\KeywordFollow')
                                ->isFollowed($keyword['id'], $currentUser->getId());
                        } else {
                            $key['isFollowed'] = 0;
                        }
                        $data['keywords'][] = $key;
                    }
                }
            }

            if (array_search('answersList', $info) !== false) {
                $answers = $this->objectManager
                    ->getRepository('Qolve\Entity\Answer')
                    ->getPartialAnswers($object->getId());

                $data['answersList'] = array();
                foreach ($answers as $answer) {
                    $data['answersList'][] = $this->extract($answer, $info, $currentUser);
                }
            }

            if (array_search('commentsList', $info) !== false) {
                $comments = $this->objectManager
                    ->getRepository('Qolve\Entity\Comment')
                    ->getPartialComments($object->getId(), null);

                $data['commentsList'] = array();
                foreach ($comments as $comment) {
                    $data['commentsList'][] = $this->extract(
                        $comment, $info, $currentUser
                    );
                }
            }

            if (array_search('documentsList', $info) !== false) {
                $documents = $this->objectManager
                    ->getRepository('Qolve\Entity\Document')
                    ->getDocuments($object->getId(), null);

                $data['documentsList'] = array();
                foreach ($documents as $document) {
                    $data['documentsList'][] = $this->extract($document);
                }
            }

            if ($currentUser instanceof User) {
                $reask = $this->objectManager
                    ->getRepository('Qolve\Entity\Asker')
                    ->find(
                            array(
                                'userId'        => $currentUser->getId(),
                                'questionId'    => $object->getId()
                                )
                          );

                if ($reask instanceof Asker) {
                    $data['isAsked'] = $reask->getStatus() * 10
                        + $reask->getVisibility();
                    $data['userCredit']       = $reask->getCredit();
                    $data['acceptedAnswerId'] = $reask->getAnswerId();
                } else {
                    $data['isAsked']          = 0;
                    $data['userCredit']       = 0;
                    $data['acceptedAnswerId'] = null;
                }
            } else {
                $data['isAsked']          = 0;
                $data['userCredit']       = 0;
                $data['acceptedAnswerId'] = null;
            }

            if ($currentUser instanceof User) {
                $vote = $this->objectManager
                    ->getRepository('Qolve\Entity\Vote')
                    ->getUserVote(
                        $currentUser->getId(),
                        $object->getId()
                    );

                if ($vote instanceof \Qolve\Entity\Vote) {
                    $data['isVoted'] = (int) $vote->getScore();
                } else {
                    $data['isVoted'] = 0;
                }
            } else {
                $data['isVoted'] = 0;
            }

            if (array_search('userAnswerId', $info) !== false) {
                if ($currentUser instanceof User) {
                    $answer = $this->objectManager
                        ->getRepository('Qolve\Entity\Answer')
                        ->getUserAnswer($object->getId(), $currentUser->getId());

                    if ($answer instanceof Answer) {
                        $data['userAnswerId'] = $answer->getId();
                    } else {
                        $data['userAnswerId'] = null;
                    }
                } else {
                    $data['userAnswerId'] = null;
                }
            }

            // Add User's shares list
            $shareds = $this->objectManager
                ->getRepository('Qolve\Entity\Share')
                ->getQuestionShared($object->getId());

            $data['share'] = array();
            foreach ($shareds as $shared) {
                $data['share'][] = $this->extract(
                    $shared, array(), $currentUser
                );
            }

            if ($currentUser instanceof User) {

                similar_text(
                    $asker->getId(), $currentUser->getId(), $percent
                );

                if (   $percent == 100
                    && $object->getAnswers() == 0
                    && $object->getStatus() == Question::STATUS_OPEN
                ) {
                    $data['editable'] = 1;
                } else {
                    $data['editable'] = 0;
                }
            } else {
                $data['editable'] = 0;
            }

            if ($currentUser instanceof User) {
                $reask = $this->objectManager
                    ->getRepository('Qolve\Entity\Asker')
                    ->find(
                            array(
                                'userId'     => $currentUser->getId(),
                                'questionId' => $object->getId()
                                )
                          );

                $answer = $this->objectManager
                    ->getRepository('Qolve\Entity\Answer')
                    ->getUserAnswer($object->getId(), $currentUser->getId());

                if (!$reask instanceof Asker && !$answer instanceof Answer) {
                    $data['canWriteAnswer'] = 1;
                } else {
                    $data['canWriteAnswer'] = 0;
                }
            }

            if ($object->getPrivacy() == Question::PRIVACY_PUBLIC
                && $object->getCredits() == 0
            ) {
                $data['answerPrivacyIsSettable'] = 1;
            } else {
                $data['answerPrivacyIsSettable'] = 0;
            }

            $data['visibility'] = $ask->getVisibility();

            if (!empty($data['levels'])) {
                $data['levels'] = explode(',', $data['levels']);
            } else {
                $data['levels'] = array();
            }
        }

        if ($object instanceof Answer) {
            $invisible = 0;
            if ($object->getPrivacy() == Answer::PRIVACY_PRIVATE) {
                if (!$currentUser instanceof User) {
                    $invisible = 1;
                } else {
                    similar_text($object->getUserId(), $currentUser->getId(), $percent);
                    if ($percent < 100) {
                        $isAsked = $this->objectManager
                            ->getRepository('Qolve\Entity\Question')
                            ->isAsked(
                                $object->getQuestionId(), $currentUser->getId()
                            );

                        if (!$isAsked) {
                            $privilege = $this->objectManager
                                ->getRepository('Qolve\Entity\AnswerPrivilege')
                                ->find(
                                    array(
                                        'answerId' => $object->getId(),
                                        'userId'   => $currentUser->getId()
                                    )
                                );

                            if (!$privilege instanceof AnswerPrivilege) {
                                $invisible = 1;
                            }
                        }
                    }
                }
            }

            if (array_search('commentsList', $info) !== false) {
                $comments = $this->objectManager
                    ->getRepository('Qolve\Entity\Comment')
                    ->getPartialComments(
                        $object->getQuestionId(), $object->getId()
                    );

                $data['commentsList'] = array();
                foreach ($comments as $comment) {
                    $data['commentsList'][] = $this->extract(
                        $comment, $info, $currentUser
                    );
                }
            }

            if ($currentUser instanceof User) {
                $vote = $this->objectManager
                    ->getRepository('Qolve\Entity\Vote')
                    ->getUserVote(
                        $currentUser->getId(),
                        $object->getQuestionId(),
                        $object->getId()
                    );

                if ($vote instanceof Vote) {
                    $data['isVoted'] = (int) $vote->getScore();
                } else {
                    $data['isVoted'] = 0;
                }
            } else {
                $data['isVoted'] = 0;
            }

            if ($invisible) {
                unset($data['description']);
            } else {
                if (array_search('documentsList', $info) !== false) {
                    $documents = $this->objectManager
                        ->getRepository('Qolve\Entity\Document')
                        ->getDocuments($object->getQuestionId(), $object->getId());

                    $data['documentsList'] = array();
                    foreach ($documents as $document) {
                        $data['documentsList'][] = $this->extract($document);
                    }
                }
            }

            if (isset($data['user'])) {

                if ($currentUser) {
                    similar_text($currentUser->getId(), $data['user']['id'], $percent);
                }

                if (   $object->getVisibility() == Answer::VISIBILITY_ANONYMOUS
                    && (!$currentUser || $percent < 100)
                ) {

                    $data['user'] = array_map(function () {return null;}, $data['user']);
                    $data['user']['name'] = Answer::ANONYMOUS_NAME;
                }

                $data['user']['invisible'] = (int) ($object->getVisibility() == Answer::VISIBILITY_ANONYMOUS);
            }

            if ($currentUser instanceof User) {
                similar_text(
                    $object->getUserId(), $currentUser->getId(), $percent
                );

                if (   $percent == 100
                    && $object->getStatus() == Answer::STATUS_NORMAL
                ) {
                    $data['editable'] = 1;
                } else {
                    $data['editable'] = 0;
                }
            } else {
                $data['editable'] = 0;
            }

            $data['invisible'] = (int) $invisible;
        }

        if ($object instanceof Comment) {

            $data['editable'] = 0;
            if ($currentUser instanceof User) {
                similar_text(
                    $object->getUserId(), $currentUser->getId(), $percent
                );

                if ($percent == 100) {
                    $data['editable'] = 1;
                } else {
                    if (is_null($object->getAnswerId())) {
                        $asker = $this->objectManager
                            ->getRepository('Qolve\Entity\Question')
                            ->getAsker($object->getQuestionId());
                        if ($asker instanceof User) {
                            similar_text(
                                $asker->getId(),
                                $currentUser->getId(),
                                $percent
                            );

                            if ($percent == 100) {
                                $data['editable'] = 1;
                            }
                        }
                    } else {
                        $answer = $this->objectManager
                            ->getRepository('Qolve\Entity\Answer')
                            ->find($object->getAnswerId());
                        if ($answer instanceof Answer) {
                            similar_text(
                                $answer->getUserId(),
                                $currentUser->getId(),
                                $percent
                            );

                            if ($percent == 100) {
                                $data['editable'] = 1;
                            }
                        }
                    }
                }
            }
        }

        if ($object instanceof Keyword) {

            if (!empty($currentUser) && $currentUser->getId() != null) {
                $data['isFollowed'] = $this->objectManager
                    ->getRepository('Qolve\Entity\KeywordFollow')
                    ->isFollowed($object->getId(), $currentUser->getId());
            } else {
                $data['isFollowed'] = 0;
            }
        }

        if ($object instanceof BookmarkList) {
            $invisible = 0;
            if ($object->getPrivacy() == BookmarkList::PRIVACY_PRIVATE) {
                if (!$currentUser instanceof User) {
                    $invisible = 1;
                } else {
                    similar_text($object->getUserId(), $currentUser->getId(), $percent);
                    if ($percent < 100) {
                        $invisible = 1;
                    }
                }
            }

            if ($invisible) {
                $data = array_map(function () {return null;}, $data);
            } else {
                if (array_search('questionsList', $info) !== false) {
                    $questions = $this->objectManager
                        ->getRepository('Qolve\Entity\Bookmark')
                        ->getPartialQuestionsOfList($object->getId());

                    $data['questionsList'] = array();
                    foreach ($questions as $question) {
                        $data['questionsList'][] = $this->extract(
                            $question, $info, $currentUser
                        );
                    }
                }

                if (array_search('listKeywords', $info) !== false) {
                    $data['listKeywords'] = $this->objectManager
                        ->getRepository('Qolve\Entity\BookmarkList')
                        ->getListKeywords($object->getId());
                }
            }

            if ($currentUser instanceof User) {
                //set isFollowed
                $data['isFollowed'] = $this->objectManager
                    ->getRepository('Qolve\Entity\BookmarkListFollow')
                    ->isFollowed($object->getId(), $currentUser->getId());

                //set editable and deletable
                similar_text(
                    $currentUser->getId(),
                    $object->getUserId(),
                    $isOwner
                );

                similar_text(
                    $object->getName(),
                    BookmarkList::NAME_UNCATEGORIZED,
                    $isUncategorized
                );

                $pref = $this->objectManager
                    ->getRepository('Application\Entity\UserPref')
                    ->find(
                        array(
                            'userId' => $currentUser->getId(),
                            'key'    => 'defaultBookmarkListId'
                        )
                    );
                if (!$pref instanceof UserPref) {
                    $isDefault = 0;
                } else {
                    similar_text(
                        $pref->getValue(),
                        $object->getId(),
                        $isDefault
                    );
                }

                if ($isOwner == 100 && $isUncategorized < 100) {
                    $data['editable'] = 1;
                } else {
                    $data['editable'] = 0;
                }

                if ($isOwner == 100
                    && $isUncategorized < 100 && $isDefault < 100
                ) {
                    $data['deletable'] = 1;
                } else {
                    $data['deletable'] = 0;
                }

            } else {
                $data['isFollowed'] = 0;
                $data['editable'] = 0;
                $data['deletable'] = 0;
            }

            $data['invisible'] = $invisible;
        }

        if ($object instanceof Feed) {

            $fromAnonymous = false;

            switch ($object->getVerb()) {
            case 'CreateQuestion':
            case 'PickBestAnswer':
            case 'RaiseCredit':
            case 'ReaskQuestion':
            case 'UpdateQuestion':
                $ask = $this->objectManager
                    ->getRepository('Qolve\Entity\Asker')
                    ->find(
                        array(
                            'questionId' => $object->getQuestionId(),
                            'userId'     => $object->getActorId()
                        )
                    );

                if (   $ask instanceof \Qolve\Entity\Asker
                    && $ask->getVisibility() == Asker::VISIBILITY_ANONYMOUS
                ) {
                    $fromAnonymous = true;
                }

                break;

            case 'CreateAnswer':
            case 'UpdateAnswer':
                $answer = $this->objectManager
                    ->getRepository('Qolve\Entity\Answer')
                    ->find($object->getAnswerId());

                if (   $answer instanceof Answer
                    && $answer->getVisibility() == Answer::VISIBILITY_ANONYMOUS
                ) {
                    $fromAnonymous = true;
                }

                break;
            }

            if ($fromAnonymous) {
                $data['actor'] = array_map(function () {return null;}, $data['actor']);
                $data['actor']['name'] = 'ANONYMOUS';
            }

            $data['actor']['invisible'] = (int) ($fromAnonymous);


            switch ($object->getVerb()) {
            case 'FollowBookmarkList':
                $list = $this->objectManager
                    ->getRepository('Qolve\Entity\BookmarkList')
                    ->find($object->getContent());

                if ($list instanceof BookmarkList) {
                    $data['list'] = $this->extract(
                        $list, $info, $currentUser
                    );
                }
                break;

            case 'FollowUser':
                $folloyee = $this->objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($object->getContent());

                if ($folloyee instanceof User) {
                    $data['folloyee'] = $this->extract(
                        $folloyee, $info, $currentUser
                    );
                }
                break;

            case 'PickBestAnswer':
                $bestAnswer = $this->objectManager
                    ->getRepository('Qolve\Entity\Answer')
                    ->find($object->getContent());

                if ($bestAnswer instanceof Answer) {
                    $data['bestAnswer'] = $this->extract(
                        $bestAnswer, $info, $currentUser
                    );
                }
                break;

            case 'WriteComment':
                $comment = $this->objectManager
                    ->getRepository('Qolve\Entity\Comment')
                    ->find($object->getContent());

                if ($comment instanceof Comment) {
                    $data['comment'] = $this->extract(
                        $comment, $info, $currentUser
                    );
                }
                break;
            }
        }

        if ($object instanceof Notification) {

            $fromAnonymous = false;

            switch ($object->getVerb()) {
            case 'CreateQuestion':
            case 'PickBestAnswer':
            case 'RaiseCredit':
            case 'ReaskQuestion':
            case 'UpdateQuestion':
                $ask = $this->objectManager
                    ->getRepository('Qolve\Entity\Asker')
                    ->find(
                        array(
                            'questionId' => $object->getQuestionId(),
                            'userId'     => $object->getFromId()
                        )
                    );

                if (   $ask instanceof \Qolve\Entity\Asker
                    && $ask->getVisibility() == Asker::VISIBILITY_ANONYMOUS
                ) {
                    $fromAnonymous = true;
                }

                break;

            case 'CreateAnswer':
            case 'UpdateAnswer':
                $answer = $this->objectManager
                    ->getRepository('Qolve\Entity\Answer')
                    ->find($object->getAnswerId());

                if (   $answer instanceof Answer
                    && $answer->getVisibility() == Answer::VISIBILITY_ANONYMOUS
                ) {
                    $fromAnonymous = true;
                }

                break;
            }

            if ($fromAnonymous) {
                $data['from'] = array_map(function () {return null;}, $data['from']);
                $data['from']['name'] = 'ANONYMOUS';
            }

            $data['from']['invisible'] = (int) ($fromAnonymous);


            switch ($object->getVerb()) {
            case 'FollowBookmarkList':
                $list = $this->objectManager
                    ->getRepository('Qolve\Entity\BookmarkList')
                    ->find($object->getContent());

                if ($list instanceof BookmarkList) {
                    $data['list'] = $this->extract(
                        $list, $info, $currentUser
                    );
                }
                break;
            }
        }

        return $data;
    }
}
