<?php
namespace Qolve\Job;

use SlmQueue\Job\AbstractJob;

class VotingStats extends AbstractJob
{
    protected $serviceLocator;

    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function execute()
    {
        $objectManager = $this->serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        
        $elastica = $this->serviceLocator
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];


        $elasticaQuery     = new \Elastica\Query();
        $search            = new \Elastica\Search($elasticaClient);
        $boolMust          = new \Elastica\Query\Bool();
        $elasticaIndex     = $elasticaClient->getIndex($index);
        $elasticaType      = $elasticaIndex->getType('question');

        $now      = new \DateTime('now', new \DateTimeZone('UTC'));
        $range    = new \Elastica\Query\Range();

        $fromDate = date('Y-m-d H:i:s', strtotime('-7 days', strtotime('now')));

        $range->addField('last_update', array('from' => $fromDate));
        $boolMust->addMust($range);

        $elasticaQuery = new \Elastica\Query($boolMust);

        $resultSet = $search->addIndex($index)
            ->addType('question')
            ->search($elasticaQuery);

        $returns = array();
        foreach ($resultSet as $result) {
            $returns[] = $result->getData();
        }

        foreach ($returns as $return) {

            $questionId = $return['question_id'];
            $question  = $objectManager->getRepository('Qolve\Entity\Question')
                ->find($questionId);

            if ($question instanceof \Qolve\Entity\Question) {

                $voteCount = $objectManager->getRepository('Qolve\Entity\Vote')
                    ->getVotersCount($return['question_id']);

                $param     = array('recent_votes' => $voteCount);

                $elasticaIndex  = $elasticaClient->getIndex($index);
                $elasticaType   = $elasticaIndex->getType('question');
                $document       = $elasticaType->getDocument($questionId);

                $document->setData($param);
                $elasticaType->updateDocument($document);
                $elasticaType->getIndex()->refresh();

            }
        }
    }
}
