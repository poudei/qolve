<?php
namespace Qolve\Job;

use SlmQueue\Job\AbstractJob,
    Qolve\Entity\Question,
    Qolve\Entity\Answer,
    Qolve\Entity\Transaction;

class Payment extends AbstractJob
{
    protected $serviceLocator;

    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function execute()
    {
        $data = $this->getContent();
        if (isset($data['questionId'])) {
            return $this->_payForQuestion($data['questionId']);
        }

        $offset = 0;
        $limit = 100;

        while (true) {
            $objectManager = $this
                ->serviceLocator
                ->get('Doctrine\ORM\EntityManager');
            $questions = $objectManager
                ->getRepository('\Qolve\Entity\Question')
                ->findBy(array(
                    'status' => Question::STATUS_CLOSED,
                    'paid'   => Question::PAID_INPROGRESS
                    ), array('modifiedOn'=> 'ASC'), $limit, $offset
            );

            foreach ($questions as $question) {
                $this->_payForQuestion($question);
            }

            if (count($questions) < $limit) {
                break;
            }

            $offset += $limit;
        }
    }

    private function _payForQuestion($question)
    {
        $objectManager = $this->serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        if (!$question instanceof Question) {
            $question = $objectManager
                ->getRepository('\Qolve\Entity\Question')
                ->find($question);
            if (!$question instanceof Question) {
                return false;
            }
        }

        if ($question->getStatus() != Question::STATUS_CLOSED) {
            return false;
        }

        if ($question->getPaid() != Question::PAID_INPROGRESS) {
            return false;
        }

        $bestAnswer = $objectManager
            ->getRepository('Qolve\Entity\Answer')
            ->findOneBy(array(
                'questionId' => $question->getId(),
                'status'     => Answer::STATUS_BEST
            )
        );
        if (!$bestAnswer instanceof Answer) {
            return false;
        }
           
        if ($question->getCredits() >= 0) {

            $userShare = $question->getCredits();
            
//            $payment  = $this->serviceLocator
//                ->get('payment');
//            $config = $this->serviceLocator->get('config');
            //charge amount with stripe
//            $charge = $payment->charge(
//                $config['payment']['customerId'],
//                $config['payment']['cardId'],
//                $userShare
//            );

//            if(is_null($charge)) {
//
//                $this->getResponse()->setStatusCode(400);
//                return new JsonModel(
//                    [
//                        'error' => 'Token Id and Card Id not send'
//                    ]
//                );
//            }
//            $userShare = $charge;

            $now   = new \DateTime('now', new \DateTimeZone('UTC'));
            $notes = json_encode(array(
                'questionId' => $question->getId(),
                'answerId'   => $bestAnswer->getId()
            ));

            $transaction = new Transaction();
            $transaction->setUserId($bestAnswer->getUserId());
            $transaction->setCredit($userShare);
            $transaction->setStatus(Transaction::STATUS_DONE);
            $transaction->setNotes($notes);
            $transaction->setCreatedOn($now);
            $objectManager->persist($transaction);

            $preTransactions = $objectManager
                ->getRepository('\Qolve\Entity\Transaction')
                ->findByQuestionId($question->getId());

            foreach ($preTransactions as $_transaction) {
                $_transaction
                    ->setStatus(Transaction::STATUS_DONE);
                $objectManager->persist($_transaction);
            }

//            $userCredit = $objectManager
//                ->getRepository('Qolve\Entity\Transaction')
//                ->calculateUserCredit($bestAnswer->getUserId());
            $userPref = $objectManager
                ->getRepository('Application\Entity\UserPref')
                ->getUserPref($bestAnswer->getUserId(), 'credit');

//            $userPref->setValue($userCredit + $userShare);
            $userPref->setValue((int)$userPref->getValue() + $userShare);
            $objectManager->persist($userPref);

            $question->setPaid(Question::PAID_DONE);

            $objectManager->flush();

            // *** Insert into Elastica *** //

            $hydrator   = $this->serviceLocator->get('Hydrator');
            $keyNames   = array();

            $elastica = $this->serviceLocator
                ->get('Elastica\Client');
            $elasticaClient = $elastica['client'];
            $index          = $elastica['index'];

            $questionKeywords = $objectManager
                ->getRepository('Qolve\Entity\QuestionKeyword')
                ->findByQuestionId($question->getId());

            foreach ($questionKeywords as $questionKeyword) {
                $questionKeyword = $hydrator->extract($questionKeyword);
                $keywordId       = $questionKeyword['keywordId'];
                $keyword         = $objectManager
                    ->getRepository('Qolve\Entity\Keyword')
                    ->find($keywordId);

                if (!$keyword instanceof Keyword) {
                    return false;
                }

                $keyword = $hydrator->extract($keyword);
                array_push($keyNames, $keyword['name']);
            }

            $item = array (
                'question_id' => $question->getId(),
                'votes'       => $question->getVotes(),
                'levels'      => $question->getLevels(),
                'credits'     => $question->getCredits(),
                'keyword'     => $keyNames,
                'status'      => $question->getStatus(),
                'paid'        => $question->getPaid(),
                'created_on'  => $question->getCreatedOn(),
            );

            $elasticaIndex = $elasticaClient->getIndex($index);
            $elasticaType  = $elasticaIndex->getType('question');
            $content       = new \Elastica\Document($question->getId(), $item);

            $elasticaType->addDocument($content);
            $elasticaType->getIndex()->refresh();
        }
    }
}
