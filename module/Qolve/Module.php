<?php
namespace Qolve;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),

            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Application' => __DIR__ . '/../Application/src/Application',
                ),
            ),
        );
    }

    public function getConsoleUsage(\Zend\Console\Adapter\AdapterInterface $console)
    {
        return array(
            'question bestanswer questionId' => 'Find Best Answer for a Question',
            array(
                'questionId' => 'Question\'s Id'
            )
        );
    }

    public function onBootstrap(\Zend\Mvc\MvcEvent $e)
    {
        $hydrator = $e->getApplication()->getServiceManager()->get('Hydrator');
        $service  = $e->getApplication()->getServiceManager();
        $identity = $e->getApplication()->getServiceManager()->get('ZfcRbac\Service\Rbac');
        $user     = $identity->getIdentity();
        $hydrator->addStrategy('createdOn', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy('r'));
        $hydrator->addStrategy('modifiedOn', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy('r'));
        $hydrator->addStrategy('askedOn', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy('r'));
        $hydrator->addStrategy('writtenOn', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy('r'));
        $hydrator->addStrategy('votedOn', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy('r'));
        $hydrator->addStrategy('birthday', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy('r'));
        $hydrator->addStrategy('requestTime', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy('r'));
        $hydrator->addStrategy('deadline', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy('r', $user, $service));
    }
}
