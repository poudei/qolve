<?php
return array(

    'router' => array(
        'routes' => array(
            'api' => array(
                'child_routes' => array(
                    'questions' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '[/][users][/][:user_id]/questions[/][:id][/][:by]',
                            'constraints' => array(
                                'id'     => '[0-9]+',
                                'by'  => '.*',
                                'user_id' => '[0-9]+'
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\QuestionRest',
                            ),
                        ),
                    ),

                    'answers' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/answers[/][:id]',
                            'constraints' => array(
                                'question_id' => '[0-9]+',
                                'id'          => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\AnswerRest',
                            ),
                        ),
                    ),

                    'question_comments' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/comments[/][:id]',
                            'constraints' => array(
                                'id'        => '[0-9]+',
                                'question_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\CommentRest'
                            ),
                        ),
                    ),
                    'answer_comments' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/answers/[:answer_id]/comments[/][:id]',
                            'constraints' => array(
                                'id'          => '[0-9]+',
                                'question_id' => '[0-9]+',
                                'answer_id'   => '[0-9]+',

                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\CommentRest'
                            ),
                        ),
                    ),

                    'askers' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/askers[/][:id]',
                            'constraints' => array(
                                'question_id' => '[0-9]+',
                                'id'          => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\AskerRest',
                            ),
                        ),
                    ),

                    'question_votes' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/votes[/][:id]',
                            'constraints' => array(
                                'question_id'   => '[0-9]+',
                                'id'            => '.*'
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\VoteRest'
                            ),
                        ),
                    ),
                    'answer_votes' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/answers/[:answer_id]/votes[/][:id]',
                            'constraints' => array(
                                'question_id'   => '[0-9]+',
                                'answer_id'     => '[0-9]+',
                                'id'            => '.*'
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\VoteRest'
                            ),
                        ),
                    ),

                    'user_question_votes' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/user/[:user_id]/questions/[:question_id]/votes',
                            'constraints' => array(
                                'user_id'       => '[0-9]+',
                                'question_id'   => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\VoteRest'
                            ),
                        ),
                    ),
                    'user_answer_votes' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/user/[:user_id]/questions/[:question_id]/answers/[:answer_id]/votes',
                            'constraints' => array(
                                'user_id'       => '[0-9]+',
                                'question_id'   => '[0-9]+',
                                'answer_id'     => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\VoteRest'
                            ),
                        ),
                    ),

                    'keywords' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/keywords[/][:id]',
                            'constraints' => array(
                                'id'     => '.*',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\KeywordRest',
                            ),
                        ),
                    ),

                    'question_keywords' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/keywords[/][:id]',
                            'constraints' => array(
                                'id'     => '.*',
                                'question_id' => '[0-9]+'
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\QuestionKeywordRest',
                            ),
                        ),
                    ),

                    'shares' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/shares[/][:id]',
                            'constraints' => array(
                                'id'     => '[0-9]+',
                                'question_id' => '[0-9]+'
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\ShareRest',
                            ),
                        ),
                    ),


                    'question_documents' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/documents[/][:id]',
                            'constraints' => array(
                                'id'          => '[0-9]+',
                                'question_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\DocumentRest'
                            ),
                        ),
                    ),
                    'answer_documents' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/answers/[:answer_id]/documents[/][:id]',
                            'constraints' => array(
                                'id'          => '[0-9]+',
                                'question_id' => '[0-9]+',
                                'answer_id'   => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\DocumentRest'
                            ),
                        ),
                    ),

                    'user_followers' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/[users/[:user_id]/]followers',
                            'constraints' => array(
                                'user_id'    => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\UserFollowersRest'
                            ),
                        ),
                    ),

                    'user_followings' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '[/users/[:user_id]]/followings[/][:id]',
                            'constraints' => array(
                                'id'         => '[0-9]+',
                                'user_id'    => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\UserFollowingRest'
                            ),
                        ),
                    ),

                    'user_follows' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/[users/[:user_id]/]follows',
                            'constraints' => array(
                                'user_id'    => '([0-9]+|me)',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\UserFollowRest'
                            ),
                        ),
                    ),

                    'question_follows' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/follows[/][:id]',
                            'constraints' => array(
                                'question_id' => '[0-9]+',
                                'id'          => '.*',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\QuestionFollowRest'
                            ),
                        ),
                    ),

                    'keyword_follows' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/keywords/follows[/][:id]',
                            'constraints' => array(
                                'id'          => '.*',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\KeywordFollowRest'
                            ),
                        ),
                    ),

                    'lists' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/lists[/][:id]',
                            'constraints' => array(
                                'id'     => '([0-9]+|Uncategorized|uncategorized)',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\BookmarkListRest'
                            ),
                        ),
                    ),

                    'listQuestions' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/lists/[:list_id]/questions[/][:id]',
                            'constraints' => array(
                                'list_id'     => '([0-9]+|Uncategorized|uncategorized)',
                                'id'     => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\BookmarkListQuestionsRest'
                            ),
                        ),
                    ),

                    'bookmarklist_follows' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/lists/[:list_id]/follows[/][:id]',
                            'constraints' => array(
                                'list_id'     => '[0-9]+',
                                'id'          => '.*',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\BookmarkListFollowRest'
                            ),
                        ),
                    ),

                    'explore' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/explores[/][:id]',
                            'constraints' => array(
                                'id'          => '[a-zA-Z0-9]*',

                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\Explore',
                                'action'     => 'search',
                            ),
                        ),
                    ),

//                    'credits' => array(
//                        'type' => 'segment',
//                        'options' => array(
//                            'route'    => '/credits',
//                            'constraints' => array(
//                            ),
//                            'defaults' => array(
//                                'controller' => 'Qolve\Controller\CreditRest'
//                            ),
//                        ),
//                    ),

                    'elastic-mapping' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/elastics[/][:action][/:index]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'index'  => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\Mapping',
                                'action'     => 'index',
                            ),
                        ),
                    ),

                    'random-insert' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/randoms[/][:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                             ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\Random'
                            ),
                        ),
                    ),

                    'upload-file' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/uploads/file',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\UploadFile',
                                'action'     => 'upload',

                            ),
                        ),
                    ),

                    'report' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id][/][answers][/][:answer_id]/reports[/][:id]',
                            'constraints' => array(
                                'question_id' => '[0-9]+',
                                'answer_id'   => '[0-9]+',
                                'id'          => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\ReportRest',
                            ),
                        ),
                    ),

                    'answer_buy' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/answers/[:answer_id]/buys',
                            'constraints' => array(
                                'question_id' => '[0-9]+',
                                'answer_id'   => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\BuyAnswerRest'
                            ),
                        ),
                    ),

                    'answer_claim' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/questions/[:question_id]/answers/[:answer_id]/claims',
                            'constraints' => array(
                                'question_id' => '[0-9]+',
                                'answer_id'   => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\ClaimRest'
                            ),
                        ),
                    ),

                    'transactions' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/transactions',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\TransactionRest',
                            ),
                        ),
                    ),
                    'register' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/registers[/][:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\RegisterRequest',
                                'action'     => 'demand'
                            ),
                        ),
                    ),
                    'payment' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/payments[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\PaymentRest',
                            ),
                        ),
                    ),

                    'checkout' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'       => '/checkouts[/][:action]',
                            'constraints' => array(
                                'action' => '[a-z][a-zA-Z]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\Checkout',
                                'action'     => 'checkout'
                            ),
                        ),
                    ),
                )
            ),

            'admin' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/admin',
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'feedback' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/feedbacks[/][:id]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\FeedbackAdminRest',
                            ),
                        ),
                    ),
                    'question' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/questions',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\QuestionAdminRest',
                            ),
                        ),
                    ),
                    'user' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/users',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\UserAdminRest',
                            ),
                        ),
                    ),
                    'request' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/requests',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\RequestAdminRest',
                            ),
                        ),
                    ),

                    'audit' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/audits/[:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Qolve\Controller\AuditAdmin',
                            ),
                        ),
                    ),

                )
            ),
        )
    ),

    'console'   => array(
        'router'    => array(
            'routes'    => array(
                'gearman-worker' => array(
                    'options'   => array(
                        'route'     => 'gworker',
                        'defaults'  => array(
                            'controller'    => 'Qolve\Controller\GearmanWorker',
                            'action'        => 'worker'
                        )
                    )
                )
            )
        )
    ),

    'doctrine' => array(
        'driver' => array(
            'qolve_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Qolve/Entity')
            ),

            'orm_default' => array(
                'drivers' => array(
                    'Qolve\Entity' => 'qolve_entities'
                )
            )
        )
    ),


    'controllers' => array(
        'invokables' => array(
            'Qolve\Controller\QuestionRest'             => 'Qolve\Controller\QuestionRestController',
            'Qolve\Controller\AnswerRest'               => 'Qolve\Controller\AnswerRestController',
            'Qolve\Controller\CommentRest'              => 'Qolve\Controller\CommentRestController',
            'Qolve\Controller\AskerRest'                => 'Qolve\Controller\AskerRestController',
            'Qolve\Controller\VoteRest'                 => 'Qolve\Controller\VoteRestController',
            'Qolve\Controller\KeywordRest'              => 'Qolve\Controller\KeywordRestController',
            'Qolve\Controller\ShareRest'                => 'Qolve\Controller\ShareRestController',
            'Qolve\Controller\DocumentRest'             => 'Qolve\Controller\DocumentRestController',
            'Qolve\Controller\QuestionkeywordRest'      => 'Qolve\Controller\QuestionkeywordRestController',
            'Qolve\Controller\KeywordRest'              => 'Qolve\Controller\KeywordRestController',
            'Qolve\Controller\UserFollowersRest'        => 'Qolve\Controller\UserFollowersRestController',
            'Qolve\Controller\UserFollowingRest'        => 'Qolve\Controller\UserFollowingRestController',
            'Qolve\Controller\UserFollowRest'           => 'Qolve\Controller\UserFollowRestController',
            'Qolve\Controller\QuestionFollowRest'       => 'Qolve\Controller\QuestionFollowRestController',
            'Qolve\Controller\KeywordFollowRest'        => 'Qolve\Controller\KeywordFollowRestController',
            'Qolve\Controller\GearmanWorker'            => 'Qolve\Controller\GearmanWorkerController',
            'Qolve\Controller\Explore'                  => 'Qolve\Controller\ExploreController',
            'Qolve\Controller\BookmarkListRest'         => 'Qolve\Controller\BookmarkListRestController',
            'Qolve\Controller\BookmarkListQuestionsRest'=> 'Qolve\Controller\BookmarkListQuestionsRestController',
            'Qolve\Controller\BookmarkListFollowRest'   => 'Qolve\Controller\BookmarkListFollowRestController',
            'Qolve\Controller\CreditRest'               => 'Qolve\Controller\CreditRestController',
            'Qolve\Controller\CurrentUserRest'          => 'Qolve\Controller\CurrentUserRestController',
            'Qolve\Controller\Mapping'                  => 'Qolve\Controller\MappingController',
            'Qolve\Controller\Random'                   => 'Qolve\Controller\RandomController',
            'Qolve\Controller\BuyAnswerRest'            => 'Qolve\Controller\BuyAnswerRestController',
            'Qolve\Controller\ClaimRest'                => 'Qolve\Controller\ClaimRestController',
            'Qolve\Controller\TransactionRest'          => 'Qolve\Controller\TransactionRestController',
            'Qolve\Controller\UploadFile'               => 'Qolve\Controller\UploadFileController',
            'Qolve\Controller\ReportRest'               => 'Qolve\Controller\ReportRestController',
            'Qolve\Controller\RegisterRequest'          => 'Qolve\Controller\RegisterRequestController',
            'Qolve\Controller\FeedbackAdminRest'        => 'Qolve\Controller\FeedbackAdminRestController',
            'Qolve\Controller\QuestionAdminRest'        => 'Qolve\Controller\QuestionAdminRestController',
            'Qolve\Controller\UserAdminRest'            => 'Qolve\Controller\UserAdminRestController',
            'Qolve\Controller\RequestAdminRest'         => 'Qolve\Controller\RequestAdminRestController',
            'Qolve\Controller\AuditAdmin'               => 'Qolve\Controller\AuditAdminController',
            'Qolve\Controller\PaymentRest'              => 'Qolve\Controller\PaymentRestController',
            'Qolve\Controller\Checkout'                 => 'Qolve\Controller\CheckoutController'
        ),
    ),

    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),

        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    'slm_queue' => array(
        'job_manager' => array(
            'factories' => array(
                'Qolve\Job\Payment' => function ($locator) {
                    return new Qolve\Job\Payment($locator->getServiceLocator());
                },
            )
        ),
        'queue_manager' => array(
            'factories' => array(
                'payment' => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
            )
        )
    ),

);
