<?php

namespace QolveTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class AbstractControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        $appDir = realpath(__DIR__ . '/../../../../../');
        $config = include $appDir . '/config/application.config.php';
        $config['module_listener_options'] = array(
            'module_paths' => array(
                $appDir . '/module',
                $appDir . '/vendor'
            ),
            'config_glob_paths' => array($appDir . '/config/autoload/{,*.}{global,local}.php')
        );

        $this->setApplicationConfig(
            $config
        );
        parent::setUp();

        $ZfcUserMock = $this->getMock('ZfcRbac\Identity\IdentityInterface');
        $ZfcUserMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(array('user_id' => 3648288291867853834)));
        $ZfcUserMock->expects($this->any())
            ->method('getRoles')
            ->will($this->returnValue(array('admin')));

        $authMock = $this->getMock('Zend\Authentication\AuthenticationService');
        $authMock->expects($this->any())
            ->method('hasIdentity')
            ->will($this->returnValue(true));
        $authMock->expects($this->any())
            ->method('getIdentity')
            ->will($this->returnValue($ZfcUserMock));

        $sm = $this->getApplicationServiceLocator();
        $sm->setAllowOverride(true);
        $sm->setService('Zend\Authentication\AuthenticationService', $authMock);

        if ($sm->has('ZfcRbac\Service\Rbac')) {
            $sm->get('ZfcRbac\Service\Rbac')->setIdentity($ZfcUserMock);
        }
    }
}
