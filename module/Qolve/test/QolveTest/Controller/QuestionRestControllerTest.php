<?php
namespace QolveTest\Controller;

use QolveTest\Bootstrap;
use Qolve\Controller\QuestionRestController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use PHPUnit_Framework_TestCase;
use Application\Controller\AbstractRestfulController;
use Zend\Mvc\Controller\Plugin\Identity;
use Zend\Authentication\AuthenticationService;

class QuestionControllerTest extends  PHPUnit_Framework_TestCase
{
    protected $traceError = true;
    
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;
 
    protected function setUp()
    {
        $serviceManager   = Bootstrap::getServiceManager();
        
        $this->controller = new QuestionRestController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->event      = new MvcEvent();
        $config           = $serviceManager->get('Config');
        $routerConfig     = isset($config['router']) ? $config['router'] : array();
        $router           = HttpRouter::factory($routerConfig);
        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
        $mockAuth = $this->getMock('ZfcUser\Entity\UserInterface');

        $ZfcUserMock = $this->getMock('ZfcUser\Entity\User');  
        $ZfcUserMock->expects($this->any())->method('getName')
            ->will($this->returnValue(array('name' => 'arbi@arb.com')));
        $ZfcUserMock->expects($this->any())->method('getPassword')
            ->will($this->returnValue(array('password' => 'yup')));
         // Getting mock of authentication object, which is used as a plugin.
        $authMock = $this->getMock('ZfcUser\Controller\Plugin\ZfcUserAuthentication');
        //print_r($authMock);exit;
        // Some expectations of the authentication service.
        $authMock->expects($this->any())
            -> method('hasIdentity')
            -> will($this->setValue(true));  
        $authMock   -> expects($this->any())
            -> method('getIdentity')
            -> will($this->returnValue($ZfcUserMock));
            print_r($authMock);exit;
        // At this point, PluginManager disallows mock being assigned as plugin because 
        // it will not implement plugin interface, as mentioned.
       $this -> controller->getPluginManager()
            ->setService('ZfcUser\Controller\Plugin\ZfcUserAuthentication', $authMock);
    }

    /*public function testGetListCanBeAccessed()
    {
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
 
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function testGetCanBeAccessed()
    {
        $this->routeMatch->setParam('id', '3648290990860534794');
 
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
 
        $this->assertEquals(200, $response->getStatusCode());
    }*/
 
    public function testCreateCanBeAccessed()
    {
        $this->request->setMethod('post');
      
        //$this->request->getPost()->set('email', 'arbi@arb.com');
        //$this->request->getPost()->set('password', 'yup');
        $this->request->getPost()->set('id', '3648290990860534793');
        $this->request->getPost()->set('credit', '12');
        $this->request->getPost()->set('privacy', '1');
        $this->request->getPost()->set('visibility', '1');
        $this->request->getPost()->set('language', 'eng');
        $this->request->getPost()->set('level', '1');
        $this->request->getPost()->set('course', '2');
        $this->request->getPost()->set('tags', array('php','java'));
        //print_r($this->controller);
        //var_dump($this->request);exit;
        $result   = $this->controller->dispatch($this->request);
        exit;
        
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }
 
    /*public function testUpdateCanBeAccessed()
    {
        $this->routeMatch->setParam('id', '1');
        $this->request->setMethod('put');
 
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
 
        $this->assertEquals(200, $response->getStatusCode());
    }
 
    public function testDeleteCanBeAccessed()
    {
        $this->routeMatch->setParam('id', '1');
        $this->request->setMethod('delete');
 
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
 
        $this->assertEquals(200, $response->getStatusCode());
    }*/
}
?>

