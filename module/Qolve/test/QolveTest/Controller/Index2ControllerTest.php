<?php

namespace QolveTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use QolveTest\Controller\AbstractControllerTest;

class Index2ControllerTest extends AbstractControllerTest
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testGetListCanBeAccessed()
    {
        $this->dispatch('/api/questions');
        $this->assertResponseStatusCode(200);
        $this->assertControllerClass('QuestionRestController');
        $this->assertModuleName('Qolve');
        $this->assertControllerName('Qolve\Controller\QuestionRest');
        $this->assertMatchedRouteName('api/questions');

        $content = $this->getResponse()->getContent();
        //$array   = json_decode($content);

        $this->assertCount(0, $array);
    }
}
