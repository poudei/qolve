<?php

return array(
    'module_listener_options' => array(
        'modules' => array(
            'DoctrineModule',
            'DoctrineORMModule',
            'Application',
            'Qolve'
        ),
    'config_glob_paths' => array('../../../config/application.config.php', 
                                 '../../../config/autoload/{,*.}{global,local}.php'
                                ),
    'module_paths' => array(
            'module',
            'vendor',
        ),
    )
);
