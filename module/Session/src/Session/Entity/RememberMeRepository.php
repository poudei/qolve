<?php
namespace Session\Entity;

use Session\Entity\Remember,
    Application\Entity\BaseRepository,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class RememberMeRepository extends BaseRepository
{
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $this->getEntityManager()
            ->getConnection()
            ->getShardManager()
            ->selectGlobal();

        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        $this->getEntityManager()
            ->getConnection()
            ->getShardManager()
            ->selectGlobal();

        return parent::findOneBy($criteria, $orderBy);
    }
}
