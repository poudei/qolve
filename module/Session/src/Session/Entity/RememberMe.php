<?php
namespace Session\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="user_remember_me")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Session\Entity\RememberMeRepository")
 */
class RememberMe
{
    /**
    * @ORM\Id
    * @ORM\Column(type="string")
    */
    protected $sid;

    /**
     * @ORM\Column(type="string")
     */
    protected $token;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint")
     */
    protected $user_id;

    public function getSid()
    {
        return $this->sid;
    }

    public function setSid($sid)
    {
        $this->sid = $sid;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function getUserId()
    {
        return $this->user_id;
    }
}
