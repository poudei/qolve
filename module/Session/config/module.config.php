<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'session_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/Session/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Session\Entity' => 'session_entities'
                )
            )
        )
    ),
);
