<?php

namespace Application\Listener;

use Zend\Mvc\MvcEvent;
use Zend\Http\Request as HttpRequest;

class OAuth
{
    /**
     * @param MvcEvent $e
     */
    public static function onRoute(MvcEvent $e)
    {
        if (!$e->getRequest() instanceof HttpRequest) {
            return;
        }

        $app       = $e->getTarget();
        $sm        = $app->getServiceManager();
        $token     = $e->getRequest()->getQuery('oauth_token', null);
        $hasHeader = $e->getRequest()->getHeaders()->has('Authorization');

        if (!$token && !$hasHeader) {
            return;
        }

        $sm->get('Zend\Authentication\AuthenticationService')
            ->setStorage(new \Zend\Authentication\Storage\NonPersistent());

        try {
            $provider = $sm->get('OAuthProvider');
            $provider->checkOAuthRequest();

            $user = $sm->get('zfcuser_user_mapper')->findById($provider->user_id);

            if ($user) {
                $sm->get('Zend\Authentication\AuthenticationService')->getStorage()->write($user);
                $sm->get('ZfcRbac\Service\Rbac')->setIdentity($user);

                return true;
            }

        } catch (\OAuthException $e) {
            return;
        }

        //TODO throw new exception
    }
}
