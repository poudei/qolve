<?php
namespace Application\Event;

use Doctrine\ORM\Events,
    Doctrine\Common\EventSubscriber,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Doctrine\Common\Annotations\SimpleAnnotationReader,
    Application\Service\UidGenerator,
    Zend\ServiceManager\ServiceManager;

class DoctrineEventSubscriber implements EventSubscriber
{
    protected $serviceManager;

    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity  = $args->getEntity();
        $em      = $args->getEntityManager();
        //$idField = $em->getClassMetadata(get_class($entity))->getSingleIdentifierFieldName();

        $reader      = new SimpleAnnotationReader();
        $reflClass   = $em->getClassMetadata(get_class($entity))->getReflectionClass();
        $annotations = $reader->getClassAnnotations($reflClass);
        $annotation  = null;

        foreach ($annotations as $annot) {
            if ($annot instanceof \Application\ORM\Annotations\ShardChooseColumn) {
                $annotation = $annot;
            }
        }

        if ($annotation == null) {
            return;
        }

        $shard = 0;

        if ($annotation->type == "independent") {
            $shard = 1; //Current Active Shard
        } else {
            $dependId = call_user_func(array($entity, 'get' . ucfirst($annotation->dependColumn)));
            $shard    = UidGenerator::fetchData($dependId, 'shard');
        }

        $dependColumn = null;

        if (!empty($annotation->column)) {
            $id = UidGenerator::generate($shard, 10);
            call_user_func(array($entity, 'set' . ucfirst($annotation->column)), $id);

            $dependColumn = $annotation->column;
        }

        if (!empty($annotation->dependColumn)) {
            $dependColumn = $annotation->dependColumn;
        }

        $distributionValue = call_user_func(array($entity, 'get' . ucfirst($dependColumn)));

        $args->getEntityManager()
            ->getConnection()
            ->getShardManager()
            ->selectShard($distributionValue);
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
