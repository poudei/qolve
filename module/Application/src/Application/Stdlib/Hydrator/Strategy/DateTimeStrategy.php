<?php
namespace Application\Stdlib\Hydrator\Strategy;

use DateTime,
    Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;

class DateTimeStrategy extends DefaultStrategy
{
    private $format;

    public function __construct($format = 'Y-m-d H:i:s', $user = null, $service = null)
    {
        $this->format  = $format;
        $this->user    = $user;
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     *
     * Convert a string value into a DateTime object
     */
    public function hydrate($value)
    {

        if (is_string($value) && "" === $value) {
            $value = null;
        } elseif (is_string($value)) {
            $value = new DateTime($value);
        }
        if ($value instanceof DateTime) {

            $date = $value->format('r');
            $date = gmdate('Y-m-d H:i:s', strtotime($date));
            date_default_timezone_set('UTC');
            $value = new \DateTime($date);

        }
        return $value;
    }

    public function extract($value)
    {

        if ($value instanceof DateTime) {

            $value = $value->format('Y-m-d H:i:s');
            date_default_timezone_set('UTC');
            $value = new DateTime($value);
            $value = $value->format('r');

        }
        return $value;
    }
}
