<?php
namespace Application\Job;

use Zend\Mail\Message,
    Zend\Mail\Transport\Smtp as SmtpTransport,
    Zend\Mime\Message as MimeMessage,
    Zend\Mime\Part as MimePart,
    Zend\Mail\Transport\SmtpOptions,

    SlmQueue\Job\AbstractJob;

class SendEmail extends AbstractJob
{
    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function execute()
    {
        $data = $this->getContent();
        if (!isset($data['to'])) {
            return false;
        }
        if (!isset($data['subject'])) {
            return false;
        }
        if (!isset($data['template'])) {
            return false;
        }

        $email   = $data['to'];
        $subject = $data['subject'];

        $params  = isset($data['params']) ?
            $data['params'] :
            array();

        $config    = $this->serviceLocator->get('config');

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'host'              => $config['email']['host'],
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => $config['email']['username'],
                'password' => $config['email']['password'],
                'ssl'      => $config['email']['ssl']
            ),
            'port' => $config['email']['port']
        ));


        //Create Body
        $view = new \Zend\View\Renderer\PhpRenderer();
        $resolver = new \Zend\View\Resolver\TemplateMapResolver();
        $resolver->setMap(array(
                'mailLayout' => __DIR__ . '/../../../view/layout/layout-mail.phtml',
                'mailTemplate' => __DIR__ . '/../../../view/email/'.$data['template'].'.phtml'
        ));
        $view->setResolver($resolver);

        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setTemplate('mailTemplate')
            ->setVariables($params);

        $content = $view->render($viewModel);

        $viewLayout = new \Zend\View\Model\ViewModel();
        $viewLayout->setTemplate('mailLayout')
            ->setVariables(array(
                'content' => $content
        ));

        !empty($data['from']) ? $from = $data['from'] : $from = 'Qolve';
        $body = $view->render($viewLayout);
        $htmlPart = new MimePart($body);
        $htmlPart->type = 'text/html';
        $body = new MimeMessage();
        $body->setParts(array($htmlPart));
        $message = new Message();
        $message->setEncoding('UTF-8')
            ->addFrom($config['email']['from'], $from)
            ->addTo($email)
            ->addBcc($email)
            ->setSubject($subject)
            ->setBody($body);
        $transport->setOptions($options);
        $transport->send($message);
    }
}
