<?php
namespace Application\Job;

use SlmQueue\Job\AbstractJob;

class PublishStatus extends AbstractJob
{
    protected $serviceLocator;

    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function execute()
    {
        $data = $this->getContent();

        if (!is_callable($data['verb'] . "::publishStatus")) {
            return false;
        }
        
        $data['verb']::publishStatus($data, $this->serviceLocator);
    }
}
