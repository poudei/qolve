<?php
namespace Application\Job;

use SlmQueue\Job\AbstractJob;

class PublishFeed extends AbstractJob
{
    protected $serviceLocator;

    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function execute()
    {
        $data = $this->getContent();

        if (!is_callable($data['verb'] . "::publishFeed")) {
            return false;
        }
        
        $data['verb']::publishFeed($data, $this->serviceLocator);
    }
}
