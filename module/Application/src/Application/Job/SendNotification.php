<?php
namespace Application\Job;

use SlmQueue\Job\AbstractJob;

class SendNotification extends AbstractJob
{
    protected $serviceLocator;

    public function __construct($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function execute()
    {
        $data = $this->getContent();
        
        if (!is_callable($data['verb'] . "::sendNotification")) {
            return false;
        }
        
        $data['verb']::sendNotification($data, $this->serviceLocator);
    }
}
