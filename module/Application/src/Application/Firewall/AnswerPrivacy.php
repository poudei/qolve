<?php
namespace Application\Firewall;

use ZfcRbac\Firewall\AbstractFirewall;

class AnswerPrivacy extends AbstractFirewall
{
    public function __construct()
    {
    }

    /**
     * Checks if access is granted.
     *
     * @param string $resource
     * @return bool
     */
    public function isGranted($e)
    {
        $app     = $e->getTarget();
        $route   = $e->getRouteMatch();
        
        $objectManager = $app->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $currentUser = $app->getServiceManager()->get('zfcuser_auth_service')->getIdentity();
        $params = $route->getParams();
        
        switch ($route->getMatchedRouteName()) {
        case 'api/answers':
            if (isset($params['id'])) {
                $answerId = $params['id'];
            }
            break;
            
        case 'api/answer_votes':
        case 'api/user_answer_votes':
        case 'api/answer_documents':
        case 'api/report':
            if (isset($params['answer_id'])) {
                $answerId = $params['answer_id'];
            }
            break;
        }
        
        if (isset($answerId)) {
            $answer = $objectManager->getRepository('Qolve\Entity\Answer')
                    ->find($answerId);
            
            if (!$answer instanceof \Qolve\Entity\Answer) {
                return false;
            }

            if ($answer->getPrivacy() == \Qolve\Entity\Answer::PRIVACY_PRIVATE) {
                if (!$currentUser instanceof \Application\Entity\User) {
                    return false;
                }
                
                similar_text($answer->getUserId(), $currentUser->getId(), $percent);
                if ($percent < 100) {
                    
                    $ask = $objectManager->getRepository('Qolve\Entity\Asker')
                        ->findOneBy(array(
                            'questionId' => $answer->getQuestionId(),
                            'userId'     => $currentUser->getId()
                        ));
                    
                    if (!$ask instanceof \Qolve\Entity\Asker) {
                        
                        $privilege = $objectManager->getRepository('Qolve\Entity\AnswerPrivilege')
                                ->findOneBy(array(
                                    'answerId'   => $answer->getId(),
                                    'userId'     => $currentUser->getId()
                                ));
                        
                        if (!$privilege instanceof \Qolve\Entity\AnswerPrivilege) {
                            return false;
                        }
                    }
                }
            }
        }
        
        return true;
    }

    /**
     * Get the firewall name.
     *
     * @return string
     */
    public function getName()
    {
        return 'answer-privacy';
    }
}
