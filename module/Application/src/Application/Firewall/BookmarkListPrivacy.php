<?php
namespace Application\Firewall;

use ZfcRbac\Firewall\AbstractFirewall;

class BookmarkListPrivacy extends AbstractFirewall
{
    public function __construct()
    {
    }

    /**
     * Checks if access is granted.
     *
     * @param string $resource
     * @return bool
     */
    public function isGranted($e)
    {
        $app     = $e->getTarget();
        $route   = $e->getRouteMatch();
        
        $objectManager = $app->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $currentUser = $app->getServiceManager()->get('zfcuser_auth_service')->getIdentity();
        $params = $route->getParams();
        
        switch ($route->getMatchedRouteName()) {
        case 'api/lists':
            if (isset($params['id'])) {
                $listId = $params['id'];
            }
            break;
            
        case 'api/listQuestions':
        case 'api/bookmarklist_follows':
            if (isset($params['list_id'])) {
                $listId = $params['list_id'];
            }
            break;
        }
        
        if (isset($listId)) {
            if (strtolower($listId) == strtolower(\Qolve\Entity\BookmarkList::NAME_UNCATEGORIZED)) {
                if (!$currentUser instanceof \Application\Entity\User) {
                    return false;
                }
                
                return true;
            }
            
            $list = $objectManager->getRepository('Qolve\Entity\BookmarkList')
                    ->find($listId);
            if (!$list instanceof \Qolve\Entity\BookmarkList) {
                return false;
            }

            if ($list->getPrivacy() == \Qolve\Entity\BookmarkList::PRIVACY_PRIVATE) {
                if (!$currentUser instanceof \Application\Entity\User) {
                    return false;
                }
                
                similar_text($currentUser->getId(), $list->getUserId(), $percent);
                if ($percent < 100) {
                    return false;
                }
            }
        }
        
        return true;
    }

    /**
     * Get the firewall name.
     *
     * @return string
     */
    public function getName()
    {
        return 'list-privacy';
    }
}
