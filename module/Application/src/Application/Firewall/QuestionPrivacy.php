<?php
namespace Application\Firewall;

use ZfcRbac\Firewall\AbstractFirewall;

class QuestionPrivacy extends AbstractFirewall
{
    public function __construct()
    {
    }

    /**
     * Checks if access is granted.
     *
     * @param string $resource
     * @return bool
     */
    public function isGranted($e)
    {
        $app     = $e->getTarget();
        $route   = $e->getRouteMatch();
        
        $objectManager = $app->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $currentUser = $app->getServiceManager()->get('zfcuser_auth_service')->getIdentity();
        $params = $route->getParams();
        
        switch ($route->getMatchedRouteName()) {
        case 'api/questions':
        case 'api/listQuestions':
            if (isset($params['id'])) {
                $questionId = $params['id'];
            }
            break;
            
        case 'api/answers':
        case 'api/question_comments':
        case 'api/answer_comments':
        case 'api/askers':
        case 'api/question_votes':
        case 'api/answer_votes':
        case 'api/user_question_votes':
        case 'api/user_answer_votes':
        case 'api/question_keywords':
        case 'api/shares':
        case 'api/question_documents':
        case 'api/answer_documents':
        case 'api/question_follows':
        case 'api/report':
            if (isset($params['question_id'])) {
                $questionId = $params['question_id'];
            }
            break;
        }
        
        if (isset($questionId)) {
            $question = $objectManager->getRepository('Qolve\Entity\Question')
                    ->find($questionId);
            
            if (!$question instanceof \Qolve\Entity\Question) {
                return false;
            }

            if ($question->getPrivacy() == \Qolve\Entity\Question::PRIVACY_SHARED) {
                if (!$currentUser instanceof \Application\Entity\User) {
                    return false;
                }

                $share = $objectManager->getRepository('Qolve\Entity\Share')
                        ->findOneBy(array(
                            'questionId' => $question->getId(),
                            'userId'     => $currentUser->getId()
                        ));
                if (!$share instanceof \Qolve\Entity\Share) {
                    $asker = $objectManager->getRepository('Qolve\Entity\Asker')
                            ->findOneBy(array(
                                'questionId' => $question->getId(),
                                'status'     => \Qolve\Entity\Asker::STATUS_ASKER
                            ));
                    if (!$asker instanceof \Qolve\Entity\Asker) {
                        return false;
                    }
                    
                    similar_text($asker->getUserId(), $currentUser->getId(), $percent);
                    if ($percent < 100) {
                        return false;
                    }
                }
            }
        }
        
        return true;
    }

    /**
     * Get the firewall name.
     *
     * @return string
     */
    public function getName()
    {
        return 'question-privacy';
    }
}
