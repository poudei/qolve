<?php
namespace Application\Service;

use Zend\ServiceManager\FactoryInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    Application\Annotation\AnnotationReader;


class AuthServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $sm)
    {
        return new \Application\Authentication\AuthenticationService(
            $sm->get('ZfcUser\Authentication\Storage\Db'),
            $sm->get('ZfcUser\Authentication\Adapter\AdapterChain')
        );
    }
}
