<?php
namespace Application\Service;

use Zend\ServiceManager\FactoryInterface,
    Zend\ServiceManager\ServiceLocatorInterface;


class ElasticFactory implements FactoryInterface
{
    private $serviceLocator = null;

    public function createService(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
        $config = $this->serviceLocator->get('config');
        if (isset($config['elastic']) && $config['elastic']['enable']) {
            if (isset($config['elastic']['servers']) && isset($config['elastic']['port'])) {
                $elasticaClient = new \Elastica\Client(
                        array(
                            'host' => $config['elastic']['servers'],
                            'port' => $config['elastic']['port']
                            )
                        );
            } else {
                $elasticaClient = new \Elastica\Client(
                        array(
                            'host' => '127.0.0.1',
                            'port' => '9200'
                            )
                        );
            }
        }
        $index = empty($config['elastic']['index']) ? 'qolve' : $config['elastic']['index'] ;
        return array(
            'client' => $elasticaClient, 
            'index'  => $index
        );

    }
}
