<?php
namespace Application\Service;

use Zend\ServiceManager\FactoryInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    Qolve\Entity\Transaction;
    require_once(VENDOR_PATH . '/stripe-php/lib/Stripe.php');


class PaymentFactory implements FactoryInterface
{
    private $serviceLocator = null;

    public function createService(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
        return $this;

    }

    /**
     * @param: $device [1 = IOS] 
     */
    public function charge($user, $cardToken, $cardId, $amount, $device = 0)
    {
        $objectManager = $this->serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $config = $this->serviceLocator
            ->get('config');
                   
//          $amount = round(($amount + 30) / 0.971, 0, PHP_ROUND_HALF_UP);
        $added = 0;
        if (($amount % 500) == 0) {
            $added = ((($amount / 500) + 1) * 50) + 10;
        }else {
            $added = (floor($amount / 500) * 50) + 60;
        }

        $amount += $added;
        
        $customerId = $user->getCustomerId();
        \Stripe::setApiKey($config['payment']['apiKey']);

        if (is_null($customerId)) {
            $customer = \Stripe_Customer::create(
                [
                    "description" => "Customer for" . $user->getEmail(),
                    "card"        => $cardToken
                ]
            );
            $customerId = $customer['id'];
            $user->setCustomerId($customerId);
            $objectManager->persist($user);
            $objectManager->flush($user);

            $charge = \Stripe_Charge::create(
                [
                    "amount"      => $amount,
                    "currency"    => "usd",
                    "customer"    => $customerId,
                    "description" => "Charge for test@example.com"
                ]
            );

            $now = new \DateTime('now', new \DateTimeZone('UTC'));

//            $transaction = new Transaction();
//            $transaction->setUserId($user->getId());
//            $transaction->setDebit($charge['amount']);
//            $transaction->setStatus(Transaction::STATUS_DONE);
//            $transaction->setCreatedOn($now);
//            $objectManager->persist($transaction);
//            $objectManager->flush($transaction);

            return $charge['amount'];

        } elseif (!is_null($customerId) && !empty($cardId)) {

            $charge = \Stripe_Charge::create(
                [
                    'amount'      => $amount,
                    'currency'    => 'usd',
                    'customer'    => $customerId,
                    'card'        => $cardId,
                    'description' => "Charge for test@example.com"
                ]
            );

            return $charge['amount'];

        } elseif(!is_null($customerId) && !empty($cardToken)) {
            $cardToken = $cardToken;
            $cu        = \Stripe_Customer::retrieve($user->getCustomerId());
            $card      = $cu->cards->create(array("card" => $cardToken));
            $cardId    = $card['id'];

            $charge = \Stripe_Charge::create(
                [
                    'amount'      => $amount,
                    'currency'    => 'usd',
                    'customer'    => $customerId,
                    'card'        => $cardId,
                    'description' => "Charge for test@example.com"
                ]
            );

            return $charge['amount'];
        } else {
            return false;
        }
    }

    public function token()
    {
        \Stripe::setApiKey("sk_test_JeXLtIB1ZGvZYHHZkVdcRx5P");
        $token = \Stripe_Token::create(
            [
                "card" =>
                    [
                        "number"    => "4242424242424242",
                        "exp_month" => 5,
                        "exp_year"  => 2015,
                        "cvc"       => "314"
                    ]
            ]
        );
        var_dump($token);exit;
    }
}
