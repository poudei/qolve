<?php

namespace Application\Service\Gearman;



class Client
{

    private $gClient = null;

    public function __construct($sm)
    {
        $config = $sm->get('Config');
        if ($config['gearman']['enable']) {
            $this->gClient = new \GearmanClient();
            if (isset($config['gearman']) && isset($config['gearman']['servers'])) {
                $this->gClient->addServers($config['gearman']['servers']);
            } else {
                $this->gClient->addServer();
            }


        }
    }


    public function __call($method, $arguments)
    {

        var_dump($arguments);exit;
        switch ($method) {
        case 'do':
        case 'doBackground':
        case 'doHigh' :
        case 'doHighBackground' :
        case 'doLow':
        case 'doLowBackground':
        case 'doNormal':
            $arguments[1] = serialize($arguments[1]);
            return call_user_func_array(array($this->gClient, $method), $arguments);
            break;

        default :
            throw new Exception('The method : ' . $method. ' is not exist');
            break;
        }
    }

    public function getGearmanClient()
    {
        return $this->gClient;
    }

}
