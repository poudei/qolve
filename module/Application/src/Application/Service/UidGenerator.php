<?php

namespace Application\Service;


class UidGenerator
{
    public static function generate($shard, $uid)
    {
        list($msec, $sec) = explode(' ', microtime());

        $msec = intval($msec * 1000);
        $uid  = intval($uid % 1024);

        $sec = time();
        $sec = $sec - strtotime('2000-01-01 00:00:00');

        $sid   = str_pad(base_convert($sec, 10, 2), 31, '0', STR_PAD_LEFT);
        $mid   = str_pad(base_convert($msec, 10, 2), 10, '0', STR_PAD_LEFT);
        $shard = str_pad(base_convert($shard, 10, 2), 13, '0', STR_PAD_LEFT);
        $uid   = str_pad(base_convert($uid, 10, 2), 10, '0', STR_PAD_LEFT);
        $id    = $sid . $mid . $shard .  $uid;

        if (extension_loaded('gmp')) {
            $a = gmp_init($id, 2);
            return gmp_strval($a, 10);
        }

        return base_convert($id, 2, 10);
    }

    public static function fetchData($id, $field = null)
    {
        if (extension_loaded('gmp')) {
            $id = gmp_strval(gmp_init($id, 10), 2);
        } else {
            $id = base_convert($id, 10, 2);
        }

        $id = str_pad($id, 64, '0', STR_PAD_LEFT);

        $time  = base_convert(substr($id, 0, 31), 2, 10);
        $msec  = base_convert(substr($id, 31, 10), 2, 10);
        $shard = base_convert(substr($id, 41, 13), 2, 10);
        $uid   = base_convert(substr($id, 54, 10), 2, 10);

        $res = array(
            'time'  => $time,
            'msec'  => $msec,
            'shard' => $shard,
            'uid'   => $uid
        );

        return isset($res[$field]) ? $res[$field] : $res;
    }
}
