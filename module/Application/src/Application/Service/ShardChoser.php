<?php

namespace Application\Service;

use Doctrine\Shards\DBAL\PoolingShardConnection,
    Application\Service\UidGenerator;


class ShardChoser implements \Doctrine\Shards\DBAL\ShardChoser\ShardChoser
{
    protected $serviceManager;

    public function pickShard($distributionValue, PoolingShardConnection $conn)
    {
        return UidGenerator::fetchData($distributionValue, 'shard');
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }
}
