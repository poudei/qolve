<?php
namespace Application\Service;

use Zend\ServiceManager\FactoryInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    OAuthProvider,
    OAuthException;


class OAuthProviderFactory implements FactoryInterface
{
    private $serviceLocator = null;

    public function createService(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;

        try {
            $provider = new OAuthProvider();
            $provider->consumerHandler(array($this,'lookupConsumer'));
            $provider->timestampNonceHandler(array($this,'timestampNonceChecker'));
            $provider->tokenHandler(array($this,'tokenHandler'));
            $provider->setRequestTokenPath('/api/oauth2/token');
        } catch (OAuthException $E) {
            OAuthProvider::reportProblem($E);
            return null;
        }

        return $provider;
    }

    public function lookupConsumer(OAuthProvider $provider)
    {
        $em = $this->serviceLocator->get('Doctrine\ORM\EntityManager');
        $client = $em->getRepository('Application\Entity\OauthClient')
            ->findOneBy(array('clientId' => $provider->consumer_key));

        if (!$client) {
            return OAUTH_CONSUMER_KEY_UNKNOWN;
        }

        $provider->consumer_secret = $client->getClientSecret();

        return OAUTH_OK;
    }

    public function timestampNonceChecker(OAuthProvider $provider)
    {
        return OAUTH_OK;
    }

    public function tokenHandler(OAuthProvider $provider)
    {
        $em    = $this->serviceLocator->get('Doctrine\ORM\EntityManager');
        $token = $em->getRepository('Application\Entity\OauthAccessToken')
            ->findOneBy(array('token' => $provider->token));

        if (!$token) {
            return OAUTH_VERIFIER_INVALID;
        }

        $provider->token_secret = $token->getTokenSecret();
        $provider->user_id      = $token->getUserId();
        return OAUTH_OK;
    }
}
