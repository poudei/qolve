<?php

namespace Application\ORM;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\Configuration;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Doctrine\Common\Annotations\SimpleAnnotationReader;

class EntityManager extends \Doctrine\ORM\EntityManager
{
    /**
     * Factory method to create EntityManager instances.
     *
     * @param mixed $conn An array with the connection parameters or an existing
     *      Connection instance.
     * @param Configuration $config The Configuration instance to use.
     * @param EventManager $eventManager The EventManager instance to use.
     * @return EntityManager The created EntityManager.
     */
    public static function create($conn, Configuration $config, EventManager $eventManager = null)
    {
        if ( ! $config->getMetadataDriverImpl()) {
            throw ORMException::missingMappingDriverImpl();
        }

        switch (true) {
            case (is_array($conn)):
                $conn = \Doctrine\DBAL\DriverManager::getConnection(
                    $conn, $config, ($eventManager ?: new EventManager())
                );
                break;

            case ($conn instanceof Connection):
                if ($eventManager !== null && $conn->getEventManager() !== $eventManager) {
                     throw ORMException::mismatchedEventManager();
                }
                break;

            default:
                throw new \InvalidArgumentException("Invalid argument: " . $conn);
        }

        return new EntityManager($conn, $config, $conn->getEventManager());
    }

    public function find($entityName, $id, $lockMode = LockMode::NONE, $lockVersion = null)
    {
        if (!is_array($id)) {
            $idCol = $this->getClassMetadata($entityName)->identifier[0];
            $id    = array($idCol => $id);
        }

        if ($this->isShardable($entityName, $shardColumn)) {
            $this->getConnection()
                ->getShardManager()
                ->selectShard($id[$shardColumn]);
        } else {
            $this->getConnection()
                ->getShardManager()
                ->selectGlobal();
        }

        // Retrieve an instance of the Entity Manager
        $qb  = $this->createQueryBuilder();
        $and = $qb->expr()->andX();
        foreach ($id as $col => $val) {
            $val = $qb->expr()->literal($val);
            $col = 'o.' . $col;
            $and->add($qb->expr()->eq($col, $val));
        }

        $qb->select('o')
            ->from($entityName, 'o')
            ->where($and);

        $query = $qb->getQuery();

        if ($cache = $this->getConfiguration()->getResultCacheImpl()) {
            if ($this->isCacheable($entityName, $data)) {
                $cacheKey = $this->generateCacheKey($entityName, $id);
                $query->useResultCache(true, $data['lifetime'], $cacheKey);
            }
        }

        $result = $query->getOneOrNullResult();

        return $result;
    }

    protected function isShardable($entityName, &$shardColumn)
    {
        $reader      = new SimpleAnnotationReader();
        $reflClass   = $this->getClassMetadata($entityName)->getReflectionClass();
        $annotations = $reader->getClassAnnotations($reflClass);
        $annotation  = null;

        foreach ($annotations as $annot) {
            if ($annot instanceof \Application\ORM\Shardable\Shardable) {

                if (!empty($annot->column)) {
                    $shardColumn = $annot->column;
                } else {
                    $shardColumn = $annot->dependColumn;
                }

                return true;
            }
        }

        return false;
    }

    protected function isCacheable($entityName, &$data)
    {
        $reader      = new SimpleAnnotationReader();
        $reflClass   = $this->getClassMetadata($entityName)->getReflectionClass();
        $annotations = $reader->getClassAnnotations($reflClass);
        $annotation  = null;

        foreach ($annotations as $annot) {
            if ($annot instanceof \Application\ORM\Cacheable\Cacheable) {
                $data = array(
                    'lifetime' => $annot->lifetime,
                    'type'     => $annot->type
                );
                return true;
            }
        }

        return false;
    }

    public function generateCacheKey($entityName, $id)
    {
        if (!is_array($id)) {
            $idCol = $this->getClassMetadata($entityName)->identifier[0];
            $id    = array($idCol => $id);
        }

        ksort($id);

        return $entityName . md5(serialize($id));
    }
}
