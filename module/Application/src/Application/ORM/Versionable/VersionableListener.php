<?php

namespace Application\ORM\Versionable;

use Doctrine\ORM\Events,
    Doctrine\ORM\EntityManager,
    Doctrine\Common\EventSubscriber,
    Doctrine\ORM\Event\OnFlushEventArgs,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Doctrine\Common\Annotations\SimpleAnnotationReader,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject,
    Application\ORM\Versionable\Entity\ResourceVersion,
    Zend\ServiceManager\ServiceManager,
    Zend\Authentication\AuthenticationService;

class VersionableListener implements EventSubscriber
{
    /**
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * serviceManager
     * @var ServiceManager
     */
    protected $serviceManager;

    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        $resourceClass = $em->getClassMetadata('Application\ORM\Versionable\Entity\ResourceVersion');

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($this->isEntitySupported($em, $entity)) {
                $entityClass = $em->getClassMetadata(get_class($entity));
                $entityId    = $entityClass->getIdentifierValues($entity);

                if (count($entityId) == 1 && current($entityId)) {
                    $entityId = current($entityId);
                } else {
                    throw new \Exception('single identifier required');
                }

                $object  = array();
                foreach ($entityClass->reflFields as $name => $refProp) {
                    $value = $refProp->getValue($entity);

                    if (is_object($value) && method_exists($value, 'toString')) {
                        $value = $value->toString();
                    } elseif ($value instanceof \DateTime) {
                        $value = $value->format('c');
                    }
                    $object[$name] = $value;
                }

                $changes = array();
                foreach ($uow->getEntityChangeSet($entity) as $field => $change) {
                    $changes[$field] = array(
                        'old' => $change[0],
                        'new' => $change[1]
                    );

                    $object[$field] = $change[0];
                }

                $versionField  = "version";
                $entityVersion = $entityClass->reflFields[$versionField]->getValue($entity);
                $entityVersion++;

                unset($changes[$entityClass->versionField]);
                unset($changes[$entityClass->getSingleIdentifierFieldName()]);
                unset($changes['modifiedDate']);
                unset($changes['creationDate']);

                $versionData = array(
                    'changes' => $changes,
                    'object'  => $object
                );

                $resourceVersion = new ResourceVersion();
                $resourceVersion->setResourceName($entityClass->name);
                $resourceVersion->setResourceId($entityId);
                $resourceVersion->setVersionedData($versionData);
                $resourceVersion->setVersion($entityVersion);
                $resourceVersion->setAction("UPDATE");
                $resourceVersion->setCreationDate(new \DateTime('now'));

                if ($this->authenticationService->hasIdentity()) {
                    $identity      = $this->authenticationService->getIdentity();
                    $identityClass = $em->getClassMetadata(get_class($identity));
                    $identityId    = $identityClass->getIdentifierValues($identity);
                    if (count($identityId) == 1 && current($identityId)) {
                        $identityId = current($identityId);
                        $resourceVersion->setUserId($identityId);
                    }
                }

                $em->persist($resourceVersion);
                $uow->computeChangeSet($resourceClass, $resourceVersion);
            }
        }
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::onFlush,
        );
    }

    private function isEntitySupported(EntityManager $em, $entity)
    {
        $enClass     = get_class($entity);
        $reflClass   = $em->getClassMetadata($enClass)->getReflectionClass();
        $reader      = new SimpleAnnotationReader();
        $annotations = $reader->getClassAnnotations($reflClass);
        $annotation  = null;

        foreach ($annotations as $annot) {
            if ($annot instanceof \Application\ORM\Versionable\Versionable) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    /**
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
