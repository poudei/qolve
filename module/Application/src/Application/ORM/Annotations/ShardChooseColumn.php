<?php

namespace Application\ORM\Annotations;


/**
 * ShardChoseColumn
 *
 * @Annotation
 * @Target("CLASS")
 */
class ShardChooseColumn
{
    /**
     * dependColumn
     *
     * @var string
     * @access public
     */
    public $dependColumn;

    /**
     * type
     *
     * @var string
     * @access public
     */
    public $type;

    /**
     * column
     *
     * @var mixed
     * @access public
     */
    public $column;
}
