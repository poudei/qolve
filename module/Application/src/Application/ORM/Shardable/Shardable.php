<?php

namespace Application\ORM\Shardable;


/**
 * Shardable
 *
 * @Annotation
 * @Target("CLASS")
 */
class Shardable
{
    /**
     * dependColumn
     *
     * @var string
     * @access public
     */
    public $dependColumn;

    /**
     * type
     *
     * @var string
     * @access public
     */
    public $type;

    /**
     * column
     *
     * @var mixed
     * @access public
     */
    public $column;
}
