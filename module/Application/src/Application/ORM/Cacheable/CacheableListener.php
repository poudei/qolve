<?php
namespace Application\ORM\Cacheable;

use Doctrine\ORM\Events,
    Doctrine\Common\EventSubscriber,
    Doctrine\ORM\Event\OnFlushEventArgs,
    Doctrine\Common\Annotations\SimpleAnnotationReader,
    Application\Service\UidGenerator,
    Zend\ServiceManager\ServiceManager;

class CacheableListener implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            Events::onFlush,
        );
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        if (!$cache = $em->getConfiguration()->getResultCacheImpl()) {
            return;
        }

        $busted = array_merge(
            $uow->getScheduledEntityInsertions(),
            $uow->getScheduledEntityUpdates(),
            $uow->getScheduledEntityDeletions()
        );

        foreach ($busted as $entity) {
            $entityClass = $em->getClassMetadata(get_class($entity));
            $entityId    = $entityClass->getIdentifierValues($entity);
            $cacheKey    = $em->generateCacheKey(get_class($entity), $entityId);

            $cache->delete($cacheKey);
        }
    }
}
