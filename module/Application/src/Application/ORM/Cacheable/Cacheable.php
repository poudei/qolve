<?php

namespace Application\ORM\Cacheable;


/**
 * Shardable
 *
 * @Annotation
 * @Target("CLASS")
 */
class Cacheable
{
    /**
     * lifetime
     *
     * @var string
     * @access public
     */
    public $lifetime = 3600;

    /**
     * type
     *
     * @var string
     * @access public
     */
    public $type;
}
