<?php
namespace Application\Controller;

use Application\Controller\AbstractController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Zend\Crypt\Password\Bcrypt,
    Zend\Form\Annotation\AnnotationBuilder,
    Application\Entity\User,
    Application\Entity\OauthClient,
    Application\Entity\OauthAccessToken,
    Application\Entity\Error;

class AuthenticateController extends AbstractController
{
    public function getTokenAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $hydrator       = $this->getServiceLocator()->get('Hydrator');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        $clientId  = $data['client_id'];
        $clientSec = $data['client_secret'];
        $identity  = $data['identity'];
        $password  = $data['password'];
        
        if (is_string($identity)) {
            $identity = strtolower(trim($identity));
        }

        $client = $objectManager
            ->getRepository('Application\Entity\OauthClient')
            ->findOneBy(array('clientId' => $clientId));
        if (!$client instanceof OauthClient) {
            $this->getResponse()->setStatusCode(Error::ClientNotFound_code);
            return new JsonModel(
                array(
                    'error' => Error::ClientNotFound_message
                )
            );
        }

        $return = $objectManager
            ->getRepository('Application\Entity\User')
            ->getOneByElastic($serviceLocator, 'email', $identity);

        $user = array();
        if (!empty($return)) {
            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($return['user_id']);
        }
    
        if (!$user instanceof User) {

            $return = $objectManager
                ->getRepository('Application\Entity\User')
                ->getOneByElastic($serviceLocator, 'username', $identity);

            $user = array();
            if (!empty($return)) {
                $user = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($return['user_id']);
            }

            if (!$user instanceof User) {
                $this->getResponse()->setStatusCode(Error::UsernameInvalid_code);
                return new JsonModel(
                    array(
                        'error' => Error::UsernameInvalid_message
                    )
                );
            }
        }
        
        if ($user->getDeleted() == 1) {
            $user->setDeleted(0);
            $objectManager->persist($user);
            $objectManager->flush();
        }

        $passCost = $serviceLocator
            ->get('ZfcUser\Authentication\Adapter\Db')
            ->getOptions()
            ->getPasswordCost();

        $bcrypt = new Bcrypt();
        $bcrypt->setCost($passCost);

        if (!$bcrypt->verify($password,$user->getPassword())) {
            $this->getResponse()->setStatusCode(
                Error::SuppliedCredentialInvalid_code
            );
            return new JsonModel(
                array(
                    'error' => Error::SuppliedCredentialInvalid_mesage
                )
            );
        }

        $provider = $serviceLocator->get('OAuthProvider');
        $token    = bin2hex($provider->generateToken(16));
        $secret   = bin2hex($provider->generateToken(16));

        $accessToken = new OauthAccessToken();
        $accessToken->setToken($token);
        $accessToken->setTokenSecret($secret);
        $accessToken->setClientId($client->getClientId());
        $accessToken->setUserId($user->getId());
        $accessToken->setExpires(new \DateTime('+1 month'));
        $accessToken->setScope('');

        $objectManager->persist($accessToken);

        $objectManager->flush();

        return new JsonModel(
            array(
                'oauth_token'        => $token,
                'oauth_token_secret' => $secret,
                'user_id'            => $user->getId()
            )
        );
    }
}
