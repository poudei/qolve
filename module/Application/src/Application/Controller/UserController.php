<?php
namespace Application\Controller;

use Application\Controller\AbstractController,
    Application\Entity\User,
    Application\Entity\UserProfile,
    Application\Entity\UserProvider,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Application\Entity\Password,
    Application\Entity\UserPref,
    Application\Entity\OauthAccessToken,
    Application\Entity\OauthClient,
    Application\Entity\Error,
    Qolve\Entity\BookmarkList,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Zend\Crypt\Password\Bcrypt,
    Zend\Mvc\MvcEvent,
    Zend\Form\Annotation\AnnotationBuilder;

define('AUTH_CONFIG', dirname(__DIR__) . "/../../../../config/autoload/hybridauth.php");
class UserController extends AbstractController
{
    public function loginAction()
    {

        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        if ($this->zfcUserAuthentication()->getAuthService()->hasIdentity()) {
            $user = $this->identity();
            if ($user->getDeleted() == 1) {
                $user->setDeleted(0);
                $objectManager->persist($user);
                $objectManager->flush();
            }
            $hydrator = $this->getServiceLocator()->get('Hydrator');
            $user     = $hydrator->extract($user);
            return new JsonModel(array('user' => $user));
        }
        $adapter  = $this->zfcUserAuthentication()->getAuthAdapter();
        $result   = $adapter->prepareForAuthentication($this->getRequest());
        $auth     = $this->zfcUserAuthentication()
            ->getAuthService()
            ->authenticate($adapter);

        if (!$auth->isValid()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'code'  => Error::InvalidUserPass_code,
                    'error' => Error::InvalidUserPass_message
                )
            );
        }

        $user = $this->identity();
        if ($user->getDeleted() == 1) {
            $user->setDeleted(0);
            $objectManager->persist($user);
            $objectManager->flush();
        }


        $defaultListPref = $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->find(
                array(
                    'userId' => $user->getId(),
                    'key'    => 'defaultBookmarkListId'
                )
            );
        if (!$defaultListPref instanceof UserPref) {
            $defaultListPref = new UserPref();
            $defaultListPref->setUserId($user->getId());
            $defaultListPref->setKey('defaultBookmarkListId');

            $objectManager->persist($defaultListPref);
        }

        if (is_null($defaultListPref->getValue())) {
            $list = $objectManager
                ->getRepository('Qolve\Entity\BookmarkList')
                ->findOneBy(
                    array(
                        'userId' => $user->getId(),
                        'name'   => BookmarkList::NAME_UNCATEGORIZED
                    )
                );
            if (!$list instanceof BookmarkList) {
                $now = new \DateTime('now', new \DateTimeZone('UTC'));

                $list = new BookmarkList();
                $list->setName(BookmarkList::NAME_UNCATEGORIZED);
                $list->setUserId($user->getId());
                $list->setPrivacy(BookmarkList::PRIVACY_PRIVATE);
                $list->setQuestions(0);
                $list->setFollows(0);
                $list->setCreatedOn($now);

                $objectManager->persist($list);
            }

            $defaultListPref->setValue($list->getId());
//            $objectManager->persist($defaultListPref);

            $objectManager->flush();
        }

        $info = array(
            'credit',
            'followersList',
            'followingsList'
        );

        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $user     = $hydrator->extract($user, $info, $user);

        return new JsonModel(array('user' => $user));
    }

    public function loginviaproviderAction()
    {
        $hybridauth = $this->getServiceLocator()->get('HybridAuth');
        $hybridauth->logoutAllProviders();

        $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
        $this->zfcUserAuthentication()->getAuthAdapter()->logoutAdapters();
        $this->zfcUserAuthentication()->getAuthService()->clearIdentity();

        return $this->redirect()->toUrl('/signIn?error=loginbyprovider');
    }

    public function signupviamobileAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        $requiredParams = array(
            'provider',
            'providerId',
            'accessToken',
            'client_id',
            'client_secret'
        );
        if (isset($data['provider'])
            && strtolower($data['provider']) == 'twitter'
        ) {
            $requiredParams[] = 'accessTokenSecret';
        }
        
        foreach ($requiredParams as $reqParam) {
            if (!isset($data[$reqParam])) {
                
                $reqParam = explode('_', $reqParam);
                foreach ($reqParam as $key => &$val) {
                    $val = ucfirst($val);
                }
                $reqParam = join('', $reqParam);
                
                $errorRef = new \ReflectionClass('\Application\Entity\Error');
                $error    = $reqParam . "NotSent";
                
                $this->getResponse()->setStatusCode(
                    $errorRef->getConstant("$error"."_code")
                );
                return new JsonModel(
                    array(
                        'error' => $errorRef->getConstant("$error"."_message")
                    )
                );
            }
        }
        
        $client = $objectManager
            ->getRepository('Application\Entity\OauthClient')
            ->findOneBy(array('clientId' => $data['client_id']));
        if (!$client instanceof OauthClient) {
            $this->getResponse()->setStatusCode(Error::ClientNotFound_code);
            return new JsonModel(
                array(
                    'error' => Error::ClientNotFound_message
                )
            );
        }
        if ($client->getClientSecret() != $data['client_secret']) {
            $this->getResponse()->setStatusCode(
                Error::ClientSecretInvalid_code
            );
            return new JsonModel(
                array(
                    'error' => Error::ClientSecretInvalid_message
                )
            );
        }

        $data['provider'] = strtolower($data['provider']);

        if (!isset($data['email'])) {
            $data['email'] = 'NOTSET';
        }

        if (!isset($data['password'])) {
            $data['password'] = strtolower($data['provider']) . 'ToLocalUser';
        }
        
        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getByProviderId($data['provider'], $data['providerId']);
        if ($userProvider instanceof UserProvider) {
            $user = $objectManager->getRepository('Application\Entity\User')
                ->find($userProvider->getUserId());
            
            if (!$user instanceof User) {
                $objectManager->remove($userProvider);
                $objectManager->flush();
            } else {
                if (!is_null($user->getUsername())
                    && !is_null($user->getPassword()))
                {
                    $this->getResponse()->setStatusCode(
                        Error::LoginViaProvider_code
                    );
                    return new JsonModel(
                        array(
                            'error' => Error::LoginViaProvider_message
                        )
                    );
                }
            }
        }
        
        if (!isset($user) || !$user instanceof User) {
            
            $user         = new User();
            $userProfile  = new UserProfile();
            $userProvider = new UserProvider();


            $builder = new AnnotationBuilder();
            $now = new \DateTime('now', new \DateTimeZone('UTC'));

            //Save User
            $userForm = $builder->createForm($user);
            $userForm->setHydrator($hydrator);
            $userForm->bind($user);
            $userForm->setData($data);

            if (!$userForm->isValid()) {
                $this->getResponse()->setStatusCode(Error::FormInvalid_code);

                return new JsonModel(
                    array(
                        'error' => $userForm->getMessages()
                    )
                );
            }

            $user = $userForm->getData();

            if (!is_null($user->getEmail())
                && strtoupper($user->getEmail()) != 'NOTSET'
            ) {
                //Elastic search
                $elastica = $this->getServiceLocator()
                    ->get('Elastica\Client');
                $elasticaClient = $elastica['client'];
                $index          = $elastica['index'];

                $elasticaQuery = new \Elastica\Query();
                $search        = new \Elastica\Search($elasticaClient);
                $elasticaIndex = $elasticaClient->getIndex($index);
                $elasticaType  = $elasticaIndex->getType('user');

                $boolMust      = new \Elastica\Query\Bool();
                $term          = new \Elastica\Query\Term();

                $term->setParam('email', trim($user->getEmail()));
                $boolMust->addMust($term);

                $elasticaQuery = new \Elastica\Query($boolMust);

                $resultSet = $search
                    ->addIndex($index)
                    ->addType('user')
                    ->search($elasticaQuery);

                foreach ($resultSet as $result) {
                    $return[] = $result->getData();
                }

                if (isset($return['user_id'])) {
                    $preUser = $objectManager
                        ->getRepository('Application\Entity\User')
                        ->find($return['user_id']);
                    if ($preUser instanceof User) {
                        $this->getResponse()->setStatusCode(
                            Error::DuplicateEmail_code
                        );

                        return new JsonModel(
                            array(
                                'error' => Error::DuplicateEmail_message
                            )
                        );
                    }
                }
            }

            if (!is_null($user->getUsername())) {

                $boolMust      = new \Elastica\Query\Bool();
                $term          = new \Elastica\Query\Term();

                $term->setParam('username', trim($user->getUsername()));
                $boolMust->addMust($term);

                $elasticaQuery = new \Elastica\Query($boolMust);

                $resultSet = $search
                    ->addIndex($index)
                    ->addType('user')
                    ->search($elasticaQuery);

                foreach ($resultSet as $result) {
                    $return[] = $result->getData();
                }


                $preUser = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($return['user_id']);
                if ($preUser instanceof User) {
                    $user->setUsername(null);
                }
            }

            $user->setStatus(1);
            $user->setDeleted(0);
            $user->setCreatedOn($now);
            $user->setModifiedOn($now);

            $objectManager->persist($user);
            
            //Save User's Image
            if (isset($data['socialImageUrl'])
                && !empty($data['socialImageUrl'])) 
            {
                $userId = $user->getId();
                if (!file_exists(
                    PUBLIC_PATH."/upfiles/users/$userId/profile")
                ) {
                    mkdir(
                        PUBLIC_PATH."/upfiles/users/$userId/profile", 0777, true
                    );
                }
                $destination = copy(
                    $data['socialImageUrl'],
                    PUBLIC_PATH . "/upfiles/users/$userId/profile/image.jpg"
                );
                if ($destination) {
                    $user->setImagePath(
                        "upfiles/users/$userId/profile/image.jpg"
                    );
                }
            }

            //Save UserProfile
            $profileForm = $builder->createForm($userProfile);
            $profileForm->setHydrator($hydrator);
            $profileForm->bind($userProfile);
            $profileForm->setData($data);

            if (!$profileForm->isValid()) {
                $this->getResponse()->setStatusCode(Error::FormInvalid_code);

                return new JsonModel(
                    array(
                        'error' => $profileForm->getMessages()
                    )
                );
            }

            $userProfile = $profileForm->getData();

            $userProfile->setUserId($user->getId());
            $userProfile->setDeleted(0);
            $userProfile->setCreatedOn($now);
            $userProfile->setModifiedOn($now);

            $objectManager->persist($userProfile);

            //Save UserProvider
            $userProvider->setUserId($user->getId());
            $userProvider->setProvider($data['provider']);
            $userProvider->setProviderId($data['providerId']);
            $userProvider->setPublished(UserProvider::PUBLISHED_YES);
            
            switch ($data['provider']) {
            case 'facebook':
            case 'google':
                $userProvider->setAccessToken(
                    serialize($data['accessToken'])
                );
                break;
            
            case 'twitter':
                $userProvider->setAccessToken(
                    serialize(array(
                        'token'  => $data['accessToken'],
                        'secret' => $data['accessTokenSecret']
                    ))
                );
                break;
            }

            $objectManager->persist($userProvider);

            $objectManager->flush();

            $createdOn = $user->getCreatedOn()->format('Y-m-d H:i:s');
            $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));

            //Add user to Elastic
            $elastica  = $this
                ->getServiceLocator()
                ->get('Elastica\Client');
            $elasticaClient = $elastica['client'];
            $index          = $elastica['index'];
            $item = array (
                'user_id'    => $user->getId(),
                'name'       => $user->getname(),
                'username'   => $user->getUsername(),
                'email'      => $user->getEmail(),
                'status'     => $user->getStatus(),
                'deleted'    => $user->getDeleted(),
                'created_on' => $createdOn
            );
            $elasticaIndex = $elasticaClient->getIndex($index);
            $elasticaType  = $elasticaIndex->getType('user');
            $content       = new \Elastica\Document($user->getId(), $item);

            $elasticaType->addDocument($content);
            $elasticaType->getIndex()->refresh();
        }
        
        $provider = $this->getServiceLocator()->get('OAuthProvider');
        $token    = bin2hex($provider->generateToken(16));
        $secret   = bin2hex($provider->generateToken(16));
        
        $accessToken = new OauthAccessToken();
        $accessToken->setToken($token);
        $accessToken->setTokenSecret($secret);
        $accessToken->setClientId($client->getClientId());
        $accessToken->setUserId($user->getId());
        $accessToken->setExpires(new \DateTime('+1 month'));
        $accessToken->setScope('');
        
        $objectManager->persist($accessToken);
        $objectManager->flush();
        
        $this->getServiceLocator()
            ->get('Zend\Authentication\AuthenticationService')
            ->getStorage()->write($user->getId());
        $this->getServiceLocator()
            ->get('ZfcRbac\Service\Rbac')->setIdentity($user);

        $_user = $hydrator->extract($user, array(), $user);
        $_user['email'] = $user->getEmail();

        return new JsonModel(
            array(
                'user'               => $_user,
                'oauth_token'        => $token,
                'oauth_token_secret' => $secret,
            )
        );
    }

    public function changepasswordAction()
    {
        if (!$this->zfcUserAuthentication()->getAuthService()->hasIdentity()) {
            return $this->redirect()->toUrl('/');
        }

        $hydrator    = $this->getServiceLocator()->get('Hydrator');
        $data        = $this->getRequest()->getContent();
        $data        = \Zend\Json\Json::decode(
            $data,
            \Zend\Json\Json::TYPE_ARRAY
        );
        $userService = $this->getServiceLocator()->get('zfcuseruserservice');
        $form        = $userService->getChangePasswordForm();
        $user        = $this->identity();
        $identity    = $user->getEmail();

        $data['identity'] = $identity;

        $form->setData($data);
        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(array('error' => $form->getMessages()));
        }
        if (!$userService->changePassword($form->getData())) {
            $this->getResponse()->setStatusCode(
                Error::PermissionDenied_code
            );
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }
        $user = $this->identity();
        $user = $hydrator->extract($user);
        return new JsonModel(array('user' => $user));
    }

    public function sendforgotemailAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if (!$this->zfcUserAuthentication()->getAuthService()->hasIdentity()) {
            return $this->redirect()->toUrl('/');
        }

        $hydrator    = $this->getServiceLocator()->get('Hydrator');
        $data        = $this->getRequest()->getContent();
        $data        = \Zend\Json\Json::decode(
            $data,
            \Zend\Json\Json::TYPE_ARRAY
        );
        $userService = $this->getServiceLocator()->get('zfcuseruserservice');
        $form        = $userService->getChangePasswordForm();
        $user        = $this->identity();
        $identity    = $user->getEmail();

        $data['identity'] = $identity;

        $form->setData($data);
        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(array('error' => $form->getMessages()));
        }
        if (!$userService->changePassword($form->getData())) {
            $this->getResponse()->setStatusCode(Error::PermissionDenied_code);
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }
        $user = $this->identity();
        $user = $hydrator->extract($user);
        return new JsonModel(array('user' => $user));
    }

    public function forgotpasswordAction()
    {
        $data     = $this->getRequest()->getContent();
        $data     = \Zend\Json\Json::decode(
            $data,
            \Zend\Json\Json::TYPE_ARRAY
        );
        $editableProperties = array('email');

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $return = $objectManager
            ->getRepository('Application\Entity\User')
            ->getOneByElastic(
                $serviceLocator,
                'email',
                $data['email']
        );

        $user = array();

        if (!empty($return)) {
            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($return['user_id']);
        }

        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(Error::UserNotFound_code);
            return new JsonModel(array('error' => Error::UserNotFound_message));
        }

        $password = $objectManager
            ->getRepository('Application\Entity\Password')
            ->findOneByUserId($user->getId());

        if ($password instanceof Password) {

            $objectManager->remove($password);
            $objectManager->flush($password);
        }

        // $hashKey  = hash();
        $length      = rand(15,20);
        $bytes       = openssl_random_pseudo_bytes($length, $cstrong);
        $hashKey     = bin2hex($bytes);
        $requestTime = new \DateTime('now', new \DateTimeZone('UTC'));
        $password    = new Password();

        $password->setRequestKey($hashKey);
        $password->setUserId($user->getId());
        $password->setRequestTime($requestTime);
        $objectManager->persist($password);
        $objectManager->flush();
        $config =  $this->getServiceLocator()->get('config');
        $emailQueue = $serviceLocator
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('email');

        $job = new \Application\Job\SendEmail($serviceLocator);
        $job->setContent(array(
            'to'        => $data['email'],
            'subject'   => 'Forgot Password',
            'template'  => 'ForgotPassword',
            'params'    => array(
                'resetpath' => $config['baseUrl'] . '/resetPassword/'.$hashKey,
                'baseUrl'   => $config['baseUrl'],
                'name'      => $user->getName()
            )
        ));

        $emailQueue->push(
            $job,
            array('delay' => new \DateInterval("PT10S"))
        );

        return new jsonModel(array('Success'));
    }

    public function resetpasswordAction()
    {

        $reqKey   = $this->params()->fromQuery('req_key');
        $data     = $this->getRequest()->getContent();
        $data     = \Zend\Json\Json::decode(
            $data,
            \Zend\Json\Json::TYPE_ARRAY
        );
        $editableProperties = array(
            'newpass',
            'confirmpass'
        );
        foreach ($data as $name => $value) {

            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }
        similar_text($data['newpass'], $data['confirmpass'], $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(Error::PasswordNotMatch_code);
            return new jsonModel(
                array(
                    'error' => Error::PasswordNotMatch_message
                )
            );
        }
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $hydrator   = $this->getServiceLocator()->get('Hydrator');
        $expireTime = new \DateTime((int)86400 . ' seconds ago');
        $expireTime = $expireTime->format('Y-m-d H:i:s');

        $password   = $objectManager
            ->getRepository('Application\Entity\Password')
            ->findOneByRequestKey($reqKey);

        if (!$password instanceof Password) {
            $this->getResponse()->setStatusCode(Error::RequestKeyInvalid_code);
            return new jsonModel(
                array(
                    'error' => Error::RequestKeyInvalid_message
                )
            );
        }

        if (strtotime(  $password->getRequestTime()->format('Y-m-d H:i:s'))
                      < strtotime($expireTime)) {
            $objectManager->remove($password);
            $objectManager->flush($password);
            $this->getResponse()->setStatusCode(Error::RequestOutOfDate_code);
            return new JsonModel(
                array(
                    'error' => Error::RequestOutOfDate_message
                )
            );
        }

        $user = $objectManager
            ->getRepository('Application\Entity\User')
            ->find($password->getUserId());

        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(Error::UserNotFound_code);
            return new JsonModel(array('error' => Error::UserNotFound_message));
        }

        $bcrypt  = new Bcrypt();
        $newpass = $bcrypt->create($data['newpass']);

        $user->setPassword($newpass);
        $objectManager->persist($user);
        $objectManager->remove($password);
        $objectManager->flush();


        return new jsonModel(array('Success'));
    }

    public function logoutAction()
    {
        $hybridauth = $this->getServiceLocator()->get('HybridAuth');
        $provider   = $hybridauth->logoutAllProviders();
        $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
        $this->zfcUserAuthentication()->getAuthAdapter()->logoutAdapters();
        $this->zfcUserAuthentication()->getAuthService()->clearIdentity();

        return $this->redirect()->toUrl('/');
    }

    public function addproviderAction()
    {
        //        $this->redirect()->toRoute('scn-social-auth-user/login/provider', array('provider' => 'facebook'));

        //        $this->redirect()->toUrl('/user/login/facebook');


        $hybridauth = $this->getServiceLocator()->get('HybridAuth');
        ////        $hybridauth->setup('facebook');
        $result = $hybridauth->authenticate('facebook');

        //        return $this->redirect()->toUrl('/');
    }

    public function deleteaccountAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        
        $id = isset($info['userId']) ? $info['userId'] : null;
        $user = $this->identity();
        if (strtoupper($id) != 'ME') {
            similar_text($id, $user->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $user->setDeleted(1);
        $objectManager->persist($user);
        $objectManager->flush();
        $this->clearCache($objectManager);

        return $this->logoutAction();
    }

    private function clearCache($objectManager)
    {
        $followers = array();
        $followers = $objectManager
            ->getRepository('Qolve\Entity\UserFollow')
            ->findByUserId($user->getId());

        foreach($followers as $follower) {

            $cache = $objectManager->getConfiguration()->getResultCacheImpl();
            $cache->delete(
                'user_' . $follower->getFollowerId()
                . '_following_' . $follower->getUserId()
                . '_follow'
            );

            $cache->delete('user_' . $follower->getFollowerId() . '_followings');
            $cache->delete('user_' . $follower->getFollowerId() . '_follows');
        }

        $folloyees = array();
        $folloyees = $objectManager
            ->getRepository('Qolve\Entity\UserFollow')
            ->findByFollowerId($user->getId());

        foreach($folloyees as $folloyee) {

            $cache = $objectManager->getConfiguration()->getResultCacheImpl();

            $cache->delete('user_' . $folloyee->getUserId()     . '_followers');
            $cache->delete('user_' . $folloyee->getUserId()     . '_follows');
        }
    }


}
