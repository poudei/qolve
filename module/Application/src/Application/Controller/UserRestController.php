<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\UserPref,
    Application\Entity\UserProfile,
    Application\Entity\Error,
    Qolve\Entity\BookmarkList,
    Zend\Form\Annotation\AnnotationBuilder;

define('AUTH_CONFIG', dirname(__DIR__) . "/../../../../config/autoload/hybridauth.php");

class UserRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $userRepo = $objectManager->getRepository('Application\Entity\User');

        $limit   = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 10)
        );
        $offset  = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
        );
        $orderBy = $this->params()->fromQuery(
            'orderBy',
            array('name' => 'ASC')
        );
        $name    = $this->params()->fromQuery('name', '');

        $filter = array(
            'name'    => $name,
            'deleted' => 0
        );

        $currentUser  = $this->identity();

        $users = $userRepo->getUsersByFilter(
            $filter,
            $orderBy,
            $offset,
            $limit
        );
        $count = $objectManager
            ->getRepository('Application\Entity\User')
            ->countBy($filter);

        $_users = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($users as $user) {
            if ($user instanceof User) {
                $_users['list'][] = $hydrator->extract(
                    $user,
                    array(),
                    $currentUser
                );
            }
        }

        return new JsonModel(array("users" => $_users));
    }

    public function get($id)
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $hydrator       = $this->getServiceLocator()->get('Hydrator');

        if (strtoupper($id) == 'ME') {

            if (!$this->identity() instanceof \Application\Entity\User) {
                $this->getResponse()->setStatusCode(Error::UserNotFound_code);
                return new JsonModel(
                    array(
                        'error' => Error::UserNotFound_message
                    )
                );
            }

            $user = $this->identity();
        } else {
            $user = array();
            if (preg_match('/[a-zA-Z\-_]+/',$id)) {
                //Elastic search
                $return = $objectManager
                ->getRepository('Application\Entity\User')
                ->getOneByElastic(
                    $serviceLocator,
                    'username',
                    $id
            );

            $user = array();
            if (!empty($return)) {
                $user = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($return['user_id']);
                }

            } else {
                $user = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($id);
            }
        }

        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(Error::UserNotFound_code);
            return new JsonModel(
                array(
                    'error' => Error::UserNotFound_message
                )
            );
        }

        if ($user->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(Error::UserUnavailable_code);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $currentUser = $this->identity();

        if ($currentUser instanceof User) {
            similar_text($user->getId(), $currentUser->getId(), $percent);
        } else {
            $percent = 0;
        }

        if ($percent == 100) {
            $defaultListPref = $objectManager
                ->getRepository('Application\Entity\UserPref')
                ->find(
                    array(
                        'userId' => $user->getId(),
                        'key'    => 'defaultBookmarkListId'
                    )
                );
            if (!$defaultListPref instanceof UserPref) {
                $defaultListPref = new UserPref();
                $defaultListPref->setUserId($user->getId());
                $defaultListPref->setKey('defaultBookmarkListId');

                $objectManager->persist($defaultListPref);
            }

            if (is_null($defaultListPref->getValue())) {
                $list = $objectManager
                    ->getRepository('Qolve\Entity\BookmarkList')
                    ->findOneBy(
                        array(
                            'userId' => $user->getId(),
                            'name'   => BookmarkList::NAME_UNCATEGORIZED
                        )
                    );
                if (!$list instanceof BookmarkList) {
                    $now = new \DateTime('now', new \DateTimeZone('UTC'));

                    $list = new BookmarkList();
                    $list->setName(BookmarkList::NAME_UNCATEGORIZED);
                    $list->setUserId($user->getId());
                    $list->setPrivacy(BookmarkList::PRIVACY_PRIVATE);
                    $list->setQuestions(0);
                    $list->setFollows(0);
                    $list->setCreatedOn($now);

                    $objectManager->persist($list);
                }

                $defaultListPref->setValue($list->getId());
                $objectManager->persist($defaultListPref);

                $objectManager->flush();
            }
        }

        return new JsonModel(array("user" => $hydrator->extract(
            $user,
            array('listsCount'),
            $currentUser))
        );
    }

    public function update($id, $data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(Error::UserUnavailable_code);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();

        $user = $this->identity();

        $meId = $user->getId();
        similar_text($id, $meId, $percentage);
        if ($percentage < 100) {

            $this->getResponse()->setStatusCode(Error::PermissionDenied_code);
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $signup = isset($data['signup']) ? $data['signup'] : false;

        if ($signup) {
            $requiredParams = array(
                'username',
                'password',
                'email'
            );
            foreach ($requiredParams as $param) {
                if (!isset($data[$param])) {

                    $param = explode('_', $param);
                    foreach ($param as $key => &$val) {
                        $val = ucfirst($val);
                    }
                    $param = join('', $param);

                    $errorRef = new \ReflectionClass('\Application\Entity\Error');
                    $error    = $param . "NotSent";

                    $this->getResponse()->setStatusCode(
                        $errorRef->getConstant("$error"."_code")
                    );
                    return new JsonModel(
                        array(
                            'error' => $errorRef->getConstant("$error"."_message")
                        )
                    );
                }
            }

            $username = $data['username'];
            $pass     = $data['password'];
            $email    = $data['email'];

            unset($data['username'], $data['password'], $data['email']);

            if (!$objectManager
                ->getRepository('Application\Entity\User')
                ->isValidUsername($username)) {

                $this->getResponse()->setStatusCode(Error::UsernameInvalid_code);
                return new JsonModel(
                    array(
                        'error' => Error::UsernameInvalid_message
                    )
                );
            }

            if (!$objectManager
                ->getRepository('Application\Entity\User')
                ->isValidPassword($pass)) {

               $this->getResponse()->setStatusCode(Error::PasswordInvalid_code);
               return new JsonModel(
                    array(
                        'error' => Error::PasswordInvalid_message
                    )
                );
           }

           $passCost = $this->getServiceLocator()
                ->get('ZfcUser\Authentication\Adapter\Db')
                ->getOptions()
                ->getPasswordCost();
            $bcrypt = new \Zend\Crypt\Password\Bcrypt();
            $bcrypt->setCost($passCost);
            $pass = $bcrypt->create($pass);

            $currentUser = $this->identity();

            $return = $objectManager
                ->getRepository('Application\Entity\User')
                ->getOneByElastic($serviceLocator, 'username', $username);
            $_user = array();
            if (!empty($return)) {
                $_user = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($return['user_id']);
            }

            if ($_user instanceof User) {
                similar_text($_user->getId(), $user->getId(), $percent);
                if ($percent < 100) {
                    $this->getResponse()->setStatusCode(
                        Error::DuplicateUsername_code
                    );
                    return new JsonModel(
                        array(
                            'error' => Error::DuplicateUsername_message
                        )
                    );
                }
            }
        }

        //TODO: remove email from this array
        $editableProperties = array(
            'name',
            'imagePath',
            'username',
            'email',
            'about',
            'gender',
            'genderHide',
            'birthday',
            'birthdayHide',
            'occupation',
            'school',
            'location',
            'timezone',
            'profile'
        );

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (isset($data['gender'])) {
            $data['gender'] = (int)$data['gender'];
        }

        if (isset($data['genderHide'])) {
            $data['genderHide'] = (int)$data['genderHide'];
        }

        if (isset($data['birthdayHide'])) {
            $data['birthdayHide'] = (int)$data['birthdayHide'];
        }

        if (isset($data['username'])) {

            if (!$objectManager
                ->getRepository('Application\Entity\User')
                ->isValidUsername($data['username'])) {

                $this->getResponse()->setStatusCode(
                    Error::UsernameInvalid_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::UsernameInvalid_message
                    )
                );
            }

            $return = $objectManager
                ->getRepository('Application\Entity\User')
                ->getOneByElastic(
                    $serviceLocator,
                    'username',
                    $data['username']
            );

            $sameUsername = array();
            if (!empty($return)) {
                $sameUsername = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($return['user_id']);
            }

            if ($sameUsername instanceof User) {
                similar_text($id, $sameUsername->getId(), $percentage);
                if ($percentage != 100) {

                    $this->getResponse()->setStatusCode(
                        Error::DuplicateUsername_code
                    );
                    return new JsonModel(
                        array(
                            'error' => Error::DuplicateUsername_message
                        )
                    );
                }
            }
        }

        if (isset($data['profile'])) {
            $userId      = $user->getId();
            $name        = $data['profile']['name'];

            if (!file_exists(PUBLIC_PATH."/upfiles/users/$userId/profile")) {
                mkdir(PUBLIC_PATH."/upfiles/users/$userId/profile", 0777, true);
            }

            $destination = copy(PUBLIC_PATH.$data['profile']['location'] ,
                    PUBLIC_PATH."/upfiles/users/$userId/profile/$name"
                );
            if (!$destination) {

                unlink(PUBLIC_PATH.$data['profile']['location']);
                system('rm -rf ' . escapeshellarg(PUBLIC_PATH."/upfiles/tempo/$userId"),
                    $retval
                );
                $this->getResponse()->setStatusCode(
                    Error::FileTransferingProblem_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::FileTransferingProblem_message
                    )
                );
            }
            $location = "upfiles/users/$userId/profile/";
            $user->setImagePath($location.$name);
            $objectManager->persist($user);
            $objectManager->flush();
            system('rm -f ' . escapeshellarg(PUBLIC_PATH."/upfiles/tempo/$userId/$name"),
                    $retval
            );

            return new JsonModel(array("user" => $hydrator->extract($user)));
        }

        if (isset($data['imagePath'])) {
            if (strtolower($data['imagePath']) == 'deleted') {
                $preImagePath = $user->getImagePath();
                if (!empty($preImagePath)) {
                    if (file_exists(PUBLIC_PATH . "/$preImagePath")) {
                        unlink(PUBLIC_PATH . "/$preImagePath");
                    }
                    $user->setImagePath('');
                }
            }

            unset($data['imagePath']);
        }

        $preData = $hydrator->extract($user);
        $preData['email'] = $user->getEmail();
        $data    = array_merge($preData, $data);
        $form    = $builder->createForm($user);
        $form->setHydrator($hydrator);
        $form->setBindOnValidate(false);
        $form->bind($user);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $form->bindValues();
        $user->setModifiedOn($now);


        $profileData = array();
        foreach (array(
            'occupation',
            'school',
            'location',
            'timezone') as $property) {

            if (isset($data[$property])) {
                $profileData[$property] = $data[$property];
            }
        }

        if (!empty($profileData)) {
            $profile = $objectManager
                ->getRepository('Application\Entity\UserProfile')
                ->find($user->getId());

            if (!$profile instanceof UserProfile) {
                $profile = new UserProfile();

                $form    = $builder->createForm($profile);
                $form->setHydrator($hydrator);
                $form->bind($profile);
                $form->setData($profileData);

                if (!$form->isValid()) {
                    $this->getResponse()->setStatusCode(Error::FormInvalid_code);
                    return new JsonModel(array(
                        'error' => $form->getMessages())
                    );
                }

                $profile = $form->getData();
                $profile->setUserId($user->getId());
                $profile->setDeleted(0);
                $profile->setCreatedOn($now);

                $objectManager->persist($profile);

            } else {
                $_profile    = $hydrator->extract($profile);
                $profileData = array_merge($_profile, $profileData);

                $form    = $builder->createForm($profile);
                $form->setHydrator($hydrator);
                $form->setBindOnValidate(false);
                $form->bind($profile);
                $form->setData($profileData);

                if (!$form->isValid()) {
                    $this->getResponse()->setStatusCode(Error::FormInvalid_code);
                    return new JsonModel(array(
                        'error' => $form->getMessages())
                    );
                }

                $form->bindValues();
                $profile->setModifiedOn($now);
            }
        }

        if ($signup) {
            $user->setUsername($username);
            $user->setPassword($pass);
            $user->setEmail($email);

            $objectManager->persist($user);
        }

        $elastica = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $elasticaIndex  = $elasticaClient->getIndex($index);
        $elasticaType   = $elasticaIndex->getType('user');

        $objectManager->flush();

        $term = new \Elastica\Query\Term();

        $term->setParam('user_id', $user->getId());

        $search        = new \Elastica\Search($elasticaClient);
        $elasticaQuery = new \Elastica\Query($term);

        $resultSet = $search->addIndex($index)
            ->addType('user')
            ->search($elasticaQuery);

        $return = array();
        foreach ($resultSet as $result) {
            $return = $result->getData();
        }

        if (!empty($return)) {
            $param = array(
                'username' => $user->getUsername(),
                'email'    => $user->getEmail(),
                'name'     => $user->getName()
            );
            $document = $elasticaType->getDocument($user->getId());

            $document->setData($param);
            $elasticaType->updateDocument($document);
        } else {

            $createdOn = $user->getCreatedOn()->format('Y-m-d H:i:s');
            $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));

            $item = array (
                'user_id'    => $user->getId(),
                'name'       => $user->getName(),
                'username'   => $user->getUsername(),
                'email'      => $user->getEmail(),
                'status'     => $user->getStatus(),
                'deleted'    => $user->getDeleted(),
                'created_on' => $createdOn
            );
            $elasticaIndex = $elasticaClient->getIndex($index);
            $elasticaType  = $elasticaIndex->getType('user');
            $content       = new \Elastica\Document($user->getId(), $item);

            $elasticaType->addDocument($content);
        }

        $elasticaType->getIndex()->refresh();

        $userInfo = $hydrator->extract($user);
        $userInfo['email'] = $user->getEmail();
        return new JsonModel(array("user" => $userInfo));
    }

    public function delete($id)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(Error::UserUnavailable_code);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $user = $this->identity();
        if (strtoupper($id) != 'ME') {
            similar_text($id, $user->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $user->setDeleted(1);
        $objectManager->persist($user);
        $objectManager->flush();

        return new JsonModel(array('Success'));
    }
}
