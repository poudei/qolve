<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\Feedback,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

class FeedbackRestController extends AbstractRestfulController
{
    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );

            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(Error::UserUnavailable_code);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }
        
        $clientIp = isset($_SERVER['REMOTE_ADDR']) 
            ? $_SERVER['REMOTE_ADDR'] : "";
        $userAgent = isset($_SERVER['HTTP_USER_AGENT'])
            ? $_SERVER['HTTP_USER_AGENT'] : "";
        $acceptLanguage = isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])
            ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : "";
        
        if (strlen($clientIp) > 30) {
            $clientIp = substr($clientIp, 0, 30);
        }
        if (strlen($userAgent) > 255) {
            $userAgent = substr($userAgent, 0, 255);
        }
        if (strlen($acceptLanguage) > 30) {
            $acceptLanguage = substr($acceptLanguage, 0, 30);
        }
        

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $builder  = new AnnotationBuilder();
        $feedback = new Feedback();
        $form     = $builder->createForm($feedback);
        $form->setHydrator($hydrator);
        $form->bind($feedback);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(
                array(
                    'error' => $form->getMessages()
                )
            );
        }

        $feedback = $form->getData();
        $user     = $this->identity();
        $now      = new \DateTime('now', new \DateTimeZone('UTC'));

        $feedback->setUserId($user->getId());
        $feedback->setRank(0);
        $feedback->setClientIp($clientIp);
        $feedback->setUserAgent($userAgent);
        $feedback->setAcceptLanguage($acceptLanguage);
        $feedback->setCreatedOn($now);

        $objectManager->persist($feedback);
        $objectManager->flush();
        //Send email to 
        $config     = $this->getServiceLocator()->get('config');
        $emailQueue = $this->getServiceLocator()
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('email');

        $job = new \Application\Job\SendEmail($this->getServiceLocator());
        $job->setContent(array(
            'to'        => 'support@whosolves.com',
            'subject'   => $user->getName() . ", just posted a feedback ",
            'template'  => 'FeedBack',
            'params'    => array(
                'baseUrl'  => $config['baseUrl'],
                'reporter' => $user->getName(),
                'username' => $user->getUsername(),
                'id'       => $feedback->getId(),
                'content'  => $feedback->getContent(),
                'type'     => $feedback->getType(),
                'agent'    => $feedback->getUserAgent()
            )
        ));

        $emailQueue->push(
            $job,
            array('delay' => new \DateInterval("PT30S"))
        );

        return new JsonModel(
            array(
                'success' => true
            )
        );
    }
}

