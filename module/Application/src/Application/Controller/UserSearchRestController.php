<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Zend\Form\Annotation\AnnotationBuilder;

class UserSearchRestController extends AbstractRestfulController
{
    public function getList()
    {
        $servicelocator = $this
            ->getServiceLocator();
        $objectManager  = $servicelocator
            ->get('Doctrine\ORM\EntityManager');

        $hydrator    = $this->getServiceLocator()->get('Hydrator');

        $name        = $this->params()->fromQuery('name', NULL);
        $limit       = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 10)
        );
        $offset      = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
       );

        $currentUser = $this->identity();
        $elastica    = $this->getServiceLocator()
            ->get('Elastica\Client');

        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('user');
        $search        = new \Elastica\Search($elasticaClient);
        $boolMust      = new \Elastica\Query\Bool();
        $boolShould    = new \Elastica\Query\Bool();

        $wildcard1      = new \Elastica\Query\Wildcard();
        $wildcard1->setValue('username', '*'.$name.'*');
        $boolShould->addShould($wildcard1);

        $wildcard2      = new \Elastica\Query\Wildcard();
        $wildcard2->setValue('name', '*'.$name.'*');
        $boolShould->addShould($wildcard2);

        $boolMust->addMust($boolShould);
        $elasticaQuery = new \Elastica\Query($boolMust);

        $elasticaQuery->setFrom($offset);
        $elasticaQuery->setSize($limit);

        $resultSet = $search->addIndex($index)
            ->addType('user')
            ->search($elasticaQuery);

        $resultCount = $search->addIndex($index)
            ->addType('user')
            ->count($elasticaQuery);

        $returns = array();
        foreach ($resultSet as $result) {
            $returns[] = $result->getData();
        }

        if (empty($returns)) {
            return new JsonModel(array(
                'users' => array(
                    'list'  => array(),
                    'count' => 0
                )
            ));

        }
        $users = array();
        foreach ($returns as $return) {
            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($return['user_id']);
            if ($user instanceof User) {
                $user    = $hydrator->extract($user, array(), $currentUser);
                $users[] = $user;
            }

        }
        if (empty($users)) {
            return new JsonModel(array(
                'users' => array(
                    'list'  => array(),
                    'count' => 0
                      
                )
            ));
        }
        
        $results = array(
            'list'  => $users,
            'count' => $resultCount
        );
        return new JsonModel(array('users' => $results));
    }
}
