<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\JsonModel;

class AbstractController extends AbstractActionController
{
    public function normalize($key, $value)
    {
        switch ($key) {
        case 'limit':
            
            $value = abs($value);
            if ($value == 0) {
                $value = 10;
            }
            if ($value > 50) {
                $value = 50;
            }
            
            break;
            
        case 'offset':
            
            $value = abs($value);
            
            break;
        }
        
        return $value;
    }
}
