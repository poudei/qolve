<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\User,
    Application\Entity\Error,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Zend\Form\Annotation\AnnotationBuilder;

class ActivityRestController extends AbstractRestfulController
{
    public function getList()
    {
        if (!$this->identity() instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(array(
                'error' => Error::AuthenticationFailed_message
            ));
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $limit = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 10)
        );
        $offset = $this->normalize(
            'offset', 
            $this->params()->fromQuery('offset', 0)
        );
        
        $fromId = $this->params()->fromQuery('fromId');
        $toId   = $this->params()->fromQuery('toId');

        $byComment  = $this->params()->fromQuery('comment', true);
        $byAnswer   = $this->params()->fromQuery('answer', true);
        $byDocument = $this->params()->fromQuery('document', true);

        $user = $this->identity();

        $fromTime = NULL;
        if ($fromId) {
            $feed = $objectManager
                ->getRepository('Application\Entity\Feed')
                ->find($fromId);

            if ($feed instanceof Feed) {
                $feed = $hydrator->extract($feed);
                $fromTime = $feed['createdOn'];
            }
        }

        $toTime = NULL;
        if ($toId) {
            $feed = $objectManager
                ->getRepository('Application\Entity\Feed')
                ->find($toId);

            if ($feed instanceof Feed) {
                $feed = $hydrator->extract($feed);
                $toTime = $feed['createdOn'];
            }


        }
        
        $feeds = $objectManager
            ->getRepository('Application\Entity\Feed')
            ->getActivities(
                $user->getId(), $offset, $limit, $count, $fromTime, $toTime
            );

        $info = array(
            'asker',
            'keywords',
            'userAnswerId'
        );

        if ($byAnswer) {
            $info[] = 'answersList';
        }

        if ($byComment) {
            $info[] = 'commentsList';
        }

        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $_feeds = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($feeds as $feed) {
            $_feeds['list'][] = $hydrator->extract($feed[0], $info, $user);
        }

        return new JsonModel(array("activities" => $_feeds));
    }
}
