<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\UserPref,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

class UserPrefRestController extends AbstractRestfulController
{
    public function getList()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $currentUser = $this->identity();

        $userId = $this->getEvent()->getRouteMatch()->getParam('user_id', '');
        if (strtoupper($userId) == 'ME') {
            $userId = $currentUser->getId();
        }

        $user = $objectManager
            ->getRepository('\Application\Entity\User')
            ->find($userId);
        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(Error::UserNotFound_code);
            return new JsonModel(
                array(
                    'error' => Error::UserNotFound_message
                )
            );
        }

        similar_text($user->getId(), $currentUser->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(Error::PermissionDenied_code);
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $prefs = $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->getUserPref($user->getId());

        return new JsonModel(array("prefs" => $prefs));
    }

    public function get($key)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $config = $this
            ->getServiceLocator()
            ->get('config');

        $currentUser = $this->identity();

        $userId = $this->getEvent()->getRouteMatch()->getParam('user_id', '');
        if (strtoupper($userId) == 'ME') {
            $userId = $currentUser->getId();
        }

        $user = $objectManager
            ->getRepository('\Application\Entity\User')
            ->find($userId);
        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(Error::UserNotFound_code);
            return new JsonModel(
                array(
                    'error' => Error::UserNotFound_message
                )
            );
        }

        similar_text($user->getId(), $currentUser->getId(), $percent);
        if ($percent < 100) {
            $this->getResponse()->setStatusCode(Error::PermissionDenied_code);
            return new JsonModel(
                array(
                    'error' => Error::PermissionDenied_message
                )
            );
        }

        $pref = $objectManager
            ->getRepository('Application\Entity\UserPref')
            ->findOneBy(array(
                'userId' => $user->getId(),
                'key'    => $key
        ));

        $value = ($pref instanceof UserPref) ? $pref->getValue() : null;

//        $deduct  = $config['payment']['deduct'];
//        $deduct  = !is_null($value) ? $value - $deduct : 0;
//        $stripe  = ($deduct * 0.029) + 30;
        $payment = 0;
        if ($value > 0) {
            if (($value % 500) == 0) {
                $payment = ((($value / 500) + 1) * 30) + 10;
            }else {
                $payment = (floor($value / 500) * 30) + 40;
            }
        }

        $return = [
            $key      => $value,
            'limit'   => $config['payment']['limit'],
            'payment' => $value - $payment

        ];

        return new JsonModel($return);
    }
}
