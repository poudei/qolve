<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController;
use Application\Entity\Notification;
use Application\Entity\Error;
use Zend\View\Model\JsonModel;

class NotificationRestController extends AbstractRestfulController
{
    public function getList()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');

        $mode   = $this->params()->fromQuery('mode', 'NEW');
        $limit  = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 10)
        );
        $offset = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
        );

        $fromId     = $this->params()->fromQuery('fromId', null);
        $toId       = $this->params()->fromQuery('toId', null);
        $byComment  = $this->params()->fromQuery('comment', true);
        $byAnswer   = $this->params()->fromQuery('answer', true);
        $byDocument = $this->params()->fromQuery('document', true);


        $user = $this->identity();

        if (strtoupper($mode) == 'NEW') {
            $notifs = $objectManager
                ->getRepository('Application\Entity\Notification')
                ->getNewNotifications($user->getId(), $count, $limit);
        } else {
            $notifs = $objectManager
                ->getRepository('Application\Entity\Notification')
                ->getAllNotifications(
                    $user->getId(),
                    $count,
                    $offset,
                    $limit,
                    $fromId,
                    $toId
            );
        }

        $info = array(
            'asker',
            'keywords',
            'userAnswerId'
        );

        if ($byAnswer) {
            $info[] = 'answersList';
        }

        if ($byComment) {
            $info[] = 'commentsList';
        }

        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $_notifs = array(
            'list'  => array(),
            'count' => $count
        );

        foreach ($notifs as $notif) {
            $_notifs['list'][] = $hydrator->extract($notif, $info, $user);
        }

        return new JsonModel(array("notifications" => $_notifs));
    }

    public function update($lastId, $data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $user = $this->identity();

        $objectManager->getRepository('Application\Entity\Notification')
                ->see($user->getId(), $lastId);

        return new JsonModel(array("success" => true));
    }
}

