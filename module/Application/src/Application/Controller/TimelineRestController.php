<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

class TimelineRestController extends AbstractRestfulController
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $limit  = $this->normalize(
            'limit',
            $this->params()->fromQuery('limit', 10)
        );
        $offset = $this->normalize(
            'offset',
            $this->params()->fromQuery('offset', 0)
        );

        $byComment  = $this->params()->fromQuery('comment', true);
        $byAnswer   = $this->params()->fromQuery('answer', true);
        $byDocument = $this->params()->fromQuery('document', true);

        $currentUser = $this->identity();

        $userId = $this->getEvent()->getRouteMatch()->getParam('user_id', '');
        if ($currentUser instanceof User && strtoupper($userId) == 'ME') {
            $userId = $currentUser->getId();
        }

        $user = $objectManager
            ->getRepository('\Application\Entity\User')
            ->find($userId);
        if (!$user instanceof User) {
            $this->getResponse()->setStatusCode(Error::UserNotFound_code);
            return new JsonModel(
                array(
                    'error' => Error::UserNotFound_message
                )
            );
        }

        if ($user->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(Error::UserUnavailable_code);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }
        
        $currentUserId = ($currentUser instanceof User) 
            ? $currentUser->getId() : null;

        $feeds = $objectManager
            ->getRepository('Application\Entity\Feed')
            ->getTimeline(
                $user->getId(), $offset, $limit, $count, $currentUserId
            );

        $info = array(
            'asker',
            'keywords',
            'userAnswerId'
        );

        if ($byAnswer) {
            $info[] = 'answersList';
        }

        if ($byComment) {
            $info[] = 'commentsList';
        }

        if ($byDocument) {
            $info[] = 'documentsList';
        }

        $_feeds = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($feeds as $feed) {
            $_feeds['list'][] = $hydrator->extract(
                $feed,
                $info,
                $currentUser
            );
        }

        return new JsonModel(array("timeline" => $_feeds));
    }
}

