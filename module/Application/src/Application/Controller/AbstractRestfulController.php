<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController as ZendAbstractRestfulController,
    Zend\View\Model\JsonModel,
    Application\Entity\Error;

class AbstractRestfulController extends ZendAbstractRestfulController
{
    protected function getIdentifier($routeMatch, $request)
    {
        $identifier = $this->getIdentifierName();
        $id = $routeMatch->getParam($identifier, false);
        if ($id !== false) {
            return $this->_explodeId($id);
        }

        $id = $request->getQuery()->get($identifier, false);
        if ($id !== false) {
            return $this->_explodeId($id);
        }

        return false;
    }

    private function _explodeId($id)
    {
        if (strpos($id, ',') === false) {
            return $id;
        }

        return explode(',', $id);
    }

    public function getList()
    {
        $this->getResponse()
            ->setStatusCode(Error::NotImplemented_code);

        return new JsonModel(array('error' => Error::NotImplemented_message));
    }

    public function get($id)
    {
        $this->getResponse()
            ->setStatusCode(Error::NotImplemented_code);

        return new JsonModel(array('error' => Error::NotImplemented_message));
    }

    public function create($data)
    {
        $this->getResponse()
            ->setStatusCode(Error::NotImplemented_code);

        return new JsonModel(array('error' => Error::NotImplemented_message));
    }

    public function update($id, $data)
    {
        $this->getResponse()
            ->setStatusCode(Error::NotImplemented_code);

        return new JsonModel(array('error' => Error::NotImplemented_message));
    }

    public function delete($id)
    {
        $this->getResponse()
            ->setStatusCode(Error::NotImplemented_code);

        return new JsonModel(array('error' => Error::NotImplemented_message));
    }
    
    public function normalize($key, $value)
    {
        switch ($key) {
        case 'limit':
            
            $value = abs($value);
            if ($value == 0) {
                $value = 10;
            }
            if ($value > 50) {
                $value = 50;
            }
            
            break;
            
        case 'offset':
            
            $value = abs($value);
            
            break;
        }
        
        return $value;
    }
}
