<?php
namespace Application\Controller;

use Application\Controller\AbstractController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\UserProvider,
    Application\Entity\UserProviderContact,
    Application\Entity\Error,
    Zend\Crypt\Password\Bcrypt,
    Zend\Mvc\MvcEvent,
    Zend\Form\Annotation\AnnotationBuilder;

define('AUTH_CONFIG', dirname(__DIR__) . "/../../../../config/autoload/hybridauth.php");

class SocialController extends AbstractController
{
    public function checkemailAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        $email = isset($info['email']) ? $info['email'] : null;

        $currentUser = $this->identity();

        $return = $objectManager
            ->getRepository('Application\Entity\User')
            ->getOneByElastic($serviceLocator, 'username', $email);

        $user = array();
        if (!empty($return)) {
            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($return['user_id']);
        }
        if ($user instanceof User) {
            $percent = 0;
            if ($currentUser instanceof User) {
                similar_text($user->getId(), $currentUser->getId(), $percent);
            }
            $exists = ($percent < 100);

        } else {
            $exists = false;
        }

        return new JsonModel(array("exists" => $exists));
    }

    public function checkusernameAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');
        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        $username = isset($info['username']) ? $info['username'] : null;

        $currentUser = $this->identity();

        $return = $objectManager
            ->getRepository('Application\Entity\User')
            ->getOneByElastic($serviceLocator, 'username', $username);

        $user = array();
        if (!empty($return)) {
            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($return['user_id']);
        }

        if ($user instanceof User) {
            $percent = 0;
            if ($currentUser instanceof User) {
                similar_text($user->getId(), $currentUser->getId(), $percent);
            }
            $exists = ($percent < 100);

        } else {
            $exists = false;
        }

        return new JsonModel(array("exists" => $exists));
    }

    //for android
    public function signupAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $hydrator       = $this->getServiceLocator()->get('Hydrator');

        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        $requiredParams = array(
            'provider',
            'providerId',
            'email',
            'username',
            'password',
            'name'
        );
        foreach ($requiredParams as $param) {
            if (!isset($info[$param])) {
                
                $param = explode('_', $param);
                foreach ($param as $key => &$val) {
                    $val = ucfirst($val);
                }
                $param = join('', $param);
                
                $errorRef = new \ReflectionClass('\Application\Entity\Error');
                $error    = $param . "NotSent";
                
                $this->getResponse()->setStatusCode(
                    $errorRef->getConstant("$error"."_code")
                );
                return new JsonModel(
                    array(
                        'error' => $errorRef->getConstant("$error"."_message")
                    )
                );
            }
        }

        $return = $objectManager
            ->getRepository('Application\Entity\User')
            ->getOneByElastic(
                $serviceLocator,
                'email',
                $info['email']
        );
        $user = array();
        if (!empty($return)) {
            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($return['user_id']);
        }

        if ($user instanceof User) {
            $this->getResponse()->setStatusCode(
                Error::DuplicateEmail_code
            );
            return new JsonModel(
                array(
                    'error' =>
                        Error::DuplicateEmail_message
                    )
                );
        }

        $provider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findOneBy(array(
                'provider'   => $info['provider'],
                'providerId' => $info['providerId']
                )
        );
        if ($provider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(
                Error::DuplicateProvider_code
            );
            return new JsonModel(
                array(
                    'error' => Error::DuplicateProvider_message
                )
            );
        }

        if (!isset($info['published'])) {
            $info['published'] = UserProvider::PUBLISHED_YES;
        }

        $passCost = $this->getServiceLocator()
            ->get('ZfcUser\Authentication\Adapter\Db')
            ->getOptions()
            ->getPasswordCost();
        $bcrypt = new Bcrypt();
        $bcrypt->setCost($passCost);
        $info['password'] = $bcrypt->create($info['password']);

        $user     = new User();
        $provider = new UserProvider();
        $builder  = new AnnotationBuilder();

        $userForm = $builder->createForm($user);
        $userForm->setHydrator($hydrator);
        $userForm->bind($user);
        $userForm->setData($info);

        $providerForm = $builder->createForm($provider);
        $providerForm->setHydrator($hydrator);
        $providerForm->bind($provider);
        $providerForm->setData($info);

        if (!$userForm->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(array('error' => $userForm->getMessages()));
        }
        if (!$providerForm->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(array(
                'error' => $providerForm->getMessages())
            );
        }

        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        $user = $userForm->getData();
        $user->setStatus(1);
        $user->setDeleted(0);
        $user->setCreatedOn($now);
        $objectManager->persist($user);

        $provider = $providerForm->getData();
        $provider->setUserId($user->getId());
        $objectManager->persist($provider);

        $objectManager->flush();

        $createdOn = $user->getCreatedOn()->format('Y-m-d H:i:s');
        $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));

        //Add user to Elastic
        $elastica  = $this
            ->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];
        $item = array (
            'user_id'    => $user->getId(),
            'name'       => $user->getname(),
            'username'   => $user->getUsername(),
            'email'      => $user->getEmail(),
            'status'     => $user->getStatus(),
            'deleted'    => $user->getDeleted(),
            'created_on' => $createdOn
        );

        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('user');
        $content       = new \Elastica\Document($user->getId(), $item);

        $elasticaType->addDocument($content);
        $elasticaType->getIndex()->refresh();

        $this->getResponse()->setStatusCode(200);
        return new JsonModel(array('success' => true));
    }

    public function findfriendsAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        $provider = isset($info['provider']) ? $info['provider'] : '';
        $sIds     = isset($info['ids']) ? $info['ids'] : array();

        if (empty($sIds)) {
            //request from web
            return $this->saveprovidercontactsAction();
        }

        //request from android
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $currentUser = $this->identity();

        $offset = $this->params()->fromQuery('offset');
        $limit  = $this->params()->fromQuery('limit');

        $users = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getUsersByProviderIds(
                $provider,
                $sIds,
                $count,
                $offset,
                $limit
        );

        $_users = array(
                'list'  => array(),
                'count' => $count
                );
        foreach ($users as $user) {
            $_user = $hydrator->extract($user[0], array(), $currentUser);
            $_user['provider']   = $user['provider'];
            $_user['providerId'] = $user['providerId'];
            $_users['list'][]  = $_user;
        }

        return new JsonModel(array('users' => $_users));
    }

    public function saveprovidercontacts2Action()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hybridauth = $this->getServiceLocator()->get('HybridAuth');
        
        $currentUser = $this->identity();
        
        $provider = $this->params()->fromQuery('provider');
        if (!isset($provider)) {
            $userProvider = $objectManager
                ->getRepository('Application\Entity\UserProvider')
                ->findOneByUserId($currentUser->getId());
            if ($userProvider instanceof UserProvider) {
                $provider = $userProvider->getProvider();
            }
        }

        if (!isset($provider)) {
            $this->getResponse()->setStatusCode(Error::ProviderNotSent_code);
            return new JsonModel(
                    array(
                        'error' => Error::ProviderNotSent_message
                        )
                    );
        }

        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getUserProvider($currentUser->getId(), $provider);
        if ($userProvider instanceof UserProvider) {

            $adapter = $hybridauth->getAdapter($provider);

            $providerId = $userProvider->getProviderId();
            \Hybrid_Auth::storage()->set( "hauth_session.{$provider}.is_logged_in", 1 );

            $token = @unserialize($userProvider->getAccessToken());
            if ($token && is_array($token)) {
                \Hybrid_Auth::storage()->set( "hauth_session.{$provider}.token.access_token", $token['token'] );
                \Hybrid_Auth::storage()->set( "hauth_session.{$provider}.token.access_token_secret", $token['secret'] );
            } else {
                if (!$token) {
                    $token = $userProvider->getAccessToken();
                }
                \Hybrid_Auth::storage()->set( "hauth_session.{$provider}.token.access_token", $token );
            }

            $contacts = array();
            switch ($provider) {
            case 'facebook':
                $_contacts = $adapter->api()->api('/me/friends',array('limit' => 10, 'offset' => 130));
                foreach ($_contacts['data'] as $_contact) {
                    $contacts[] = array(
                            'identifier'  => isset($_contact['id']) ? $_contact['id'] : null,
                            'photoURL'    => isset($_contact['id']) ? "https://graph.facebook.com/" . $_contact['id']  . "/picture?width=150&height=150" : null,
                            'email'       => isset($_contact['email']) ? $_contact['email'] : null,
                            'displayName' => isset($_contact['name']) ? $_contact['name'] : null,
                            );
                }
                break;

            case 'twitter':
                $end    = false;
                $cursor = '-1';
                while(!$end) {
                    $_contacts = $adapter->api()->get(
                        'friends/list.json',
                        array(
                            'cursor' => $cursor,
                            'user_id' => $userProvider->getProviderId()
                        )
                    );
                    foreach ($_contacts->users as $_contact) {
                        $contacts[] = array(
                            'identifier'  => $_contact->id,
                            'photoURL'    => $_contact->profile_image_url,
                            'email'       => null,
                            'displayName' => $_contact->screen_name
                        );
                    }
                    
                    if (empty($_contacts->users)) {
                        $end = true;
                    } else {
                        $cursor = $_contacts->next_cursor;
                        sleep(0.001);
                    }
                }
                break;

            case 'google':
                $contacts = $hybridauth->getUserContacts();
                var_dump($contacts);exit;
                break;
            }
            
        } else {
            $adapter    = $hybridauth->authenticate($provider);
            $_contacts  = $adapter->getUserContacts();

            $contacts = array();
            foreach ($_contacts as $_contact) {
                $contacts[] = array(
                        'identifier'  => $_contact->identifier,
                        'photoURL'    => $_contact->photoURL,
                        'email'       => $_contact->email,
                        'displayName' => $_contact->displayName
                        );
            }
        }

        $identifiers = array();
        foreach ($contacts as $contact) {
            $identifiers[] = $contact['identifier'];
        }

        $regIds = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getRegisteredIds($provider, $identifiers);

        foreach ($contacts as $contact) {
            $imagePath = isset($contact['photoURL']) ? $contact['photoURL'] : null;
            $email     = isset($contact['email']) ? $contact['email'] : null;

            $userProviderContact = $objectManager
                ->getRepository('Application\Entity\UserProviderContact')
                ->getByUserProviderIdentifier(
                        $currentUser->getId(), $provider, $contact['identifier']
                        );

            if (!$userProviderContact instanceof UserProviderContact) {
                $userProviderContact = new UserProviderContact();
                $userProviderContact->setUserId($currentUser->getId());
                $userProviderContact->setProvider($provider);
                $userProviderContact->setContactProviderId($contact['identifier']);
                $userProviderContact->setName($contact['displayName']);
                $userProviderContact->setImagePath($imagePath);
                $userProviderContact->setEmail($email);

                $objectManager->persist($userProviderContact);
            }

            if (isset($regIds[$contact['identifier']])) {
                $userProviderContact->setContactId(
                        $regIds[$contact['identifier']]
                        );
            } else {
                $userProviderContact->setContactId(null);
            }
        }

        $objectManager->flush();
    }
    
    public function saveprovidercontactsAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hybridauth = $this->getServiceLocator()->get('HybridAuth');

        $currentUser = $this->identity();

        $provider = $this->params()->fromQuery('provider');
        if (!isset($provider)) {
            $userProvider = $objectManager
                ->getRepository('Application\Entity\UserProvider')
                ->findOneByUserId($currentUser->getId());
            if ($userProvider instanceof UserProvider) {
                $provider = $userProvider->getProvider();
            }
        }

        if (!isset($provider)) {
            $this->getResponse()->setStatusCode(Error::ProviderNotSent_code);
            return new JsonModel(
                array(
                    'error' => Error::ProviderNotSent_message
                )
            );
        }

        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getUserProvider($currentUser->getId(), $provider);
        if (!$userProvider instanceof UserProvider) {
            $adapter = $hybridauth->authenticate($provider);
            $profile = $adapter->getUserProfile();
            
            $userProvider = new UserProvider();
            $userProvider->setUserId($currentUser->getId());
            $userProvider->setProvider(strtolower($provider));
            $userProvider->setProviderId($profile->identifier);
            $userProvider->setPublished(UserProvider::PUBLISHED_YES);
            
            $accessToken       = $adapter->token('access_token');
            $accessTokenSecret = $adapter->token('acces_token_secret');

            $token = is_null($accessTokenSecret) 
                ? $accessToken 
                : serialize(array(
                    'token'  => $accessToken,
                    'secret' => $accessTokenSecret
                ));
            $userProvider->setAccessToken($token);
            
            $objectManager->persist($userProvider);
            $objectManager->flush();
        }

        $adapter = $hybridauth->getAdapter($provider);

        $providerId = $userProvider->getProviderId();
        \Hybrid_Auth::storage()->set( "hauth_session.{$provider}.is_logged_in", 1 );

        $token = @unserialize($userProvider->getAccessToken());
        if ($token && is_array($token)) {
            \Hybrid_Auth::storage()->set( "hauth_session.{$provider}.token.access_token", $token['token'] );
            \Hybrid_Auth::storage()->set( "hauth_session.{$provider}.token.access_token_secret", $token['secret'] );
        } else {
            if (!$token) {
                $token = $userProvider->getAccessToken();
            }
            \Hybrid_Auth::storage()->set( "hauth_session.{$provider}.token.access_token", $token );
        }

        switch ($provider) {
        case 'facebook':
            $contacts = $this->_getFacebookContacts($adapter);
            break;

        case 'twitter':
            $contacts = $this->_getTwitterContacts(
                $adapter, $userProvider->getProviderId()
            );
            break;  
        
        case 'google':
            $contacts = $this->_getGoogleContacts(
                $adapter, $userProvider->getProviderId()
            );
            break;
        }

        $identifiers = array();
        foreach ($contacts as $contact) {
            $identifiers[] = $contact['identifier'];
        }

        $regIds = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getRegisteredIds($provider, $identifiers);

        foreach ($contacts as $contact) {
            $imagePath = isset($contact['photoURL']) ? $contact['photoURL'] : null;
            $email     = isset($contact['email']) ? $contact['email'] : null;

            $userProviderContact = $objectManager
                ->getRepository('Application\Entity\UserProviderContact')
                ->getByUserProviderIdentifier(
                    $currentUser->getId(), $provider, $contact['identifier']
                );

            if (!$userProviderContact instanceof UserProviderContact) {
                $userProviderContact = new UserProviderContact();
                $userProviderContact->setUserId($currentUser->getId());
                $userProviderContact->setProvider($provider);
                $userProviderContact->setContactProviderId($contact['identifier']);
                $userProviderContact->setName($contact['displayName']);
                $userProviderContact->setImagePath($imagePath);
                $userProviderContact->setEmail($email);

                $objectManager->persist($userProviderContact);
            }

            if (isset($regIds[$contact['identifier']])) {
                $userProviderContact->setContactId(
                    $regIds[$contact['identifier']]
                );
            } else {
                $userProviderContact->setContactId(null);
            }
        }

        $objectManager->flush();
    }

    public function getprovidercontactsAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $currentUser = $this->identity();

        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findOneByUserId($currentUser->getId());
        if (!$userProvider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(
                Error::UserProviderNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserProviderNotFound_message
                )
            );
        }

        $provider = $this->params()->fromQuery('provider');
        if (!isset($provider)) {
            $provider = $userProvider->getProvider();
        }

        if (!isset($provider)) {
            $this->getResponse()->setStatusCode(Error::ProviderNotSent_code);
            return new JsonModel(
                array(
                    'error' => Error::ProviderNotSent_message
                )
            );
        }

        $contacts = $objectManager
            ->getRepository('Application\Entity\UserProviderContact')
            ->getContacts($currentUser->getId(), $provider);

        $_users = array(
            'registereds' => array(
                'list'  => array(),
                'count' => 0
            ),
            'unregistereds' => array(
                'list'  => array(),
                'count' => 0
            )
        );

        foreach ($contacts as $contact) {
            if ($contact->getContactId() != null) {
                $_contact = $objectManager
                    ->getRepository('Application\Entity\User')
                    ->find($contact->getContactId());
                if ($_contact instanceof User) {
                    $_users['registereds']['list'][] = $hydrator->extract(
                        $_contact, array(), $currentUser
                    );
                    $_users['registereds']['count']++;
                }
            } else {
                $unregContact = new User();
                $unregContact->setName($contact->getName());
                $unregContact->setImagePath($contact->getImagePath());

                $_users['unregistereds']['list'][] = $hydrator->extract(
                    $unregContact, array(), $currentUser
                );
            }
        }

        $hybridauth = $this->getServiceLocator()->get('HybridAuth');
        $adapter    = $hybridauth->authenticate($provider);

        switch ($provider) {
        case 'facebook':
            $count = $adapter->api()->api(array(
                'method' => 'fql.query',
                'query'  => 'SELECT friend_count FROM user WHERE uid =me()'
            ));
            $count =(int)$count[0]["friend_count"];
            break;

        case 'twitter':
            $count = $adapter->api()->get(
                'users/show.json',
                array(
                    'user_id' => $userProvider->getProviderId()
                )
            )->friends_count;
            break;

        case 'google':
            $count = count($contacts);
            break;
        }

        $_users['unregistereds']['count'] =
            $count - $_users['registereds']['count'];

        return new JsonModel(
            array(
                'users' => $_users
            )
        );
    }
    
    public function saveandgetprovidercontactsAction()
    {
        $this->saveprovidercontactsAction();
        return $this->getprovidercontactsAction();
    }
    
    public function addUserProviderAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(
                array(
                    'errors' => [
                        'code'    => Error::AuthenticationFailed_code,
                        'message' => Error::AuthenticationFailed_message
                    ]
                )
            );
        }
        
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        $requiredParams = array(
            'provider',
            'providerId',
            'accessToken'
        );

        if (isset($data['provider'])
            && strtolower($data['provider']) == 'twitter'
        ) {
            $requiredParams[] = 'accessTokenSecret';
        }

        foreach ($requiredParams as $reqParam) {
            if (!isset($data[$reqParam])) {

                $reqParam = explode('_', $reqParam);
                foreach ($reqParam as $key => &$val) {
                    $val = ucfirst($val);
                }
                $reqParam = join('', $reqParam);

                $errorRef = new \ReflectionClass('\Application\Entity\Error');
                $error    = $reqParam . "NotSent";

                $this->getResponse()->setStatusCode(400);
                return new JsonModel(
                    array(
                        'errors' => [
                            'code'    => $errorRef->getConstant("$error"."_code"),
                            'message' => $errorRef->getConstant("$error"."_message")
                        ]
                    )
                );

            }
        }
        
        $currentUser = $this->identity();
        
        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findOneBy(array(
                'userId'   => $currentUser->getId(),
                'provider' => strtolower($data['provider'])
            ));
        if ($userProvider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(
                array(
                    'errors' => [
                        'code'    => Error::DuplicateProvider_code,
                        'message' => Error::DuplicateProvider_message
                    ]
                )
            );
        }
        
        $userProvider = new UserProvider();
        $userProvider->setUserId($currentUser->getId());
        $userProvider->setProvider(strtolower($data['provider']));
        $userProvider->setProviderId($data['providerId']);
        $userProvider->setPublished(UserProvider::PUBLISHED_YES);
        
        switch (strtolower($data['provider'])) {
        case 'facebook':
        case 'google':
            $userProvider->setAccessToken(
                serialize($data['accessToken'])
            );
            break;

        case 'twitter':
            $userProvider->setAccessToken(
                serialize(array(
                    'token'  => $data['accessToken'],
                    'secret' => $data['accessTokenSecret']
                ))
            );
            break;
        }
        
        $objectManager->persist($userProvider);
        $objectManager->flush();
        
        return new JsonModel(
            array(
                'success' => true
            )
        );
    }

    public function finduserAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $offset = $this->params()->fromQuery('offset');
        $limit  = $this->params()->fromQuery('limit');

        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        $emails   = isset($info['email']) ? $info['email'] : array();
        $currentUser = $this->identity();

        $users = $objectManager
            ->getRepository('Application\Entity\User')
            ->getByEmails($emails, $count, $offset, $limit);
        $_users = array(
            'list'  => array(),
            'count' => $count
        );
        foreach ($users as $user) {
            $_user = $hydrator->extract(
                $user,
                array('isFollowed'),
                $currentUser
            );
            $_user['email'] = $user->getEmail();
            
            $_users['list'][] = $_user;
        }

        return new JsonModel(array('users' => $_users));
    }

    public function invitationAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $limit    = $this->params()->fromQuery('limit', 10);
        $offset   = $this->params()->fromQuery('offset', 0);
        $user     = $this->identity();

        $userId   = $user->getId();
        $data     = $this->getRequest()->getContent();
        $data     = \Zend\Json\Json::decode(
            $data,
            \Zend\Json\Json::TYPE_ARRAY
        );
        $editableProperties = array('email');

        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (!isset($data['email']))  {
            $this->getResponse()->setStatusCode(Error::EmailNotSent_code);
            return new JsonModel(array('error' => Error::EmailNotSent_message));

        }

        !is_array($data['email']) ?
            $emails = array($data['email']) :
            $emails = $data['email'];

        $emailQueue = $this
            ->getServiceLocator()
            ->get('\SlmQueue\Queue\QueuePluginManager')
            ->get('email');

        $job = new \Application\Job\SendEmail($this->getServiceLocator());
        $job->setContent(array('email' => $emails));

        $emailQueue->push(
            $job,
            array('delay' => new \DateInterval("PT30S"))
        );

        return new JsonModel(array('Success'));
    }

    public function setuserpassAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator
            ->get('Doctrine\ORM\EntityManager');

        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        $username   = $info['username'];
        $pass       = $info['password'];
        $email      = $info['email'];

        if (!$objectManager
            ->getRepository('Application\Entity\User')
            ->isValidUsername($username)) {

            $this->getResponse()->setStatusCode(Error::UsernameInvalid_code);
            return new JsonModel(
                array(
                    'error' => Error::UsernameInvalid_message
                )
            );
        }

        if (!$objectManager
             ->getRepository('Application\Entity\User')
             ->isValidPassword($pass)) {

            $this->getResponse()->setStatusCode(Error::PasswordInvalid_code);
            return new JsonModel(
                array(
                    'error' => Error::PasswordInvalid_message
                )
            );
        }

        $passCost = $this->getServiceLocator()
            ->get('ZfcUser\Authentication\Adapter\Db')
            ->getOptions()
            ->getPasswordCost();
        $bcrypt = new Bcrypt();
        $bcrypt->setCost($passCost);
        $pass = $bcrypt->create($pass);

        $currentUser = $this->identity();

        $return = $objectManager
            ->getRepository('Application\Entity\User')
            ->getOneByElastic($serviceLocator, 'username', $username);
        $user = array();
        if (!empty($return)) {
            $user = $objectManager
                ->getRepository('Application\Entity\User')
                ->find($return['user_id']);
        }

        if ($user instanceof User) {
            similar_text($user->getId(), $currentUser->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::DuplicateUsername_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::DuplicateUsername_message
                        )
                    );
            }
        }

        $currentUser->setUsername($username);
        $currentUser->setPassword($pass);
        $currentUser->setEmail($email);

        $objectManager->persist($currentUser);
        $objectManager->flush($currentUser);


        $elastica = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $elasticaIndex  = $elasticaClient->getIndex($index);
        $elasticaType   = $elasticaIndex->getType('user');
        $term = new \Elastica\Query\Term();

        $term->setParam('user_id', $currentUser->getId());

        $search        = new \Elastica\Search($elasticaClient);
        $elasticaQuery = new \Elastica\Query($term);

        $resultSet = $search->addIndex($index)
            ->addType('user')
            ->search($elasticaQuery);

        $return = array();
        foreach ($resultSet as $result) {
            $return = $result->getData();
        }

        if (!empty($return)) {
            $param = array(
                'username' => $currentUser->getUsername(),
                'name'     => $currentUser->getName()
            );
            $document = $elasticaType->getDocument($currentUser->getId());

            $document->setData($param);
            $elasticaType->updateDocument($document);
        }
        return new JsonModel(array('success' => true));
    }

    public function deleteaccountAction()
    {
        //TODO: remove this method

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
/*
        $userProvider = $objectManager->getRepository('Application\Entity\UserProvider')
                ->findOneBy(array(
                    'provider'      => 'facebook',
                    'providerId'    => '100006700602611'

//                    'provider'      => 'twitter',
//                    'providerId'    => '1939901544'
                ));
        if ($userProvider instanceof \Application\Entity\UserProvider) {
            $user = $objectManager->getRepository('Application\Entity\User')
                    ->find($userProvider->getUserId());
            if ($user instanceof User) {
                $objectManager->remove($user);
                $objectManager->remove($userProvider);
                $objectManager->flush();
            }
        }
*/


        $providers = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findAll();
        foreach ($providers as $provider) {
            $objectManager->remove($provider);
        }

        $users = $objectManager
            ->getRepository('Application\Entity\User')
            ->findByEmail('quolve@gmail.com');
        foreach ($users as $user) {
            $objectManager->remove($user);
        }

        $objectManager->flush();

        return new JsonModel(array('success' => true));
    }

    public function shareAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hybridauth = $this->getServiceLocator()->get('HybridAuth');

        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode(
            $data,
            \Zend\Json\Json::TYPE_ARRAY
        );

        $providers  = $info['providers'];
        $objectType = isset($info['object']) ? $info['object'] : 'question';
        $objectId   = $info['objectId'];

        $user = $this->identity();

        switch (strtolower($objectType)) {
        case 'question':
            $object = $objectManager
                ->getRepository('Qolve\Entity\Question')
                ->find($objectId);
            if (   $object instanceof \Qolve\Entity\Question
                && ($object->getPrivacy()
                    == \Qolve\Entity\Question::PRIVACY_PUBLIC)
            ) {
                $message = "http://qolve.com/questions/$objectId";
            }

            break;
        }

        if (isset($message)) {
            $hybridauth->initialize(AUTH_CONFIG);

            foreach ($providers as $provider) {
                $userProvider = $objectManager
                    ->getRepository('Application\Entity\UserProvider')
                    ->getUserProvider($user->getId(), $provider);

                if ($userProvider instanceof UserProvider) {
                    $accessToken = $userProvider->getAccessToken();

                    $storage = $hybridauth->storage();
                    $storage->set("hauth_session."
                        . ucfirst($provider) . ".token.access_token",
                        $accessToken
                    );
                    $storage->set("hauth_session."
                        . ucfirst($provider) . ".is_logged_in", 1
                    );

                    $adapter = $hybridauth->getAdapter($provider);
                    $adapter->setUserStatus($message);
                }
            }
        }

        return new JsonModel(array('success' => true));
    }

    public function settingsAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $data = $this->getRequest()->getContent();
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);

        if (!isset($data['provider'])) {
            $this->getResponse()->setStatusCode(Error::ProviderNotSent_code);
            return new JsonModel(
                array(
                    'error' => Error::ProviderNotSent_message
                )
            );
        }

        $user = $this->identity();

        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getUserProvider($user->getId(), $data['provider']);
        if (!$userProvider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(
                Error::UserProviderNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserProviderNotFound_message
                )
            );
        }

        $editableProperties = array(
            'published'
        );
        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (isset($data['published'])) {

            if ((int)$data['published'] == 0) {
                $userProvider->setPublished(
                    \Application\Entity\UserProvider::PUBLISHED_NO
                );
            } else {
                $userProvider->setPublished(
                    \Application\Entity\UserProvider::PUBLISHED_YES
                );
            }

            $objectManager->flush();
        }

        return new JsonModel(array('success' => true));
    }

    public function addProviderAction()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $provider = strtolower($this->params()->fromQuery('provider'));

        if (!isset($provider)) {
            $this->getResponse()->setStatusCode(Error::ProviderNotSent_code);
            return new JsonModel(
                array(
                    'error' => Error::ProviderNotSent_message
                )
            );
        }

        $user = $this->identity();

        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getUserProvider($user->getId(), $provider);
        if ($userProvider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(Error::DuplicateProvider_code);
            return new JsonModel(
                array(
                    'error' => Error::DuplicateProvider_message
                )
            );
        }

        $hybridauth = $this->getServiceLocator()->get('HybridAuth');
        $adapter    = $hybridauth->authenticate($provider);

        $profile    = $adapter->getUserProfile();
        $providerId = $profile->identifier;

        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getByProviderId($provider, $providerId);
        if ($userProvider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(Error::ProviderInvalid_code);
            return new JsonModel(
                array(
                    'error' => Error::ProviderInvalid_message
                )
            );
        }

        $accessToken = $adapter->getAccessToken();
        $accessToken = $accessToken['access_token'];

        $userProvider = new UserProvider();
        $userProvider->setUserId($user->getId());
        $userProvider->setProvider($provider);
        $userProvider->setProviderId($providerId);
        $userProvider->setPublished(UserProvider::PUBLISHED_YES);
        $userProvider->setAccessToken($accessToken);

        $objectManager->persist($userProvider);
        $objectManager->flush();

        return new JsonModel(array('success' => true));
    }

    public function testAction()
    {  
        
        $serviceLocator = $this->getServiceLocator();
                $objectManager  = $serviceLocator
                            ->get('Doctrine\ORM\EntityManager');

        $user = $objectManager->getRepository('Application\Entity\User')->findOneByEmail('qolveapp@gmail.com');
        var_dump($user);exit;
        
        
         
         $config = array(
         "base_url" => "http://qolve.com",
         "providers" => array (
         "Google" => array (
         "enabled" => true,
         "keys" => array ( "id" => "167879314210.apps.googleusercontent.com", "secret" => "-9P6-2N1iB97P0selH2n_f1G" ),
         "scope" => "https://www.googleapis.com/auth/userinfo.profile ". // optional
         "https://www.googleapis.com/auth/userinfo.email" , // optional
         "access_type" => "offline", // optional
         "approval_prompt" => "force", // optional
         "hd" => "domain.com" // optional
         )));

//         require_once( "/../../../../vendor/hybridauth/hybridauth/hybridauth/Hybrid/Auth.php" );

         $hybridauth = new \Hybrid_Auth( $config );
         $adapter = $hybridauth->authenticate( "Google" );
         var_dump("middle");exit;
         $user_profile = $adapter->getUserProfile();
         var_dump("end");exit;
         exit;

// Elastica delete //
        $id = $this->params()->fromQuery('id');
        $elastica = $this->getServiceLocator()
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];
        
        $elasticaIndex  = $elasticaClient->getIndex($index);
        $elasticaType   = $elasticaIndex->getType('user');
        $elasticaType->deleteById($id);
        $elasticaType->getIndex()->refresh();

        return new JsonModel(array('success' => true));

    }
    
    public function fortestAction()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        
        $users = $objectManager->getRepository('Application\Entity\User')
            ->findBy(array('name' => 'Pooya Khaloo'));
        
        $entitiesToDelete = array(
            'Application\Entity\UserProvider',
            'Application\Entity\UserProfile',
            'Application\Entity\UserPref',
        );
        
        foreach ($users as $user) {
            $objectManager->remove($user);
            
            foreach ($entitiesToDelete as $entity) {
                $objectManager->getRepository($entity)
                    ->deleteBy(array('userId' => $user->getId()));
            }
        }
        
        $objectManager->flush();
        
        var_dump('Done');
    }
    
    //for test
    public function removebyemailAction()
    {
        $data = $this->getRequest()->getContent();
        $info = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        $email = isset($info['email']) ? $info['email'] : 'hazhir.dabiri@yahoo.com';
        
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        
        $users = $objectManager->getRepository('Application\Entity\User')
            ->findBy(array('email' => $email));
        
        $entitiesToDelete = array(
            'Application\Entity\UserProvider',
            'Application\Entity\UserProfile',
            'Application\Entity\UserPref',
        );
        
        foreach ($users as $user) {
            foreach ($entitiesToDelete as $entity) {
                $objectManager->getRepository($entity)
                    ->deleteBy(array('userId' => $user->getId()));
            }
            
            $objectManager->remove($user);
        }
        
        $objectManager->flush();
        
        var_dump('Done');
    }
    
    private function _getFacebookContacts($adapter)
    {
        $contacts = array();

        $_contacts = $adapter->api()->api('/me/friends');
        foreach ($_contacts['data'] as $_contact) {
            $contacts[] = array(
                'identifier'  => isset($_contact['id']) ? $_contact['id'] : null,
                'photoURL'    => isset($_contact['id']) ? "https://graph.facebook.com/" . $_contact['id']  . "/picture?width=150&height=150" : null,
                'email'       => isset($_contact['email']) ? $_contact['email'] : null,
                'displayName' => isset($_contact['name']) ? $_contact['name'] : null,
            );
        }

        return $contacts;
    }
    
    private function _getTwitterContacts($adapter, $providerId)
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $contactsIds = array();

        $end    = false;
        $cursor = '-1';
        while (!$end) {
            $parameters = array(
                'cursor'  => $cursor,
                'user_id' => $providerId
            );
            $response = $adapter->api()->get( 'friends/ids.json', $parameters );

            if( !$response || !count( $response->ids ) ){
                $end = true;
            } else {
                $contactsIds = array_merge($contactsIds, $response->ids);
                $cursor = $response->next_cursor;
            }
        }

        $regIds = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->getRegisteredIds('twitter', $contactsIds);

        $unregIds = array();
        foreach ($contactsIds as $contactId) {
            if (!isset($regIds[$contactId])) {
                $unregIds[] = $contactId;
            }
        }


        $contacts = array();

        foreach ($regIds as $providerId => $userId) {
            $user = $objectManager->getRepository('Application\Entity\User')
                ->find($userId);

            $contacts[] = array(
                'identifier'  => $providerId,
                'photoURL'    => $user->getImagePath(),
                'email'       => $user->getEmail(),
                'displayName' => $user->getName(),
            );
        }

        $chunks = array_chunk ($unregIds, 75);
        foreach( $chunks as $chunk ) {
            $parameters = array( 'user_id' => implode( ",", $chunk ) );
            $response   = $adapter->api()->get( 'users/lookup.json', $parameters );

            if (!is_array($response)) {
                break;
            }

            if ($response && count( $response )) {
                foreach($response as $item) {
                    $contacts[] = array(
                        'identifier'  => (property_exists($item,'id')) ? $item->id : null,
                        'photoURL'    => (property_exists($item,'profile_image_url')) ? $item->profile_image_url : null,
                        'email'       => null,
                        'displayName' => (property_exists($item,'screen_name')) ? $item->screen_name : null,
                    );
                }
            }
        }
            
        return $contacts;
    }

    private function _getGoogleContacts($adapter, $providerId)
    {
        $contacts = array();

        $_contacts = $adapter->api()->api("https://www.googleapis.com/plus/v1/people/$providerId/people/visible")->items;

        if (!is_array($_contacts)) {
            return array();
        }

        foreach ($_contacts as $_contact) {
            $contacts[] = array(
                    'identifier'  => $_contact->id,
                    'photoURL'    => $_contact->image->url,
                    'email'       => null,
                    'displayName' => $_contact->displayName
            );
        }

        return $contacts;
    }
}
