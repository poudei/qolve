<?php
namespace Application\Controller;

use Application\Controller\AbstractController;
use Zend\View\Model\ViewModel;

use Application\Entity\User;

class IndexController extends AbstractController
{
    public function indexAction()
    {
        $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $hydrator    = $this->getServiceLocator()->get('Hydrator');
        $user        = $this->identity();
        if ($user instanceof User) {

            $user = $hydrator->extract($user);

        }
        return new ViewModel(array("user" => $user));

        $conn = $this
            ->getServiceLocator()
            ->get('doctrine.connection.orm_default');

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');


        $user = new User();
        $user->setUsername("alireza");
        $user->setPassword("123456");
        $user->setName("Alireza Meskin");
        $user->setEmail("alireza@hyperoffice.com");
        $user->setStatus(1);
        $user->setDeleted(0);
        $user->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));
        $objectManager->persist($user);
        $objectManager->flush();

        $user2 = $objectManager->find('Application\Entity\User', $user->getId());
        var_dump($user2);
        exit;

        return new ViewModel();
    }
}
