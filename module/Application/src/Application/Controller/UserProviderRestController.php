<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Application\Entity\User,
    Application\Entity\UserProvider,
    Application\Entity\Error,
    Zend\Form\Annotation\AnnotationBuilder;

class UserProviderRestController extends AbstractRestfulController
{
    public function getList()
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $userId = $this->params()->fromRoute('user_id');
        $user   = $this->identity();

        if (strtoupper($userId) != 'ME') {
            similar_text($userId, $user->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $providers = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findByUserId($user->getId());

        $_providers = array(
            'list'  => array(),
            'count' => 0
        );
        foreach ($providers as $provider) {
            $_providers['list'][] = $hydrator->extract($provider);
        }

        return new JsonModel(array("providers" => $_providers));
    }

    public function get($provider)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }
        
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $userId = $this->params()->fromRoute('user_id');
        $user   = $this->identity();

        if (strtoupper($userId) != 'ME') {
            similar_text($userId, $user->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $userProvider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findOneBy(array(
                'userId'    => $user->getId(),
                'provider'  => $provider
        ));

        return new JsonModel(array(
            'provider' => $hydrator->extract($userProvider))
        );
    }

    public function create($data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(Error::UserUnavailable_code);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $userId = $this->params()->fromRoute('user_id');
        $user   = $this->identity();

        if (strtoupper($userId) != 'ME') {
            similar_text($userId, $user->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        if (!isset($data['provider'])) {
            $this->getResponse()->setStatusCode(Error::ProviderNotSent_code);
            return new JsonModel(
                array(
                    'error' => Error::ProviderNotSent_message
                )
            );
        }
        if (array_search($data['provider'], array(
            'facebook',
            'twitter',
            'google'
        )) === false) {
            $this->getResponse()->setStatusCode(Error::NotImplemented_code);
            return new JsonModel(
                array(
                    'error' => Error::NotImplemented_message
                )
            );
        }

        $provider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findOneBy(array(
                'userId'   => $user->getId(),
                'provider' => $data['provider']
        ));
        if ($provider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(Error::DuplicateProvider_code);
            return new JsonModel(
                array(
                    'error' => Error::DuplicateProvider_message
                )
            );
        }

        if (!isset($data['published'])) {
            $data['published'] = 1;
        }

        $builder  = new AnnotationBuilder();
        $provider = new UserProvider();
        $form     = $builder->createForm($provider);
        $form->setHydrator($hydrator);
        $form->bind($provider);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $provider = $form->getData();
        $provider->setUserId($user->getId());
        $objectManager->persist($provider);
        $objectManager->flush();

        return new JsonModel(array('success' => true));
    }

    public function update($provider, $data)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }

        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(Error::UserUnavailable_code);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $userId = $this->params()->fromRoute('user_id');
        $user   = $this->identity();

        if (strtoupper($userId) != 'ME') {
            similar_text($userId, $user->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $provider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findOneBy(array(
                'userId'   => $user->getId(),
                'provider' => $provider
        ));
        if (!$provider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(
                Error::UserProviderNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserProviderNotFound_message
                )
            );
        }

        $editableProperties = array(
            'published'
        );
        foreach ($data as $name => $value) {
            if (array_search($name, $editableProperties) === false) {
                unset($data[$name]);
            }
        }

        if (isset($data['published'])) {
            if (array_search($data['published'], array(0,1)) === false) {
                $data['published'] = UserProvider::PUBLISHED_YES;
            }
        }

        $builder  = new AnnotationBuilder();
        $preData  = $hydrator->extract($provider);
        $data     = array_merge($preData, $data);
        $form     = $builder->createForm($provider);
        $form->setHydrator($hydrator);
        $form->setBindOnValidate(false);
        $form->bind($provider);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(Error::FormInvalid_code);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $form->bindValues();
        $objectManager->flush();

        return new JsonModel(array('success' => true));
    }

    public function delete($provider)
    {
        if (!$this->identity() instanceof \Application\Entity\User) {
            $this->getResponse()->setStatusCode(
                Error::AuthenticationFailed_code
            );
            return new JsonModel(
                array(
                    'error' => Error::AuthenticationFailed_message
                )
            );
        }
        
        if ($this->identity()->getDeleted() == 1) {
            $this->getResponse()->setStatusCode(Error::UserUnavailable_code);
            return new JsonModel(
                array(
                    'error' => Error::UserUnavailable_message
                )
            );
        }

        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $userId = $this->params()->fromRoute('user_id');
        $user   = $this->identity();

        if (strtoupper($userId) != 'ME') {
            similar_text($userId, $user->getId(), $percent);
            if ($percent < 100) {
                $this->getResponse()->setStatusCode(
                    Error::PermissionDenied_code
                );
                return new JsonModel(
                    array(
                        'error' => Error::PermissionDenied_message
                    )
                );
            }
        }

        $provider = $objectManager
            ->getRepository('Application\Entity\UserProvider')
            ->findOneBy(array(
                'userId'   => $user->getId(),
                'provider' => $provider
        ));
        if (!$provider instanceof UserProvider) {
            $this->getResponse()->setStatusCode(
                Error::UserProviderNotFound_code
            );
            return new JsonModel(
                array(
                    'error' => Error::UserProviderNotFound_message
                )
            );
        }

        $objectManager->remove($provider);
        $objectManager->flush();

        return new JsonModel(array('success' => true));
    }
}
