<?php
namespace Application\Entity;

class Error
{
    const AuthenticationFailed_code    = 401;
    const AuthenticationFailed_message = 'Authentication Failed';
    
    
    const FileTransferingProblem_code    = 500;
    const FileTransferingProblem_message = 'File Transfering Problem';
    
    const NotImplemented_code    = 501;
    const NotImplemented_message = 'Not Implemented';
    
    
    const EmptyData_code    = 600;
    const EmptyData_message = 'Empty Data';
    
    const ProviderNotSent_code    = 601;
    const ProviderNotSent_message = 'Bad Request: parameter provider should be sent.';
    
    const ProviderIdNotSent_code    = 602;
    const ProviderIdNotSent_message = 'Bad Request: parameter providerId should be sent.';
    
    const EmailNotSent_code    = 603;
    const EmailNotSent_message = 'Bad Request: parameter email should be sent.';
    
    const UsernameNotSent_code    = 604;
    const UsernameNotSent_message = 'Bad Request: parameter username should be sent.';
    
    const PasswordNotSent_code    = 605;
    const PasswordNotSent_message = 'Bad Request: parameter password should be sent.';
    
    const NameNotSent_code    = 606;
    const NameNotSent_message = 'Bad Request: parameter name should be sent.';
    
    const AccessTokenNotSent_code    = 607;
    const AccessTokenNotSent_message = 'Bad Request: parameter accessToken should be sent.';
    
    const ClientIdNotSent_code    = 608;
    const ClientIdNotSent_message = 'Bad Request: parameter client_id should be sent.';
    
    const ClientSecretNotSent_code    = 609;
    const ClientSecretNotSent_message = 'Bad Request: parameter client_secret should be sent.';
    
    const UserIdNotSent_code    = 610;
    const UserIdNotSent_message = 'Bad Request: parameter userId should be sent.';
    
    const QuestionIdNotSent_code    = 611;
    const QuestionIdNotSent_message = 'Bad Request: parameter questionId should be sent.';
    
    const ShareNotSent_code    = 612;
    const ShareNotSent_message = 'Bad Request: parameter share should be sent.';
    
    const CreditNotSent_code    = 613;
    const CreditNotSent_message = 'Bad Request: parameter credit should be sent.';
    
    const ActionNotSent_code    = 614;
    const ActionNotSent_message = 'Bad Request: parameter action should be sent.';
    
    const FileNotSent_code    = 615;
    const FileNotSent_message = 'Bad Request: file (doc) should be sent.';
    
    const OrderNotSent_code    = 616;
    const OrderNotSent_message = 'Bad Request: order should be sent.';

    const ChargeProblem_code    = 617;
    const ChargeProblem_message = 'Problem in charging';
 
    
    const UserNotFound_code    = 701;
    const UserNotFound_message = 'User Not Found';
    
    const UserProviderNotFound_code    = 702;
    const UserProviderNotFound_message = 'UserProvider Not Found';
    
    const QuestionNotFound_code    = 703;
    const QuestionNotFound_message = 'Question Not Found';
    
    const AnswerNotFound_code    = 704;
    const AnswerNotFound_message = 'Answer Not Found';
    
    const CommentNotFound_code    = 705;
    const CommentNotFound_message = 'Comment not found or is not for this object.';
    
    const DocumentNotFound_code    = 706;
    const DocumentNotFound_message = 'Document Not Found';
    
    const BookmarkListNotFound_code    = 707;
    const BookmarkListNotFound_message = 'BookmarkList Not Found';
    
    const KeywordNotFound_code    = 708;
    const KeywordNotFound_message = 'Keyword Not Found';
    
    const QuestionKeywordNotFound_code    = 709;
    const QuestionKeywordNotFound_message = 'QuestionKeyword Not Found';
    
    const FeedbackNotFound_code    = 710;
    const FeedbackNotFound_message = 'Feedback Not Found';
    
    const AskerNotFound_code    = 711;
    const AskerNotFound_message = 'Asker Not Found';
    
    const ShareNotFound_code    = 712;
    const ShareNotFound_message = 'Share Not Found';
    
    const VoteNotFound_code    = 713;
    const VoteNotFound_message = 'Vote Not Found';
    
    const RegisterRequestNotFound_code    = 714;
    const RegisterRequestNotFound_message = 'RegisterRequest Not Found.';
    
    const ClientNotFound_code    = 715;
    const ClientNotFound_message = 'Client Not Found';
    
    const FileNotFound_code    = 716;
    const FileNotFound_message = 'File Not Found';
    
    
    const UsernameInvalid_code    = 801;
    const UsernameInvalid_message = 'Username is Invalid';
    
    const PasswordInvalid_code    = 802;
    const PasswordInvalid_message = 'Password is Invalid';
    
    const EmailInvalid_code    = 803;
    const EmailInvalid_message = 'Email is Invalid';
    
    const ClientSecretInvalid_code    = 804;
    const ClientSecretInvalid_message = 'ClientSecret is Invalid';
    
    const SuppliedCredentialInvalid_code    = 805;
    const SuppliedCredentialInvalid_mesage  = 'Supplied Credential is Invalid.';
    
    const RequestKeyInvalid_code    = 806;
    const RequestKeyInvalid_message = 'RequestKey is Invalid';
    
    const ScoreInvalid_code    = 807;
    const ScoreInvalid_message = 'Score is Invalid';
    
    const ProviderInvalid_code    = 808;
    const ProviderInvalid_message = 'This Provider is Already Registered for Another User';
    
    const DeadlineInvalid_code    = 809;
    const DeadlineInvalid_message = 'Unacceptable Deadline Date';
    
    const FormInvalid_code   = 810;
    const FormInvalid_mesage = '';
    
    
    const DuplicateUsername_code    = 901;
    const DuplicateUsername_message = 'This Username Already Exists.';
    
    const DuplicateEmail_code    = 902;
    const DuplicateEmail_message = 'This Email Already Exists.';
    
    const DuplicateProvider_code    = 903;
    const DuplicateProvider_message = 'This Provider Already Exists.';
    
    const DuplicateSharedUser_code    = 904;
    const DuplicateSharedUser_message = 'User is already shared in this Question.';
    
    
    const Error_code    = 1000;
    const Error_message = 'some errors occured.';
    
    const UserUnavailable_code    = 1001;
    const UserUnavailable_message = 'User Unavailable';
    
    const LoginViaProvider_code    = 1002;
    const LoginViaProvider_message = 'Login Via Provider';
    
    const PasswordNotMatch_code    = 1003;
    const PasswordNotMatch_message = 'Password Not Match';
    
    const PermissionDenied_code    = 1004;
    const PermissionDenied_message = 'Permission Denied';
    
    const RequestOutOfDate_code    = 1005;
    const RequestOutOfDate_message = 'This Request is out of date.';
    
    const UserNotRegistered_code    = 1006;
    const UserNotRegistered_message = 'User not registered';
    
    const UserHasAnsweredThisQuestion_code    = 1007;
    const UserHasAnsweredThisQuestion_message = 'User has already answered this Question.';
    
    const UserHasReaskedThisQuestion_code    = 1008;
    const UserHasReaskedThisQuestion_message = 'User has already reasked this question.';
    
    const UserHasFollowedThisObject_code    = 1009;
    const UserHasFollowedThisObject_message = 'User has already followed it.';
    
    const UserHasNotFollowedThisObject_code    = 1010;
    const UserHasNotFollowedThisObject_message = 'User has not followed it.';
    
    const UserIsAsker_code    = 1011;
    const UserIsAsker_message = 'User is Asker of this Question.';
    
    const QuestionIsNotOpen_code    = 1012;
    const QuestionIsNotOpen_message = 'This question is not open.';
    
    const QuestionIsClosed_code    = 1013;
    const QuestionIsClosed_message = 'This question is closed.';
    
    const AnswerIsNotNormal_code    = 1014;
    const AnswerIsNotNormal_message = 'This answer is already chosen as Accepted or BestAnswer';
    
    const NotHaveEnoughCredit_code    = 1015;
    const NotHaveEnoughCredit_message = 'User does not have enough credit.';
    
    const CreditIsNegative_code    = 1016;
    const CreditIsNegative_message = 'Credit should be a positive number.';
    
    const CreditIsLow_code    = 1017;
    const CreditIsLow_message = 'Credit should be a greather number.';
    
    const QuestionIsNotBookmarkedInList_code    = 1018;
    const QuestionIsNotBookmarkedInList_message = 'Question is not bookmarked in this List.';
    
    const AnswerIsNotPrivate_code    = 1019;
    const AnswerIsNotPrivate_message = 'Answer is not private';
    
    const AnswerIsNotAccepted_code    = 1020;
    const AnswerIsNotAccepted_message = 'Answer is not accepted';
    
    const QuestionIsNotPaid_code    = 1021;
    const QuestionIsNotPaid_message = 'Question is not paid.';
    
    const QuestionHasAnswer_code    = 1022;
    const QuestionHasAnswer_message = 'Question has answers.';
    
    const QuestionHasAcceptedAnswer_code    = 1023;
    const QuestionHasAcceptedAnswer_message = 'Question has accepted answer.';
    
    const DocumentNotBelongsObject_code    = 1024;
    const DocumentNotBelongsObject_message = 'Document is not for this object';
    
    const NotImage_code    = 1025; 
    const NotImage_message = 'This is not an image file.';
    
    
    const InvalidUserPass_code    = 1040;
    const InvalidUserPass_message = 'Inavlid Username or Password';

    const Undefined_device_code    = 1041;
    const Undefined_device_message = 'Device type does not set: 1 for IOS 2 for Android';

    const Problem_in_tranaction_history_code = "1042";
    const Problem_in_tranaction_history_message = "The User had not paid any money!";
}

