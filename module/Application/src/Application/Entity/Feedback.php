<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation as Form;


/**
 * Feedback
 *
 * @ORM\Table(name="feedback")
 * @ORM\Entity(repositoryClass="Application\Entity\FeedbackRepository")
 * 
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="userId")
 *
 * @Form\Name("feedback")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */

class Feedback
{
    const TYPE_BUG          = 1;
    const TYPE_ENHANCEMENT  = 2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
     private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
     private $userId;
     
    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
      * 
     */
    private $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=10000, nullable=false)
     * 
     */
    private $content;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="rank", type="smallint", nullable=false)
     * 
     * @Form\Exclude()
     */
    private $rank;
    
    /**
     * @var string
     *
     * @ORM\Column(name="client_ip", type="string", length=30, nullable=true)
     * 
     * @Form\Exclude()
     */
    private $clientIp;
    
    /**
     * @var string
     *
     * @ORM\Column(name="user_agent", type="string", length=255, nullable=true)
     * 
     * @Form\Exclude()
     */
    private $userAgent;
    
    /**
     * @var string
     *
     * @ORM\Column(name="accept_language", type="string", length=30, nullable=true)
     * 
     * @Form\Exclude()
     */
    private $acceptLanguage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     *
     * @Form\Exclude()
     */
    private $createdOn;

    
    /**
     * Set id
     *
     * @param integer $id
     * @return Feedback
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Feedback
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * Set type
     *
     * @param integer $type
     * @return Feedback
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set content
     *
     * @param string $content
     * @return Feedback
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Set rank
     *
     * @param integer $rank
     * @return Feedback
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }
    
    /**
     * Set clientIp
     *
     * @param string $clientIp
     * @return Feedback
     */
    public function setClientIp($clientIp)
    {
        $this->clientIp = $clientIp;

        return $this;
    }

    /**
     * Get clientIp
     *
     * @return string
     */
    public function getClientIp()
    {
        return $this->clientIp;
    }
    
    /**
     * Set userAgent
     *
     * @param string $userAgent
     * @return Feedback
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }
    
    /**
     * Set acceptLanguage
     *
     * @param string $acceptLanguage
     * @return Feedback
     */
    public function setAcceptLanguage($acceptLanguage)
    {
        $this->acceptLanguage = $acceptLanguage;

        return $this;
    }

    /**
     * Get acceptLanguage
     *
     * @return string
     */
    public function getAcceptLanguage()
    {
        return $this->acceptLanguage;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return Feedback
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}
