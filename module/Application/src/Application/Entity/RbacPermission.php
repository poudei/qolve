<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * RbacPermission
 *
 * @ORM\Table(name="rbac_permission")
 * @ORM\Entity
 */
class RbacPermission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="perm_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $permId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="perm_name", type="string", length=32, nullable=false, unique=true)
     */
    private $permName;
    
    
    /**
     * set permId
     *
     * @param integer $permId
     * @access public
     * @return RbacRole
     */
    public function setPermId($permId)
    {
        $this->permId = $permId;

        return $this;
    }

    /**
     * Get permId
     *
     * @return integer
     */
    public function getPermId()
    {
        return $this->permId;
    }
    
    /**
     * Set permName
     *
     * @param  string   $permName
     * @return RbacRole
     */
    public function setPermName($permName)
    {
        $this->permName = $permName;

        return $this;
    }

    /**
     * Get permName
     *
     * @return string
     */
    public function getPermName()
    {
        return $this->permName;
    }
}
