<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository,
    Doctrine\ORM\Query\ResultSetMapping,
    Doctrine\ORM\Query\ResultSetMappingBuilder,
    Application\Entity\UserProvider;

class UserProviderRepository extends EntityRepository
{
    public function getUserProviders($userId)
    {
        $return  = array();

        $query = $this->getEntityManager()->createQuery(
            "SELECT p.provider, p.published
                FROM  Application\Entity\UserProvider p
                WHERE p.userId = '$userId'"
        );
        $query->useResultCache(true, LIFE_TIME,
            'user_'. $userId . '_providers'
        );
        $results = $query->getResult();
        foreach ($results as $result) {
            $return[$result['provider']] = $result['published'];
        }
        return $return;
    }
    
    public function getUserProvider($userId, $provider = null)
    {
        if ($provider != null) {
            $provider = strtolower($provider);
            
            $query = $this->getEntityManager()->createQuery(
                "SELECT p FROM Application\Entity\UserProvider p 
                    WHERE p.userId = '$userId' 
                        AND p.provider = '$provider'"
            );
            $query->useResultCache(
                true, LIFE_TIME, 'user_' . $userId . '_provider_' . $provider
            );
        } else {
            $query = $this->getEntityManager()->createQuery(
                "SELECT p FROM Application\Entity\UserProvider p 
                    WHERE p.userId = '$userId'"
            );
            $query->setMaxResults(1);
            $query->useResultCache(
                true, LIFE_TIME, 'user_' . $userId . '_provider'
            );
        }
        
        $results = $query->getResult();
        
        return isset($results[0]) ? $results[0] : null;
    }
    
    public function getRegisteredIds($provider, $identifiers)
    {
        if (empty($identifiers)) {
            return array();
        }

        foreach ($identifiers as &$identifier) {
            $identifier = "'$identifier'";
        }
        $_ids = join(',', $identifiers);

        $query = $this->getEntityManager()->createQuery(
            "SELECT p.providerId, p.userId 
                FROM Application\Entity\UserProvider p 
                JOIN Application\Entity\User u
                WITH p.userId = u.id
                    WHERE p.provider = '$provider' 
                        AND p.providerId IN ($_ids)
                        AND u.username IS NOT NULL"
        );

        $results = $query->getResult();

        $regIds = array();
        foreach ($results as $result) {
            $regIds[$result['providerId']] = $result['userId'];
        }

        return $regIds;
    }
    
    public function getByProviderId($provider, $providerId)
    {
        $provider = strtolower($provider);
            
        $query = $this->getEntityManager()->createQuery(
            "SELECT p FROM Application\Entity\UserProvider p 
                WHERE p.provider = '$provider' 
                    AND p.providerId = '$providerId'"
        );
        $query->useResultCache(
            true, LIFE_TIME, 'userProvider_' . $provider 
                . '_providerId_' . $providerId
        );
        
        $results = $query->getResult();
        
        return isset($results[0]) ? $results[0] : null;
    }
    
    public function getUsersByProviderIds($provider, $ids, &$count,
        $offset = 0, $limit = null
    ) {
        if (empty($ids)) {
            $count = 0;
            return array();
        }

        $_ids = array_map(function($id) {return $id = "'$id'";}, $ids);
        $_ids = join(',', $_ids);

        if (count($ids) == 1) {
            $query = $this->getEntityManager()->createQuery("
                SELECT user, userp.provider, userp.providerId
                    FROM \Application\Entity\UserProvider userp
                    JOIN \Application\Entity\User user
                    WITH userp.userId = user.id
                    WHERE userp.provider = '$provider'
                    AND userp.providerId = $_ids
                    ORDER BY user.name ASC"
            );

            $countQuery = $this->getEntityManager()->createQuery("
                SELECT COUNT(userp) num
                    FROM \Application\Entity\UserProvider userp
                    WHERE userp.provider = '$provider'
                    AND userp.providerId = $_ids"
            );
        } else {
            $query = $this->getEntityManager()->createQuery("
                SELECT user, userp.provider, userp.providerId
                    FROM \Application\Entity\UserProvider userp
                    JOIN \Application\Entity\User user
                    WITH userp.userId = user.id
                    WHERE userp.provider = '$provider'
                    AND userp.providerId IN ($_ids)
                    ORDER BY user.name ASC"
            );

            $countQuery = $this->getEntityManager()->createQuery("
                SELECT COUNT(userp) num
                    FROM \Application\Entity\UserProvider userp
                    WHERE userp.provider = '$provider'
                    AND userp.providerId IN ($_ids)"
            );
        }

        if ($limit != null) {
            $query->setFirstResult($offset)
                ->setMaxResults($limit);
        }

        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $query->getResult();
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "p.$key IS NULL";
            } else {
                $conditions[] = "p.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Application\Entity\UserProvider p
                WHERE $conditions"
        );
        return $query->execute();
    }
}
