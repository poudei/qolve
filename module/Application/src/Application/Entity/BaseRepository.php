<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class BaseRepository extends EntityRepository
{
    public function findByIds($ids)
    {
        $return = array();
        foreach ($ids as $id) {
            $return[] = $this->find($id);
        }

        return $return;
    }
}
