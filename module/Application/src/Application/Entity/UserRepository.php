<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository,
    Doctrine\ORM\Query\ResultSetMapping,
    Doctrine\ORM\Query\ResultSetMappingBuilder,
    Application\Entity\User;

class UserRepository extends EntityRepository
{
    public function getList()
    {
        $objectManager = $this
            ->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager');

        $userName = $this->getEvent()->getRouteMatch()->getParam('id');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $userRepo = $objectManager->getRepository('Application\Entity\User');

        $userRepo->getUserList($userName);
    }

    public function getUserInfo($userId, $properties = null)
    {
        if ($properties === null) {
            $properties = array(
                'id',
                'roleId',
                'contactId',
                'email',
                'username',
                'name',
                'status',
                'imagePath',
                'createdBy',
                'createdOn',
                'modifiedBy',
                'modifiedOn'
            );
        }

        $multiProp = true;
        if (is_string($properties)) {
            $properties = array($properties);
            $multiProp = false;
        }

        if (empty($properties)) {
            return array();
        }

        $query = "SELECT ";
        if (count($properties) == 0) {
            $query .= "user ";
        } else {
            foreach ($properties as $property) {
                $query .= "user.$property, ";
            }
            $query = substr($query, 0, strlen($query) - 2) . " ";
        }

        $query .= "FROM \Application\Entity\User user
                    WHERE user.id = '$userId'";

        $query  = $this->getEntityManager()->createQuery($query);
        $result = $query->getResult();

        if (!$multiProp) {
            return $result[0][$properties[0]];
        }
        return $result[0];
    }

    public function getUsersByFilter($filter, $orderBy,
        $offset = 0, $limit = 20
    ) {
        $userRepo = $this->getEntityManager()
            ->getRepository('Application\Entity\User');

        $query = $userRepo->createQueryBuilder('user')->where("1=1");

        foreach ($filter as $property => $value) {
            switch ($property) {
            case 'name':
                $query->andWhere("LOWER(user.name) LIKE LOWER('%$value%')");
                break;

            default:
                $query->andWhere("user.$property = '$value'");
            }
        }

        foreach ($orderBy as $property => $order) {
            $query->addOrderBy("user.$property", $order);
        }

        $query->setFirstResult($offset)
                ->setMaxResults($limit);

        return $query->getQuery()->getResult();
    }

    public function getByName($name, $order, $offset, $limit)
    {
        $_order = array();
        foreach ($order as $by => $dir) {
            $_order[] = "user.$by $dir";
        }

        $query = "SELECT user
            FROM \Application\Entity\User user
            WHERE LOWER(user.name) LIKE LOWER('$name%')";

        if (!empty($_order)) {
            $_order = join(',', $_order);
            $query .= "ORDER BY $_order";
        }

        $query = $this->getEntityManager()
            ->createQuery($query)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $query->getResult();
    }
    
    public function getByUsername($username)
    {
        $username = strtolower($username);
        
        $query = $this->getEntityManager()->createQuery(
            "SELECT u FROM \Application\Entity\User u 
                WHERE LOWER(u.username) = '$username'"
        );
        $query->setMaxResults(1);
        $query->useResultCache(true, LIFE_TIME, 'user_username_' . $username);
        
        $result = $query->getResult();
        
        return isset($result[0]) ? $result[0] : null;
    }
    
    public function getByEmail($email)
    {
        $email = strtolower($email);
        
        $query = $this->getEntityManager()->createQuery(
            "SELECT u FROM \Application\Entity\User u 
                WHERE LOWER(u.email) = '$email'"
        );
        $query->setMaxResults(1);
        $query->useResultCache(true, LIFE_TIME, 'user_email_' . $email);
        
        $result = $query->getResult();
        
        return isset($result[0]) ? $result[0] : null;
    }

    public function countBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $property => $value) {
            if ($value === null) {
                $conditions[] = "user.$property IS NULL";
            } else {
                if ($property == 'name') {
                    $conditions[] = "LOWER(user.name) LIKE LOWER('%$value%')";
                } else {
                    $conditions[] = "user.$property = '$value'";
                }
            }
        }

        $queryStr = "SELECT COUNT(user) num
            FROM \Application\Entity\User user ";
        if (!empty($conditions)) {
            $queryStr .= "WHERE " . join(' AND ', $conditions);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);
        $result = $query->getResult();
        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function getSimilarUsers($name, $offset = 0, $limit = 10)
    {
        //TODO : DO it by Doctrine !!!
        $conn = pg_pconnect("host=localhost port=5432 dbname=whosolves
            user=whosolves password=LR9ZwE6Jx7jsQtX"
        );
        $result = pg_query($conn, "SELECT id,
            similarity(username, '$name') AS sim1,
            similarity(name, '$name') AS sim2,
            similarity(email, '$name') AS sim3
            FROM \"user\"
            WHERE deleted != 1
            ORDER BY sim1 desc, sim2 desc, sim3 desc"
        );
        while ($row = pg_fetch_assoc($result)) {
           if (   ($row['sim1'] > 0)
               || ($row['sim2'] > 0)
               || ($row['sim3'] > 0)
           ) {

                $results[] = $row['id'];
           }
        }

        empty($results) ? $results = array() : '';
        return $results;
    }

    public function isValidUsername($username)
    {

        if (preg_match('/^[a-zA-Z]|[a-zA-Z][A-Za-z0-9_\.]{1,32}$/', $username)) {
            return true;
        }
        return false;
    }

    public function getOneByElastic($serviceLocator, $type, $value)
    {

        //Elastic search
        $elastica = $serviceLocator
            ->get('Elastica\Client');
        $elasticaClient = $elastica['client'];
        $index          = $elastica['index'];

        $elasticaQuery = new \Elastica\Query();
        $search        = new \Elastica\Search($elasticaClient);
        $elasticaIndex = $elasticaClient->getIndex($index);
        $elasticaType  = $elasticaIndex->getType('user');

        $boolMust      = new \Elastica\Query\Bool();
        $term          = new \Elastica\Query\Term();

        $term->setParam($type, $value);
        $boolMust->addMust($term);

        $elasticaQuery = new \Elastica\Query($boolMust);

        $resultSet = $search
            ->addIndex($index)
            ->addType('user')
            ->search($elasticaQuery);

        $return = array();
        foreach ($resultSet as $result) {
            $return = $result->getData();
        }

        return $return;
    }
    
    public function getByEmails($emails, &$count, $offset = 0, $limit = null) 
    {
        if (empty($emails)) {
            $count = 0;
            return array();
        }

        $_emails = array_map(
            function($email) {return $email = "'$email'";}, $emails
        );
        $_emails = join(',', $_emails);

        if (count($emails) == 1) {
            $query = $this->getEntityManager()->createQuery("
                SELECT user
                    FROM \Application\Entity\User user
                    WHERE user.email = $_emails
                    ORDER BY user.name ASC"
            );

            $countQuery = $this->getEntityManager()->createQuery("
                SELECT COUNT(user) num
                    FROM \Application\Entity\User user
                    WHERE user.email = $_emails"
            );
        } else {
            $query = $this->getEntityManager()->createQuery("
                SELECT user
                    FROM \Application\Entity\User user
                    WHERE user.email IN ($_emails)
                    ORDER BY user.name ASC"
            );

            $countQuery = $this->getEntityManager()->createQuery("
                SELECT COUNT(user) num
                    FROM \Application\Entity\User user
                    WHERE user.email IN ($_emails)"
            );
        }

        if ($limit != null) {
            $query->setFirstResult($offset)
                    ->setMaxResults($limit);
        }

        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $query->getResult();
    }


    //TODO
    public function isValidPassword($password)
    {
        return true;
    }
}
