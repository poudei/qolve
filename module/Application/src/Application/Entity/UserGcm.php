<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation ;

/**
 * UserGcm
 *
 * @ORM\Table(name="user_gcm")
 * @ORM\Entity
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="userGcm", lifetime="3600")
 *
 * @Annotation\Name("userGcm")
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class UserGcm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false, unique=false)
     * @ORM\Id
     *
     * @Annotation\Exclude()
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="gcm_regid", type="string", length=255,nullable=false, unique=false)
     */
    private $gcmRegid;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false, unique=false)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=true, unique=false)
     */
    private $createdOn;


    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set gcmRegid
     *
     * @param string $gcmRegid
     * @return UserGcm
     */
    public function setGcmRegid($gcmRegid)
    {
        $this->gcmRegid = $gcmRegid;
    
        return $this;
    }

    /**
     * Get gcmRegid
     *
     * @return string 
     */
    public function getGcmRegid()
    {
        return $this->gcmRegid;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return UserGcm
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return UserGcm
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    
        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}
