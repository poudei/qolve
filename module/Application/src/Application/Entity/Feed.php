<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation as Form;


/**
 * Feed
 *
 * @ORM\Table(name="feed")
 * @ORM\Entity(repositoryClass="Application\Entity\FeedRepository")
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="feed", lifetime="3600")
 *
 * @Form\Name("feed")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */

class Feed
{
    const PRIVACY_PUBLIC  = 1;
    const PRIVACY_PRIVATE = 2;

     /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
     private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     *
     * @Form\Exclude()
     */
     private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="actor_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
     private $actorId;

   /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
     private $answerId;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=1000, nullable=true)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Exclude()
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="privacy", type="smallint", nullable=false)
     * @Form\Exclude()
     */
    private $privacy;

    /**
     * @var string
     *
     * @ORM\Column(name="verb", type="string", length=20, nullable=false)
     *
     * @Form\Exclude()
     */
    private $verb;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     *
     * @Form\Exclude()
     */
    private $createdOn;


    /**
     * Set id
     *
     * @param integer $id
     * @return Feed
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Feed
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set actorId
     *
     * @param integer $actorId
     * @return Feed
     */
    public function setActorId($actorId)
    {
        $this->actorId = $actorId;

        return $this;
    }

    /**
     * Get actorId
     *
     * @return integer
     */
    public function getActorId()
    {
        return $this->actorId;
    }
    /**
     * Set questionId
     *
     * @param integer $questionId
     * @return Feed
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set answerId
     *
     * @param integer $answerId
     * @return Feed
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

   /**
     * Set content
     *
     * @param string $content
     * @return Feed
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set privacy
     *
     * @param integer $privacy
     * @return Question
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return integer
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set verb
     *
     * @param string $verb
     * @return Feed
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;

        return $this;
    }

    /**
     * Get verb
     *
     * @return string
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return Feed
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}
