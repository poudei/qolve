<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Preference
 *
 * @ORM\Table(name="preference")
 * @ORM\Entity
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="preference", lifetime="3600")
 */
class Preference
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=1024, precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $value;


    /**
     * Set id
     *
     * @param integer $id
     * @return Preference
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Preference
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Preference
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
