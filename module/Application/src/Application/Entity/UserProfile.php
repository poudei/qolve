<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * UserProfile
 *
 * @ORM\Table(name="user_profile")
 * @ORM\Entity(repositoryClass="Application\Entity\UserProfileRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="userProfile", lifetime="3600")
 *
 * @Form\Name("userProfile")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class UserProfile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="occupation", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     *
     * @Attributes({"type":"text"})
     * @Form\Filter({"name" : "htmlentities" })

     * @Form\Required(false)
     */
    private $occupation;
    
    /**
     * @var string
     *
     * @ORM\Column(name="school", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name" : "htmlentities" })
     * @Attributes({"type":"text"})
     * 
     * @Form\Required(false)
     */
    private $school;
    
    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=1000, precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name" : "htmlentities" })
     *
     * @Attributes({"type":"text"})
     * 
     * @Form\Required(false)
     */
    private $location;
    
    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name" : "htmlentities" })
     *
     * @Attributes({"type":"text"})
     * 
     * @Form\Required(false)
     */
    private $timezone;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Required(false)
     * 
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Exclude()
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_by", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Exclude()
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_on", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Exclude()
     */
    private $modifiedOn;

    
    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserProfile
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * Set occupation
     *
     * @param string $occupation
     * @return UserProfile
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get occupation
     *
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }
    
    /**
     * Set school
     *
     * @param string $school
     * @return UserProfile
     */
    public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return string
     */
    public function getSchool()
    {
        return $this->school;
    }
    
    /**
     * Set location
     *
     * @param string $location
     * @return UserProfile
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
    
    /**
     * Set timezone
     *
     * @param string $timezone
     * @return UserProfile
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return UserProfile
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
    
    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return UserProfile
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     * @return UserProfile
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set modifiedOn
     *
     * @param \DateTime $modifiedOn
     * @return UserProfile
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modifiedOn = $modifiedOn;

        return $this;
    }

    /**
     * Get modifiedOn
     *
     * @return \DateTime
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * clearCache
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        $cache->delete('user_' . $this->getUserId() . '_profile');
    }
}
