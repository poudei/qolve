<?php
namespace Application\Entity;


use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * UserPref
 *
 * @ORM\Table(name="user_pref")
 * @ORM\Entity(repositoryClass="Application\Entity\UserPrefRepository")
 * @ORM\HasLifecycleCallbacks
 * 
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="userPref", lifetime="3600")
 *
 * @Form\Name("userPref")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class UserPref
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     * 
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=50, nullable=false)
     * @ORM\Id
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=50, nullable=true)
     * 
     * @Form\Required(false)
     */
    private $value;

    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserProvider
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return UserPref
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return UserPref
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * clearCache
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        $cache->delete('user_' . $this->getUserId() . '_prefs');
    }
}
