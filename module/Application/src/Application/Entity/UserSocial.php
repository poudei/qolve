<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation ;

/**
 * UserSocial
 *
 * @ORM\Table(name="user_social")
 * @ORM\Entity(repositoryClass="Application\Entity\UserSocialRepository")
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="userSocial", lifetime="3600")
 *
 * @Annotation\Name("userSocial")
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class UserSocial
{
    const PUBLISHED_NO  = 0;
    const PUBLISHED_YES = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false, unique=false)
     * @ORM\Id
     *
     * @Annotation\Exclude()
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="social", type="string", length=50, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $social;

    /**
     * @var string
     *
     * @ORM\Column(name="social_id", type="string", length=30, nullable=false, unique=false)
     */
    private $socialId;

    /**
     * @var integer
     *
     * @ORM\Column(name="published", type="smallint", nullable=false, unique=false)
     */
    private $published;


    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserSocial
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set social
     *
     * @param string $social
     * @return UserSocial
     */
    public function setSocial($social)
    {
        $this->social = $social;

        return $this;
    }

    /**
     * Get social
     *
     * @return string
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * Set socialId
     *
     * @param integer $socialId
     * @return UserSocial
     */
    public function setSocialId($socialId)
    {
        $this->socialId = $socialId;

        return $this;
    }

    /**
     * Get socialId
     *
     * @return integer
     */
    public function getSocialId()
    {
        return $this->socialId;
    }

    /**
     * Set published
     *
     * @param integer $published
     * @return UserSocial
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return integer
     */
    public function getPublished()
    {
        return $this->published;
    }
}
