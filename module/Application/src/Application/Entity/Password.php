<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation as Form;


/**
 * Password
 *
 * @ORM\Table(name="password")
 * @ORM\Entity
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="password", lifetime="3600")
 * @Form\Name("password")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")

 */
class Password
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="request_key", type="string", length=100, nullable=false)
     * @Form\Exclude()
     */
    private $requestKey;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="request_time", type="datetime", nullable=false)
     * @Form\Exclude()
     */
    private $requestTime;

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Password
     */
    public function SetUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set requestKey
     *
     * @param string $requestKey
     * @return Password
     */
    public function setRequestKey($requestKey)
    {
        $this->requestKey = $requestKey;

        return $this;
    }

    /**
     * Get requestKey
     *
     * @return string
     */
    public function getRequestKey()
    {
        return $this->requestKey;
    }

    /**
     * Set requestTime
     *
     * @param \DateTime $requestTime
     * @return Password
     */
    public function setRequestTime($requestTime)
    {
        $this->requestTime = $requestTime;

        return $this;
    }

    /**
     * Get requestTime
     *
     * @return \DateTime
     */
    public function getRequestTime()
    {
        return $this->requestTime;
    }
}
