<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository,
    Application\Entity\UserPref;

class UserPrefRepository extends EntityRepository
{
    public function getUserKey($userId, $key)
    {
        $pref = $this->getUserPref($userId, $key);

        if (!$pref instanceof UserPref) {
            return null;
        }
        return $pref->getValue();
    }

    public function getUserPref($userId, $key = null)
    {
        $validKeys = array(
            'credit',
            'followersCount',
            'userFollowingsCount',
            'questionFollowingsCount',
            'timezone',
            'defaultBookmarkListId'
        );
        if ($key != null && array_search($key, $validKeys) === false) {
            return null;
        }

        if (is_string($key)) {
            $pref = $this->find(
                array(
                    'userId' => $userId,
                    'key'    => $key
                )
            );

            if (!$pref instanceof UserPref) {
                $pref = new UserPref();
                $pref->setUserId($userId);
                $pref->setKey($key);
            }

            return $pref;
        }

        $query = $this->getEntityManager()->createQuery(
            "SELECT p FROM \Application\Entity\UserPref p
                WHERE p.userId = '$userId'"
        );
        $query->useResultCache(true, LIFE_TIME, 'user_'. $userId . '_prefs');

        $prefs = $query->getResult();

        $_prefs = array();
        foreach ($prefs as $pref) {
            $_prefs[$pref->getKey()] = $pref->getValue();
        }

        return $_prefs;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "p.$key IS NULL";
            } else {
                $conditions[] = "p.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Application\Entity\UserPref p
                WHERE $conditions"
        );
        return $query->execute();
    }
}
