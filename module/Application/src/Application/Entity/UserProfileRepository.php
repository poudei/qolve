<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository,
    Application\Entity\UserProfile;

class UserProfileRepository extends EntityRepository
{
    public function getUserProfile($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT p FROM \Application\Entity\UserProfile p
                WHERE p.userId = '$userId'"
        );
        $query->useResultCache(true, LIFE_TIME,
            'user_'. $userId . '_profile'
        );

        $result = $query->getResult();
        return isset($result[0]) ? $result[0] : null;
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "p.$key IS NULL";
            } else {
                $conditions[] = "p.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Application\Entity\UserProfile p
                WHERE $conditions"
        );
        return $query->execute();
    }
}
