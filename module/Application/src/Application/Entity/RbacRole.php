<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * RbacRole
 *
 * @ORM\Table(name="rbac_role")
 * @ORM\Entity
 */
class RbacRole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $roleId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="parent_role_id", type="bigint", nullable=true)
     *
     * @Form\required(false)
     */
    private $parentRoleId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="role_name", type="string", length=32, nullable=false, unique=true)
     */
    private $roleName;
    
    
    /**
     * set roleId
     *
     * @param integer $roleId
     * @access public
     * @return RbacRole
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * Get roleId
     *
     * @return integer
     */
    public function getRoleId()
    {
        return $this->roleId;
    }
    
    /**
     * set parentRoleId
     *
     * @param integer $parentRoleId
     * @access public
     * @return RbacRole
     */
    public function setParentRoleId($parentRoleId)
    {
        $this->parentRoleId = $parentRoleId;

        return $this;
    }

    /**
     * Get parentRoleId
     *
     * @return integer
     */
    public function getParentRoleId()
    {
        return $this->parentRoleId;
    }
    
    /**
     * Set roleName
     *
     * @param  string   $roleName
     * @return RbacRole
     */
    public function setRoleName($roleName)
    {
        $this->roleName = $roleName;

        return $this;
    }

    /**
     * Get roleName
     *
     * @return string
     */
    public function getRoleName()
    {
        return $this->roleName;
    }
}
