<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class UserSocialRepository extends EntityRepository
{
    public function findUsersBySocialIds($social, $ids, &$count,
        $offset = 0, $limit = null
    ) {
        if (empty($ids)) {
            $count = 0;
            return array();
        }

        $_ids = array_map(function($id) {return $id = "'$id'";}, $ids);
        $_ids = join(',', $_ids);

        if (count($ids) == 1) {
            $query = $this->getEntityManager()->createQuery("
                SELECT user, socialuser.social, socialuser.socialId
                    FROM \Application\Entity\UserSocial socialuser
                    JOIN \Application\Entity\User user
                    WITH socialuser.userId = user.id
                    WHERE socialuser.social = '$social'
                    AND socialuser.socialId = $_ids
                    ORDER BY user.name ASC"
            );

            $countQuery = $this->getEntityManager()->createQuery("
                SELECT COUNT(socialuser) num
                    FROM \Application\Entity\UserSocial socialuser
                    WHERE socialuser.social = '$social'
                    AND socialuser.socialId = $_ids"
            );
        } else {
            $query = $this->getEntityManager()->createQuery("
                SELECT user, socialuser.social, socialuser.socialId
                    FROM \Application\Entity\UserSocial socialuser
                    JOIN \Application\Entity\User user
                    WITH socialuser.userId = user.id
                    WHERE socialuser.social = '$social'
                    AND socialuser.socialId IN ($_ids)
                    ORDER BY user.name ASC"
            );

            $countQuery = $this->getEntityManager()->createQuery("
                SELECT COUNT(socialuser) num
                    FROM \Application\Entity\UserSocial socialuser
                    WHERE socialuser.social = '$social'
                    AND socialuser.socialId IN ($_ids)"
            );
        }

        if ($limit != null) {
            $query->setFirstResult($offset)
                ->setMaxResults($limit);
        }

        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $query->getResult();
    }

    public function findUserByEmail($emails, &$count,
        $offset = 0, $limit = null
    ) {
        if (empty($emails)) {
            $count = 0;
            return array();
        }

        $_emails = array_map(
            function($email) {return $email = "'$email'";}, $emails
        );
        $_emails = join(',', $_emails);

        if (count($emails) == 1) {
            $query = $this->getEntityManager()->createQuery("
                SELECT user
                    FROM \Application\Entity\User user
                    WHERE user.email = $_emails
                    ORDER BY user.name ASC"
            );

            $countQuery = $this->getEntityManager()->createQuery("
                SELECT COUNT(user) num
                    FROM \Application\Entity\User user
                    WHERE user.email = $_emails"
            );
        } else {
            $query = $this->getEntityManager()->createQuery("
                SELECT user
                    FROM \Application\Entity\User user
                    WHERE user.email IN ($_emails)
                    ORDER BY user.name ASC"
            );

            $countQuery = $this->getEntityManager()->createQuery("
                SELECT COUNT(user) num
                    FROM \Application\Entity\User user
                    WHERE user.email IN ($_emails)"
            );
        }

        if ($limit != null) {
            $query->setFirstResult($offset)
                    ->setMaxResults($limit);
        }

        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $query->getResult();
    }
}
