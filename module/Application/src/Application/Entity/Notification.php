<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Zend\Form\Annotation as Form;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="Application\Entity\NotificationRepository")
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="fromId")
 * @Application\ORM\Cacheable\Cacheable(type="notification", lifetime="3600")

 * @Form\Name("notification")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Notification
{
    const SEEN_NO  = 0;
    const SEEN_YES = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="to_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $toId;

    /**
     * @var string
     *
     * @ORM\Column(name="`to`", type="string", length=255, nullable=true)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Exclude()
     */
    private $to;


    /**
     * @var integer
     *
     * @ORM\Column(name="from_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $fromId;

    /**
     * @var integer
     *
     * @ORM\Column(name="question_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $questionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="answer_id", type="bigint", nullable=true)
     *
     * @Form\Exclude()
     */
    private $answerId;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=1000, nullable=true)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Exclude()
     */
    private $content;


    /**
     * @var string
     *
     * @ORM\Column(name="verb", type="string", length=255, nullable=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Exclude()
     */
    private $verb;

    /**
     * @var integer
     *
     * @ORM\Column(name="seen", type="smallint", nullable=false)
     *
     * @Form\Exclude()
     */
    private $seen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     *
     * @Form\Exclude()
     */
    private $createdOn;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Notification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set toId
     *
     * @param integer $toId
     * @return Notification
     */
    public function setToId($toId)
    {
        $this->toId = $toId;

        return $this;
    }

    /**
     * Get toId
     *
     * @return integer
     */
    public function getToId()
    {
        return $this->toId;
    }

   /**
     * Set to
     *
     * @param string $to
     * @return Notification
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set fromId
     *
     * @param integer $fromId
     * @return Notification
     */
    public function setFromId($fromId)
    {
        $this->fromId = $fromId;

        return $this;
    }

    /**
     * Get fromId
     *
     * @return integer
     */
    public function getFromId()
    {
        return $this->fromId;
    }

    /**
     * Set questionId
     *
     * @param integer $questionId
     * @return Notification
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return integer
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set answerId
     *
     * @param integer $answerId
     * @return Notification
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Get answerId
     *
     * @return integer
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

   /**
     * Set content
     *
     * @param string $content
     * @return Notification
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set verb
     *
     * @param string $verb
     * @return Notification
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;

        return $this;
    }

    /**
     * Get verb
     *
     * @return string
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * Set seen
     *
     * @param integer $seen
     * @return Notification
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * Get seen
     *
     * @return integer
     */
    public function getSeen()
    {
        return $this->seen;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return Notification
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}
