<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository,
    Doctrine\ORM\Query\ResultSetMapping,
    Doctrine\ORM\Query\ResultSetMappingBuilder,
    Application\Entity\UserProvider;

class UserProviderContactRepository extends EntityRepository
{
    public function getByUserProviderIdentifier($userId, $provider, $contactProviderId)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT upc FROM  Application\Entity\UserProviderContact upc
                WHERE upc.userId = '$userId'
                    AND upc.provider = '$provider'
                    AND upc.contactProviderId = '$contactProviderId'"
        );
        $query->setMaxResults(1);
        $query->useResultCache(
            true, LIFE_TIME, 
            'user_'. $userId . '_provider_' . $provider 
                . '_contact_' . $contactProviderId
        );
        
        $results = $query->getResult();
        
        return isset($results[0]) ? $results[0] : null;
    }
    
    public function getContacts($userId, $provider)
    {
        $query = $this->getEntityManager()->createQuery(
            "SELECT upc FROM  Application\Entity\UserProviderContact upc
                WHERE upc.userId = '$userId'
                    AND upc.provider = '$provider'"
        );
        $query->useResultCache(
            true, LIFE_TIME, 
            'user_'. $userId . '_provider_' . $provider . '_contacts'
        );
        
        return $query->getResult();
    }
}
