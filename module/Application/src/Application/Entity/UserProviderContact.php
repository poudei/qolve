<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs;


/**
 * UserProviderContact
 *
 * @ORM\Table(name="user_provider_contact")
 * @ORM\Entity(repositoryClass="Application\Entity\UserProviderContactRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="userProviderContact", lifetime="3600")
 *
 */
class UserProviderContact
{   
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=50, nullable=false)
     */
    private $provider;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_provider_id", type="string", length=30, nullable=false)
     */
    private $contactProviderId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="contact_id", type="bigint", nullable=true)
     */
    private $contactId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=350, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)

     */
    private $imagePath;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     *
     * @Attributes({"type":"text"})
     */
    private $email;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return UserProviderContact
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserProviderContact
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

   /**
     * Set provider
     *
     * @param string $provider
     * @return UserProviderContact
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set contactProviderId
     *
     * @param string $contactProviderId
     * @return UserProviderContact
     */
    public function setContactProviderId($contactProviderId)
    {
        $this->contactProviderId = $contactProviderId;

        return $this;
    }

    /**
     * Get contactProviderId
     *
     * @return string
     */
    public function getContactProviderId()
    {
        return $this->contactProviderId;
    }

    /**
     * Set contactId
     *
     * @param integer $contactId
     * @return UserProviderContact
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UserProviderContact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return UserProviderContact
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }
    
    /**
     * Set email
     *
     * @param string $email
     * @return UserProviderContact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()
                ->getResultCacheImpl();
        
        $cache->delete(
            'user_'. $this->getUserId() . '_provider_' . $this->getProvider() 
                . '_contact_' . $this->getContactProviderId()
        );
        $cache->delete(
            'user_'. $this->getUserId() . '_provider_' . $this->getProvider() 
                . '_contacts'
        );
    }
}
