<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository,
    Application\Entity\Notification;

class NotificationRepository extends EntityRepository
{
    public function getNewNotifications($userId, &$count, $limit = 10)
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT COUNT(notif) num
                FROM \Application\Entity\Notification notif
                WHERE notif.toId = '$userId'
                AND notif.seen = " . Notification::SEEN_NO
        );
        $result = $query->getResult();
        $count  = isset($result[0]['num']) ? $result[0]['num'] : 0;

        if ($count < $limit) {
            $query = $this->getEntityManager()->createQuery("
                SELECT notif
                    FROM \Application\Entity\Notification notif
                    WHERE notif.toId = '$userId'
                    ORDER BY notif.seen ASC, notif.id DESC"
            )
            ->setFirstResult(0)
            ->setMaxResults($limit);
        } else {
            $query = $this->getEntityManager()->createQuery("
                SELECT notif
                    FROM \Application\Entity\Notification notif
                    WHERE notif.toId = $userId
                    AND notif.seen = ".Notification::SEEN_NO. "
                    ORDER BY notif.id DESC"
            );
        }

        return $query->getResult();
    }

    public function getAllNotifications($userId, &$count,
        $offset, $limit = 10, $fromId = null, $toId = null
    ) {
        $conditions[] = "(notif.toId = '$userId')";

        if ($fromId) {
            $conditions[] = "notif.id > $fromId";
        }
        if ($toId) {
            $conditions[] = "notif.id < $toId";
        }

        $conditions = join(' AND ', $conditions);
        $query = $this->getEntityManager()->createQuery("
            SELECT COUNT(notif) num
                FROM \Application\Entity\Notification notif
                WHERE $conditions"
        );

        $result = $query->getResult();
        $count  = isset($result[0]['num']) ? $result[0]['num'] : 0;

        $query = $this->getEntityManager()->createQuery("
            SELECT notif
                FROM \Application\Entity\Notification notif
                WHERE $conditions
                ORDER BY notif.seen ASC, notif.id DESC"
        )
        ->setFirstResult($offset)
        ->setMaxResults($limit);

        return $query->getResult();
    }

    public function see($userId, $lastNotifId)
    {
        $query = $this->getEntityManager()->createQuery("
            UPDATE \Application\Entity\Notification notif
                SET notif.seen = ".Notification::SEEN_YES."
                WHERE notif.toId = '$userId'
                AND   notif.id <= '$lastNotifId'"
        );

        return $query->getResult();
    }
    
    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "n.$key IS NULL";
            } else {
                $conditions[] = "n.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Application\Entity\Notification n
                WHERE $conditions"
        );
        return $query->execute();
    }
}
