<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class FeedbackRepository extends EntityRepository
{
    public function countBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $property => $value) {
            if ($value === null) {
                $conditions[] = "feedback.$property IS NULL";
            } else {
                $conditions[] = "feedback.$property = '$value'";
            }
        }

        $queryStr = "SELECT COUNT(feedback) num
            FROM \Application\Entity\Feedback feedback ";
        if (!empty($conditions)) {
            $queryStr .= "WHERE " . join(' AND ', $conditions);
        }

        $query = $this->getEntityManager()->createQuery($queryStr);
        $result = $query->getResult();

        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }
}
