<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Zend\Form\Annotation as Form;

/**
 * User
 *
 * @ORM\Table(name="`user`")
 * @ORM\Entity(repositoryClass="Application\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 * @Application\ORM\Cacheable\Cacheable(type="user", lifetime="3600")
 * @Form\Name("user")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class User implements \ZfcUser\Entity\UserInterface,  \ZfcRbac\Identity\IdentityInterface
{
    const GENDER_FEMALE = 1;
    const GENDER_MALE   = 2;
    
    const ROLE_ADMIN  = 1;
    const ROLE_MEMBER = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()     
     */
    private $roleId;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_id", type="string", length=128, precision=0, scale=0, nullable=false, unique=false)
     */
    private $customerId;


    /**
     * @var integer
     *
     * @ORM\Column(name="contact_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()     
     */
    private $contactId;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     * @Form\Filter({"name":"StringTrim"})     
     * @Form\Required(false)          
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, precision=0, scale=0, nullable=false, unique=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, precision=0, scale=0, nullable=true, unique=true)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "1", "max" : "255"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Filter({"name":"StringTrim"})    
     * @Form\Required(false)          
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=350, precision=0, scale=0, nullable=false, unique=false)
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Filter({"name":"StringTrim"})    
     * @Form\Required(false)          
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"Digits"})
     * @Form\Exclude()
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Exclude()
     */
    private $imagePath;

    /**
     * @var integer
     *
     * @ORM\Column(name="gender", type="smallint", precision=0, scale=0, nullable=true)
     * @Form\Validator({"name":"Digits"})

     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)          
     */
    private $gender;

    /**
     * @var integer
     *
     * @ORM\Column(name="gender_hide", type="smallint", precision=0, scale=0, nullable=true)
     * @Form\Validator({"name":"Digits"})

     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)     
     */
    private $genderHide;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)     
     */
    private $birthday;

    /**
     * @var integer
     *
     * @ORM\Column(name="birthday_hide", type="smallint", precision=0, scale=0, nullable=true)
     * @Form\Validator({"name":"Digits"})

     * @Form\Required(false)
     */
    private $birthdayHide;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Validator({"name":"Digits"})

     * @Form\Required(false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="about", type="string", length=80, nullable=true)
     * @Form\Validator({"name":"StringLength", "options":{"min" : "1", "max" : "80"}})
     * @Form\Filter({"name" : "htmlentities" })
     * @Form\Required(false)
     */
    private $about;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_by", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_on", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()
     */
    private $modifiedOn;


    /**
     * set id
     *
     * @param integer $id
     * @access public
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roleId
     *
     * @param integer $roleId
     * @return User
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * Get roleId
     *
     * @return integer
     */
    public function getRoleId()
    {
        return $this->roleId;
    }


    /**
     * Set customerId
     *
     * @param string $customerId
     * @return User
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }


    /**
     * Set contactId
     *
     * @param integer $contactId
     * @return User
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        if (is_string($email)) {
            $email = strtolower(trim($email));
        }
        
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return User
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        if (is_string($username)) {
            $username = strtolower(trim($username));
        }
        
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        if (is_string($this->username)) {
            $this->username = strtolower(trim($this->username));
        }
        
        return $this->username;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set genderHide
     *
     * @param integer $genderHide
     * @return User
     */
    public function setGenderHide($genderHide)
    {
        $this->genderHide = $genderHide;

        return $this;
    }

    /**
     * Get genderHide
     *
     * @return integer
     */
    public function getGenderHide()
    {
        return $this->genderHide;
    }

    /**
     * Set birthday
     *
     * @param integer $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return integer
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

        /**
     * Set birthdayHide
     *
     * @param integer $birthdayHide
     * @return User
     */
    public function setBirthdayHide($birthdayHide)
    {
        $this->birthdayHide = $birthdayHide;

        return $this;
    }

    /**
     * Get birthdayHide
     *
     * @return integer
     */
    public function getBirthdayHide()
    {
        return $this->birthdayHide;
    }


    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return User
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     * @return User
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set modifiedOn
     *
     * @param \DateTime $modifiedOn
     * @return User
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modifiedOn = $modifiedOn;

        return $this;
    }

    /**
     * Get modifiedOn
     *
     * @return \DateTime
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->name;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     * @return UserInterface
     */
    public function setDisplayName($displayName)
    {
        $this->name = $displayName;

        return $this;
    }

    /**
     * Get state.
     *
     * @return int
     */
    public function getState()
    {
        return $this->status;
    }

    /**
     * Set state.
     *
     * @param int $state
     * @return UserInterface
     */
    public function setState($state)
    {
        $this->status = $state;

        return $this;
    }
    

    public function getRoles()
    {
        $roles = array('member');
        
        switch ($this->getRoleId()) {
        case self::ROLE_ADMIN:
            $roles[] = 'admin';
            break;
        }
        
        return $roles;
     
        //TODO
//        return isset($this->roles) ? $this->roles : array('guest');
    }
    
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()->getResultCacheImpl();
        
        $cache->delete('user_username' . $this->getUsername());
        $cache->delete('user_email' . $this->getEmail());
    }
}
