<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;
/**
 * RbacRolePermission
 *
 * @ORM\Table(name="rbac_role")
 * @ORM\Entity
 */
class RbacRolePermission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $roleId;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="perm_id", type="bigint", nullable=false)
     * @ORM\Id
     *
     * @Form\Exclude()
     */
    private $permId;
    
    
    /**
     * set roleId
     *
     * @param integer $roleId
     * @access public
     * @return RbacRolePermission
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * Get roleId
     *
     * @return integer
     */
    public function getRoleId()
    {
        return $this->roleId;
    }
    
    /**
     * set permId
     *
     * @param integer $permId
     * @access public
     * @return RbacRolePermission
     */
    public function setPermId($permId)
    {
        $this->permId = $permId;

        return $this;
    }

    /**
     * Get permId
     *
     * @return integer
     */
    public function getPermId()
    {
        return $this->permId;
    }
}
