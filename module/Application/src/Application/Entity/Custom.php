<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Custom
 *
 * @ORM\Table(name="custom")
 * @ORM\Entity
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="ownerId")
 * @Application\ORM\Cacheable\Cacheable(type="custom", lifetime="3600")
 */
class Custom
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="owner_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $ownerId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="data_type", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @ORM\Column(name="default_vlaue", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $defaultVlaue;

    /**
     * @var string
     *
     * @ORM\Column(name="control_type", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $controlType;

    /**
     * @var integer
     *
     * @ORM\Column(name="required", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $required;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="string", length=1000, precision=0, scale=0, nullable=true, unique=false)
     */
    private $options;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $deleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdOn;

    /**
     * @var integer
     *
     * @ORM\Column(name="modified_by", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_on", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $modifiedOn;

    /**
     * set id
     *
     * @param integer $id
     * @access public
     * @return Custom
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ownerId
     *
     * @param integer $ownerId
     * @return Custom
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * Get ownerId
     *
     * @return integer
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Custom
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Custom
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dataType
     *
     * @param string $dataType
     * @return Custom
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;

        return $this;
    }

    /**
     * Get dataType
     *
     * @return string
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * Set defaultVlaue
     *
     * @param string $defaultVlaue
     * @return Custom
     */
    public function setDefaultVlaue($defaultVlaue)
    {
        $this->defaultVlaue = $defaultVlaue;

        return $this;
    }

    /**
     * Get defaultVlaue
     *
     * @return string
     */
    public function getDefaultVlaue()
    {
        return $this->defaultVlaue;
    }

    /**
     * Set controlType
     *
     * @param string $controlType
     * @return Custom
     */
    public function setControlType($controlType)
    {
        $this->controlType = $controlType;

        return $this;
    }

    /**
     * Get controlType
     *
     * @return string
     */
    public function getControlType()
    {
        return $this->controlType;
    }

    /**
     * Set required
     *
     * @param integer $required
     * @return Custom
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return integer
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return Custom
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return Custom
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return Custom
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return Custom
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     * @return Custom
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set modifiedOn
     *
     * @param \DateTime $modifiedOn
     * @return Custom
     */
    public function setModifiedOn($modifiedOn)
    {
        $this->modifiedOn = $modifiedOn;

        return $this;
    }

    /**
     * Get modifiedOn
     *
     * @return \DateTime
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }
}
