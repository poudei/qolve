<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\ORM\Event\LifecycleEventArgs;


/**
 * UserProvider
 *
 * @ORM\Table(name="user_provider")
 * @ORM\Entity(repositoryClass="Application\Entity\UserProviderRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 * @Application\ORM\Cacheable\Cacheable(type="userProvider", lifetime="3600")
 *
 */
class UserProvider
{
    const PUBLISHED_NO  = 0;
    const PUBLISHED_YES = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_id", type="string", length=50, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $providerId;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=255, nullable=false)
     */
    private $provider;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="published", type="smallint", nullable=true, unique=false)
     */
    private $published;
    
    /**
     * @var string
     *
     * @ORM\Column(name="access_token", type="string", length=1024, nullable=true)
     */
    private $accessToken;

    
    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserProvider
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set providerId
     *
     * @param string $providerId
     * @return UserProvider
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * Get providerId
     *
     * @return string
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Set provider
     *
     * @param string $provider
     * @return UserProvider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }
    
    /**
     * Set published
     *
     * @param integer $published
     * @return UserProvider
     */
    public function setPublished($published)
    {
        $this->published = $published;
    
        return $this;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
    
    /**
     * Set accessToken
     *
     * @param string $accessToken
     * @return UserProvider
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    
        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     * @ORM\PostRemove
     * removeCachePostPersist
     *
     * @access public
     * @return void
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $cache = $args->getObjectManager()->getConfiguration()
            ->getResultCacheImpl();
        $cache->delete('user_'. $this->getUserId() . '_providers');
        $cache->delete('user_'. $this->getUserId() . '_provider');
        $cache->delete(
            'user_'. $this->getUserId() . '_provider_' . $this->getProvider()
        );
        $cache->delete(
            'userProvider_' . $this->getProvider() 
                . '_providerId_' . $this->getProviderId()
        );
    }
}
