<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository,
    Application\Entity\Feed;

class FeedRepository extends EntityRepository
{
    public function getFeeds(
        $userId, $followingsId = null, $limit, &$count,
        $fromTime = null, $toTime = null
    ) {
        $conditions[] = "(feed.userId = '$userId')";

        if ($fromTime) {
            $conditions[] = "feed.createdOn > '$fromTime'";
        }
        if ($toTime) {
            $conditions[] = "feed.createdOn < '$toTime'";
        }

        $conditions[] = "feed.verb IN('CreateAnswer', 'Vote',
            'CreateQuestion', 'ReaskQuestion')";

        $conditions = join(' AND ', $conditions);
        //TODO: add sharding ID
        $query = $this->getEntityManager()->createQuery("
            SELECT DISTINCT(feed.id), feed
                FROM \Application\Entity\Feed feed
                WHERE $conditions
                ORDER BY feed.createdOn DESC"
        )
        ->setFirstResult(0)
        ->setMaxResults($limit);

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT  COUNT(feed) num
                FROM \Application\Entity\Feed feed
                WHERE  $conditions"
        );

        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;
        return $query->getResult();
    }

    public function getTimeline($userId, $offset,
        $limit, &$count, $currentUserId = null
    ) {
        if (!isset($currentUserId)) {
            $cond = "feed.actorId = '$userId' AND
                feed.userId  = '$userId' AND
                feed.privacy = " . Feed::PRIVACY_PUBLIC;
        } else {
            $cond = "((
                feed.actorId = '$userId' AND
                feed.userId  = '$userId' AND
                feed.privacy = " . Feed::PRIVACY_PUBLIC.
                ")" .
                "OR" .
                "(
                feed.actorId = '$userId' AND
                feed.userId  = '$currentUserId' AND
                feed.privacy = " . Feed::PRIVACY_PRIVATE.
            "))";
        }
        $cond .= " AND feed.verb IN('CreateAnswer',
            'CreateQuestion', 'ReaskQuestion')";
        $query = $this->getEntityManager()->createQuery("
            SELECT feed
                FROM \Application\Entity\Feed feed
                WHERE $cond
                ORDER BY feed.createdOn DESC, feed.id DESC"
        )
        ->setFirstResult($offset)
        ->setMaxResults($limit);

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT  COUNT(feed) num
                FROM \Application\Entity\Feed feed
                WHERE  $cond"
        );
        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $query->getResult();
    }

    public function getActivities($userId, $offset, $limit, &$count,
        $fromTime = null, $toTime = null)
    {
        $conditions = array();
        
        if ($fromTime) {
            $conditions[] = "feed.createdOn > '$fromTime'";
        }
        if ($toTime) {
            $conditions[] = "feed.createdOn < '$toTime'";
        }

        $conditions[] = "(
            feed.actorId = '$userId' AND
            feed.userId  = '$userId'
        )";

        $conditions = join(' AND ', $conditions);

        $query = $this->getEntityManager()->createQuery("
            SELECT DISTINCT(feed.id), feed
                FROM \Application\Entity\Feed feed
                WHERE $conditions
                ORDER BY feed.createdOn DESC"
        )
        ->setMaxResults($limit);
        
        if (isset($offset)) {
            $query->setFirstResult($offset);
        }

        $countQuery = $this->getEntityManager()->createQuery(
            "SELECT  COUNT(feed) num
                FROM \Application\Entity\Feed feed
                WHERE  $conditions"
        );
        $countResult = $countQuery->getResult();
        $count = isset($countResult[0]['num']) ? $countResult[0]['num'] : 0;

        return $query->getResult();
    }

    public function activitiesCount($userId)
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT COUNT(feed) num
                FROM \Application\Entity\Feed feed
                WHERE feed.userId = '$userId'
                AND feed.actorId = '$userId'"
        );

        $result = $query->getResult();
        return isset($result[0]['num']) ? $result[0]['num'] : 0;
    }

    public function deleteBy($criteria = array())
    {
        $conditions = array();
        foreach ($criteria as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "f.$key IS NULL";
            } else {
                $conditions[] = "f.$key = '$value'";
            }
        }
        $conditions = join(" AND ", $conditions);

        $query = $this->getEntityManager()->createQuery(
            "DELETE FROM Application\Entity\Feed f
                WHERE $conditions"
        );
        return $query->execute();
    }
    public function setPreFeeds($userId, $actorId)
    {
        $results = array();
        //TODO: ADD : $objectManager->getConnection()-getShardManager()->selectShard($actorId);
        $objectManager = $this->getEntityManager();
        $query = $objectManager->createQuery("
            SELECT f
                FROM \Application\Entity\Feed f
                WHERE f.userId  = $actorId
                AND   f.actorId = $actorId
                AND   f.privacy = 1
                AND   f.verb IN ('CreateQuestion', 'ReaskQuestion')
                ORDER BY f.createdOn DESC"
        )
        ->setFirstResult(0)
        ->setMaxResults(20);

        $results = $query->getResult();

        $query = $objectManager->createQuery("
            SELECT f
                FROM \Application\Entity\Feed f
                WHERE f.userId  = $actorId
                AND   f.actorId = $actorId
                AND   f.privacy = 1
                AND   f.verb = 'CreateAnswer'
                ORDER BY f.createdOn DESC"
        )
        ->setFirstResult(0)
        ->setMaxResults(20);

        $ansResults = array();
        $ansResults = $query->getResult();

        foreach ($ansResults as $ansResult) {
            $results[] = $ansResult;
        }

        $query = $objectManager->createQuery("
            SELECT f
                FROM \Application\Entity\Feed f
                WHERE f.userId  = $actorId
                AND   f.actorId = $actorId
                AND   f.privacy = 1
                AND   f.verb = 'Vote'
                ORDER BY f.createdOn DESC"
        )
        ->setFirstResult(0)
        ->setMaxResults(10);

        $voteResults = array();
        $voteResults = $query->getResult();
        foreach ($voteResults as $voteResult) {
            $results[] = $voteResult;
        }

        foreach ($results as $result) {
            $feed = new Feed();
            $feed->setUserId($userId);
            $feed->setActorId($actorId);
            $feed->setQuestionId($result->getQuestionId());
            $feed->setAnswerId($result->getAnswerId());
            $feed->setPrivacy(1);
            $feed->setVerb($result->getVerb());
            $feed->setCreatedOn($result->getCreatedOn());
            $objectManager->persist($feed);
        }
        //TODO: ADD : $objectManager->getConnection()-getShardManager()->selectShard($userId);

        $objectManager->flush();
    }
    
    public function updatePrivacy($properties, $privacy) 
    {    
        $conditions = array();
        foreach ($properties as $key => $value) {
            if (is_null($value)) {
                $conditions[] = "f.$key IS NULL";
            } else {
                $conditions[] = "f.$key = '$value'";
            }
        }
        $conditions = join(' AND ', $conditions);
        
        $query = $this->getEntityManager()->createQuery(
            "update \Application\Entity\Feed f 
                SET f.privacy = $privacy 
                where $conditions"
        );
        
        return $query->execute();
    }
}
