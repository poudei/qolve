<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
        'login_via_provider' => array(
               'type' => 'literal',
               'options' => array(
                  'route' => '/provider/login/back',
                   'defaults' => array(
                       'controller' => 'Application\Controller\User',
                       'action'     => 'loginviaprovider',
                   ),
               )
            ),
        'signup' => array(
               'type' => 'literal',
               'options' => array(
                  'route' => '/signup/register',
                   'defaults' => array(
                       'controller' => 'Application\Controller\Index',
                       'action'     => 'index',
                   ),
               )
            ),
            'homepage' => array(
               'type' => 'literal',
               'options' => array(
                  'route' => '/home',
                   'defaults' => array(
                       'controller' => 'Application\Controller\Index',
                       'action'     => 'index',
                   ),
               )
            ),
            'api' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/api',
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'users' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/users[/][:id]',
                            'constraints' => array(
                                'id' => '(.*)+'
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\UserRest',
                                ),
                            ),
                        ),
                    'thumbnail' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/thumbnail',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Thumbnail',
                                'action'     => 'resize',
                                ),
                            ),
                        ),
                    'authenticate' => array(
                            'type' => 'segment',
                            'options' => array(
                                'route'    => '/oauth2/token',
                                'defaults' => array(
                                    'controller' => 'Application\Controller\Authenticate',
                                    'action'     => 'getToken',
                                    ),
                                ),
                            ),

                    'user-login' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/user[/[:action]][/[:req_key]]',
                            'constraints' => array(
                                'action'  => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'req_key' => '[a-zA-Z0-9]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\User',
                                'action'     => 'login',
                            ),
                        ),
                    ),

                    'user_prefs' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/users/[:user_id]/prefs[/][:id]',
                            'constraints' => array(
                                'id' => '.*',
                                'user_id' => '([0-9]+|me|ME)'
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\UserPrefRest',
                            ),
                        ),
                    ),

                    'user_provider' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/users/[:user_id]/providers[/][:id]',
                            'constraints' => array(
                                'id' => '.*',
                                'user_id' => '([0-9]+|me|ME)'
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\UserProviderRest',
                            ),
                        ),
                    ),

                    'user_search' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/users/search',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\UserSearchRest',
                            ),
                        ),
                    ),

                    'feed' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route'    => '/feeds',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\FeedRest',
                            ),
                        ),
                    ),

                    'user_timeline' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/users/[:user_id]/timeline',
                            'constraints' => array(
                                'user_id' => '([0-9]+|me|ME)'
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\TimelineRest',
                                ),
                            ),
                    ),

                    'user_activity' => array(
                            'type' => 'literal',
                            'options' => array(
                                'route'    => '/activities',
                                'constraints' => array(
                                    ),
                                'defaults' => array(
                                    'controller' => 'Application\Controller\ActivityRest',
                                    ),
                                ),
                     ),

                    'social' => array(
                            'type' => 'segment',
                            'options' => array(
                                'route'    => '/social/[:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\Social'
                            ),
                        ),
                    ),

                    'notification' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/notifications[/][:id]',
                            'constraints' => array(
                                'id'     => '[0-9]+'
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\NotificationRest',
                            ),
                        ),
                    ),
                    
                    'feedback' => array(
                        'type' => 'literal',
                        'options' => array(
                                'route'    => '/feedbacks',
                                'constraints' => array(
                                ),
                                'defaults' => array(
                                    'controller' => 'Application\Controller\FeedbackRest',
                                ),
                        ),
                    ),
                )
            ),

            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Regex',
                'options' => array(
                    'regex' => '/(?<x>questions/(?<qid>[a-zA-Z0-9_-]+)|home|FAQ|findfriends|findfriends/(.*)|editProfile|users/(?<uid>[a-zA-Z0-9_-]+)|newQuestion|editQuestion/(.*)|notifications|lists|signIn|activity|explore|explore/(.*)|signup|signup/(.*)|forgotPassword|help|findFriends|settings|)',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                    'spec' => '%',
                ),
            ),

            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),

        'factories' => array (
            'Doctrine\ORM\EntityManager' => 'Application\Service\EntityManagerFactory',
            'zfcuser_auth_service'       => 'Application\Service\AuthServiceFactory',
            'OAuthProvider'              => 'Application\Service\OAuthProviderFactory',
            'apns'                       => 'Application\Service\ApnsFactory',
            'Elastica\Client'            => 'Application\Service\ElasticFactory',
            'payment'                    => 'Application\Service\PaymentFactory',
            'ScnSocialAuth\Authentication\Adapter\HybridAuth' => 'Application\Service\HybridAuthAdapterFactory',
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),

    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index'              => 'Application\Controller\IndexController',
            'Application\Controller\UserRest'           => 'Application\Controller\UserRestController',
            'Application\Controller\Authenticate'       => 'Application\Controller\AuthenticateController',
            'Application\Controller\User'               => 'Application\Controller\UserController',
            'Application\Controller\UserPrefRest'       => 'Application\Controller\UserPrefRestController',
            'Application\Controller\UserProviderRest'   => 'Application\Controller\UserProviderRestController',
            'Application\Controller\UserSearchRest'     => 'Application\Controller\UserSearchRestController',
            'Application\Controller\Social'             => 'Application\Controller\SocialController',
            'Application\Controller\FeedRest'           => 'Application\Controller\FeedRestController',
            'Application\Controller\TimelineRest'       => 'Application\Controller\TimelineRestController',
            'Application\Controller\NotificationRest'   => 'Application\Controller\NotificationRestController',
            'Application\Controller\ActivityRest'       => 'Application\Controller\ActivityRestController',
            'Application\Controller\Thumbnail'          => 'Application\Controller\ThumbnailController',
            'Application\Controller\FeedbackRest'       => 'Application\Controller\FeedbackRestController',
        ),
    ),

    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),

    'doctrine' => array(

        'connection' => array(
            'orm_default' => array(
                'params' => array(
                    'shardChoser' => 'Application\Service\ShardChoser'
                )
            )
        ),

        'driver' => array(

            'application_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Application/Entity')
            ),

            'orm_default' => array(
                'drivers' => array(
                    'Application\Entity' => 'application_entities'
                )
            ),
        ),

    ),

    'session' => array(
        'config' => array(
            'class' => 'Zend\Session\Config\SessionConfig',
            'options' => array(
                'name' => 'myapp',
            ),
        ),
        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
        'validators' => array(
            array(
                'Zend\Session\Validator\RemoteAddr',
                'Zend\Session\Validator\HttpUserAgent',
            ),
        ),
    ),

    'slm_queue' => array(
        'job_manager' => array(
            'factories' => array(
                'Application\Job\SendEmail' => function ($locator) {
                    return new Application\Job\SendEmail($locator->getServiceLocator());
                },

                'Application\Job\CreateThumb' => function ($locator) {
                    return new Application\Job\CreateThumb($locator->getServiceLocator()->get('Thumbnailer'));
                },

                'Application\Job\SendNotification' => function ($locator) {
                    return new Application\Job\SendNotification($locator->getServiceLocator());
                },

                'Application\Job\PublishFeed' => function ($locator) {
                    return new Application\Job\PublishFeed($locator->getServiceLocator());
                }

//                'Application\Job\PublishStatus' => function ($locator) {
//                    return new Application\Job\PublishStatus($locator->getServiceLocator());
//                },
            )
        ),
        'queue_manager' => array(
            'factories' => array(
                'email'         => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
                'thumbnail'     => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
                'notifications' => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
                'feeds'         => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory'
//                'status'        => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
            )
        )
    ),
);
