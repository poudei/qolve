<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Event\DoctrineEventSubscriber,
    Zend\Mvc\ModuleRouteListener,
    Zend\Mvc\MvcEvent,
    Zend\Session\SessionManager,
    Zend\Session\Container,
    Zend\Form\Annotation\AnnotationBuilder,
    Application\Mvc\View\JsonExceptionStrategy,
    Zend\Validator\AbstractValidator;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $this->bootstrapTranslator($e);
        //$this->bootstrapExceptionStrategy($e);

        $serviceManager = $e->getApplication()->getServiceManager();
        $application    = $e->getApplication();

        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach('finish', function($e) {
                $serviceManager = $e->getApplication()->getServiceManager();
                $logger = $serviceManager->get('doctrine.sql_logger_collector.orm_default');
                $queries = $logger->getQueries();
//                var_dump($queries);exit;
                foreach ($queries as $query) {
               //     file_put_contents('/tmp/queries', $query['sql'] . "\n\n", FILE_APPEND);
                }
        });

        $eventSubscriber      = new DoctrineEventSubscriber();
        $doctrineEventManager = $serviceManager->get('doctrine.eventmanager.orm_default');
        $eventSubscriber->setServiceManager($serviceManager);
        $doctrineEventManager->addEventSubscriber($eventSubscriber);

        $application
            ->getEventManager()
            ->attach('route', array('Application\Listener\OAuth', 'onRoute'), 200000);
        $application
            ->getEventManager()
            ->attach('route', array('Application\Listener\Route', 'onRoute'), -1000);

        $events = $e->getApplication()->getEventManager()->getSharedManager();
        $events->attach('ZfcUser\Form\Register','init', function($e) {
            $form = $e->getTarget();
            // Do what you please with the form instance ($form)
        });

        $zfcServiceEvents = $e->getApplication()->getServiceManager()->get('zfcuser_user_service')->getEventManager();
        $zfcServiceEvents->attach('register', function($ev) {
            $form = $ev->getParam('form');
            $user = $ev->getParam('user');

            $user->setStatus(1);
            $user->setDeleted(0);
            $user->setCreatedOn(new \DateTime('now', new \DateTimeZone('UTC')));
            $user->setModifiedOn(new \DateTime('now', new \DateTimeZone('UTC')));
        });

        $cacheable   = new \Application\ORM\Cacheable\CacheableListener();
        $doctrineEventManager->addEventSubscriber($cacheable);

        $shardable   = new \Application\ORM\Shardable\ShardableListener();
        $doctrineEventManager->addEventSubscriber($shardable);

        $sclServiceEvents = $e->getApplication()->getServiceManager()
            ->get('ScnSocialAuth\Authentication\Adapter\HybridAuth')->getEventManager();

        $sclServiceEvents->attach('registerViaProvider', function($ev) use($e) {

            $em = $ev->getTarget()->getServiceManager()
                ->get('Doctrine\ORM\EntityManager');

            $user       = $ev->getParam('user');
            $profile    = $ev->getParam('userProfile');
            
            $now = new \DateTime('now', new \DateTimeZone('UTC'));

            if ($user->getEmail() != null) {

                $return = $em->getRepository('Application\Entity\User')
                    ->getOneByElastic($ev->getTarget()->getServiceManager(), 
                        'email', $user->getEmail()
                    );

                $preUser = array();
                if (!empty($return)) {
                    $preUser = $em
                        ->getRepository('Application\Entity\User')
                        ->find($return['user_id']);
                }

                if ($preUser instanceof Entity\User) {
                    $options = $ev->getTarget()->getServiceManager()
                        ->get("zfcuser_module_options");
                    $options->setLoginRedirectRoute('redirect');
                    return;
                }
            }

            $gender = (strtolower($profile->gender) == 'female') ?
                Entity\User::GENDER_FEMALE : Entity\User::GENDER_MALE;

            if (isset($profile->birthYear) &&
                isset($profile->birthMonth) &&
                isset($profile->birthDay)) {

                    $birthday = new \DateTime($profile->birthYear .
                        '-' . $profile->birthMonth .
                        '-' . $profile->birthDay);
                } else {
                    $birthday = null;
                }

            if ($user->getEmail() == null) {
                $user->setEmail("NOTSET");
            }
            $user->setGender($gender);
            $user->setBirthday($birthday);
            $user->setImagePath($profile->photoURL);
            $user->setStatus(1);
            $user->setDeleted(0);
            $user->setCreatedOn($now);
            $user->setModifiedOn($now);

            $em->persist($user);
            $em->flush($user);

            $createdOn = $user->getCreatedOn()->format('Y-m-d H:i:s');
            $createdOn = gmdate('Y-m-d H:i:s', strtotime($createdOn));

            //Add user to Elastic
            $elastica = $ev->getTarget()
                ->getServiceManager()
                ->get('Elastica\Client');
            $elasticaClient = $elastica['client'];
            $index          = $elastica['index'];
            
            $item = array (
                'user_id'    => $user->getId(),
                'name'       => $user->getname(),
                'username'   => $user->getUsername(),
                'email'      => $user->getEmail(),
                'status'     => $user->getStatus(),
                'deleted'    => $user->getDeleted(),
                'created_on' => $createdOn
            );
            $elasticaIndex = $elasticaClient->getIndex($index);
            $elasticaType  = $elasticaIndex->getType('user');
            $content       = new \Elastica\Document($user->getId(), $item);

            $elasticaType->addDocument($content);
            $elasticaType->getIndex()->refresh();

            if (isset($profile->city) && isset($profile->country)) {
                $userProfile = new Entity\UserProfile();
                $userProfile->setUserId($user->getId());
                $userProfile->setLocation("$profile->city, $profile->country");
                $userProfile->setDeleted(0);
                $userProfile->setCreatedOn($now);
                $userProfile->setModifiedOn($now);

                $em->persist($userProfile);
                $em->flush($userProfile);
            }

            $options = $ev->getTarget()->getServiceManager()
                ->get("zfcuser_module_options");
            $options->setLoginRedirectRoute('signup');
        });

        $sclServiceEvents->attach('register.post', function($ev) use($e) {

            $em = $ev->getTarget()->getServiceManager()
                ->get('Doctrine\ORM\EntityManager');
            $hybridauth = $ev->getTarget()->getServiceManager()
                ->get('HybridAuth');

            //register userProvider
            $userProvider = $ev->getParam('userProvider');
            if ($userProvider instanceof Entity\UserProvider &&
                $userProvider->getPublished() == null
            ) {
                $provider = ucfirst($userProvider->getProvider());
                $adapter  = $hybridauth->getAdapter($provider);
                
                $accessToken       = $adapter->token('access_token');
                $accessTokenSecret = $adapter->token('acces_token_secret');
                
                $token = is_null($accessTokenSecret) 
                    ? $accessToken 
                    : serialize(array(
                        'token'  => $accessToken,
                        'secret' => $accessTokenSecret
                    ));
                $userProvider->setAccessToken($token);
                
                $userProvider->setPublished(Entity\UserProvider::PUBLISHED_YES);
                
                $em->flush();
            }

            $options = $ev->getTarget()->getServiceManager()
                ->get("zfcuser_module_options");
            $options->setLoginRedirectRoute('signup');
        });

        $sclServiceEvents->attach('login.via.provider', function($ev) use($e) {

            $options = $ev->getTarget()->getServiceManager()
                ->get("zfcuser_module_options");
            $options->setLoginRedirectRoute('login_via_provider');
        });

        $options = $serviceManager->get("zfcuser_module_options");
        $options->setLoginRedirectRoute('homepage');
        
        $authAdapter = $e->getApplication()->getServiceManager()
            ->get('ZfcUser\Authentication\Adapter\AdapterChain')->getEventManager();
        $authAdapter->attach('authenticate.pre', function($ev) {
            $request    = $ev->getParam('request');
            $identity   = $request->getPost('identity');
            $credential = $request->getPost('credential');
            
            
            if (is_string($identity)) {
                $identity = strtolower(trim($identity));
            }
            
            $post = new \Zend\Stdlib\Parameters(array(
                'identity'   => $identity,
                'credential' => $credential
            ));
            
            $ev->getParam('request')->setPost($post);
        });
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),

            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }


    public function bootstrapTranslator($e)
    {
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $translator
            //->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            ->setFallbackLocale('en_US');
        AbstractValidator::setDefaultTranslator($translator);
    }

    public function bootstrapExceptionStrategy($e)
    {
        $application    = $e->getTarget();
        $serviceManager = $application->getServiceManager();
        $config         = $serviceManager->get('Config');


        // Config json enabled exceptionStrategy
        $exceptionStrategy = new JsonExceptionStrategy();
        $displayExceptions = false;

        if (isset($config['view_manager']['display_exceptions'])) {
            $displayExceptions = $config['view_manager']['display_exceptions'];
        }

        $exceptionStrategy->setDisplayExceptions($displayExceptions);
        $exceptionStrategy->attach($application->getEventManager());
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'ZfcUser\Form\Register' => function($sm) {
                },

                'Application\Service\UidGenerator' => function ($sm) {
                    return new \Application\Service\UidGenerator();
                },
            )
        );
    }
}
